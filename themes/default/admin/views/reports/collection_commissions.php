<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$v = "";

if ($this->input->post('biller')) {
    $v .= "&biller=" . $this->input->post('biller');
}
if ($this->input->post('seller')) {
    $v .= "&seller=" . $this->input->post('seller');
}
if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}
if ($this->input->post('date_select_collection_commissions')) {
    $v .= "&date_select_collection_commissions=" . $this->input->post('date_select_collection_commissions');
}
if ($this->input->post('commision_status')) {
    $v .= "&commision_status=" . $this->input->post('commision_status');
}
?>
<script>
    $(document).ready(function () {

        $('.tip').tooltip();

        var pb = <?= json_encode($pb); ?>;
        function paid_by(x) {
            return (x != null) ? (pb[x] ? pb[x] : x) : x;
        }

        function ref(x) {
            return (x != null) ? x : ' ';
        }
        <?php if (isset($_POST['biller'])) { ?>
            oTable = $('#collectionData').dataTable({
                "aaSorting": [[3, "asc"]],
                "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true, 'bServerSide': true,
                'sAjaxSource': '<?= admin_url('reports/getCollectionCommisions/?v=1' . $v) ?>',
                'fnServerData': function (sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                "aoColumns": [
                                {"mRender": checkbox},
                                null,
                                null,
                                {"mRender": fld},
                                {"mRender": view_payments, 'bSearchable' : false},
                                null,
                                {"mRender": currencyFormat, 'bSearchable' : false},
                                {"mRender": currencyFormat, 'bSearchable' : false},
                                {"mRender": currencyFormat, 'bSearchable' : false},
                                null,
                                {"mRender": comm_payment_status, 'bSearchable' : false}
                            ],
                'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                    var oSettings = oTable.fnSettings();
                    nRow.id = aData[0];
                    nRow.setAttribute('data-total', aData[8]);
                    nRow.className = "invoice_link_first re"+aData[3];
                    return nRow;
                },
                "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                    var total = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        if (aaData[aiDisplay[i]][6] == 'sent')
                            total -= parseFloat(aaData[aiDisplay[i]][5]);
                        else if (aaData[aiDisplay[i]][6] == 'returned')
                            total += parseFloat(aaData[aiDisplay[i]][5]);
                        else
                            total += parseFloat(aaData[aiDisplay[i]][5]);
                    }
                    var nCells = nRow.getElementsByTagName('th');
                    nCells[5].innerHTML = currencyFormat(parseFloat(total));
                }
            }).fnSetFilteringDelay().dtFilter([
                {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                {column_number: 1, filter_default_label: "[<?=lang('payment_ref');?>]", filter_type: "text", data: []},
                {column_number: 2, filter_default_label: "[<?=lang('sale_ref');?>]", filter_type: "text", data: []},
                {column_number: 3, filter_default_label: "[<?=lang('purchase_ref');?>]", filter_type: "text", data: []},
                {column_number: 4, filter_default_label: "[<?=lang('paid_by');?>]", filter_type: "text", data: []},
                {column_number: 6, filter_default_label: "[<?=lang('type');?>]", filter_type: "text", data: []},
            ], "footer");
        <?php } ?>

    });

    $(document).ready(function () {
        $('#form').hide();
        <?php if ($this->Owner || $this->Admin || !$this->session->userdata('biller_id')): ?>
            <?php if ($this->input->post('biller')) { ?>
            $('#rbiller').select2({ allowClear: true });
            <?php } ?>
        <?php endif ?>

        <?php if ($this->input->post('supplier')) { ?>
        $('#rsupplier').val(<?= $this->input->post('supplier') ?>).select2({
            minimumInputLength: 1,
            allowClear: true,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: "<?= admin_url('suppliers/getSupplier') ?>/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#rsupplier').val(<?= $this->input->post('supplier') ?>);
        <?php } ?>
        <?php if ($this->input->post('customer')) { ?>
        $('#rcustomer').val(<?= $this->input->post('customer') ?>).select2({
            minimumInputLength: 1,
            allowClear: true,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: "<?= admin_url('customers/getCustomer') ?>/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        <?php } ?>
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo admin_form_open("reports/collection_commissions", 'id="form_filter"'); ?>
                                <div class="row">
                                    <?php if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')): ?>
                                        <input type="hidden" name="biller" id="rbiller" value="<?= $this->session->userdata('biller_id') ?>">
                                    <?php else: ?>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label" for="rbiller"><?= lang("biller"); ?></label>
                                                <?php
                                                $bl[''] = lang('select');
                                                foreach ($billers as $biller) {
                                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                                }
                                                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ""), 'class="form-control" id="rbiller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                                ?>
                                            </div>
                                        </div>
                                    <?php endif ?>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="rseller"><?= lang("seller"); ?></label>
                                            <?php
                                            $sl[''] = lang('select');
                                            foreach ($sellers as $seller) {
                                                $sl[$seller->id] = $seller->company != '-' ? $seller->company : $seller->name;
                                            }
                                            echo form_dropdown('seller', $sl, (isset($_POST['seller']) ? $_POST['seller'] : ""), 'class="form-control" id="rseller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("seller") . '"');
                                            ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="rdate_select_collection_commissions"><?= lang("date_select_collection_commissions"); ?></label>
                                            <?php
                                            $opt = [
                                                    '1' => lang('payment_receipt_date'),
                                                    '2' => lang('payment_register_date'),
                                                    '3' => lang('invoice_register_date'),
                                                    '4' => lang('commision_payment_register_date'),
                                                    ];

                                            echo form_dropdown('date_select_collection_commissions', $opt, (isset($_POST['date_select_collection_commissions']) ? $_POST['date_select_collection_commissions'] : ""), 'class="form-control" id="rdate_select_collection_commissions" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("date_select_collection_commissions") . '"');
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4" style="display: none;">
                                        <?= lang('date_records_filter', 'date_records_filter') ?>
                                        <select name="date_records_filter" id="date_records_filter" class="form-control">
                                            <option value="0"><?= lang('types_filter_records')[0] ?></option>
                                            <option value="5" selected="selected"><?= lang('types_filter_records')[5] ?></option>
                                            <option value="1"><?= lang('types_filter_records')[1] ?></option>
                                            <option value="2"><?= lang('types_filter_records')[2] ?></option>
                                            <option value="3"><?= lang('types_filter_records')[3] ?></option>
                                            <option value="4"><?= lang('types_filter_records')[4] ?></option>
                                            <option value="6"><?= lang('types_filter_records')[6] ?></option>
                                            <option value="7"><?= lang('types_filter_records')[7] ?></option>
                                        </select>
                                    </div>
                                    <div class="date_controls">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("start_date", "start_date"); ?>
                                                <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date" autocomplete="off"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("end_date", "end_date"); ?>
                                                <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date" autocomplete="off"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label><?= lang('total_commisions_to_pay') ?></label>
                                        <input type="text" name="tpay" class="form-control total_to_pay" readonly="readonly">
                                    </div>
                                    <div class="col-sm-12">
                                        <label>
                                            <input type="radio" name="commision_status" id="commision_status_pending" value="only_pending" <?= ($this->input->post('commision_status') && $this->input->post('commision_status') == 'only_pending') || !$this->input->post('commision_status') ? 'checked="true"' : '' ?>>
                                            <?= lang('view_commisions_only_pending') ?>
                                        </label>
                                        <label>
                                            <input type="radio" name="commision_status" id="commision_status_paid" value="only_paid"  <?= $this->input->post('commision_status') && $this->input->post('commision_status') == 'only_paid' ? 'checked="true"' : '' ?>>
                                            <?= lang('view_commisions_only_paid') ?>
                                        </label>
                                        <label>
                                            <input type="radio" name="commision_status" id="commision_status_paid" value="all"  <?= $this->input->post('commision_status') && $this->input->post('commision_status') == 'all' ? 'checked="true"' : '' ?>>
                                            <?= lang('view_all_commisions') ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <hr class="col-sm-11">
                                </div>
                                    <div class="form-group">
                                        <div
                                            class="controls"> <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?>
                                        </div>
                                    </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php if ($Owner || $Admin || $GP['bulk_actions']) {
        echo admin_form_open('payments/payment_actions', 'id="action-form"');
    }
?>
<input type="hidden" name="start_date_action" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : NULL ?>">
<input type="hidden" name="end_date_action" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : NULL ?>">
<input type="hidden" name="company_id_action" value="<?= isset($_POST['seller']) ? $_POST['seller'] : NULL ?>">
<input type="hidden" name="total_pay" id="total_pay">

    <div class="row" <?= !isset($_POST['biller']) ? 'style="display:none;"' : '' ?>>
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <?php if ($this->input->post('seller') && $this->input->post('date_select_collection_commissions') != 4 && $this->input->post('commision_status') && $this->input->post('commision_status') == 'only_pending'): ?>
                                            <a href="#" id="pay_commisions" data-action="pay_commisions">
                                                <i class="fa fa-dollar"></i> <?=lang('pay_commisions')?>
                                            </a>
                                        <?php else: ?>
                                            <?php

                                                $msg = lang('select_a_seller');

                                                if ($this->input->post('only_paid')) {
                                                    $msg = lang('commisions_paid_on_result');
                                                }

                                             ?>
                                             <?php if (($this->Owner || $this->Admin || $this->GP['sales-pay_commissions']) && $this->input->post('commision_status') && $this->input->post('commision_status') == 'only_pending'): ?>
                                                <a style="cursor: not-allowed;" data-toggle="tooltip" data-placement="top" class="tip" title="<?= $msg ?>">
                                                    <i class="fa fa-dollar"></i> <?=lang('pay_commisions')?>
                                                </a>
                                             <?php endif ?>
                                        <?php endif ?>
                                    </li>
                                    <li>
                                        <a id="print_pdf">
                                            <i class="fa fa-file-pdf-o"></i> <?=lang('export_to_pdf')?>
                                        </a>
                                    </li>
                                    <li>
                                        <a id="print_xls">
                                            <i class="fa fa-file-excel-o"></i> <?=lang('export_to_excel')?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="collectionData"
                                    class="table table-bordered table-hover table-condensed reports-table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <input class="checkbox checkft" type="checkbox" name="check"/>
                                            </th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("seller"); ?></th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("payments"); ?></th>
                                            <th><?= lang("days"); ?></th>
                                            <th><?= lang("comm_perc"); ?></th>
                                            <th><?= lang("comm_base"); ?></th>
                                            <th><?= lang("comm_amount"); ?></th>
                                            <th><?= lang("comm_payment_reference_no"); ?></th>
                                            <th><?= lang("payment_status"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th style="width: 4.5%;"></th>
                                            <th style="width: 11.5%;"><?= lang("reference_no"); ?></th>
                                            <th style="width: 10.5%;"><?= lang("seller"); ?></th>
                                            <th style="width: 6%;"><?= lang("date"); ?></th>
                                            <th style="width: 4.5%;"><?= lang("payments"); ?></th>
                                            <th style="width: 5.5%;"><?= lang("days"); ?></th>
                                            <th style="width: 16%;"><?= lang("comm_perc"); ?></th>
                                            <th style="width: 17%;"><?= lang("comm_base"); ?></th>
                                            <th style="width: 12.5%;"><?= lang("comm_amount"); ?></th>
                                            <th style="width: 12.5%;"><?= lang("comm_amount"); ?></th>
                                            <th style="width: 5%;"><?= lang("payment_status"); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php if ($Owner || $Admin || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>
</div>
<!-- Body -->
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        if (localStorage.getItem('total_pay')) {
            localStorage.removeItem('total_pay');
        }
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getPaymentsReport/pdf/?v=1'.$v)?>";
            return false;
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getPaymentsReport/0/xls/?v=1'.$v)?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
        $('#date_records_filter').on('change', function(){
            filter = $(this).val();
            fecha_inicial = "<?= isset($_POST['start_date']) ? $_POST['start_date'] : date('01/m/Y 00:00') ?>";
            fecha_final = "<?= isset($_POST['end_date']) ? $_POST['end_date'] : date('d/m/Y 23:59') ?>";
            hide_date_controls = true;
            if (filter == 5) { //RANGO DE FECHAS
                hide_date_controls = false;
            } else if (filter == 1) { // HOY
                fecha_inicial = "<?= date('d/m/Y 00:00') ?>";
            } else if (filter == 2) { // MES
                fecha_inicial = "<?= date('01/m/Y 00:00') ?>";
            } else if (filter == 3) { // TRIMESTRE
                fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
            } else if (filter == 4) { // AÑO
                fecha_inicial = "<?= date('01/01/Y 00:00') ?>";
            }else if (filter == 6) { // MES PASADO
                fecha_inicial = "<?= date("d/m/Y 00:00",strtotime(date("Y-m-01")."- 1 month")) ?>";
                fecha_final = "<?= date("d/m/Y 23:59",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
            }else if (filter == 7) { // AÑO PASADO
                fecha_inicial = "<?= date("01/01/Y 00:00",strtotime(date("Y-m-01")."- 1 year")) ?>";
                fecha_final = "<?= date("31/12/Y 23:59",strtotime(date("Y-m-01")."- 1 year")) ?>";
            } else if (filter == 0) {
                fecha_inicial = "";
                fecha_final = "";
                hide_date_controls = false;
            }
            $('#start_date').val(fecha_inicial);
                $('#end_date').val(fecha_final);
            if (hide_date_controls) {
                $('.date_controls').css('display', 'none');
            } else {
                $('.date_controls').css('display', '');
            }
        });
        $('#date_records_filter').val(5).trigger('change');


        $(document).on('click', '#print_pdf', function(){
            var form = $('#form_filter');
            var prev_action = form.prop('action');
            form.prop('action', '<?= admin_url('reports/getCollectionCommisions/1') ?>');
            form.prop('target', '_blank');
            form.submit(); //volver a la action anterior, submit en nueva pestaña
            setTimeout(function() {
                form.prop('action', prev_action);
                form.removeAttr('target');
            }, 850);
        });

        $(document).on('click', '#print_xls', function(){
            var form = $('#form_filter');
            var prev_action = form.prop('action');
            form.prop('action', '<?= admin_url('reports/getCollectionCommisions/0/1') ?>');
            form.prop('target', '_blank');
            form.submit(); //volver a la action anterior, submit en nueva pestaña
            setTimeout(function() {
                form.prop('action', prev_action);
                form.removeAttr('target');
            }, 850);
        });

    });

$(document).on('ifChanged', '.multi-select', function(){
    check = $(this);
    status = check.is(':checked');
    console.log(status);
    total = check.closest('tr.invoice_link_first').data('total');
    total_pay = 0;
    if (status == 'true') {
        if (total_pay = parseFloat(localStorage.getItem('total_pay'))) {
            total_pay += parseFloat(total);
            localStorage.setItem('total_pay', total_pay);
        } else {
            total_pay = total;
            localStorage.setItem('total_pay', total_pay);
        }
    } else {
        if (total_pay = parseFloat(localStorage.getItem('total_pay'))) {
            total_pay -= parseFloat(total);
            localStorage.setItem('total_pay', total_pay);
        }
    }

    if (total_pay < 0) {
        total_pay = 0;
    }

    $('.total_to_pay').val(formatMoney(total_pay));
    $('#total_pay').val(total_pay);

});

$(document).on('change', '#rdate_select_collection_commissions', function(){
    rdate = $(this).val();

    if (rdate == 4) {
        $('#commision_status_paid').iCheck('check');
        $('#commision_status_pending').prop('disabled', true);
    } else {
        $('#commision_status_pending').iCheck('check');
        $('#commision_status_pending').prop('disabled', false);
    }

});

</script>
