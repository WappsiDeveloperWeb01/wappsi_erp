<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

$v = "";
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').hide();

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<?php

	if (!isset($_POST['start_date']) && !isset($_POST['end_date'])) {
		// $fech_ini = date('Y-m-d');
		// $fech_fin = date("Y-m-d",strtotime($fech_ini."+ 1 days"));
		// foreach ($sales as $sale)
		// {
		// 	if($fech_ini>$sale['date'])
		// 	{
		// 		$fech_ini = $sale['date'];
		// 	}
		// }
		// $fech_ini = substr($fech_ini,0,10);

		$fech_ini = date('Y-m-01');
		$fech_fin = date('Y-m-d');

	} else {
		$fech_ini = $_POST['start_date'];
		$fech_fin = $_POST['end_date'];
	}

?>

<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
					<div class="row">
                        <div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<h3><?php echo lang('filter_date');?> <span id="textini"><?php echo $fech_ini; ?></span> a <span id="textfin"><?php echo $fech_fin; ?></span> </h3>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<?php echo form_open('admin/reports/load_bills' , 'id="form_filter"'); ?>
                            			<input type="hidden" name="filter_action" id="filter_action" value="0">
										<div class="row">
											<div class="col-md-4">
												<label for="biller1" class="control-label"> <?php echo lang('branch'); ?> </label>
												<?php if ($this->Settings->big_data_limit_reports == 1): ?>
													<select class="form-control " onchange="ActualizarVendedores(this.value);" name="Sucursal" id="Sucursal" style="width:100%;" required>
														<option value="" > <?= lang('select').' '.lang('branch') ?> </option>
														<?php foreach ($billers as $biller): ?>
															<option value="<?php echo $biller['cod_companie']; ?>"><?php echo $biller['nombre']; ?></option>
														<?php endforeach; ?>
													</select>
												<?php else: ?>
													<div class="form-group">
														<br>
														<?php if ((!$this->session->userdata('biller_id'))): ?>
															<label class="radio-inline">
																<input type="radio" name="valueSucursal" id="biller1" value="Todas" <?= !isset($_POST['valueSucursal']) || (isset($_POST['valueSucursal']) && $_POST['valueSucursal'] == 'Todas') ? 'checked' : '' ?>> <?= lang('alls') ?>
															</label>
														<?php endif ?>
															
														<label class="radio-inline">
															<input type="radio" name="valueSucursal" id="biller2" value="Una"  <?= ((isset($_POST['valueSucursal']) && $_POST['valueSucursal'] == 'Una') || ($this->session->userdata('biller_id'))) ? 'checked' : '' ?>> <?= lang('aa') ?>
														</label>
													</div>
													
													<?php
					                                $bl[""] = "";
					                                $bldata = [];
					                                foreach ($billers as $biller) {
					                                    $bl[$biller['cod_companie']] = $biller['nombre'];
					                                    $bldata[$biller['cod_companie']] = $biller;
					                                }
					                                // exit(var_dump($this->session->userdata('biller_id')));
					                                ?>

					                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
														<div class="collapse" id="collapseBiller">
															<select class="form-control "  onchange="ActualizarVendedores(this.value);" name="Sucursal" id="Sucursal" style="width:100%;" data-target="Sucursal">
																<option value="all" > <?= lang('select').' '.lang('branch') ?> </option>
																<?php foreach ($billers as $biller): ?>
																	<option value="<?php echo $biller['cod_companie']; ?>"  <?= (isset($_POST['Sucursal']) && $_POST['Sucursal'] == $biller['cod_companie'] ? "selected" : "") ?> ><?php echo $biller['nombre']; ?></option>
																<?php endforeach; ?>
															</select>
														</div>    
					                                <?php } else { ?>
															<select class="form-control " onchange="ActualizarVendedores(this.value);" name="Sucursal" id="Sucursal" style="width:100%;" data-target="Sucursal">
					                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
					                                                    $biller = $bldata[$this->session->userdata('biller_id')];
					                                                ?>
					                                                    <option value="<?php echo $biller['cod_companie']; ?>"><?php echo $biller['nombre']; ?></option>
					                                                <?php endif ?>
					                                            </select>
					                                <?php } ?>
													
													
												<?php endif ?>
											</div>
											<div class="col-xs-12 col-md-4">
	                                            <div class="form-group">
	                                                <label for="inputEmail3" class="control-label"> <?= lang('seller') ?> </label><br>
	                                                    <?php if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right') && $this->session->userdata('company_id')): ?>
	                                                        <label class="radio-inline">
	                                                            <input type="radio" name="valueVendedor" id="seller2" value="Un" checked> <?= lang('a').' '.lang('seller') ?>
	                                                        </label>
	                                                    <?php else: ?>
	                                                        <label class="radio-inline">
	                                                            <input type="radio" name="valueVendedor" id="seller1" value="Todas" <?= !isset($_POST['valueVendedor']) || (isset($_POST['valueVendedor']) && $_POST['valueVendedor'] == 'Todas') ? 'checked' : '' ?>> <?= lang('alls') ?>
	                                                        </label>
	                                                        <label class="radio-inline">
	                                                            <input type="radio" name="valueVendedor" id="seller2" value="Un" <?= isset($_POST['valueCliente']) || isset($_POST['valueVendedor']) && $_POST['valueVendedor'] == 'Un' ? 'checked' : '' ?>> <?= lang('a').' '.lang('seller') ?>
	                                                        </label>
	                                                    <?php endif ?>
	                                            </div>
	                                            <div class="collapse" id="collapseSeller">
	                                                <select class="form-control " onchange="Search_Vendedor(this.value);" name="Vendedor" id="Vendedor" style="width:100%;">
	                                                    <?php if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right') && $this->session->userdata('company_id')): ?>
	                                                            <?php foreach ($sellers as $seller): ?>
	                                                                <?php if ($seller['cod_seller'] == $this->session->userdata('company_id')): ?>
	                                                                    <option value="<?php echo $seller['Document']; ?>"><?php echo $seller['nombre']; ?></option>
	                                                                <?php endif ?>
	                                                            <?php endforeach; ?>
	                                                        <?php else: ?>
	                                                            <option value="all" > <?= lang('select').' '.lang('seller') ?> </option>
	                                                            <?php foreach ($sellers as $seller): ?>
	                                                                <option value="<?php echo $seller['Document']; ?>"><?php echo $seller['nombre']; ?></option>
	                                                            <?php endforeach; ?>
	                                                        <?php endif ?>
	                                                </select>
	                                            </div>
	                                        </div>
											<div class="col-md-4">
												<label for="inputEmail3" class="control-label"> <?php echo lang('view_customer_total'); ?> </label>
												<label class="radio-inline">
													<input type="radio" name="valueCliente" id="Cliente1" value="Todas"  <?= !isset($_POST['valueCliente']) || (isset($_POST['valueCliente']) && $_POST['valueCliente'] == "Todas") ? "checked" : "" ?> >  <?= lang('alls') ?>
												</label>
												<label class="radio-inline">
													<input type="radio" name="valueCliente" id="Cliente2" value="Un"  <?= isset($_POST['valueCliente']) && $_POST['valueCliente'] == "Un" ? "checked" : "" ?> > <?= lang('a').' '.lang('view_customer_total') ?>
												</label>
												<div class="collapse" id="collapseBustomer">
													<br>
													<select class="form-control " onchange="Search_Cliente(this.value);" name="Cliente" id="Cliente" data-target="Cliente">
														<option value="all" > <?php echo lang('select').' '.lang('view_customer_total'); ?> </option>
														<?php foreach ($customers as $customer): ?>
															<option value="<?php echo $customer['cod_id']; ?>"><?php echo $customer['nombre']; ?></option>
														<?php endforeach; ?>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">
												<label for="biller1" class="control-label">Fecha Ini.</label>
												<input type="date" name="start_date" id="start_date" value="<?php echo $fech_ini; ?>" class="form-control input-sm" require>
											</div>
											<div class="col-md-4">
												<label for="biller1" class="control-label">Fecha Fin</label>
												<input type="date" name="end_date" id="end_date" value="<?php echo $fech_fin; ?>" class="form-control input-sm" require>
											</div>
											<div class="col-md-4">
												<label for="inputEmail3" class="control-label"> <?= lang('print_type') ?> </label>
												<label class="radio-inline">
													<input type="radio" name="Tipo" id="Tipo" value="Detallado"  <?= !isset($_POST['Tipo']) || (isset($_POST['Tipo']) && $_POST['Tipo'] == "Detallado") ? "checked" : "" ?> > <?= lang('detailed') ?>
												</label>
												<label class="radio-inline">
													<input type="radio" name="Tipo" id="Tipo" value="Resumido"  <?= isset($_POST['Tipo']) && $_POST['Tipo'] == "Resumido" ? "checked" : "" ?> > <?= lang('summarized') ?>
												</label>
											</div>
										</div>
										<div class="row">
		                                    <div class="col-md-4">
		                                        <div class="form-group">
		                                            <label class="control-label" for="warehouse"><?= lang("zone"); ?></label>
		                                            <select name="zone" id="zone" class="form-control">
		                                                <option value="">Seleccione...</option>
		                                                <?php foreach ($zones as $zone): ?>
		                                                    <option value="<?= $zone->id ?>" data-code="<?= $zone->zone_code ?>"><?= $zone->zone_name ?></option>
		                                                <?php endforeach ?>
		                                            </select>
		                                        </div>
		                                    </div>
		                                    <div class="col-md-4">
		                                        <?= lang("subzone", "subzone"); ?>
		                                        <select class="form-control select" name="subzone" id="subzone">
		                                        <option value=""><?= lang("select") ?></option>
		                                        </select>
		                                        <input type="hidden" name="subzone_code" id="subzone_code" <?= (isset($_POST['subzone_code'])) ? "value='".$_POST['subzone_code']."'" : "" ?>>
		                                    </div>
		                                    <div class="col-sm-4">
		                                    	<label>Agrupar por : </label>
		                                    	<br>
		                                    	<label>
		                                    		<input type="checkbox" name="group_by_zone" id="group_by_zone" class="form-control" <?= (isset($_POST['group_by_zone']) && $_POST['group_by_zone']) ? "checked" : "" ?>>
		                                    		Zona
		                                    	</label>
		                                    	<label>
		                                    		<input type="checkbox" name="group_by_subzone" id="group_by_subzone" class="form-control" <?= (isset($_POST['group_by_subzone']) && $_POST['group_by_subzone']) ? "checked" : "" ?>>
		                                    		Barrio
		                                    	</label>
		                                    </div>
		                                </div>
										</br>
										<div class="row">
											<div class="col-md-12">
												<button type="submit" class="btn btn-success" class="Buscar" id="Buscar" ><?php echo lang('submit');?></button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						</br>
						<?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
							<div class="col-lg-12">
								<div class="table-responsive">
									<table id="TableData" class="table table-stripped" >
										<thead>
											<tr>
												<td class="text-center" style="width:11.55%;"  > <b><?= lang('seller') ?></b></td>
												<td class="text-center" style="width:11.55%;"  > <b><?= lang('biller') ?></b></td>
												<td class="text-center" style="width:11.55%;"  > <b><?= lang('customer') ?></b></td>
												<td class="text-center" style="width:8%;"  > <b><?= lang('date') ?></b></td>
												<td class="text-center" style="width:8%;"  > <b><?= lang('reference_no') ?></b></td>
												<td class="text-center" style="width:8%;"  > <b><?= lang('reference_no') ?></b></td>
												<td class="text-right"  style="width:11.11%;"  > <b><?= lang('subtotal') ?></b></td>
												<td class="text-right"  style="width:11.11%;"  > <b><?= lang('tax') ?></b></td>
												<td class="text-right"  style="width:11.11%;"  > <b><?= lang('total') ?></b></td>
												<td class="text-center" style="width:8%;"  > <b><?= lang('status') ?></b></td>
											</tr>
										</thead>
										<tbody>
											<?php
											$sellers = [];
											$zones = [];
											$subzones = [];
											$subtotal = 0;
											$total_iva = 0;
											$total = 0;
											$subtotal_seller = 0;
											$total_iva_seller = 0;
											$total_seller = 0;
											$subtotal_zone = 0;
											$total_iva_zone = 0;
											$total_zone = 0;
											$subtotal_subzone = 0;
											$total_iva_subzone = 0;
											$total_subzone = 0;
											 ?>
											<?php foreach ($sales as $sale): ?>
												<?php
													$referencia = ($sale['return_sale_ref']!='') ? $sale['return_sale_ref'] : $sale['reference_no'] ;
													$afec = ($sale['return_sale_ref']!='') ? $sale['reference_no'] : '' ;
													if (!isset($sellers[$sale['cod_seller']])) {
														$zones = [];
														$subzones = [];
														$sellers[$sale['cod_seller']] = 1;
														?>
														<?php if ($total_subzone != 0 && isset($_POST['group_by_subzone']) && $_POST['group_by_subzone']): ?>
															<tr>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th>Total Barrio</th>
																<th><?= $this->sma->formatMoney($subtotal_subzone) ?></th>
																<th><?= $this->sma->formatMoney($total_iva_subzone) ?></th>
																<th class="text-right"><?= $this->sma->formatMoney($total_subzone) ?></th>
																<th></th>
															</tr>
															<?php
																$subtotal_subzone = 0;
																$total_iva_subzone = 0;
																$total_subzone = 0;
															?>
														<?php endif ?>
														<?php if ($total_zone != 0 && isset($_POST['group_by_zone']) && $_POST['group_by_zone']): ?>
															<tr>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th>Total Zona</th>
																<th><?= $this->sma->formatMoney($subtotal_zone) ?></th>
																<th><?= $this->sma->formatMoney($total_iva_zone) ?></th>
																<th class="text-right"><?= $this->sma->formatMoney($total_zone) ?></th>
																<th></th>
															</tr>
															<?php
																$subtotal_zone = 0;
																$total_iva_zone = 0;
																$total_zone = 0;
															?>
														<?php endif ?>
														<?php if ($total_seller != 0): ?>
															<tr>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th>Total Vendedor</th>
																<th><?= $this->sma->formatMoney($subtotal_seller) ?></th>
																<th><?= $this->sma->formatMoney($total_iva_seller) ?></th>
																<th class="text-right"><?= $this->sma->formatMoney($total_seller) ?></th>
																<th></th>
															</tr>
															<?php
																$subtotal_seller = 0;
																$total_iva_seller = 0;
																$total_seller = 0;
															?>
														<?php endif ?>
															<tr>
																<th class="text-center"><?= $sale['nom_seller'] ?></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
															</tr>
													<?php
													}

													if (!isset($zones[$sale['cod_zone']]) && isset($_POST['group_by_zone']) && $_POST['group_by_zone']) {
														$zones[$sale['cod_zone']] = 1;
														$subzones = [];
														?>
														<?php if ($total_subzone != 0 && isset($_POST['group_by_subzone']) && $_POST['group_by_subzone']): ?>
															<tr>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th>Total Barrio</th>
																<th><?= $this->sma->formatMoney($subtotal_subzone) ?></th>
																<th><?= $this->sma->formatMoney($total_iva_subzone) ?></th>
																<th class="text-right"><?= $this->sma->formatMoney($total_subzone) ?></th>
																<th></th>
															</tr>
															<?php
																$subtotal_subzone = 0;
																$total_iva_subzone = 0;
																$total_subzone = 0;
															?>
														<?php endif ?>
														<?php if ($total_zone != 0 && isset($_POST['group_by_zone']) && $_POST['group_by_zone']): ?>
															<tr>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th>Total Zona</th>
																<th><?= $this->sma->formatMoney($subtotal_zone) ?></th>
																<th><?= $this->sma->formatMoney($total_iva_zone) ?></th>
																<th class="text-right"><?= $this->sma->formatMoney($total_zone) ?></th>
																<th></th>
															</tr>
															<?php
																$subtotal_zone = 0;
																$total_iva_zone = 0;
																$total_zone = 0;
															?>
														<?php endif ?>
															<tr>
																<th class="text-center">Zona : <?= !empty($sale['nom_zone']) ? $sale['nom_zone'] : 'No registra' ?></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
															</tr>
													<?php
													}


													if (!isset($subzones[$sale['cod_subzone']]) && isset($_POST['group_by_subzone']) && $_POST['group_by_subzone']) {
														$subzones[$sale['cod_subzone']] = 1;
														?>
														<?php if ($total_subzone != 0): ?>
															<tr>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th>Total Barrio</th>
																<th><?= $this->sma->formatMoney($subtotal_subzone) ?></th>
																<th><?= $this->sma->formatMoney($total_iva_subzone) ?></th>
																<th class="text-right"><?= $this->sma->formatMoney($total_subzone) ?></th>
																<th></th>
															</tr>
															<?php
																$subtotal_subzone = 0;
																$total_iva_subzone = 0;
																$total_subzone = 0;
															?>
														<?php endif ?>
															<tr>
																<th class="text-center">Barrio : <?= !empty($sale['nom_subzone']) ? $sale['nom_subzone'] : 'No registra' ?></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
															</tr>
													<?php
													}



													$subtotal_seller += $sale['total'];
													$total_iva_seller += $sale['total_tax'];
													$total_seller += $sale['grand_total'];
													$subtotal_zone += $sale['total'];
													$total_iva_zone += $sale['total_tax'];
													$total_zone += $sale['grand_total'];
													$subtotal_subzone += $sale['total'];
													$total_iva_subzone += $sale['total_tax'];
													$total_subzone += $sale['grand_total'];
													$subtotal += $sale['total'];
													$total_iva += $sale['total_tax'];
													$total += $sale['grand_total'];
												?>
												<?php if ($tipo == 'Detallado' || !$tipo): ?>
													<tr>
														<td ><?php echo $sale['nom_seller']; ?></td>
														<td ><?php echo $sale['Sucursal']; ?></td>
														<td ><?php echo $sale['customer_name']; ?></td>
														<td class="text-center"><?php echo substr($sale['date'],0,10); ?></td>
														<td class="text-center"><?php echo $referencia; ?></td>
														<td ><?php echo $afec; ?></td>
														<td class="text-right"> <?php echo $this->sma->formatMoney($sale['total']); ?></td>
														<td class="text-right"> <?php echo $this->sma->formatMoney($sale['total_tax']); ?></td>
														<td class="text-right"> <?php echo $this->sma->formatMoney($sale['grand_total']); ?></td>
														<td > <?= lang($sale['payment_status']) ?> </td>
													</tr>
												<?php endif ?>
												<?php if ($sale === end($sales) ): ?>
													<?php if (isset($_POST['group_by_zone']) && $_POST['group_by_zone']): ?>
														<tr>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th>Total Zona</th>
															<th><?= $this->sma->formatMoney($subtotal_zone) ?></th>
															<th><?= $this->sma->formatMoney($total_iva_zone) ?></th>
															<th class="text-right"><?= $this->sma->formatMoney($total_zone) ?></th>
															<th></th>
														</tr>
													<?php endif ?>
													<tr>
														<th></th>
														<th></th>
														<th></th>
														<th></th>
														<th></th>
														<th>Total vendedor</th>
														<th><?= $this->sma->formatMoney($subtotal_seller) ?></th>
														<th><?= $this->sma->formatMoney($total_iva_seller) ?></th>
														<th class="text-right"><?= $this->sma->formatMoney($total_seller) ?></th>
														<th></th>
													</tr>
												<?php endif ?>
											<?php endforeach; ?>
											<tr>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
												<th>Total</th>
												<th><?= $this->sma->formatMoney($subtotal) ?></th>
												<th><?= $this->sma->formatMoney($total_iva) ?></th>
												<th class="text-right"><?= $this->sma->formatMoney($total) ?></th>
												<th></th>
											</tr>
										</tbody>
										<tfoot>
											<tr>
												<td class="text-center" style="width:11.55%;"  > <b><?= lang('seller') ?></b></td>
												<td class="text-center" style="width:11.55%;"  > <b><?= lang('biller') ?></b></td>
												<td class="text-center" style="width:11.55%;"  > <b><?= lang('customer') ?></b></td>
												<td class="text-center" style="width:8%;"  > <b><?= lang('date') ?></b></td>
												<td class="text-center" style="width:8%;"  > <b><?= lang('reference_no') ?></b></td>
												<td class="text-center" style="width:8%;"  > <b><?= lang('reference_no') ?></b></td>
												<td class="text-right"  style="width:11.11%;"  > <b><?= lang('subtotal') ?></b></td>
												<td class="text-right"  style="width:11.11%;"  > <b><?= lang('tax') ?></b></td>
												<td class="text-right"  style="width:11.11%;"  > <b><?= lang('total') ?></b></td>
												<td class="text-center" style="width:8%;"  > <b><?= lang('status') ?></b></td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<br>

<script >

	function ActualizarVendedores(valor, selected = 'all') {
		<?php if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right') && $this->session->userdata('company_id')): ?>
		<?php else: ?>
			$("#Vendedor option").remove();
			$('#Vendedor').append('<option value="all" selected="selected" disabled="true" >-- Seleccione Vendedor --</option>');
			jQuery.ajax({
				url: site.base_url+'reports/search_Seller/'+valor,
				type: 'GET',
				dataType: 'json',
				success: function(data, textStatus, xhr) {
					var obj = Object.values(data.sellers);
					obj.forEach(function(element) {
						$('#Vendedor').append('<option value="'+element.cod_seller+'" >'+element.nombre+'</option>');
					});
					selected = "<?= isset($_POST['Vendedor']) ? $_POST['Vendedor'] : 'all' ?>";
					if (selected != 'all') {
						$('#Vendedor').select2('val', selected);
					}
				},
				error: function(xhr, textStatus, errorThrown) {
					$('#ajax-content-container').html('<div class="alert alert-danger" role="alert"><strong>Error:</strong> Lo sentimos, se presento un error por favor refrescar la pantalla volver a intentarlo.</div>');
				}
			});
		<?php endif ?>
	}

	function Search_Vendedor(valor, selected = 'all') {
		$("#Cliente option").remove();
		$('#Cliente').append('<option value="all" selected="selected">-- Seleccione Cliente --</option>');
		var sucursal = $("#Sucursal option:selected").val();
		jQuery.ajax({
			url: site.base_url+'reports/search_customer/'+sucursal+'/'+valor + '/',
			type: 'GET',
			dataType: 'json',
			success: function(data, textStatus, xhr) {
				var obj = Object.values(data.sellers);
				obj.forEach(function(element) {
					$('#Cliente').append('<option value="'+element.cod_id+'" >'+element.nombre+'</option>');
				});
				if (selected != 'all') {
					$('#Cliente').select2('val', selected);
				}
			},
			error: function(xhr, textStatus, errorThrown) {
				$('#ajax-content-container').html('<div class="alert alert-danger" role="alert"><strong>Error:</strong> Lo sentimos, se presento un error por favor refrescar la pantalla volver a intentarlo.</div>');
			}
		});
	}

	function Search_Cliente(valor) {
		var sucursal = $("#Sucursal option:selected").val();
		var vendedor = $("#Vendedor option:selected").val();
	}

	function Search_seller_bills() {
		var sucursal = $("#Sucursal option:selected").val();
		var vendedor = $("#Vendedor option:selected").val();
		var Cliente = $("#Cliente option:selected").val();
		var inicial = $('#start_date').val();
		var Tipo = $('#Tipo:checked').val();
		var Final = $('#end_date').val();
	}

	$('#collapseBiller').collapse('hide');
	<?php if ((!$this->Owner && !$this->Admin && !$this->session->userdata('view_right') && $this->session->userdata('company_id'))): ?>
		$('#collapseSeller').collapse('show');
	<?php else: ?>
		$('#collapseSeller').collapse('hide');
	<?php endif ?>
	$('#collapseBustomer').collapse('hide');

	$(document).ready(function() {

		if ("<?= isset($_POST['valueSucursal']) || isset($_POST['valueVendedor']) || isset($_POST['valueCliente']) ? true : false ?>") {
			$('#collapseBiller').collapse('show');
		}

		if ("<?= isset($_POST['valueVendedor']) || isset($_POST['valueCliente']) ? true : false ?>") {
			$('#collapseSeller').collapse('show');
		}

		if ("<?= isset($_POST['valueCliente']) ? true : false ?>") {
			$('#collapseBustomer').collapse('show');
		}

		$('#TableData').DataTable({
			searching: false,
 		    ordering:  false,
			pageLength: 100,
			lengthMenu : [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
    		responsive: true,
			dom : '<"html5buttons" B>lr<"containerBtn"><"inputFiltro"f>tip',
    		buttons : [
    					{extend:'excel', title:'Ventas por vendedor', className:'btnExportarExcel', exportOptions: {columns : [0,1,2,3,4,5,6]}},
    					{extend:'pdf', title:'Ventas por vendedor', className:'btnExportarPdf', exportOptions: {columns : [0,1,2,3,4,5,6]}},
    				  ],
			oLanguage: {
				sLengthMenu: 'Mostrando _MENU_ registros por página',
				sZeroRecords: 'No se encontraron registros',
				sInfo: 'Mostrando _START_ a _END_ de _TOTAL_ registros',
				sInfoEmpty: 'Mostrando 0 a 0 de 0 registros',
				sInfoFiltered: '(Filtrado desde _MAX_ registros)',
				sSearch:         'Buscar: ',
				oPaginate:{
					sFirst:    'Primero',
					sLast:     'Último',
					sNext:     'Siguiente',
					sPrevious: 'Anterior'
				}
			}
		});


  var btnAcciones = '<div class="dropdown pull-right" id="">'+
  						'<button class="btn btn-outline btn-success dropdown-toggle pull-right" type="button" id="accionesTabla" data-toggle="dropdown" aria-haspopup="true"><?= lang('actions') ?><span class="caret"></span>'+
  						'</button>'+
  						'<ul class="dropdown-menu pull-right" aria-labelledby="accionesTabla">'+
  							'<li>'+
  								'<a  class="ExportXls" id="ExportXls"><span class="fa fa-file-excel-o"></span> Exportar XLS </a>'+
  							'</li>'+
  							'<li>'+
  								'<a class="ExportPdf" id="ExportPdf"><span class="fa fa-file-pdf-o"></span> Exportar PDF</a>'+
  							'</li>'+
  						'</ul>'+
					'</div>';

  $('.containerBtn').html(btnAcciones);

		<?php if (isset($_POST['Sucursal']) && $_POST['Sucursal'] != 'all' ) { ?>

			$('#Sucursal').val("<?= $_POST['Sucursal'] ?>");

			ActualizarVendedores("<?= $_POST['Sucursal'] ?>", "<?= isset($_POST['Vendedor']) ? $_POST['Vendedor'] : '' ?>");

		<?php } ?>

		<?php if (isset($_POST['Vendedor']) && $_POST['Vendedor'] != 'all' ) { ?>

			$('#Vendedor').val("<?= $_POST['Vendedor'] ?>");

			Search_Vendedor("<?= $_POST['Vendedor'] ?>", "<?= $_POST['Cliente'] ?>");

		<?php } ?>



	$("#ExportPdf").click(function() {
			var sucursal = $("#Sucursal option:selected").val();
			var vendedor = $("#Vendedor option:selected").val();
			var Cliente = $("#Cliente option:selected").val();
			var Tipo = $('#Tipo:checked').val();
			var inicial = $('#start_date').val();
			var Final = $('#end_date').val();
			var zone = $('#zone').val() ? $('#zone').val() : false;
			var subzone = $('#subzone').val() ? $('#subzone').val() : false;
			var group_by_zone = $('#group_by_zone').is(':checked') ? 1 : 0;
			var group_by_subzone = $('#group_by_subzone').is(':checked') ? 1 : 0;
			window.open(site.base_url+'reports/seller_bills_pdf/'+sucursal+'/'+vendedor+'/'+Cliente+'/'+inicial+'/'+Final+'/'+Tipo+'/'+zone+'/'+subzone+'/'+group_by_zone+'/'+group_by_subzone);
	});
	$("#ExportXls").click(function() {
		var sucursal = $("#Sucursal option:selected").val() ? $("#Sucursal option:selected").val() : "all";
		var vendedor = $("#Vendedor option:selected").val() ? $("#Vendedor option:selected").val() : "all";
		var Cliente = $("#Cliente option:selected").val() ? $("#Cliente option:selected").val() : "all";
		var Tipo = $('#Tipo:checked').val();
		var inicial = $('#start_date').val();
		var Final = $('#end_date').val();
		var zone = $('#zone').val() ? $('#zone').val() : false;
		var subzone = $('#subzone').val() ? $('#subzone').val() : false;
		var group_by_zone = $('#group_by_zone').is(':checked') ? 1 : 0;
		var group_by_subzone = $('#group_by_subzone').is(':checked') ? 1 : 0;
		window.open(site.base_url+'reports/seller_bills_xls/'+sucursal+'/'+vendedor+'/'+Cliente+'/'+inicial+'/'+Final+'/'+Tipo+'/'+zone+'/'+subzone+'/'+group_by_zone+'/'+group_by_subzone);
	});

	} );


$("#start_date").change(function() {
	$('#textini').html($('#start_date').val());
});
$("#end_date").change(function() {
	$('#textfin').html($('#end_date').val());
});

// Sucursal
$('#biller2').on('ifChecked', function(event){
	$('#Sucursal').select2('readonly', false);
	var Sucursal = $("#Sucursal option:selected").val();
	ActualizarVendedores(Sucursal);
	$('#collapseBiller').collapse('show');
});
$('#biller1').on('ifChecked', function(event){
	$("#Sucursal").val('all');
	$('#collapseBiller').collapse('hide');
	ActualizarVendedores('all');
	$('#Sucursal').select2('val', 'all').trigger('change');
	$('#Sucursal').select2('readonly', true);
});
// Vendedor
$('#seller2').on('ifChecked', function(event){
	$('#Vendedor').select2('readonly', false);
	$('#collapseSeller').collapse('show');
	var Vendedor = $("#Vendedor option:selected").val();
	Search_Vendedor(Vendedor);
	var Sucursal = $("#Sucursal option:selected").val();
	if(Sucursal=='all')
	{
		$("input[name=valueSucursal]").attr('disabled', true);
		$("#Sucursal").val('all');
	}
});
$('#seller1').on('ifChecked', function(event){
	$("#Vendedor").val('all');
	$('#collapseSeller').collapse('hide');
	Search_Vendedor('all');
	$('#Vendedor').prop('selectedIndex',0);
	$('#Vendedor').select2('val', 'all').trigger('change');
	$('#Vendedor').select2('readonly', true);
});
// cliente
$('#Cliente2').on('ifChecked', function(event){
	$('#Cliente').select2('readonly', false);
	var vendedor = $("#Vendedor option:selected").val();
	if(vendedor=='all')
	{
		$("input[name=valueSucursal]").attr('disabled', true);
		$("input[name=valueVendedor]").attr('disabled', true);
		$("#Vendedor").val('all');
		$("#Sucursal").val('all');
	}
	$('#collapseBustomer').collapse('show');
	var Cliente = $("#Cliente option:selected").val();
	Search_Cliente(Cliente);
});
$('#Cliente1').on('ifChecked', function(event){
	$('#Vendedor').select2('readonly', false);
	$('#Sucursal').select2('readonly', false);
	$("input[name=valueSucursal]").attr('disabled', false);
	$("input[name=valueVendedor]").attr('disabled', false);
	$("#Cliente").val('all');
	$('#collapseBustomer').collapse('hide');
	Search_Cliente('all');
	$('#Cliente').prop('selectedIndex',0);
	$('#Cliente').select2('val', 'all').trigger('change');
	$('#Cliente').select2('readonly', true);
});

$('#Buscar').on('click', function(e){
    $('#filter_action').val(1);
    $('#form_filter').submit();
});
$('#zone').on('change', function(){
    code = $('#zone option:selected').data('code');
    $('.postal_code').val(code);
    $.ajax({
      url:"<?= admin_url().'customers/get_subzones/' ?>"+code
    }).done(function(data){
      $('#subzone').html(data);
    });
  });

$(document).ready(function(){
	<?php if (isset($_POST['zone'])): ?>
	    setTimeout(function() {
	        $('#zone').select2('val', '<?= $_POST['zone'] ?>').trigger('change');
	    }, 800);
	<?php endif ?>

	<?php if (isset($_POST['subzone'])): ?>
	    setTimeout(function() {
	        $('#subzone').select2('val', '<?= $_POST['subzone'] ?>').trigger('change');
	    }, 2500);
	<?php endif ?>
});

</script>


</body>
</html>