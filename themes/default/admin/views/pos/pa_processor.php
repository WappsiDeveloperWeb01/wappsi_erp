<?php 
	if (isset($preparation_area_products)) {
		$JSON_pa_products = json_encode($preparation_area_products);
	}
	// $this->sma->print_arrays($preparation_area_products);
?>
<html>
	<head>
	    <meta charset="utf-8">
	    <title>Procesando</title>
	    <base href="<?=base_url()?>"/>
	    <meta http-equiv="cache-control" content="max-age=0"/>
	    <meta http-equiv="cache-control" content="no-cache"/>
	    <meta http-equiv="expires" content="0"/>
	    <meta http-equiv="pragma" content="no-cache"/>
	    <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
	    <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
	    <style type="text/css" media="all">
	        body { color: #000; }
	        #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
	        .btn { border-radius: 0; margin-bottom: 5px; }
	        .bootbox .modal-footer { border-top: 0; text-align: center; }
	        h3 { margin: 5px 0; }
	        .order_barcodes img { float: none !important; margin-top: 5px; }
	        @media print {
	            .no-print { display: none; }
	            #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
	            .no-border { border: none !important; }
	            .border-bottom { border-bottom: 1px solid #ddd !important; }
	            table tfoot { display: table-row-group; }
	        }
	    </style>
	</head>
	<body>
	    <div id="wrapper">
	        <div id="receiptData">
	            <div>
	                <a class="btn btn-block btn-warning no-print" href="<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>"><?= lang("back_to_pos"); ?></a>
	            </div>
	        </div>
	    </div>
	</body>
	<script type="text/javascript">
		site = <?=json_encode(array('url' => base_url(), 'base_url' => admin_url(), 'assets' => $assets, 'settings' => $Settings, 'dateFormats' => $dateFormats))?>;
	</script>
	<script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
	<script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
	<script type="text/javascript">
		<?php if (isset($preparation_area_products)): ?>
			var pa_items = <?= $JSON_pa_products ?>;
			var finished_invoice_id = <?= $finished_invoice_id ? $finished_invoice_id : 'false' ?>;
			localStorage.setItem('pa_items', JSON.stringify(pa_items));
			localStorage.setItem('finished_invoice_id', JSON.stringify(finished_invoice_id));
		<?php else: ?>
			var pa_items = JSON.parse(localStorage.getItem('pa_items'));
			var finished_invoice_id = localStorage.getItem('finished_invoice_id');
		<?php endif ?>
		$('#loading').fadeIn();
		var v = "?biller_id="+50;
		var tpa_id = null;
		if (pa_items) {
			$.each(pa_items, function(pa_id, products){
				url = site.base_url+"pos/pa_processor/1/NULL/"+pa_id+"/false/<?= $locator ?>"+v;
				newWin = window.open(url, 'blank');
				setTimeout(function() {
					newWin.close();
					$('#loading').fadeOut();
				}, 2000);
				tpa_id = pa_id;
				return false;
			});
			if (tpa_id) {
				setTimeout(function() {
					delete pa_items[tpa_id];
					localStorage.setItem('pa_items', JSON.stringify(pa_items));
					location.href=site.base_url+"pos/pa_processor/false/";
				}, 3000);
			} else {
				if (finished_invoice_id != 'false') {
					location.href=site.base_url+"pos/view/"+finished_invoice_id+"/0/1";
				} else {
					location.href=site.base_url+"pos/index";
				}
			}
		} else {
			if (finished_invoice_id != 'false') {
				location.href=site.base_url+"pos/view/"+finished_invoice_id+"/0/1";
			} else {
				location.href=site.base_url+"pos";
			}
		}
	</script>
</html>