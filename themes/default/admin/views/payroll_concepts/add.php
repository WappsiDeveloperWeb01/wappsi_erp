<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h3 class="modal-title" id="myModalLabel"><?= $page_title; ?></h3>
        </div>

        <?= admin_form_open("payroll_concepts/save", ["id"=>"add_type_concept_form"]); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <?= form_label(lang("payroll_concepts_type"), "concept_type_id"); ?>
                        <?php $topts = [""=>lang('select')]; ?>
                        <?php
                            foreach ($types_concepts as $type_concept) {
                                $topts[$type_concept->id] = $type_concept->name;
                            }
                        ?>
                        <?= form_dropdown(["name"=>"concept_type_id", "id"=>"concept_type_id", "class"=>"form-control select2", "required"=>TRUE], $topts); ?>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <?= form_label(lang('payroll_concepts_earned_deducted'), 'earned_deducted'); ?>
                        <?= form_dropdown(["name"=>"earned_deducted", "id"=>"earned_deducted", "class"=>"form-control select2", "disabled"=>TRUE], [""=>"",1=>lang("payroll_management_earnings"), 2=>lang("payroll_management_deductions")]); ?>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <?= form_label(lang("payroll_concepts_name"), "name"); ?>
                        <?= form_input(["name"=>"name", "id"=>"name", "class"=>"form-control", "required"=>TRUE]); ?>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <?= form_label(lang("payroll_concepts_status", "status")); ?>
                        <?php $sopts = [YES=>lang("active"), NOT=>lang("inactive")]; ?>
                        <?= form_dropdown(["name"=>"status", "id"=>"status", "class"=>"form-control select2", "required"=>TRUE], $sopts); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_button(["name"=>"save_button", "id"=>"save_button", "class"=>"btn btn-primary"], lang("save")); ?>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();

        $(document).on('change', '#concept_type_id', function() { get_nature_concept(); });
        $(document).on('click', '#save_button', function() { save_type_concept(); });
    });

    function get_nature_concept()
    {
        var id = $('#concept_type_id').val();

        $.ajax({
            url: '<?= admin_url("payroll_concepts/get_ajax_nature_concept"); ?>',
            type: 'GET',
            dataType: 'HTML',
            data: {
                id: id
            },
        })
        .done(function(data) {
            $('#earned_deducted').select2('val', data);
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    }

    function save_type_concept()
    {
        $("#add_type_concept_form").validate({
            ignore: [],
            rules: {
                concept_type_id:"required"
            },
            messages: {
                concept_type_id:"Este campo es obligatorio"
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem[0].tagName == "SELECT") {
                    $("#s2id_" + elem.attr("id") + " a.select2-choice").addClass('mierror');
                    $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").addClass('mierroArrow');
                } else {
                    elem.addClass(errorClass);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem[0].tagName == "SELECT") {
                    $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                    $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                } else {
                    elem.removeClass(errorClass);
                }
            }
        });

        if ($('#add_type_concept_form').valid()) {
            $('#add_type_concept_form').submit();
        }
    }
</script>
