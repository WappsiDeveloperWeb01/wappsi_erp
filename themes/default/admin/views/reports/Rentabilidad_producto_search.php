
<thead>
	<tr>
		<td class="text-center" style="width: 24%;"> <b><?php echo lang('product'); ?></b></td>
		<td class="text-right" style="width: 11%;"> <b><?php echo lang('total'); ?></b></td>
		<td class="text-right" style="width: 11%;"> <b><?php echo lang('cost'); ?></b></td>
		<td class="text-right" style="width: 11%;"> <b><?php echo lang('profit'); ?></b></td>
		<td class="text-right" style="width: 11%;"> <b><?php echo lang('margin'); ?></b></td>
	</tr>
</thead>
<tbody>
	<?php foreach ($profitabilitys as $profitability): ?>
		<tr >
			<td ><?php echo $profitability['name']; ?></td>
			<td class="text-right"> <?php echo number_format($profitability['total'],2,',',''); ?></td>
			<td class="text-right"> <?php echo number_format($profitability['costo'],2,',',''); ?></td>
			<td class="text-right"> <?php echo number_format($profitability['utilidad'],2,',',''); ?></td>
			<td class="text-right"> <?php echo number_format($profitability['margen'],2,',',''); ?> </td>
		</tr>					
	<?php endforeach; ?>
</tbody>
<tfoot>
	<tr>
		<th class="text-center" > <b><?php echo lang('customer'); ?></b></th>
		<th class="text-right" > <b><?php echo lang('total'); ?></b></th>
		<th class="text-right" > <b><?php echo lang('cost'); ?></b></th>
		<th class="text-right" > <b><?php echo lang('profit'); ?></b></th>
		<th class="text-right" > <b><?php echo lang('margin'); ?></b></th>
	</tr>
</tfoot>
