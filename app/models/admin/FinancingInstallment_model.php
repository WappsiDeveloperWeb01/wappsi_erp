<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class FinancingInstallment_model extends CI_Model
{
    private $tableName = "credit_financing_installments";

    public function __construct()
    {
        parent::__construct();
    }

    public function find($data)
    {
        $this->db->where($data);
        $q = $this->db->get($this->tableName);
        return $q->row();
    }

    public function getIn($field, $data)
    {
        $this->db->where_in($field, $data);
        $q = $this->db->get($this->tableName);
        return $q->result();
    }

    public function get($data)
    {
        $this->db->select("
            cfi.id AS installmentId,
            b.name AS billerName,
            credit_financing.credit_no AS creditNo,
            cfi.installment_no AS installmentNo,
            cfi.installment_due_date AS installmentDueDate,
            IF(DATEDIFF(CURDATE(), cfi.installment_due_date) > 0, DATEDIFF(CURDATE(), cfi.installment_due_date), 0) AS daysExpired,
            0 AS defaultInterest,
            cfi.current_interest_amount AS currentInterestAmount,
            cfi.interest_tax AS interestTax,
            cfi.capital_amount AS capitalAmount,
            cfi.installment_amount AS installmentAmount,
            c.name AS customerName");
        $this->db->where($data);
        $this->db->where("cfi.status", 0);
        $this->db->join("credit_financing_installments cfi", "cfi.financing_credit_id = credit_financing.id");
        $this->db->join("companies b", "b.id = credit_financing.biller_id");
        $this->db->join("companies c", "c.id = cfi.customer_id");
        $this->db->order_by("cfi.installment_due_date, daysExpired");
        $q = $this->db->get("credit_financing");
        return $q->result();
    }

    public function update($data, $id)
    {
        $this->db->where('id', $id);
        if ($this->db->update($this->tableName, $data)) {
            return true;
        }
        return false;
    }
}
