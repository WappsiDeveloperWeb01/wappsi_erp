var rows_start = 0;
var num_rows = 100;
var max_num_rows = 100;

$(document).ready(function(){
    localStorage.removeItem('slretenciones');
    $('#date').prop('max', max_input_date);
    $('#date').prop('min', min_input_date);
    localStorage.removeItem('total_conceptos');
    if (localStorage.getItem('rcbiller')) {
        $('#rcbiller').select2('val', localStorage.getItem('rcbiller'));
        if (biller_id) {
            $('#rcbiller').select2('readonly', true)
        }
    }
    if (localStorage.getItem('customer') || customer_id) {
        customer = localStorage.getItem('customer') ? localStorage.getItem('customer') : customer_id;
        nscustomer(customer);
        if (localStorage.getItem('sales')) {
            loadItems();
        }

        if (customer_id) {
            $('#customer').select2('readonly', true);
            $.ajax({
                url : site.base_url+"customers/get_customer_special_discount/"+customer_id,
                dataType : "JSON"
            }).done(function(data){
                if (data.deposit_amount > 0 || data.cxp_portfolio > 0) {
                    bhtml = "";
                    if (data.deposit_amount > 0) {
                        bhtml+="<h4>Anticipos</h4><p>El cliente seleccionado tiene <b>$"+formatMoney(data.deposit_amount)+"</b> en saldo de anticipos</p>";
                    }
                    if (data.cxp_portfolio > 0) {
                        bhtml+="<h4>Cuentas por pagar</h4><p>Existe un saldo de <b>$"+formatMoney(data.cxp_portfolio)+"</b> en cuentas por pagar a este tercero</p>";
                    }
                    bootbox.alert(bhtml);
                }
            });

            $.ajax({
                url: site.base_url+"sales/getCustomerAddresses",
                type: "get",
                data: {"customer_id" : customer_id}
            }).done(function(data){
                if (data != false) {
                    $('#address_id').html(data);
                    $('#address_id').select2('val', address_id);
                } else {
                    $('#address_id').select2('val', '');
                }
            });
        }
        if (sale_id == false) {
            $('#customer').trigger('change');
        }
    } else {
        nscustomer();
    }
    if (localStorage.getItem('payment_distribution')) {
        $('#payment_distribution').val(localStorage.getItem('payment_distribution'));
    }

    if (sale_id) {
        localStorage.removeItem('payment_distribution_applied');
        $.ajax({
            url: site.base_url+"sales/sale_suggestions",
            type: "GET",
            data : {
                'customer_id' :  customer_id,
                'address_id' :  $('#address_id').val(),
                'biller_id' :  biller_id,
                'sale_id' :  sale_id,
                'rows_start' :  rows_start,
                'num_rows' :  num_rows,
            },
            dataType: "json",
        }).done(function(data){
            $('#amount_1').val(0);
            $('#base').val(0);
            localStorage.removeItem('sales');
            localStorage.setItem('sales', JSON.stringify(data));
            loadItems();
            $('.enable_payment').trigger('click');
        }).fail(function(){
            loadItems();
        });
    }

    if (payment_date = localStorage.getItem('payment_date')) {
        $('#payment_date').val(payment_date);
    }

    if (consecutive_payment = localStorage.getItem('consecutive_payment')) {
        $('#consecutive_payment').val(consecutive_payment);
    }

    // $('#rcbiller').trigger('change');
    load_biller_data(false);

    $("#date").datetimepicker({
        format: site.dateFormats.js_ldate,
        fontAwesome: true,
        language: 'sma',
        weekStart: 1,
        todayBtn: 1,
        startDate: min_input_date,
        endDate: max_input_date,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0
    }).datetimepicker('update', new Date());

});

$(document).on('keyup', function(e){
    $('#ajaxCall').fadeIn();
    if (e.keyCode == 13) {
        setTimeout(function() {
            $('#add_payment').prop('disabled', false);
            $('#ajaxCall').fadeOut();
        }, 800);
    }
});

// clear localStorage and reload
$(document).on('keyup', function(e){
    $('#ajaxCall').fadeIn();
    if (e.keyCode == 13) {
        setTimeout(function() {
            $('#add_payment').prop('disabled', false);
            $('#ajaxCall').fadeOut();
        }, 800);
    }
});

// clear localStorage and reload
$('#reset').click(function (e) {
    bootbox.confirm(lang.r_u_sure, function (result) {
        remove_localstorage_data();
        $('#modal-loading').show();
        location.reload();
    });
});

$('#add_payment').click(function(){

});

function remove_localstorage_data(){
    if (localStorage.getItem('sales')) {
        localStorage.removeItem('sales');
    }
    if (localStorage.getItem('customer')) {
        localStorage.removeItem('customer');
    }
    if (localStorage.getItem('address_id')) {
        localStorage.removeItem('address_id');
    }
    if (localStorage.getItem('payment_distribution')) {
        localStorage.removeItem('payment_distribution');
    }
    if (localStorage.getItem('payment_date')) {
        localStorage.removeItem('payment_date');
    }
    if (localStorage.getItem('consecutive_payment')) {
        localStorage.removeItem('consecutive_payment');
    }
    if (localStorage.getItem('rcbiller')) {
        localStorage.removeItem('rcbiller');
    }
    if (localStorage.getItem('payment_distribution_applied')) {
        localStorage.removeItem('payment_distribution_applied');
    }
}


$(document).on('change', '#customer', function(){

    rows_start = 0;
    localStorage.setItem('customer', $(this).val());
    localStorage.removeItem('payment_distribution_applied');
    $('#amount_1').val(formatMoney(0));
    $('#total_rete_amount_2').val(formatMoney(0));
    $('#total_other_concepts').val(formatMoney(0));
    $('#total_received').val(formatMoney(0));

    if (sales = localStorage.getItem('sales')) {
        localStorage.removeItem('sales');
    }

    $.ajax({
        url: site.base_url+"sales/getCustomerAddresses",
        type: "get",
        data: {"customer_id" : $(this).val()}
    }).done(function(data){
        if (data != false) {
            $('#address_id').html(data);
            if (address_id = localStorage.getItem('address_id')) {
                $('#address_id option').each(function(index, option){
                    if ($(option).val() == address_id) {
                        $(option).prop('selected', 'selected');
                    }
                });
            }
            $('#address_id').select2();
        } else {
            $('#address_id').select2('val', '');
        }

        $.ajax({
            url : site.base_url+"sales/sale_suggestions",
            type : "GET",
            dataType: "JSON",
            data : {
                'customer_id' :  $('#customer').val(),
                'address_id' :  $('#address_id').val(),
                'biller_id' :  $('#rcbiller').val(),
                'rows_start' :  rows_start,
                'num_rows' :  num_rows,
            },
        }).done(function(data){
            if (data.response !== undefined && data.response == 0) {
                command: toastr.warning('El cliente seleccionado no tiene cartera pendiente', 'Sin resultados', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "6000",
                        "extendedTimeOut": "1000",
                    });
                localStorage.removeItem('sales');
                loadItems();
            } else {
                $('#amount_1').val(0);
                $('#base').val(0);
                localStorage.removeItem('sales');
                localStorage.setItem('sales', JSON.stringify(data));
                loadItems();
            }
        }).fail(function(){
            loadItems();
        });



    }); //FIN AJAX SUCURSALES



    var customer_validate_min_base_retention = false;
    $.ajax({
        url : site.base_url+"customers/get_customer_by_id/"+$('#customer').val()+"/"+$('#rcbiller').val(),
        dataType : "JSON"
    }).done(function(data){
        if (data.customer_validate_min_base_retention == 0) {
            customer_validate_min_base_retention = true;
        }
        localStorage.setItem('customer_validate_min_base_retention', customer_validate_min_base_retention);
        $('#customer_portfolio').val(formatMoney(data.portfolio));
    }); //FIN AJAX VENTAS

    $.ajax({
        url : site.base_url+"customers/get_customer_special_discount/"+$('#customer').val(),
        dataType : "JSON"
    }).done(function(data){
        if (data.deposit_amount > 0 || data.cxp_portfolio > 0) {
            bhtml = "";
            if (data.deposit_amount > 0) {
                bhtml+="<h4>Anticipos</h4><p>El cliente seleccionado tiene <b>$"+formatMoney(data.deposit_amount)+"</b> en saldo de anticipos</p>";
            }
            if (data.cxp_portfolio > 0) {
                bhtml+="<h4>Cuentas por pagar</h4><p>Existe un saldo de <b>$"+formatMoney(data.cxp_portfolio)+"</b> en cuentas por pagar a este tercero</p>";
            }
            bootbox.alert(bhtml);
        }
    });

});

$(document).on('click, change', '.enable_payment', function(){

    var check = $(this);
    var sales = JSON.parse(localStorage.getItem('sales'));
    var row = check.closest('tr');
    row_id = row.attr('data-item-id');
    sales[row_id].sale_selected = check.prop('checked');
    var reset_rete = false;
    if (!check.prop('checked')) {
        sales[row_id].amount_to_paid = 0;
        delete sales[row_id].amount_to_paid_manually_changed;
        reset_rete = true;
        sales[row_id].activated_manually = false;
        sales[row_id].deactivated_manually = true;
        //$('.retencion_'+sales[row_id].id).prop('check', false).trigger('change');
    } else {
        sales[row_id].amount_to_paid = sales[row_id].grand_total - sales[row_id].paid;
        sales[row_id].activated_manually = true;
        sales[row_id].deactivated_manually = false;
    }
    localStorage.removeItem('sales');
    localStorage.setItem('sales', JSON.stringify(sales));
    if (reset_rete) {
        reiniciar_retencion(row_id, true);
    } else {
        loadItems();
    }
});

$(document).on('click, change', '.enable_retencion', function(){

    var check = $(this);
    var row = check.closest('tr');
    row_id = row.attr('data-item-id');

    if (check.prop('checked')) {

    } else {

        reiniciar_retencion(row_id, true);

    }

});

$(document).on('change', '.amount', function(){
  var val = 0;
  var sales = JSON.parse(localStorage.getItem('sales'));
  var rowP = $(this).closest('tr');
  row_idP = rowP.attr('data-item-id');
  $('.amount').each(function(index){
    var row = $(this).closest('tr');
    row_id = row.attr('data-item-id')
    if (!$(this).prop('readonly')) {
      var max = $(this).data('totalpaid');
      if ($(this).val() >= 0 && $(this).val() <= max) {
        val += parseInt($(this).val());
      } else {
        $(this).val(max);
        val += parseFloat(max);
      }
    }
    if (sales[row_id].amount_to_paid != $(this).val()) {
        sales[row_id].amount_to_paid_manually_changed = true;
    }
    sales[row_id].amount_to_paid = $(this).val();
  });
  setTimeout(function() {
    $('#amount_1').val(val);
    $('#base').val(val);
    localStorage.removeItem('sales');
    localStorage.setItem('sales', JSON.stringify(sales));
    reiniciar_retencion(row_idP);
  }, 800);
});

$(document).on('change', '.paid_by', function () {
    var p_val = $(this).val();
    localStorage.setItem('paid_by', p_val);
    $('#rpaidby').val(p_val);
    if (p_val == 'cash' ||  p_val == 'other') {
        $('.pcheque_1').hide();
        $('.pcc_1').hide();
        $('.pcash_1').show();
        $('#payment_note_1').focus();
    } else if (p_val == 'CC') {
        $('.pcheque_1').hide();
        $('.pcash_1').hide();
        $('.pcc_1').show();
        $('#pcc_no_1').focus();
    } else if (p_val == 'Cheque') {
        $('.pcc_1').hide();
        $('.pcash_1').hide();
        $('.pcheque_1').show();
        $('#cheque_no_1').focus();
    } else {
        $('.pcheque_1').hide();
        $('.pcc_1').hide();
        $('.pcash_1').hide();
    }
    if (p_val == 'gift_card') {
        $('.gc').show();
        $('.ngc').hide();
        $('#gift_card_no').focus();
    } else {
        $('.ngc').show();
        $('.gc').hide();
        $('#gc_details').html('');
    }
});

$(document).on('click, change', '.check_all', function(){

    var input = $(this);
    var status = input.prop('checked') ? true : false;

    var sales = JSON.parse(localStorage.getItem('sales'));

    $('.enable_payment').each(function(index){

        var check = $(this);
        var row = check.closest('tr');
        row_id = row.attr('data-item-id');

        if (status) {

            $(this).prop('checked', status);

        } else {

            delete sales[row_id].total_rete_amount;
            delete sales[row_id].fuente_option;
            delete sales[row_id].iva_option;
            delete sales[row_id].ica_option;
            delete sales[row_id].otros_option;
            delete sales[row_id].id_rete_fuente;
            delete sales[row_id].id_rete_iva;
            delete sales[row_id].id_rete_ica;
            delete sales[row_id].id_rete_otros;
            sales[row_id].rete_fuente_percentage = 0;
            sales[row_id].rete_ica_percentage = 0;
            sales[row_id].rete_iva_percentage = 0;
            sales[row_id].rete_other_percentage = 0;
            sales[row_id].rete_fuente_base = 0;
            sales[row_id].rete_ica_base = 0;
            sales[row_id].rete_iva_base = 0;
            sales[row_id].rete_other_base = 0;
            sales[row_id].rete_fuente_total = 0;
            sales[row_id].rete_ica_total = 0;
            sales[row_id].rete_iva_total = 0;
            sales[row_id].rete_other_total = 0;
            sales[row_id].rete_fuente_account = 0;
            sales[row_id].rete_ica_account = 0;
            sales[row_id].rete_iva_account = 0;
            sales[row_id].rete_other_account = 0;

            $(this).removeAttr('checked');
        }

        var sale_id = $(this).data('saleid');

        if ($(this).prop('checked')) {
            var input = $('.sale_id_'+sale_id);
            var amount = input.data('totalpaid');
            input.prop('readonly', false).val(amount);
            sales[row_id].amount_to_paid = amount;
            sales[row_id].sale_selected = true;
            sales[row_id].activated_manually = true;
        } else {
            $(this).removeProp('checked');
            var input = $('.sale_id_'+sale_id);
            var amount = input.data('totalpaid');
            input.prop('readonly', true).val(0);
            sales[row_id].amount_to_paid = 0;
        }


        sales[row_id].sale_selected = status;

    });

    localStorage.removeItem('sales');
    localStorage.setItem('sales', JSON.stringify(sales));

    loadItems();

});

$(document).on('click', '.edit', function(){

    reset_retention_window();

    itemid = $(this).data('item-id');
    sales = JSON.parse(localStorage.getItem('sales'));
    sale = sales[itemid];

    $('#rtModalLabel').text("Factura N° "+sale.reference_no);
    $('#rtModalLabel2').text(sale.customer);

    if (sale.rete_fuente_base_paid > 0 || sale.rete_iva_base_paid > 0 || sale.rete_ica_base_paid > 0 || sale.rete_other_base_paid > 0) {
        $('#rete_otros').prop('disabled', true);
    } else {
        $('#rete_otros').prop('disabled', false);
    }

    $('#suggested_total').html(0);
    $('#suggested_tax').html(0);
    $('#suggested_subtotal').html(0);

    if (sale.sale_selected) {

        $('#rete_base_subtotal').val(0).prop('readonly', true);
        $('#rete_base_tax').val(0).prop('readonly', true);
        $('#rete_base_total').val(0).prop('readonly', true);

        $('#rete_applied').val(itemid);

        if (rete_total = sale.total_rete_amount) {
            retenciones = {
                    'id_rete_fuente' : sale.id_rete_fuente,
                    'id_rete_iva' : sale.id_rete_iva,
                    'id_rete_ica' : sale.id_rete_ica,
                    'id_rete_otros' : sale.id_rete_otros,
                    'id_rete_bomberil' : sale.id_rete_bomberil,
                    'id_rete_autoaviso' : sale.id_rete_autoaviso,
                };

            recalcular_retenciones(retenciones);
        }

        if (discount = sale.discount) {
            recalcular_descuento(discount);
        }

        if (sale.rete_autoica_total > 0) {
            $('#rete_ica').prop('disabled', true);
            command: toastr.warning('La venta cuenta con Auto retención de ICA aplicada', '¡Atención!', {
                    "showDuration": "1200",
                    "hideDuration": "1000",
                    "timeOut": "12000",
                    "extendedTimeOut": "1000",
                });
        } else {
            $('#rete_ica').prop('disabled', false);
        }

        $('#rtModal').appendTo("body").modal('show');
    }

});

$(document).on('blur', '#payment_distribution', function(){
    var val = parseFloat($(this).val());
    if (val > 0) {
        localStorage.setItem('payment_distribution', val);
    } else {
        localStorage.removeItem('payment_distribution');
        localStorage.removeItem('payment_distribution_applied');
    }
    loadItems();
});

//aca load

function loadItems(){
    $('#table_sales tbody').empty();
    var amount1 = 0;
    if (localStorage.getItem('sales')) {
        var sales = JSON.parse(localStorage.getItem('sales'));
        var max_date = null;
        var total_received = 0;
        var total_receipted = 0;
        var total_rete_amount = 0;
        var disable_submit = false;
        var count = 0;
        var max = max_num_rows;
        var payment_distribution = localStorage.getItem('payment_distribution') ? parseFloat(localStorage.getItem('payment_distribution')) : 0;
        var exists_payment_distribution = localStorage.getItem('payment_distribution') ? true : false;
        var total_discounted = 0;
        var payment_distribution_applied = localStorage.getItem('payment_distribution_applied');
        $.each(sales, function(index){
            count++;
            if (count == (max + 1)) {
                return false;
            }
            sales[index].sale_in_view = true;
            sale = this;
            if (exists_payment_distribution && sale.deactivated_manually === false && payment_distribution > 0) {
                sale_balance = (sale.amount_to_paid > 0 ? sale.amount_to_paid : (sale.grand_total - sale.paid));
                    if (sale_balance <= payment_distribution) {
                        payment_distribution -= sale_balance;
                        sale.amount_to_paid = sale_balance;
                        sale.sale_selected = true;
                    } else if (sale_balance > payment_distribution) {
                        sale.amount_to_paid = payment_distribution;
                        payment_distribution = 0;
                        sale.sale_selected = true;
                    }
            } else {
                if (!sale.activated_manually){
                    sale.sale_selected = false;
                    sale.amount_to_paid = 0;
                }
                if (sale.activated_manually === true && exists_payment_distribution && payment_distribution <= 0) {
                    sale.amount_to_paid = 0;
                }
            }
            var sale_selected = sale.sale_selected;

            if (sale_selected) {
                if (max_date == null || max_date != null && sale.max_date > max_date) {
                    max_date = sale.max_date;
                }
            }

            var sale_total_rete_amount = 0;
            var invalid_retention = false;
            total_rete_amount += formatDecimal(sale.total_rete_amount);
            sale_total_rete_amount = formatDecimal(sale.total_rete_amount);

            max_amount_to_paid_without_retention = formatDecimal(sale.grand_total * 0.9);
            advertising = false;
            if ((formatDecimal(sale.amount_to_paid) + formatDecimal(sale.paid)) > max_amount_to_paid_without_retention) {
                if(sale_total_rete_amount < 1){
                    advertising = true;
                }
            }

            if (formatDecimal(sale.amount_to_paid) < formatDecimal(sale.total_rete_amount)) {
                // disable_submit = true;
                // invalid_retention = true;
            }
            var tr_html = $('<tr data-item-id="'+index+'"></tr>');
            var discounted = 0;
            if (sale.discount) {
                var discount_html = '';
                discount = sale.discount;
                arr_apply = discount.discount_apply_to;
                arr_amount = discount.discount_amount;
                arr_calculated = discount.discount_amount_calculated;
                arr_ledger_id = discount.discount_ledger_id;
                $.each(arr_apply, function(index, val){
                    index_d = index+1;
                    apply_to = val;
                    amount = arr_amount[index];
                    amount_cal = arr_calculated[index];
                    ledger_id = arr_ledger_id[index];
                    if (amount_cal > 0) {
                        discount_html +=
                                        "<input type='hidden' name='discount_sale_id[]' value='"+sale.id+"'>"+
                                        "<input type='hidden' name='discount_sale_reference[]' value='"+sale.reference_no+"'>"+
                                        "<input type='hidden' name='discount_apply_to[]' value='"+apply_to+"'>"+
                                        "<input type='hidden' name='discount_amount[]' value='"+amount+"'>"+
                                        "<input type='hidden' name='discount_amount_calculated[]' value='"+amount_cal+"'>"+
                                        "<input type='hidden' name='discount_ledger_id[]' value='"+ledger_id+"'>"
                                        ;
                        discounted += parseFloat(amount_cal);
                        total_discounted += parseFloat(amount_cal);
                    }
                });
            }

            inv_total_discounted = (formatDecimal(tra = sale.total_rete_amount ? sale.total_rete_amount : 0) + parseFloat(discounted));
            if (formatDecimal(sale.amount_to_paid + inv_total_discounted) > formatDecimal(sale.grand_total - sale.paid)) {
                sale.amount_to_paid -= inv_total_discounted;
                if (exists_payment_distribution) {
                    payment_distribution += inv_total_discounted;
                }
            }
            commision = false;
            if (parseFloat(sale.collection_comm_perc) > 0) {
                perc_commision = parseFloat(sale.collection_comm_perc) / 100;
                if (perc_commision > 0 && sale.amount_to_paid > 0) {
                    base_rete = ((sale.amount_to_paid + discounted) / (sale.grand_total - sale.prev_retentions_total)) * sale.prev_retentions_total;
                    base_commision = (((
                                        (
                                            parseFloat((sale.amount_to_paid + discounted)) +
                                            parseFloat(base_rete)
                                            +
                                            parseFloat(formatDecimal(sale.rete_fuente_total)) +
                                            parseFloat(formatDecimal(sale.rete_iva_total)) +
                                            parseFloat(formatDecimal(sale.rete_ica_total)) +
                                            parseFloat(formatDecimal(sale.rete_other_total))
                                        )
                                        / sale.grand_total) * sale.total)) - discounted;
                    commision = base_commision * perc_commision;
                }
            }
            total_received += parseFloat(sale.amount_to_paid) + parseFloat(inv_total_discounted);
            tbody = "<td class='text-center'>"+sale.reference_no+"</td>"+
                    "<td class='text-center'>"+(sale.return_sale_ref != null ? sale.return_sale_ref : '')+"</td>"+
                    "<td class='text-center'>"+sale.date+"</td>"+
                    "<td class='text-center'>"+(sale.due_date ? sale.due_date : sale.date)+"</td>"+

                    "<td class='text-center'>"+formatDecimals(((sale.total_discount * 100) / (parseFloat(sale.total) + parseFloat(sale.product_discount))))+"</td>"+
                    "<td class='text-right'>"+formatMoney(sale.total_discount)+"</td>"+
                    "<td class='text-right'>"+formatMoney(sale.total)+"</td>"+
                    "<td class='text-right'>"+formatMoney(sale.total_tax)+"</td>"+
                    "<td class='text-center'>"+formatDecimals(sale.collection_comm_perc)+"</td>"+


                    "<td class='text-right'>"+formatMoney(sale.grand_total)+"</td>"+
                    "<td class='text-right'>"+
                        (sale.paid > 0 ? "<a href='"+site.base_url+"sales/payments/"+sale.id+"' data-toggle='modal' data-target='#myModal'> <i class='fa fa-eye'></i> "+formatMoney(sale.paid)+"</a>" :
                        formatMoney(sale.paid))+
                    "</td>"+
                    "<td class='text-right'>"+formatMoney(sale.grand_total - sale.paid < 0 ? 0 : sale.grand_total - sale.paid)+"</td>"+
                    "<td class='text-right'>"+
                        "<div class='input-group'>"+
                          "<input type='hidden' name='sale_id["+index+"]' value='"+sale.id+"'>"+

                          (advertising ? "<span class='input-group-addon' style='cursor:pointer;' data-toggle='tooltip' data-placement='bottom' title='EL valor a pagar es mayor al 90% del total de la factura, después no le será posible aplicarle retención.'><i class='text-danger fa fa-exclamation-triangle'></i></span>" : "")+

                          "<input type='text' name='amount["+index+"]' value='"+
                            (sale_selected ?
                                (sale.amount_to_paid >= 0 || exists_payment_distribution) ?
                                sale.amount_to_paid :
                                (sale.grand_total - sale.paid)
                                : 0
                            )+"' data-totalpaid='"+(sale.grand_total - sale.paid)+"' class='amount form-control number_mask sale_id_"+sale.id+" text-right' "+(sale_selected ? "" : "readonly='true'")+" >"+
                        "</div>"+
                    "</td>"+
                    "<td class='text-right'>"+
                        "<div class='input-group'>"+
                            "<input type='hidden' name='sale_id_rete_fuente["+index+"]' value='"+formatDecimal(sale.id_rete_fuente)+"'>"+
                            "<input type='hidden' name='sale_id_rete_iva["+index+"]' value='"+formatDecimal(sale.id_rete_iva)+"'>"+
                            "<input type='hidden' name='sale_id_rete_ica["+index+"]' value='"+formatDecimal(sale.id_rete_ica)+"'>"+
                            "<input type='hidden' name='sale_id_rete_bomberil["+index+"]' value='"+formatDecimal(sale.id_rete_bomberil)+"'>"+
                            "<input type='hidden' name='sale_id_rete_autoaviso["+index+"]' value='"+formatDecimal(sale.id_rete_autoaviso)+"'>"+
                            "<input type='hidden' name='sale_id_rete_other["+index+"]' value='"+formatDecimal(sale.id_rete_otros)+"'>"+

                            "<input type='hidden' name='sale_rete_fuente_total["+index+"]' value='"+sale.rete_fuente_total+"'>"+
                            "<input type='hidden' name='sale_rete_iva_total["+index+"]' value='"+sale.rete_iva_total+"'>"+
                            "<input type='hidden' name='sale_rete_ica_total["+index+"]' value='"+sale.rete_ica_total+"'>"+
                            "<input type='hidden' name='sale_rete_bomberil_total["+index+"]' value='"+sale.rete_bomberil_total+"'>"+
                            "<input type='hidden' name='sale_rete_autoaviso_total["+index+"]' value='"+sale.rete_autoaviso_total+"'>"+
                            "<input type='hidden' name='sale_rete_other_total["+index+"]' value='"+sale.rete_other_total+"'>"+

                            "<input type='hidden' name='sale_rete_fuente_percentage["+index+"]' value='"+sale.rete_fuente_percentage+"'>"+
                            "<input type='hidden' name='sale_rete_iva_percentage["+index+"]' value='"+sale.rete_iva_percentage+"'>"+
                            "<input type='hidden' name='sale_rete_ica_percentage["+index+"]' value='"+sale.rete_ica_percentage+"'>"+
                            "<input type='hidden' name='sale_rete_bomberil_percentage["+index+"]' value='"+sale.rete_bomberil_percentage+"'>"+
                            "<input type='hidden' name='sale_rete_autoaviso_percentage["+index+"]' value='"+sale.rete_autoaviso_percentage+"'>"+
                            "<input type='hidden' name='sale_rete_other_percentage["+index+"]' value='"+sale.rete_other_percentage+"'>"+

                            "<input type='hidden' name='sale_rete_fuente_base["+index+"]' value='"+sale.rete_fuente_base+"'>"+
                            "<input type='hidden' name='sale_rete_iva_base["+index+"]' value='"+sale.rete_iva_base+"'>"+
                            "<input type='hidden' name='sale_rete_ica_base["+index+"]' value='"+sale.rete_ica_base+"'>"+
                            "<input type='hidden' name='sale_rete_bomberil_base["+index+"]' value='"+sale.rete_bomberil_base+"'>"+
                            "<input type='hidden' name='sale_rete_autoaviso_base["+index+"]' value='"+sale.rete_autoaviso_base+"'>"+
                            "<input type='hidden' name='sale_rete_other_base["+index+"]' value='"+sale.rete_other_base+"'>"+

                            "<input type='hidden' name='sale_rete_fuente_account["+index+"]' value='"+sale.rete_fuente_account+"'>"+
                            "<input type='hidden' name='sale_rete_iva_account["+index+"]' value='"+sale.rete_iva_account+"'>"+
                            "<input type='hidden' name='sale_rete_ica_account["+index+"]' value='"+sale.rete_ica_account+"'>"+
                            "<input type='hidden' name='sale_rete_bomberil_account["+index+"]' value='"+sale.rete_bomberil_account+"'>"+
                            "<input type='hidden' name='sale_rete_autoaviso_account["+index+"]' value='"+sale.rete_autoaviso_account+"'>"+
                            "<input type='hidden' name='sale_rete_other_account["+index+"]' value='"+sale.rete_other_account+"'>"+
                            (invalid_retention ? "<span class='input-group-addon edit text-danger' data-item-id='"+index+"' style='cursor:pointer;' ata-toggle='tooltip' data-placement='bottom' title='El valor de la retención es mayor al monto a pagar.'><i class='fa fa-exclamation-triangle'></i></span>" : "<span class='input-group-addon edit' data-item-id='"+index+"' style='cursor:pointer;'><i class='fa fa-pencil'></i></span>")+
                            (sale.discount ? discount_html : '')+

                            "<input type='hidden' name='sale_commision_base["+index+"]' value='"+(commision ? base_commision : null)+"'>"+
                            "<input type='hidden' name='sale_commision_percentage["+index+"]' value='"+(commision ? perc_commision * 100 : null)+"'>"+
                            "<input type='hidden' name='sale_commision_amount["+index+"]' value='"+(commision ? formatDecimals(commision) : null)+"'>"+
                            "<input type='hidden' name='sale_seller_id["+index+"]' value='"+sale.seller_id+"'>"+

                            "<input type='text' class='form-control text-right' value='"+inv_total_discounted+"' readonly>"+
                            "<span class='input-group-addon'>"+
                                "<input type='checkbox' class='retencion_"+sale.id+" enable_retencion' data-saleid='"+sale.id+"' "+(tra = sale.total_rete_amount ? "checked" : "")+">"+
                            "</span>"+
                        "</div>"+
                    "</td>"+
                    "<td class='text-right'>"+
                        "<input type='checkbox' name='row_selected["+index+"]' class='enable_payment' data-saleid='"+sale.id+"'  "+(sale_selected ? 'checked="true"' : '')+">"+
                    "</td>";
            tr_html.html(tbody);
            tr_html.appendTo('#table_sales tbody');
            // total_receipted = total_received - total_rete_amount - total_discounted;
            total_receipted = total_received;
            amount1 = total_received - (parseFloat(total_rete_amount) + parseFloat(total_discounted));
            $('#amount_1').val(formatMoney(amount1));
            $('#total_rete_amount_2').val(formatMoney(total_rete_amount + total_discounted));
            $('#total_received').val(formatMoney(total_receipted));
            $('#base').val(total_received);
        });

        if (parseFloat(payment_distribution) < parseFloat(localStorage.getItem('payment_distribution'))) {
            localStorage.setItem('payment_distribution_applied', 1);
        }

        localStorage.removeItem('sales');
        localStorage.setItem('sales', JSON.stringify(sales));

        if (formatDecimal(total_receipted) < 0) {
            disable_submit = true;
            $('.negative_alert').css('display', '');
            Command: toastr.error('El monto total recibido está en negativo.', 'Error', {onHidden : function(){}})
        } else {
            $('.negative_alert').css('display', 'none');
        }

        if (disable_submit) {
            $('#add_payment').prop('disabled', true);
        } else {
            $('#add_payment').prop('disabled', false);
        }
        if (payment_distribution > 0) {
            $('#deposit_amount_text').val(formatMoney(payment_distribution));
            $('#deposit_amount').val(payment_distribution);
            $('#deposit').fadeIn();
            command: toastr.warning('Se habilitó una forma para indicar si el saldo de este monto se registrará o no cómo un anticipo para el cliente', 'Se detectó saldo del monto a distribuir', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                    onclick: function () {
                        $("html, body").animate({ scrollTop: $('#deposit')[0].scrollHeight}, 1000)
                    }
                });
            $('.set_deposit_on').iCheck('check');
        } else {
            $('#deposit_amount_text').val(formatMoney(0));
            $('#deposit_amount').val(0);
            $('#deposit').fadeOut();
            $('.set_deposit_off').iCheck('check');
        }
        set_number_mask();
        $('#paid_by_1').trigger('change');
        if (max_date != null) {
            $('input[name="date"]').prop('min', max_date).trigger('change');
        }
    }

    if (total_conceptos = localStorage.getItem('total_conceptos')) {
        amount1 += formatDecimal(total_conceptos);
        $('#total_other_concepts').val(formatMoney(total_conceptos));
        $('#amount_1').val(formatMoney(amount1));
    }
}

// JS Retenciones


    $(document).on('click', '#rete_fuente', function(){
        var check = $(this);
        $('#updateOrderRete').prop('disabled', true);
        if (check.is(':checked')) {
             $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/FUENTE"
            }).done(function(data){
                $('#rete_fuente_option').html(data);
                $('#rete_fuente_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_fuente_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_fuente_option option:selected').data('apply');
            manual_base = $('#rete_fuente_option option:selected').data('manual_base');
            $('#rete_fuente_option').select2('val', '').attr('disabled', true).select();
            $('#rete_fuente_tax').val('');
            $('#rete_fuente_valor').val('');
            setReteTotalAmount(rete_fuente_amount, '-');
            resetBase(apply, manual_base);
        }
    });

    $(document).on('click', '#rete_iva', function(){
        var check = $(this);
        $('#updateOrderRete').prop('disabled', true);
        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/IVA"
            }).done(function(data){
                $('#rete_iva_option').html(data);
                $('#rete_iva_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_iva_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_iva_option option:selected').data('apply');
            $('#rete_iva_option').select2('val', '').attr('disabled', true).select();
            $('#rete_iva_tax').val('');
            $('#rete_iva_valor').val('');
            setReteTotalAmount(rete_iva_amount, '-');
            resetBase(apply);
        }
    });

    $(document).on('click', '#rete_ica', function(){
        var check = $(this);
        $('#updateOrderRete').prop('disabled', true);
        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/ICA"
            }).done(function(data){
                $('#rete_ica_option').html(data);
                $('#rete_ica_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_ica_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                reset_rete_bomberil();
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_ica_option option:selected').data('apply');
            $('#rete_ica_option').select2('val', '').attr('disabled', true).select();
            $('#rete_ica_tax').val('');
            $('#rete_ica_valor').val('');
            setReteTotalAmount(rete_ica_amount, '-');
            resetBase(apply);
        }
    });

    $(document).on('click', '#rete_otros', function(){
        var check = $(this);
        $('#updateOrderRete').prop('disabled', true);
        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/OTRA"
            }).done(function(data){
                $('#rete_otros_option').html(data);
                $('#rete_otros_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_otros_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_otros_option option:selected').data('apply');
            $('#rete_otros_option').select2('val', '').attr('disabled', true).select();
            $('#rete_otros_tax').val('');
            $('#rete_otros_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
            resetBase(apply);
        }
    });

    $(document).on('click', '#rete_bomberil', function(){
        var check = $(this);
        $('#updateOrderRete').prop('disabled', true);
        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/BOMB"
            }).done(function(data){
                $('#rete_bomberil_option').html(data);
                $('#rete_bomberil_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_bomberil_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_bomberil_option option:selected').data('apply');
            $('#rete_bomberil_option').select2('val', '').attr('disabled', true).select();
            $('#rete_bomberil_tax').val('');
            $('#rete_bomberil_valor').val('');
            setReteTotalAmount(rete_ica_amount, '-');
            resetBase(apply);
        }
    });

    $(document).on('click', '#rete_autoaviso', function(){
        var check = $(this);
        $('#updateOrderRete').prop('disabled', true);
        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/TABL"
            }).done(function(data){
                $('#rete_autoaviso_option').html(data);
                $('#rete_autoaviso_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_autoaviso_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_autoaviso_option option:selected').data('apply');
            $('#rete_autoaviso_option').select2('val', '').attr('disabled', true).select();
            $('#rete_autoaviso_tax').val('');
            $('#rete_autoaviso_valor').val('');
            setReteTotalAmount(rete_ica_amount, '-');
            resetBase(apply);
        }
    });

    var rete_fuente_amount = 0;
    var rete_ica_amount = 0;
    var rete_iva_amount = 0;
    var rete_otros_amount = 0;

    $(document).on('change', '#rete_fuente_option, #rete_iva_option, #rete_ica_option, #rete_otros_option, #rete_bomberil_option, #rete_autoaviso_option', function(){
        rete_sale_option_changed($(this).prop('id'));
    });

    function getReteAmount(apply){

        amount = 0;

        if (apply == "ST") {
            amount = $('#rete_base_subtotal').val();
        } else if (apply == "TX") {
            amount = $('#rete_base_tax').val();
        } else if (apply == "TO") {
            amount = $('#rete_base_total').val();
        } else if (apply == "IC") {
            amount = $('#rete_ica_valor').val();
        }


        console.log(" >>>> ");
        console.log(apply);
        console.log(amount);


        return amount;
    }

    function setReteTotalAmount(amount, action){

        var sales = JSON.parse(localStorage.getItem('sales'));
        var sale = sales[$('#rete_applied').val()];
        rtotal_rete =
                    (formatDecimal($('#rete_fuente_valor').val())) +
                    (formatDecimal($('#rete_iva_valor').val())) +
                    (formatDecimal($('#rete_ica_valor').val())) +
                    (formatDecimal($('#rete_bomberil_valor').val())) +
                    (formatDecimal($('#rete_autoaviso_valor').val())) +
                    (formatDecimal($('#rete_otros_valor').val()));
        $('#total_rete_amount').text(formatMoney(rtotal_rete));
        // if (formatDecimal(rtotal_rete) > formatDecimal(sale.amount_to_paid)) {
        //     $('.advise_invalid_retention').css('display', '');
        //     $('#updateOrderRete').prop('disabled', true);
        // } else {
            $('.advise_invalid_retention').css('display', 'none');
            $('#updateOrderRete').prop('disabled', false);
        // }
    }

    function resetBase(apply){
        if (apply == "ST") {
            $('#rete_base_subtotal').val(0).prop('readonly', true);
        } else if (apply == "TX") {
            $('#rete_base_tax').val(0).prop('readonly', true);
        } else if (apply == "TO") {

        }
        total = formatDecimal($('#rete_base_subtotal').val()) + formatDecimal($('#rete_base_tax').val());
    }

    function setMaxBaseAllowed(apply, rete, item_id, manual_base = 0){
        var sales = JSON.parse(localStorage.getItem('sales'));
        var sale = sales[item_id];
        var base_paid = 0;
        var total = 0;
        if (rete == 'fuente') {
            base_paid = sale.rete_fuente_base_paid;
        } else if (rete == 'iva') {
            base_paid = sale.rete_iva_base_paid;
        } else if (rete == 'ica') {
            base_paid = sale.rete_ica_base_paid;
        } else if (rete == 'other') {
            base_paid = sale.rete_other_base_paid;
        }

        if (base_paid > 0) {
            command: toastr.warning('La compra ya tiene retenciones aplicadas, por favor verifique', 'Retenciones ya aplicadas', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
        }
        if (apply == "ST") {
            max_val = sale.total - base_paid;
            if (site.settings.aiu_management == '1') {
                max_val += (parseFloat(sale.aiu_admin_total) + parseFloat(sale.aiu_imprev_total) + parseFloat(sale.aiu_utilidad_total));
            }
            $('#rete_base_subtotal').prop('max', max_val).prop('readonly', false);
            if ($('#rete_base_subtotal').val() == 0 && manual_base == 0) {
                $('#rete_base_subtotal').val(max_val);
            }
            if (max_val == 0) {
                base_subtotal = 0;
            } else if ( (formatDecimal(sale.amount_to_paid) + formatDecimal(sale.paid)) == formatDecimal(sale.grand_total)) {
                base_subtotal = max_val;
            } else {
                base_subtotal = ((sale.amount_to_paid / sale.grand_total) * sale.total);
            }
            $('#suggested_subtotal').html(formatMoney(base_subtotal));
        } else if (apply == "TX") {
            max_val = sale.total_tax - base_paid;
            $('#rete_base_tax').prop('max', max_val).prop('readonly', false);
            if ($('#rete_base_tax').val() == 0 && manual_base == 0) {
                $('#rete_base_tax').val(max_val);
            }
            if (max_val == 0) {
                base_iva = 0;
            } else if (formatDecimal(sale.amount_to_paid + sale.paid) == formatDecimal(sale.grand_total)) {
                base_iva = max_val;
            } else {
                base_iva = ((sale.amount_to_paid / sale.grand_total) * sale.total_tax);
            }
            $('#suggested_tax').html(formatMoney(base_iva));
        } else if (apply == "TO") {
            if ($('#rete_base_subtotal').prop('readonly')) {
                max_val = sale.total - base_paid;
                $('#rete_base_subtotal').prop('max', max_val).prop('readonly', false);
                if ($('#rete_base_subtotal').val() == 0 && manual_base == 0) {
                    $('#rete_base_subtotal').val(max_val);
                }
                if (max_val == 0) {
                    base_subtotal = 0;
                } else if ( (formatDecimal(sale.amount_to_paid) + formatDecimal(sale.paid)) == formatDecimal(sale.grand_total)) {
                    base_subtotal = max_val;
                } else {
                    base_subtotal = ((sale.amount_to_paid / sale.grand_total) * sale.total);
                }
                $('#suggested_subtotal').html(formatMoney(base_subtotal));
            }

            if ($('#rete_base_subtotal').prop('readonly')) {
                max_val = sale.total_tax - base_paid;
                $('#rete_base_tax').prop('max', max_val).prop('readonly', false);
                if ($('#rete_base_tax').val() == 0 && manual_base == 0) {
                    $('#rete_base_tax').val(max_val);
                }
                if (max_val == 0) {
                    base_iva = 0;
                } else if (formatDecimal(sale.amount_to_paid + sale.paid) == formatDecimal(sale.grand_total)) {
                    base_iva = max_val;
                } else {
                    base_iva = ((sale.amount_to_paid / sale.grand_total) * sale.total_tax);
                }
                $('#suggested_tax').html(formatMoney(base_iva));
            }
            base_total = formatDecimal($('#suggested_subtotal').html()) + formatDecimal($('#suggested_tax').html());
            $('#suggested_total').html(formatMoney(base_total));
        }
        total = formatDecimal($('#rete_base_subtotal').val()) + formatDecimal($('#rete_base_tax').val());
        //console.log("Apply : "+ apply + " total : "+total+" Max total : "+max_val);
        $('#rete_base_total').val(total);
    }

    $(document).on('keyup', '#rete_base_subtotal, #rete_base_tax, #rete_base_total', function(){
        var sales = JSON.parse(localStorage.getItem('sales'));
        var sale = sales[$('#rete_applied').val()];
        var rete_base = $(this);
        var val = formatDecimal(rete_base.val());
        var max = formatDecimal(rete_base.prop('max'));
        var min = formatDecimal(rete_base.prop('min') ? rete_base.prop('min') : 0);
        if (sale.amount_to_paid == sale.grand_total) {
            rete_base.val(max);
            Command: toastr.error('No es posible modificar la base por que se está pagando el total de la factura', 'Error', {onHidden : function(){}})
        } else {
            if (val < min || val > max) {
                rete_base.val(max);
            }
            debbug_retenciones();
        }
    });

    $(document).on('focus', '#rete_base_subtotal, #rete_base_tax, #rete_base_total', function(){

        $(this).select();

    });

    $(document).on('click', '#updateOrderRete', function () {

        $('#ajaxCall').fadeIn();
        itemid = $('#rete_applied').val();
        sales = JSON.parse(localStorage.getItem('sales'));
        sales[itemid].rete_fuente_percentage = $('#rete_fuente_tax').val();
        sales[itemid].rete_ica_percentage = $('#rete_ica_tax').val();
        sales[itemid].rete_bomberil_percentage = $('#rete_bomberil_tax').val();
        sales[itemid].rete_autoaviso_percentage = $('#rete_autoaviso_tax').val();
        sales[itemid].rete_iva_percentage = $('#rete_iva_tax').val();
        sales[itemid].rete_other_percentage = $('#rete_otros_tax').val();

        sales[itemid].rete_fuente_base = $('#rete_fuente_base').val();
        sales[itemid].rete_ica_base = $('#rete_ica_base').val();
        sales[itemid].rete_bomberil_base = $('#rete_bomberil_base').val();
        sales[itemid].rete_autoaviso_base = $('#rete_autoaviso_base').val();
        sales[itemid].rete_iva_base = $('#rete_iva_base').val();
        sales[itemid].rete_other_base = $('#rete_otros_base').val();

        sales[itemid].rete_fuente_total = $('#rete_fuente_valor').val();
        sales[itemid].rete_ica_total = $('#rete_ica_valor').val();
        sales[itemid].rete_bomberil_total = $('#rete_bomberil_valor').val();
        sales[itemid].rete_autoaviso_total = $('#rete_autoaviso_valor').val();
        sales[itemid].rete_iva_total = $('#rete_iva_valor').val();
        sales[itemid].rete_other_total = $('#rete_otros_valor').val();

        sales[itemid].rete_fuente_account = $('#rete_fuente_account').val();
        sales[itemid].rete_ica_account = $('#rete_ica_account').val();
        sales[itemid].rete_bomberil_account = $('#rete_bomberil_account').val();
        sales[itemid].rete_autoaviso_account = $('#rete_autoaviso_account').val();
        sales[itemid].rete_iva_account = $('#rete_iva_account').val();
        sales[itemid].rete_other_account = $('#rete_otros_account').val();

        sales[itemid].fuente_option = $('#rete_fuente_option option:selected').data('percentage');
        sales[itemid].iva_option = $('#rete_iva_option option:selected').data('percentage');
        sales[itemid].ica_option = $('#rete_ica_option option:selected').data('percentage');
        sales[itemid].bomberil_option = $('#rete_bomberil_option option:selected').data('percentage');
        sales[itemid].autoaviso_option = $('#rete_autoaviso_option option:selected').data('percentage');
        sales[itemid].otros_option = $('#rete_otros_option option:selected').data('percentage');
        sales[itemid].id_rete_fuente = $('#rete_fuente_option').val();
        sales[itemid].id_rete_iva = $('#rete_iva_option').val();
        sales[itemid].id_rete_ica = $('#rete_ica_option').val();
        sales[itemid].id_rete_bomberil = $('#rete_bomberil_option').val();
        sales[itemid].id_rete_autoaviso = $('#rete_autoaviso_option').val();
        sales[itemid].id_rete_otros = $('#rete_otros_option').val();
        sales[itemid].total_rete_amount = $('#total_rete_amount').text();

        var discount_apply_to = [];
        var discount_amount = [];
        var discount_amount_calculated = [];
        var discount_ledger_id = [];
        $('select.discount_apply_to').each(function(index, select){
            index_d = $(select).data('index');
            d_discount_apply_to = $(select).val();
            d_discount_amount = $('.discount_amount[data-index="'+index_d+'"]').val();
            d_discount_amount_calculated = $('.discount_amount_calculated[data-index="'+index_d+'"]').val();
            d_discount_ledger_id = $(select).find('option:selected').data('ledgerid');
            if (d_discount_amount_calculated > 0) {
                discount_apply_to.push(d_discount_apply_to);
                discount_amount.push(d_discount_amount);
                discount_amount_calculated.push(d_discount_amount_calculated);
                discount_ledger_id.push(d_discount_ledger_id);
            }
        });

        if (discount_apply_to.length > 0 || discount_amount.length > 0 || discount_amount_calculated.length > 0 || discount_ledger_id.length > 0 ) {
            sales[itemid].discount = {discount_apply_to, discount_amount, discount_amount_calculated, discount_ledger_id};
        } else {
            sales[itemid].discount = false;
        }

        localStorage.removeItem('sales');
        localStorage.setItem('sales', JSON.stringify(sales));

        loadItems();

        reset_retention_window();

        $('#rtModal').modal('hide');
        $('#ajaxCall').fadeOut();
    });

     $(document).on('click', '#cancelOrderRete', function(){
        // localStorage.removeItem('retenciones');
        // loadItems();
        reset_retention_window();
        $('#updateOrderRete').trigger('click');
     });

    function recalcular_retenciones(retenciones){

        if (retenciones != null) {
            if (retenciones.id_rete_fuente > 0) {
                if (!$('#rete_fuente').is(':checked')) {
                    $('#rete_fuente').attr('data-selected', retenciones.id_rete_fuente).trigger('click');
                }
            }
            if (retenciones.id_rete_iva > 0) {
                if (!$('#rete_iva').is(':checked')) {
                    $('#rete_iva').attr('data-selected', retenciones.id_rete_iva).trigger('click');
                }
            }
            if (retenciones.id_rete_ica > 0) {
                if (!$('#rete_ica').is(':checked')) {
                    $('#rete_ica').attr('data-selected', retenciones.id_rete_ica).trigger('click');
                }
            }
            if (retenciones.id_rete_otros > 0) {
                if (!$('#rete_otros').is(':checked')) {
                    $('#rete_otros').attr('data-selected', retenciones.id_rete_otros).trigger('click');
                }
            }
            setTimeout(function() {
                $('#rete_autoaviso').attr('disabled', false);
                $('#rete_bomberil').attr('disabled', false);
                if (retenciones.id_rete_bomberil > 0) {
                    if (!$('#rete_bomberil').is(':checked')) {
                        $('#rete_bomberil').attr('data-selected', retenciones.id_rete_bomberil).trigger('click');
                    }
                }
                if (retenciones.id_rete_autoaviso > 0) {
                    if (!$('#rete_autoaviso').is(':checked')) {
                        $('#rete_autoaviso').attr('data-selected', retenciones.id_rete_autoaviso).trigger('click');
                    }
                }
            }, 1500);
        }
    }


    function debbug_retenciones(){

        if ($('#rete_fuente').is(':checked')) {
            $('#rete_fuente_option').trigger('change');
        }

        if ($('#rete_iva').is(':checked')) {
            $('#rete_iva_option').trigger('change');
        }

        if ($('#rete_ica').is(':checked')) {
            $('#rete_ica_option').trigger('change');
        }

        if ($('#rete_otros').is(':checked')) {
            $('#rete_otros_option').trigger('change');
        }

    }

    function reiniciar_retencion(row_id, by_checkbox = false){

        var sales = JSON.parse(localStorage.getItem('sales'));

        delete sales[row_id].total_rete_amount;

        delete sales[row_id].fuente_option;
        delete sales[row_id].iva_option;
        delete sales[row_id].ica_option;
        delete sales[row_id].otros_option;
        delete sales[row_id].id_rete_fuente;
        delete sales[row_id].id_rete_iva;
        delete sales[row_id].id_rete_ica;
        delete sales[row_id].id_rete_otros;

        sales[row_id].rete_fuente_percentage = 0;
        sales[row_id].rete_ica_percentage = 0;
        sales[row_id].rete_iva_percentage = 0;
        sales[row_id].rete_other_percentage = 0;

        sales[row_id].rete_fuente_base = 0;
        sales[row_id].rete_ica_base = 0;
        sales[row_id].rete_iva_base = 0;
        sales[row_id].rete_other_base = 0;

        sales[row_id].rete_fuente_total = 0;
        sales[row_id].rete_ica_total = 0;
        sales[row_id].rete_iva_total = 0;
        sales[row_id].rete_other_total = 0;

        sales[row_id].rete_fuente_account = 0;
        sales[row_id].rete_ica_account = 0;
        sales[row_id].rete_iva_account = 0;
        sales[row_id].rete_other_account = 0;
        delete sales[row_id].discount;
        if (by_checkbox) {
            delete sales[row_id].amount_to_paid_manually_changed;
        }
        if (sales[row_id].amount_to_paid_manually_changed === undefined) {
            sales[row_id].amount_to_paid = sales[row_id].grand_total - sales[row_id].paid;
        }
        $('#cancelOrderRete').trigger('click');
        localStorage.removeItem('sales');
        localStorage.setItem('sales', JSON.stringify(sales));
        loadItems();
    }

$(document).on('click', '.add_concept', function(){
    $.ajax({
        url : site.base_url+"payments/addConcept",
        type : 'get',
        data : {
            ptype : 1
        }
    }).done(function(data){
        $(data).appendTo('#table_concepts tbody');
        $('.select2').select2('destroy');
        $('.select2').select2();
        actualizarConceptos();
    })
});

$(document).on('click', '.delete_row', function(){
    tr = $(this).closest('tr');
    tr.remove();
    actualizarConceptos();
});

$(document).on('change', '.type_mov, .concepto_amount', function(){
    var val = $(this).val();
    if (val < 0) {
        $(this).val(0);
    }
    actualizarConceptos();
});

$(document).on('keyup', '.concepto_amount', function(){
    var val = $(this).val();

    this.value = this.value.replace(/[^0-9],[^0-9]/g,'');

    if (val < 0) {
        $(this).val(0);
    }

});

function actualizarConceptos(){

    localStorage.removeItem('total_conceptos');

    var total = 0;
    $('select[name="type_mov[]"]').each(function(index){
        var type = $(this);
        var amount = $('input[name="concepto_amount[]"]').eq(index);

        // console.log('Type : '+type.val()+', amount : '+amount.val());

        if (type.val() == "+") {
            total += formatDecimal(amount.val());
        } else {
            total -= formatDecimal(amount.val());
        }

    });
    localStorage.setItem('total_conceptos', total);
    loadItems();
}

$(document).on('click', '#add_payment', function(){

    var num_checkeds = 0;
    $('.enable_payment').each(function(index, check){
        if($(check).is(':checked')){
            num_checkeds++;
        }
    });

    if (num_checkeds == 0) {
        bootbox.confirm({
            message: "Aún no ha seleccionado ninguna factura, por lo que este recibo de caja no afectará cartera",
            buttons: {
                confirm: {
                    label: 'Continuar',
                    className: 'btn-success send-submit-sale btn-full'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-danger btn-full'
                }
            },
            callback: function (result) {
                if (result) {
                    var submit = true;

                    if ($('select[name="type_mov[]"]').length > 0) {

                        $('select[name="type_mov[]"]').each(function(index){

                            type_mov = $(this);

                            if (type_mov.val() == '') {
                                type_mov.select2('open');
                                submit = false;
                                return false;
                            }

                            description = $('input[name="concepto_description[]"]').eq(index);

                            if (description.val() == '') {
                                description.focus();
                                submit = false;
                                return false;
                            }

                            amount = $('input[name="concepto_amount[]"]').eq(index);

                            if (amount.val() == '') {
                                amount.focus();
                                submit = false;
                                return false;
                            }

                            if (site.settings.modulary == 1) {
                                ledger = $('select[name="ledger[]"]').eq(index);
                                if (ledger.val() == '') {
                                    ledger.select2('open');
                                    submit = false;
                                    return false;
                                }
                            }

                            if (site.settings.cost_center_selection == 0) {
                                biller_concepto = $('select[name="biller_concepto[]"]').eq(index);
                                if (biller_concepto.val() == '') {
                                    biller_concepto.select2('open');
                                    submit = false;
                                    return false;
                                }
                            } else if (site.settings.cost_center_selection == 1) {
                                cost_center_id_concepto = $('select[name="cost_center_id_concepto[]"]').eq(index);
                                if (cost_center_id_concepto.val() == '') {
                                    cost_center_id_concepto.select2('open');
                                    submit = false;
                                    return false;
                                }
                            }

                        });

                    }

                    if (submit && site.settings.cost_center_selection == 1) {
                        if ($('#cost_center_id').val() == '') {
                            $('#cost_center_id').select2('open');
                            submit = false;
                        }
                    }

                    if (!$('#add_multi_payment_sales').valid()) {
                        submit = false;
                    }
                    if (submit) {
                        $('#add_multi_payment_sales').submit();
                        $('#add_payment').prop('disabled', true);
                        $('#reset').prop('disabled', true);
                        remove_localstorage_data();
                        setTimeout(function() {
                            location.href = site.base_url+'payments/index';
                        }, 1300);
                    }
                }
            }
        });
    } else {
        var submit = true;

        if ($('select[name="type_mov[]"]').length > 0) {

            $('select[name="type_mov[]"]').each(function(index){

                type_mov = $(this);

                if (type_mov.val() == '') {
                    type_mov.select2('open');
                    submit = false;
                    return false;
                }

                description = $('input[name="concepto_description[]"]').eq(index);

                if (description.val() == '') {
                    description.focus();
                    submit = false;
                    return false;
                }

                amount = $('input[name="concepto_amount[]"]').eq(index);

                if (amount.val() == '') {
                    amount.focus();
                    submit = false;
                    return false;
                }

                if (site.settings.modulary == 1) {
                    ledger = $('select[name="ledger[]"]').eq(index);
                    if (ledger.val() == '') {
                        ledger.select2('open');
                        submit = false;
                        return false;
                    }
                }

                if (site.settings.cost_center_selection == 0) {
                    biller_concepto = $('select[name="biller_concepto[]"]').eq(index);
                    if (biller_concepto.val() == '') {
                        biller_concepto.select2('open');
                        submit = false;
                        return false;
                    }
                } else if (site.settings.cost_center_selection == 1) {
                    cost_center_id_concepto = $('select[name="cost_center_id_concepto[]"]').eq(index);
                    if (cost_center_id_concepto.val() == '') {
                        cost_center_id_concepto.select2('open');
                        submit = false;
                        return false;
                    }
                }

            });

        }

        if (submit && site.settings.cost_center_selection == 1) {
            if ($('#cost_center_id').val() == '') {
                $('#cost_center_id').select2('open');
                submit = false;
            }
        }

        if (!$('#add_multi_payment_sales').valid()) {
            submit = false;
        }
        if (submit) {
            $('#add_multi_payment_sales').submit();
            $('#add_payment').prop('disabled', true);
            $('#reset').prop('disabled', true);
            remove_localstorage_data();
            setTimeout(function() {
                location.href = site.base_url+'payments/index';
            }, 1300);
        }
    }

});


$(document).on('click', '#debug', function(){
    var sales = JSON.parse(localStorage.getItem('sales'));
    var sales_debugged = [];
    var selected = 0;
    $.each(sales, function(index){
        if (this.sale_selected == true) {
            sales_debugged.push(this);
            selected++;
        }
    });
    num_rows = max_num_rows - selected;
    if (num_rows == 0) {
        return false;
    }
    rows_start+= ((max_num_rows + 1) - selected);
    $.ajax({
        url : site.base_url+"sales/sale_suggestions",
        type : "GET",
        dataType: "JSON",
        data : {
            'customer_id' :  $('#customer').val(),
            'address_id' :  $('#address_id').val(),
            'biller_id' :  $('#rcbiller').val(),
            'rows_start' :  rows_start,
            'num_rows' :  num_rows,
        },
    }).done(function(data){
        if (data.response !== undefined && data.response == 0) {
            command: toastr.warning('Ya no se encontraron más facturas pendientes para el cliente', 'Sin resultados', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
        } else {
            $.each(data, function(index){
                sales_debugged.push(this);
            });
            setTimeout(function() {
                console.log(sales_debugged);
                localStorage.removeItem('sales');
                localStorage.setItem('sales', JSON.stringify(sales_debugged));
                loadItems();
            }, 850);
        }

    }).fail(function(){
        loadItems();
    });
});

$(document).on('change', '.discount_apply_to', function(){
    var form_select = $(this);
    var apply_to = form_select.val();
    $('.discount_apply_to').each(function(index, select){
        if (form_select.index('.discount_apply_to') != index) {
            $.each($(select).find('option'), function(index, option){
                if ($(option).val() == apply_to && apply_to != '') {
                    $(option).prop('disabled', true);
                }
            });
        }
    });
});

$(document).on('blur, change', '.discount_amount', function(){
    var input = $(this);
    var sales = JSON.parse(localStorage.getItem('sales'));
    if (input.val() != '') {
        var index = input.data('index');
        var sale_id = $('#rete_applied').val();
        var sale = sales[sale_id];
        var apply_to = $('.discount_apply_to[data-index="'+index+'"]').val();
        var base_discount = getAmountToDiscount(apply_to, sale_id);
        var item_discount = 0;
        var ds = input.val();
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = formatDecimal(parseFloat(((base_discount) * parseFloat(pds[0])) / 100));
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        $('.discount_amount_calculated[data-index="'+index+'"]').val(formatDecimals(item_discount));
        set_total_calculated_discount(sale);
    }
});

function getAmountToDiscount(apply, item_id){
    var sales = JSON.parse(localStorage.getItem('sales'));
    var sale = sales[item_id];
    var base_discount = 0;
    if (apply == "ST") {
        base_discount = parseFloat(sale.total) + parseFloat(sale.total_discount);
    } else if (apply == "TX") {
        // base_discount = ((sale.amount_to_paid / sale.grand_total) * sale.total_tax);
        base_discount = sale.total_tax;
    } else if (apply == "TO") {
        base_discount = sale.grand_total;
    } else if (apply == "PST") {
        base_discount = ((sale.amount_to_paid / sale.grand_total) * sale.total);
    } else if (apply == "PTO") {
        base_discount = ((sale.amount_to_paid / sale.grand_total) * sale.grand_total);
    }
    return base_discount;
}

function recalcular_descuento(discount){
    arr_apply = discount.discount_apply_to;
    arr_amount = discount.discount_amount;
    arr_calculated = discount.discount_amount_calculated;
    $.each(arr_apply, function(index, val){
        index_d = index+1;
        apply_to = val;
        amount = arr_amount[index];
        $('.discount_apply_to[data-index="'+index_d+'"]').select2('val', apply_to).trigger('change');
        $('.discount_amount[data-index="'+index_d+'"]').val(amount).trigger('change');
    });
}

function reset_retention_window(){
    $('#rete_fuente').prop('checked', true).trigger('click');
    $('#rete_iva').prop('checked', true).trigger('click');
    $('#rete_ica').prop('checked', true).trigger('click');
    $('#rete_otros').prop('checked', true).trigger('click');

    $('.discount_apply_to').each(function(index, select){
        $(select).find('option').prop('disabled', false);
    });
    $('.discount_apply_to').select2('val', '').trigger('change');
    $('.discount_amount').val(0).trigger('change');
    $('#total_discount').val(0);
    $('#updateOrderRete').prop('disabled', false);
}

function set_total_calculated_discount(sale){
    var total = 0;
    $('#updateOrderRete').prop('disabled', true);
    $('.discount_amount_calculated').each(function(index, input){
        if ($(input).val() > 0) {
            total += parseFloat($(input).val());
        }
    });
        if (total > 0) {
            // if (total <= sale.amount_to_paid) {
                $('#total_discount').val(formatDecimals(total));
                $('#updateOrderRete').prop('disabled', false);
            // } else {
            //     command: toastr.warning('El total de los descuentos es mayor al valor pagado', 'Inconsistencia en descuentos', {
            //         "showDuration": "500",
            //         "hideDuration": "1000",
            //         "timeOut": "6000",
            //         "extendedTimeOut": "1000",
            //     });
            // }
        }
}


$('#payment_date').on('change', function(){
    if ($(this).val() != '') {
        localStorage.setItem('payment_date', $(this).val());
    }
});

$('#consecutive_payment').on('change', function(){
    if ($(this).val() != '') {
        localStorage.setItem('consecutive_payment', $(this).val());
    }
});


$(document).on('change', '#rcbiller', function(){
    load_biller_data();
});

function load_biller_data(changed_biller = true){
    localStorage.setItem('rcbiller', $('#rcbiller').val());
  $.ajax({
    url: site.base_url+'billers/getBillersDocumentTypes/14/'+$('#rcbiller').val(),
    type:'get',
    dataType:'JSON'
  }).done(function(data){
    response = data;
    $('#document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
    if (response.status == 0) {
      $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
    }
    $('#document_type_id').trigger('change');
  });
   $.ajax({
    url: site.base_url+'billers/getBillersDocumentTypes/30/'+$('#rcbiller').val(),
    type:'get',
    dataType:'JSON'
  }).done(function(data){
    response = data;
    $('#deposit_document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
    if (response.status == 0) {
      $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
    }
    $('#deposit_document_type_id').trigger('change');
  });
  if ($('#customer').val() && sale_id == false && changed_biller == true) {
    $('#customer').trigger('change');
  }
}

$(document).on('change', '#address_id', function(){
    localStorage.setItem('address_id', $(this).val());
    $('#customer').trigger('change');
});

// $(document).on('change')

$(document).on('ifChecked', '.set_deposit_on', function(){
    $('.div_deposit_data').fadeIn();
});

$(document).on('ifUnchecked', '.set_deposit_on', function(){
    $('.div_deposit_data').fadeOut();
});

$(document).on('change', '#paid_by_1', function(){
    payment = $(this).closest('.well');
    paid_by = $(this).val();
    amount_to_pay = parseFloat(formatDecimal($('#amount_1').val()));
    customer = $('#customer').val();
    if (paid_by == "deposit") {
        $.ajax({
            url:  site.base_url+"customers/deposit_balance/"+customer+"/"+amount_to_pay,
        }).done(function(data) {
            payment.find('.deposit_message').remove();
            payment.append(data);
        });
    }
});

$(document).on('change', '#manual_reference', function(){
    manual_reference = $(this).val();
    prefix = $('#document_type_id option:selected').text();
    $.ajax({
        url : site.base_url+"payments/validate_payment_reference",
        type : "get",
        data : {
            "prefix" : prefix,
            "consecutive" : manual_reference,
        }
    }).done(function(data){
        if (data == 1) {
            command: toastr.error('El consecutivo '+prefix+'-'+manual_reference+' ya está registrado', 'Consecutivo duplicado', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
            $('#manual_reference').val('').focus();
        }
    });
});

function reset_rete_bomberil(){
    $('#rete_bomberil').prop('disabled', true).prop('checked', false).trigger('click');
    $('#rete_autoaviso').prop('disabled', true).prop('checked', false).trigger('click');
    $('#rete_bomberil_option').select2('val', '').prop('disabled', true).trigger('change');
    $('#rete_autoaviso_option').select2('val', '').prop('disabled', true).trigger('change');
    $('#rete_bomberil_tax').val('');
    $('#rete_bomberil_valor').val('');
    $('#rete_autoaviso_tax').val('');
    $('#rete_autoaviso_valor').val('');
}

$(document).on('change', '#date', function(){
    min_date = $(this).prop('min');
    sel_date = moment($(this).val(),'DD/MM/YYYY HH:mm:ss a').format('YYYY-MM-DD');
    if (sel_date < min_date) {
        command: toastr.error('La fecha seleccionada para el Recibo, no puede ser menor a la de las facturas a pagar', 'Fecha RC incorrecta', {
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "6000",
            "extendedTimeOut": "1000",
        });
        $("#date").datetimepicker('update', new Date());
    }
});