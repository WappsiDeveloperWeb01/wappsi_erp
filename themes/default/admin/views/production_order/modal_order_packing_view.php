<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <div class="text-center" style="margin-bottom:20px;">
                <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>" alt="<?= $Settings->site_name; ?>">
            </div>
            <div class="text-center" style="margin-bottom:20px;">
                <h3><?= lang('packing_order') ?> N° <?= $inv->id ?></h3>
            </div>

            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-xs-12">
                        <b><?= lang("date"); ?></b> : <?= $this->sma->hrld($inv->date); ?><br>
                        <b><?= lang("estimated_date"); ?></b> : <?= $this->sma->hrld($inv->est_date); ?><br>
                        <b>N° <?= lang("making_order"); ?></b> : <?= $inv->production_order_reference_no; ?><br>
                        <b>N° <?= lang("cutting_order"); ?></b> : <?= $inv->cutting_id; ?><br>
                        <b>N° <?= lang("assemble_order"); ?></b> : <?= $inv->assemble_reference_no; ?><br>
                        <b><?= lang("packer"); ?></b> : <?= $inv->packer_name; ?><br>
                    </div>
                    <div class="col-xs-7 text-right order_barcodes">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <label class="table-label"><?= lang("products"); ?> *</label>
                        <table class="table table-bordered table-hover print-table order-table">
                            <thead>
                                <tr>
                                    <th><?= lang("description"); ?></th>
                                    <th><?= lang("variant"); ?></th>
                                    <th><?= lang("quantity"); ?></th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $r = 1;
                                $tTotal = 0;
                                foreach ($por_rows as $row):
                                ?>
                                    <tr>
                                        <td style="vertical-align:middle;">
                                            <?= $row->product_code.' - '.$row->product_name; ?>
                                        </td>
                                        <th><?= $row->variant; ?></th>
                                        <th class="text-right"><?= $this->sma->formatQuantity($row->packed_quantity); $tTotal+= $row->packed_quantity; ?></th>
                                    </tr>
                                    <?php
                                    $r++;
                                endforeach;
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="2" class="text-right"><?= lang('total') ?></th>
                                    <th class="text-right"><?= $this->sma->formatQuantity($tTotal); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="table-responsive">
                        <label class="table-label"><?= lang("components"); ?> *</label>
                        <table class="table table-bordered table-hover print-table order-table">
                            <thead>
                                <tr>
                                    <th><?= lang("description"); ?></th>
                                    <th><?= lang("product"); ?></th>
                                    <th><?= lang("order_quantity"); ?></th>
                                    <th><?= lang("finished_quantity"); ?></th>
                                    <th><?= lang("fault_quantity"); ?></th>
                                    <th><?= lang("in_process_quantity"); ?></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $r = 1;
                                $fqty = 0;
                                $pqty = 0;
                                $ftqty = 0;
                                foreach ($rows as $row):
                                ?>
                                    <tr>
                                        <td style="vertical-align:middle;">
                                            <?= $row->product_code.' - '.$row->product_name; ?>
                                        </td>
                                        <td><?= $row->pfinished_name.' - '.$row->pfinished_code.($row->pfinished_variant ? ' - '.$row->pfinished_variant : ''); ?></td>
                                        <th class="text-right"><?= $this->sma->formatQuantity($row->packing_quantity); ?></th>
                                        <th class="text-right"><?= $this->sma->formatQuantity($row->finished_quantity); $fqty+= $row->finished_quantity; ?></th>
                                        <th class="text-right"><?= $this->sma->formatQuantity($row->fault_quantity); $ftqty+= $row->fault_quantity; ?></th>
                                        <th class="text-right"><?= $this->sma->formatQuantity($row->packing_quantity - $row->finished_quantity - $row->fault_quantity);  $pqty+= ($row->packing_quantity - $row->finished_quantity - $row->fault_quantity);?></th>
                                    </tr>
                                    <?php
                                    $r++;
                                endforeach;
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-right" colspan="3"><?= lang('total') ?></th>
                                    <th class="text-right"><?= $this->sma->formatQuantity($fqty); ?></th>
                                    <th class="text-right"><?= $this->sma->formatQuantity($ftqty); ?></th>
                                    <th class="text-right"><?= $this->sma->formatQuantity($pqty); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
            <?php if ($receipts): ?>
                <div class="row">
                    <div class="col-sm-12">
                        <h3><?= lang('receipts_registers') ?></h3>
                        <table class="table table-bordered table-hover print-table order-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th><?= lang('date') ?></th>
                                    <th><?= lang("component"); ?></th>
                                    <th><?= lang('product') ?></th>
                                    <th><?= lang('created_by') ?></th>
                                    <th><?= lang('ok_quantity') ?></th>
                                    <th><?= lang('fault_quantity') ?></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($receipts as $receipt): ?>
                                    <tr>
                                        <td><?= $receipt->id ?></td>
                                        <td><?= $receipt->date ?></td>
                                        <td><?= "(".$receipt->product_code.") ".$receipt->product_name ?></td>
                                        <td><?= $receipt->pfinished_name.' - '.$receipt->pfinished_code.($receipt->pfinished_variant ? ' - '.$receipt->pfinished_variant : ''); ?></td>
                                        <td><?= $receipt->created_by ?></td>
                                        <td class="text-right"><?= $this->sma->formatQuantity($receipt->ok_quantity) ?></td>
                                        <td class="text-right"><?= $this->sma->formatQuantity($receipt->fault_quantity) ?></td>
                                        <td>
                                            <a class="view_receipt" data-rid="<?= $receipt->id ?>">
                                                <span class="fa fa-file-alt"></span>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif ?>
        </div>
        <div class="modal-footer">
            <div class="row">
                <div class="col-xs-12 text-left">
                    <?php if ($inv->note || $inv->note != "") { ?>
                        <div class="well well-sm">
                            <p class="bold"><?= lang("note"); ?>:</p>
                            <div><?= $this->sma->decode_html($inv->note); ?></div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-xs-12 text-center">
                    <p>
                        <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.view_receipt', function(){
        var rid = $(this).data('rid');
        $('#myModal').modal('hide');
        setTimeout(function() {
            $('#myModal2').modal({remote: site.base_url+'production_order/modal_receipts_view/'+rid+'/<?= POR_PACKING_TYPE ?>/<?= $inv->id ?>'});
            $('#myModal2').modal('show');
        }, 850);
    });
</script>