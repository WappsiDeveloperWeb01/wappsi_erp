<?php

/***********

FORMATO DE DEVOLUCIÓN OTROS CONCEPTOS

************/

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Factura de venta'));
        //izquierda
        $this->RoundedRect(13, 7, 117, 29, 3, '1234', '');

        $imagen = getimagesize(base_url().'assets/uploads/logos/'.$this->logo);

        $ancho = $imagen[0] / 3.77952;
        $alto = $imagen[1] / 3.77952;

        $altura = 14.5;

        $this->Image(base_url().'assets/uploads/logos/'.$this->logo,16,9,25);
        // $this->Image(base_url().'assets/uploads/logos/'.$this->logo,45,8,50);
        $cx = 45;
        $cy = 13;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(72, 5 , $this->sma->utf8Decode(($this->biller->company != "-" ? $this->biller->company : $this->biller->name)),'',1,'L');
        $cy+=5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->biller->vat_no.($this->biller->digito_verificacion != "" ? '-'.$this->biller->digito_verificacion : '').", ".$this->tipo_regimen),'',1,'L');
        if ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer) {
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode(
                                                ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer ? 'Auto retenedor de ' : '').
                                                ($this->Settings->fuente_retainer ? 'Fuente' : '').
                                                ($this->Settings->iva_retainer ? ($this->Settings->fuente_retainer ? ', ' : '').'IVA ' : '').
                                                ($this->Settings->ica_retainer ? ($this->Settings->fuente_retainer || $this->Settings->iva_retainer ? ', ' : '').'ICA ' : '')
                                            ),'',1,'L');
        }
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address." - ".ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 29, 3, '1234', '');

        $cx = 134;

        //derecha
        $cy = 13;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'Devolución de venta'),'',1,'L');
        $cy +=6;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode('N°. '.$this->factura->reference_no),'',1,'L');
        $cy +=10;
        $this->SetFont('Arial','',$this->fuente);
        $this->setXY($cx-2, $cy);
        $this->MultiCell(77, 3 , $this->sma->utf8Decode('Tipo nota : Devolución de productos'),'','C');

        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(13, 38, 118, 5, 3, '1', 'DF');
        $this->RoundedRect(13, 43, 118, 30, 3, '4', '');
        $cx = 13;
        $cy = 38;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode('INFORMACIÓN DEL CLIENTE'),'B',1,'C');
        $cx += 3;
        $cy += 8;
        $this->setXY($cx, $cy);
        // $this->Cell(115, 5 , $this->sma->utf8Decode(mb_strtoupper($this->customer->name != '-' ? $this->customer->name : $this->customer->company)),'',1,'L');
        $customer_name = $this->customer->name != '-' ? $this->customer->name : $this->customer->company;
        if ($this->customer->type_person == 2) {
            $customer_name = $this->customer->first_name.($this->customer->second_name != '' ? ' '.$this->customer->second_name : '').($this->customer->first_lastname != '' ? ' '.$this->customer->first_lastname : '').($this->customer->second_lastname != '' ? ' '.$this->customer->second_lastname : '');
        }
        $this->MultiCell(115,3,$this->sma->utf8Decode(mb_strtoupper($customer_name)), 0, 'L');
        $columns_l = 17;
        $columns_r = 98;
        $cy += 7;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Nit/Cc : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->vat_no.($this->customer->digito_verificacion != '' ? '-'.$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Sucursal : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Dirección : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->direccion_negocio.", ".(ucfirst(mb_strtolower($this->factura->ciudad_negocio)))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Teléfonos : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->phone_negocio),'',0,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Correo : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->email),'',1,'L');

        //derecha


        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);

        $altura = 5;
        $adicional_altura = 2.3;

        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(131, 38, 78, 5, 3, '2', 'DF');
        $this->RoundedRect(131, 43, 78, 30, 3, '3', '');
        $cx = 131;
        $cy = 38;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FECHA'),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('TOTAL'),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente+1.5);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->sma->formatValue($this->value_decimals,($this->factura->grand_total * -1) * $this->trmrate)),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('AFECTA FACTURA'),'LBR',1,'C',1);
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FORMA DE PAGO'),'LBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+".($this->factura->payment_term > 0 ? $this->factura->payment_term : 0)." day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->return_sale_ref),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->sale_payment_method),'B',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(78, $altura , $this->sma->utf8Decode('VENDEDOR'),'TLBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $cy += $altura;
        $this->setXY($cx, $cy);
        if ($this->seller) {
            $this->Cell(78, $altura , $this->sma->utf8Decode(ucwords(mb_strtolower($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'',1,'C');
        } else {
            $this->Cell(78, $altura , $this->sma->utf8Decode('Ventas Varias'),'',1,'C');
        }

        $this->ln();


        $this->SetFont('Arial','B',$this->fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $current_X = $this->getX();
        $current_Y = $this->getY();
        $this->Cell(9.6 , 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(13.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(87.4, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(21.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(21.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(21, 8, $this->sma->utf8Decode(''),'TBLR',1,'C',1);

        $this->setXY($current_X, $current_Y);

        $this->Cell(9.6 , 8, $this->sma->utf8Decode('Ítem'),'',0,'C',0);
        $this->Cell(13.6, 8, $this->sma->utf8Decode('Código'),'',0,'C',0);
        $this->Cell(87.4, 8, $this->sma->utf8Decode('Nombre producto'),'',0,'C',0);
        $this->Cell(21.6, 8, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(lang('iva')),'',0,'C',0);
        $this->Cell(21.6, 8, $this->sma->utf8Decode('Vr IVA Incl.'),'',0,'C',0);
        $this->Cell(10.6, 4, $this->sma->utf8Decode('Cantidad'),'TLR',0,'C',1);
        $this->Cell(21, 8, $this->sma->utf8Decode('Subtotal'),'',1,'C',0);

        $this->setXY($current_X, $current_Y+4);

        $this->Cell(9.6 , 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(13.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(87.4, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(10.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(10.6, 4, $this->sma->utf8Decode('Dev.'),'B',0,'C',0);
        $this->Cell(21, 4, $this->sma->utf8Decode(''),'',1,'C',0);
    }

    function Footer()
    {
        // Print centered page number
        $this->RoundedRect(13, 245, 196, 20.5, 3, '1234', '');
        $this->SetXY(13, -33);

        if (isset($this->cost_center) && $this->cost_center) {
            $this->MultiCell(196,3,$this->sma->utf8Decode($this->reduceTextToDescription2( "Centro de costo : ".$this->cost_center->name." (".$this->cost_center->code.") \n".$this->description2)), 0, 'L');
        } else {
            $this->MultiCell(196,3,$this->sma->utf8Decode($this->reduceTextToDescription2($this->description2)), 0, 'L');
        }
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');
    }

    function reduceTextToDescription1($text){
        $text="Nota : ".strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 325) {
            $text = substr($text, 0, 320);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function reduceTextToDescription2($text){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 805) {
            $text = substr($text, 0, 800);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

$fuente = 6.5;
$adicional_fuente = 3;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();

$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;

$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->show_code = $show_code;
$pdf->sale_payment_method = $sale_payment_method;
$pdf->biller_logo = $biller_logo;
$pdf->customer = $customer;
$pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $this->sma;
$description1 = $inv->note;
$pdf->description2 = $invoice_footer ? $invoice_footer : "";
$pdf->view_tax = $view_tax;
$pdf->tax_inc = $tax_inc;
$pdf->Settings = $this->Settings;
$pdf->document_type = $document_type;
$pdf->trmrate = $trmrate;
$pdf->value_decimals = $value_decimals;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}

$pdf->tipo_regimen = $tipo_regimen;
$pdf->AddPage();

$maximo_footer = 235;

$plus_font_size = $product_detail_font_size != 0 ? $product_detail_font_size : 0;
$pdf->SetFont('Arial','',$fuente+$plus_font_size);

$taxes = [];
$total_bruto = 0;
$references = [];
$totalQuantity = 0;
    // exit(var_dump($rows));
    foreach ($rows as $item) {
        $totalQuantity += $item->quantity;
        $total_bruto += (($item->net_unit_price + (($item->item_discount * -1) / $item->quantity)) * $item->quantity) * $trmrate;

        if (!isset($taxes[$item->tax_rate_id])) {
            $taxes[$item->tax_rate_id]['tax'] = $item->item_tax * -1;
            $taxes[$item->tax_rate_id]['base'] = ($item->net_unit_price * $item->quantity) * -1;
        } else {
            $taxes[$item->tax_rate_id]['tax'] += $item->item_tax * -1;
            $taxes[$item->tax_rate_id]['base'] += ($item->net_unit_price * $item->quantity) * -1;
        }

        if (isset($references[$item->id])) {
            $references[$item->id]['pr_subtotal'] += ($item->subtotal * -1) * $trmrate;
            $references[$item->id]['pr_quantity'] += ($item->quantity * -1);
        } else {
            $references[$item->id]['pr_code'] = ($show_code == 1 ? $item->product_code : $item->reference);
            $pr_name = $pdf->sma->reduce_text_length(
                                                $item->product_name.
                                                (!is_null($item->variant) && ($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) ? "( ".$item->variant." )" : '').
                                                ($pdf->Settings->show_brand_in_product_search ? " - ".$item->brand_name : '').
                                                (!is_null($item->serial_no) ? " - ".$item->serial_no : '')
                                            , 600);
            $references[$item->id]['pr_name'] = $pr_name;
            $references[$item->id]['pr_discount'] = $item->discount;
            $references[$item->id]['pr_individual_discount'] = ($item->item_discount) / $item->quantity;
            $references[$item->id]['pr_tax'] = $item->tax != '' ? $item->tax : 0;
            $references[$item->id]['pr_individual_tax'] = $item->item_tax / $item->quantity;
            $references[$item->id]['unit_price_1'] = (($item->net_unit_price + (($item->item_discount) / $item->quantity))) * $trmrate;
            $references[$item->id]['unit_price_2'] = ($item->net_unit_price) * $trmrate;
            $references[$item->id]['unit_price_3'] = ($item->unit_price) * $trmrate;
            $references[$item->id]['pr_subtotal'] = ($item->subtotal * -1) * $trmrate;
            $references[$item->id]['pr_quantity'] = ($item->quantity * -1);
        }
    }

    // $this->sma->print_arrays($references);

    $cnt = 1;
    foreach ($references as $reference => $data) {
        if ($pdf->getY() > $maximo_footer) {
            $pdf->AddPage();
        }

            $inicio_fila_X = $pdf->getX();
            $inicio_fila_Y = $pdf->getY();
        $columns[1]['inicio_X'] = $pdf->getX();
        $pdf->Cell( 9.6, 5, $this->sma->utf8Decode($cnt),'',0,'C',0);
        $columns[1]['fin_X'] = $pdf->getX();
        $columns[2]['inicio_X'] = $pdf->getX();
        $pdf->Cell(13.6, 5, $this->sma->utf8Decode($data['pr_code']),'',0,'C',0);
        $columns[2]['fin_X'] = $pdf->getX();
            $cX = $pdf->getX();
            $cY = $pdf->getY();
            $inicio_altura_fila = $cY;
        $columns[3]['inicio_X'] = $pdf->getX();
        $pdf->MultiCell(87.4,5, $this->sma->utf8Decode($data['pr_name']),'','L');
            $fin_altura_fila = $pdf->getY();
            $pdf->setXY($cX+87.4, $cY);
        $columns[3]['fin_X'] = $pdf->getX();
        $columns[4]['inicio_X'] = $pdf->getX();
        $pdf->Cell(21.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,$data['unit_price_1'])),'',0,'C',0);
        $columns[4]['fin_X'] = $pdf->getX();
        $columns[5]['inicio_X'] = $pdf->getX();
        $pdf->Cell(10.6, 5, $this->sma->utf8Decode($data['pr_tax']),'',0,'C',0);
        $columns[5]['fin_X'] = $pdf->getX();
        $columns[6]['inicio_X'] = $pdf->getX();
        $pdf->Cell(21.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,$data['unit_price_3'])),'',0,'C',0);
        $columns[6]['fin_X'] = $pdf->getX();
        $columns[7]['inicio_X'] = $pdf->getX();

        $pdf->Cell(10.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,$data['pr_quantity'])),'',0,'C',0);
        $columns[7]['fin_X'] = $pdf->getX();
        $columns[8]['inicio_X'] = $pdf->getX();

        $pdf->Cell(21, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,$data['pr_subtotal'])),'',0,'C',0);
        $columns[8]['fin_X'] = $pdf->getX();
        $cnt++;
            $fin_fila_X = $pdf->getX();
            $fin_fila_Y = $pdf->getY();
            $ancho_fila = $fin_fila_X - $inicio_fila_X;
            $altura_fila = $fin_altura_fila - $inicio_altura_fila;
            $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
        // $this->sma->print_arrays($columns);
        foreach ($columns as $key => $data) {
            $ancho_column = $data['fin_X'] - $data['inicio_X'];
            $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R');
        }
        $pdf->ln($altura_fila);
    }

if ($inv->order_tax > 0) {
    if (!isset($taxes[$inv->order_tax_id])) {
        $taxes[$inv->order_tax_id]['tax'] = $inv->order_tax * -1;
        $taxes[$inv->order_tax_id]['base'] = $inv->total * -1;
    } else {
        $taxes[$inv->order_tax_id]['tax'] += $inv->order_tax * -1;
        $taxes[$inv->order_tax_id]['base'] += $inv->total * -1;
    }
}



// $pdf->Cell(39.2,5, $this->sma->utf8Decode($pdf->getY()),'BR',1,'R');

if ($pdf->getY() > 185.6) {
    $pdf->AddPage();
}

if ($document_type_invoice_format->totalize_quantity) {
    $pdf->Cell(159.6 ,5, $this->sma->utf8Decode('Total cantidad'),'LBR',0,'R');
    $pdf->Cell(36.4,5, $this->sma->utf8Decode($this->sma->formatQuantity($totalQuantity, $value_decimals)),'LBR',1,'R');
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

//izquierda

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

$pdf->RoundedRect($current_x, $current_y, 115.6, 12.25, 3, '1234', '');

$pdf->ln(1);

$pdf->Cell(1,5, $this->sma->utf8Decode(''),'',0,'L');
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(115.6,5, $this->sma->utf8Decode('VALOR (En letras)'),'',1,'L');
$pdf->SetFont('Arial','',$fuente);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->setX(14);
$pdf->SetFont('Arial','',$fuente);
// $pdf->Cell(115.6,6.125, $this->sma->utf8Decode($number_convert->convertir($inv->grand_total, $currencies_names[$inv->sale_currency])),'',1,'L');
$pdf->MultiCell(115.6, 3, $this->sma->utf8Decode($number_convert->convertir(((($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * -1) * $trmrate), (isset($currencies_names[$inv->sale_currency]) ? $currencies_names[$inv->sale_currency] : $this->Settings->default_currency))), 0, 'L');
$pdf->SetFont('Arial','B',$fuente+1.5);
$pdf->setX(13);

$pdf->setXY($current_x, $current_y+8);


    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $pdf->Cell(28.9,5, $this->sma->utf8Decode('Tipo Impuesto'),'',0,'C');
    $pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Base'),'',0,'C');
    $pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Impuesto'),'',0,'C');
    $pdf->Cell(28.9,5, $this->sma->utf8Decode('Total'),'',1,'C');
    // $pdf->Cell(38.53,5, $this->sma->utf8Decode('Valor Total'),'',1,'C');
$pdf->SetFont('Arial','',$fuente+1.5);
$total = $inv->total * -1;

foreach ($taxes as $tax => $arr) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();

    $total+=$arr['tax'];

    $pdf->Cell(28.9,3, $this->sma->utf8Decode(isset($taxes_details[$tax]) ? $taxes_details[$tax] : "Exento"),'',1,'C');

    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,$arr['base'] * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,$arr['tax'] * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,$total)),'',1,'C');
}

$total+=$inv->order_tax;

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$total+=$arr['tax'];
if ($inv->order_tax > 0) {
    $pdf->Cell(28.9,3, $this->sma->utf8Decode("(SP) ".$this->sma->formatDecimal((isset($taxes_details[$inv->order_tax_id]) ? $taxes_details[$inv->order_tax_id]." %" : "0%"))),'',1,'C');
    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,$inv->total * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,$inv->order_tax * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,$total * $trmrate)),'',1,'C');
}

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

//derecha

$pdf->setXY($cX_items_finished+124, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

// $pdf->cell(33.2, 5, $this->sma->utf8Decode('Subtotal antes de Dcto'), 'BLR', 1, 'L');
// $pdf->setXY($current_x+33.2, $current_y);
// $pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,$total_bruto * -1)), 'BR', 1, 'R');

// $current_y+=5;
// $pdf->setXY($current_x, $current_y);
// $pdf->cell(33.2, 5, $this->sma->utf8Decode('Descuento'), 'TBLR', 1, 'L');
// $pdf->setXY($current_x+33.2, $current_y);
// $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->total_discount > 0 ? "- " : "").$this->sma->formatValue($value_decimals,($inv->total_discount) * $trmrate)), 'TBR', 1, 'R');

// $current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('Subtotal'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,($inv->total * -1) * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('Vr. Impuesto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,($inv->total_tax * -1) * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('Retención en la fuente'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_fuente_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals,$inv->rete_fuente_total * $trmrate)), 'TBR', 1, 'R');

if ($inv->rete_iva_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('Retención al IVA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_iva_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals,$inv->rete_iva_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_ica_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('Retención al ICA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_ica_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals,$inv->rete_ica_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_bomberil_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('Tasa Bomberil'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_bomberil_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals,$inv->rete_bomberil_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_autoaviso_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('Tasa Autoavisos'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_autoaviso_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals,$inv->rete_autoaviso_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_other_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('Otras retenciones'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_other_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals,$inv->rete_other_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->shipping != 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Serv. Transporte'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->shipping * $trmrate)), 'TBR', 1, 'R');
}

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente+1.5);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('TOTAL A ACREDITAR'), 'TBLR', 1, 'R', 1);
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals,(($inv->grand_total + $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_bomberil_total + $inv->rete_autoaviso_total + $inv->rete_other_total) * -1) * $trmrate)), 'TBR', 1, 'R', 1);
$pdf->SetFont('Arial','',$fuente+1.5);


$pdf->Ln(5);


//FIRMAS

//izquierda
$pdf->ln(2);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 196, 11, 3, '1234', '');

$pdf->ln(1);
$current_x = $pdf->getX();
$current_y = $pdf->getY()+11;
$pdf->cell(1,3,"",0,'L');
$pdf->MultiCell(196,3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)),0,'L');

// $pdf->ln();

$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 78.4, 15, 3, '1234', '');

$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Acepto Firma y Sello Recibido'), '', 1, 'L');

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('C.C o Nit'), 'T', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y+10);
$pdf->cell(37, 5, $this->sma->utf8Decode('Fecha'), 'T', 1, 'L');

//derecha
$pdf->setXY($current_x+78.4, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 115.6, 15, 3, '1234', '');
$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Representante Legal'), '', 1, 'L');

if ($signature_root) {
    $pdf->Image($signature_root, $current_x+35,$current_y+1,35);
}

// $descargar = false;

if ($download || $for_email) {
    if ($download) {
        $pdf->Output("factura_venta.pdf", "D");
    } else if ($for_email) {
        $pdf->Output("assets/uploads/". $inv->reference_no .".pdf", "F");
    }
} else {
  $pdf->Output("factura_venta.pdf", "I");
}