<?php defined('BASEPATH') or exit('No direct script access allowed');

class Purchases extends MY_Controller
{
    const PER_OPERATION = 1;
    const WEEKLY_ACCUMULATED = 2;

    public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->load->admin_model('pos_model');
        $this->load->admin_model('quotes_model');
        $this->lang->admin_load('purchases', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('purchases_model');
        $this->load->admin_model("currency_model");
        $this->load->admin_model("products_model");
        $this->load->admin_model('SupportDocument_model');
        $this->load->admin_model('documentReception_model');
        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '2048';
        $this->data['logo'] = true;
    }

    public function index($warehouse_id = null)
    {
        $this->sma->checkPermissions();

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        $this->data['documents_types'] = $this->site->get_multi_module_document_types([5, 6, 21, 22]);
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['users'] = $this->site->get_all_users();

        $this->data['advancedFiltersContainer'] = FALSE;

        if ($this->input->post('warehouse_id') || $this->input->post('payment_status') || $this->input->post('payment_method')
            || $this->input->post('fe_aceptado') || $this->input->post('fe_correo_enviado') || $this->input->post('filter_user')
            || $this->input->post('client')) {
            $this->data['advancedFiltersContainer'] = TRUE;
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('purchases/index', ['page_title' => lang('purchases')], $this->data);
    }

    public function getPurchases($warehouse_id = null)
    {
        $this->sma->checkPermissions('index');

        $dias_vencimiento = $this->input->post('dias_vencimiento');

        if ($this->input->post('start_date')) {
            $start_date = $this->input->post('start_date');
            $start_date = $this->Settings->purchase_datetime_management == 1 ? $this->sma->fld($start_date) : ($start_date . " 00:00:00");
        } else {
            $start_date = NULL;
        }
        if ($this->input->post('end_date')) {
            $end_date = $this->input->post('end_date');
            $end_date = $this->Settings->purchase_datetime_management == 1 ? $this->sma->fld($end_date) : ($end_date . " 23:59:59");
        } else {
            $end_date = NULL;
        }

        $purchase_document_type_id = $this->input->post('purchase_reference_no') ? $this->input->post('purchase_reference_no') : NULL;
        $biller_id = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $supplier = $this->input->post('supplier') ? $this->input->post('supplier') : NULL;
        $payment_status = $this->input->post('payment_status') ? $this->input->post('payment_status') : NULL;
        $status = $this->input->post('status') ? $this->input->post('status') : NULL;
        $purchase_evaluated = $this->input->post('purchase_evaluated');
        if ((!$this->Owner || !$this->Admin) && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }
        $detail_link = anchor('admin/purchases/purchaseView/$1', '<i class="fa fa-file-text-o"></i> ' . lang('purchase_details'));
        $delete_link = "<li>
                            <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='".admin_url('purchases/delete_purchase/$1')."'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>
                                ".lang('delete_purchase')."
                            </a>
                        </li>";
        $payments_link = anchor('admin/purchases/payments/$1', '<i class="fa fa-money"></i> ' . lang('view_payments'), 'data-toggle="modal" data-target="#myModal"');
        $add_payment_link = anchor('admin/payments/padd/$1', '<i class="fa fa-money"></i> ' . lang('add_payment'), '');
        $email_link = anchor('admin/purchases/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_purchase'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('admin/purchases/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_purchase'));
        $pdf_link = anchor('admin/purchases/purchaseView/$1/true', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $print_barcode = anchor('admin/products/print_barcodes/?purchase=$1', '<i class="fa fa-print"></i> ' . lang('print_barcodes'));
        $contabilizar_compra = '<a class="reaccount_purchase_link" data-invoiceid="$1"><i class="fas fa-sync-alt"></i> ' . lang('post_purchase') . ' </a>';
        $cambiar_estado = '<a class="row_status"><i class="fas fa-retweet"></i> ' . lang('change_status') . ' </a>';
        $return_link = anchor('admin/purchases/return_purchase/$1', '<i class="fa fa-angle-double-left"></i> ' . lang('return_purchase'));
        $nota_credito_otros_conceptos = anchor('admin/purchases/return_other_concepts/$1', '<i class="fa fa-angle-double-left"></i>' . lang('return_other_concepts_purcahes'));
        $change_tag_link = anchor('admin/purchases/change_tag/$1', '<i class="fa-solid fa-tag"></i> ' . lang('change_tag'), 'data-toggle="modal" data-target="#myModal"');
        $purchase_evaluation = '<a class="row_evaluation"><i class="fa-regular fa-star-half-stroke"></i> ' . lang('purchase_evaluation') . ' </a>';
        $action = '<div class="text-center"><div class="btn-group text-left">
            <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
            <i class="fas fa-ellipsis-v fa-lg"></i></button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li>' . $detail_link . '</li>
                <li>' . $payments_link . '</li>
                <li>' . $add_payment_link . '</li>
                <li>' . $edit_link . '</li>
                <li>' . $pdf_link . '</li>
                <li>' . $print_barcode . '</li>
                <li>' . $email_link . '</li>
                <li>' . $contabilizar_compra . '</li>
                <li>' . $change_tag_link . '</li>'.
                ($this->Settings->manage_purchases_evaluation == 1 ? '<li>' . $purchase_evaluation . '</li>' : '').
                ($this->Admin || $this->Owner || $this->GP['purchases-delete_purchase'] ? '<li>' . $delete_link . '</li>' : '').
                '<li class="divider"></li>
                <li>' . $return_link . '</li>
                <li>' . $nota_credito_otros_conceptos . '</li>
            </ul>
        </div></div>';
        $this->load->library('datatables');

        $this->datatables->select("
                            purchases.id,
                            purchases.id as purchase_id,
                            DATE_FORMAT(date, '%Y-%m-%d %T') as date,
                            purchase_origin_reference_no,
                            IF(purchase_type = 1, 'Productos', 'Gastos') as tipo,
                            IF({$this->db->dbprefix('tags')}.color IS NOT NULL, CONCAT({$this->db->dbprefix('tags')}.description, '__', {$this->db->dbprefix('tags')}.color, '__', {$this->db->dbprefix('tags')}.type, '__', {$this->db->dbprefix('purchases')}.reference_no), {$this->db->dbprefix('purchases')}.reference_no) as reference_no,
                            " . ($this->Settings->management_consecutive_suppliers == 1 ? "consecutive_supplier, " : "") . "
                            supplier,
                            purchases.status,
                            grand_total,
                            paid,
                            (grand_total-paid) as balance,
                            payment_status,
                            " . ($this->Settings->documents_reception == YES ? "existsInDocumentsReception, " : "") . "
                            " . ($this->Settings->manage_purchases_evaluation == 1 ? "purchase_evaluated, " : "") . "
                            attachment")
                ->from('purchases');

        $this->datatables->join('documents_types', 'documents_types.id = purchases.document_type_id', 'inner');
        $this->datatables->join('tags', 'purchases.tag_id = tags.id', 'left');
        $this->datatables->where('(documents_types.module != 35 AND documents_types.module != 52)');

        if ($warehouse_id) {
            $this->datatables->where('purchases.warehouse_id', $warehouse_id);
        }
        if ($biller_id) {
            $this->datatables->where('purchases.biller_id', $biller_id);
        }
        if ($user_id) {
            $this->datatables->where('purchases.created_by', $user_id);
        }
        if ($purchase_document_type_id) {
            $this->datatables->where('purchases.document_type_id', $purchase_document_type_id);
        }
        if ($dias_vencimiento) {
            $this->datatables->where('purchases.payment_term', $dias_vencimiento);
        }
        if ($start_date) {
            $this->datatables->where('purchases.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('purchases.date <=', $end_date);
        }
        if ($supplier) {
            $this->datatables->where('purchases.supplier_id =', $supplier);
        }
        if ($status) {
            $this->datatables->where('purchases.status =', $status);
        }
        if ($payment_status) {
            $this->datatables->where('purchases.payment_status =', $payment_status);
        }
        if ($purchase_evaluated != '') {
            $this->datatables->where('purchases.purchase_evaluated =', $purchase_evaluated);
        }

        if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) {
            $this->datatables->where('biller_id', $this->session->userdata('biller_id'));
        }

        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Supplier) {
            $this->datatables->where('supplier_id', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", $action, "purchase_id");
        echo $this->datatables->generate();
    }

    public function supporting_document_index($warehouse_id = null)
    {
        if (!$this->enableSupportingDocument) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->sma->checkPermissions('index', null, 'supportingDocument');

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['warehouse_id'] = $warehouse_id;
            $warehouse = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
            $this->data['warehouse'] = $warehouse;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $warehouse = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
            $this->data['warehouse'] = $warehouse;
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['documents_types'] = $this->site->get_multi_module_document_types([35]);
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['users'] = $this->site->get_all_users();

        $this->page_construct('purchases/supporting_document_index', ['page_title' => lang('supporting_documents')], $this->data);
    }

    public function getPurchasesSupportingDocument($warehouse_id = null)
    {
        $this->sma->checkPermissions('index');
        $dias_vencimiento = $this->input->post('dias_vencimiento');
        if ($this->input->post('start_date')) {
            $start_date = $this->input->post('start_date');
            $start_date = $this->Settings->purchase_datetime_management == 1 ? $this->sma->fld($start_date) : ($start_date . " 00:00:00");
        } else {
            $start_date = NULL;
        }
        if ($this->input->post('end_date')) {
            $end_date = $this->input->post('end_date');
            $end_date = $this->Settings->purchase_datetime_management == 1 ? $this->sma->fld($end_date) : ($end_date . " 23:59:59");
        } else {
            $end_date = NULL;
        }
        $purchase_document_type_id = $this->input->post('purchase_reference_no') ? $this->input->post('purchase_reference_no') : NULL;
        $biller_id = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $supplier = $this->input->post('supplier') ? $this->input->post('supplier') : NULL;
        $payment_status = $this->input->post('payment_status') ? $this->input->post('payment_status') : NULL;
        $status = $this->input->post('status') ? $this->input->post('status') : NULL;

        if ((!$this->Owner || !$this->Admin) && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }
        $detail_link = anchor('admin/purchases/purchaseView/$1', '<i class="fa fa-file-text-o"></i> ' . lang('purchase_details'));
        $payments_link = anchor('admin/purchases/payments/$1', '<i class="fa fa-money"></i> ' . lang('view_payments'), 'data-toggle="modal" data-target="#myModal"');
        $add_payment_link = anchor('admin/payments/padd/$1', '<i class="fa fa-money"></i> ' . lang('add_payment'), '');
        $email_link = anchor('admin/purchases/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_purchase'), 'data-toggle="modal" data-target="#myModal"');
        // $edit_link = anchor('admin/purchases/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_purchase'));
        $edit_link = anchor('admin/purchases/edit_support_document/$1', '<i class="fa fa-edit"></i> ' . lang('edit_purchase'));
        $pdf_link = anchor('admin/purchases/purchaseView/$1/true', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $print_barcode = anchor('admin/products/print_barcodes/?purchase=$1', '<i class="fa fa-print"></i> ' . lang('print_barcodes'));
        $contabilizar_compra = '<a class="reaccount_purchase_link" data-invoiceid="$1"><i class="fas fa-sync-alt"></i> ' . lang('post_purchase') . ' </a>';
        $cambiar_estado = '<a class="row_status"><i class="fas fa-retweet"></i> ' . lang('change_status') . ' </a>';
        $return_link = anchor('admin/purchases/return_purchase/$1', '<i class="fa fa-angle-double-left"></i> ' . lang('return_purchase'));
        $nota_credito_otros_conceptos = anchor('admin/purchases/return_other_concepts/$1', '<i class="fa fa-angle-double-left"></i>' . lang('return_other_concepts_purcahes'));

        $action = '<div class="text-center">
                    <div class="btn-group text-left">
                        <button type="button" class="btn btn-default new-button btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>' . $detail_link . '</li>
                            <li>' . $payments_link . '</li>
                            <li>' . $add_payment_link . '</li>
                            <li class="editPurchase">' . $edit_link . '</li>
                            <li>' . $pdf_link . '</li>
                            <li>' . $print_barcode . '</li>
                            <li>' . $email_link . '</li>
                            <li>' . $contabilizar_compra . '</li>
                            <li class="divider"></li>
                            <li class="returnPurchase">' . $return_link . '</li>
                            <li class="returnPurchase">' . $nota_credito_otros_conceptos . '</li>
                        </ul>
                    </div>
                </div>';
        $this->load->library('datatables');
        $this->datatables->select("purchases.id,
            purchases.id as purchase_id,
            DATE_FORMAT(date, '%Y-%m-%d %T') as date,
            purchase_origin_reference_no,
            IF(purchase_type = 1, 'Productos', 'Gastos') as tipo,
            reference_no,
            return_purchase_ref,
            " . ($this->Settings->management_consecutive_suppliers == 1 ? "consecutive_supplier, " : "") . "
            supplier,
            status,
            grand_total,
            paid,
            (grand_total-paid) as balance,
            payment_status,
            attachment,
            acceptedDian,
            documents_types.factura_electronica");
        $this->datatables->from('purchases');

        $this->datatables->join('documents_types', 'documents_types.id = purchases.document_type_id', 'inner');
        $this->datatables->where('(documents_types.module = 35 OR documents_types.module = 52)');

        if ($warehouse_id) {
            $this->datatables->where('purchases.warehouse_id =', $warehouse_id);
        }
        if ($biller_id) {
            $this->datatables->where('purchases.biller_id =', $biller_id);
        }
        if ($user_id) {
            $this->datatables->where('purchases.created_by =', $user_id);
        }
        if ($purchase_document_type_id) {
            $this->datatables->where('purchases.document_type_id =', $purchase_document_type_id);
        }
        if ($dias_vencimiento) {
            $this->datatables->where('purchases.payment_term', $dias_vencimiento);
        }
        if ($start_date) {
            $this->datatables->where('purchases.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('purchases.date <=', $end_date);
        }
        if ($supplier) {
            $this->datatables->where('purchases.supplier_id =', $supplier);
        }
        if ($status) {
            $this->datatables->where('purchases.status =', $status);
        }
        if ($payment_status) {
            $this->datatables->where('purchases.payment_status =', $payment_status);
        }

        if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) {
            $this->datatables->where('biller_id', $this->session->userdata('biller_id'));
        }

        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Supplier) {
            $this->datatables->where('supplier_id', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", $action, "purchase_id");
        echo $this->datatables->generate();
    }

    public function modal_view($purchase_id = null)
    {
        if (!($this->Admin || $this->Owner || $this->GP['purchases-index'] || $this->GP['reports-tax'] || $this->GP['reports-suppliers'])) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 10);</script>");
        }

        if ($this->input->get('id')) {
            $purchase_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->purchases_model->getPurchaseByID($purchase_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['rows'] = $this->purchases_model->getAllPurchaseItems($purchase_id);
        $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['return_purchase'] = $this->purchases_model->getReturnsInvoicesByPurchaseID($purchase_id);
        $this->data['return_rows'] = $this->purchases_model->getAllReturnsItems($purchase_id);
        // $this->sma->print_arrays($this->data['return_rows']);
        $this->data['document_type'] = $this->site->getDocumentTypeById($inv->document_type_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $payments = $this->purchases_model->getPaymentsForPurchase($purchase_id);
        $purchase_main_payment_method = 'credit';
        $purchase_payments_method = '';
        if ($payments) {
            foreach ($payments as $payment) {
                if ($payment->date == $inv->date) {
                    $purchase_main_payment_method = 'counted';
                    $purchase_payment_method = $payment->paid_by;
                }
                $purchase_payments_method .= $payment->name . ", ";
            }

            $purchase_payments_method = trim($purchase_payments_method, ", ");
        }

        $this->data['purchase_main_payment_method'] = $purchase_main_payment_method;
        $this->data['purchase_payments_method'] = $purchase_payments_method;
        $currency = $this->site->getCurrencyByCode($inv->purchase_currency);
        $trmrate = 1;
        if (!empty($inv->purchase_currency) && $inv->purchase_currency != $this->Settings->default_currency) {
            $actual_currency_rate = $currency->rate;
            $trmrate = $actual_currency_rate / $inv->purchase_currency_trm;
        }
        $this->data['trmrate'] = $trmrate;
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->cost_center_id);
        }
        $this->load_view($this->theme . 'purchases/modal_view', $this->data);
    }

    public function view($purchase_id = null)
    {
        $this->sma->checkPermissions('index');

        if ($this->input->get('id')) {
            $purchase_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->purchases_model->getPurchaseByID($purchase_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['rows'] = $this->purchases_model->getAllPurchaseItems($purchase_id);
        $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['payments'] = $this->purchases_model->getPaymentsForPurchase($purchase_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['return_purchase'] = $inv->return_id ? $this->purchases_model->getPurchaseByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->purchases_model->getAllPurchaseItems($inv->return_id) : NULL;
        $this->data['purchase_id'] = $purchase_id;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('purchases'), 'page' => lang('purchases')), array('link' => '#', 'page' => lang('view')));
        $meta = array('page_title' => lang('view_purchase_details'), 'bc' => $bc);
        $this->page_construct('purchases/view', $meta, $this->data);
    }

    public function pdf($purchase_id = null, $view = null, $save_bufffer = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $purchase_id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->purchases_model->getPurchaseByID($purchase_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['rows'] = $this->purchases_model->getAllPurchaseItems($purchase_id);
        $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['inv'] = $inv;
        $this->data['return_purchase'] = $inv->return_id ? $this->purchases_model->getPurchaseByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->purchases_model->getAllPurchaseItems($inv->return_id) : NULL;
        $name = $this->lang->line("purchase") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
        $html = $this->load_view($this->theme . 'purchases/pdf', $this->data, true);
        if (!$this->Settings->barcode_img) {
            $html = preg_replace("'\<\?xml(.*)\?\>'", '', $html);
        }
        if ($view) {
            echo $html;
            die();
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer);
        } else {
            $this->sma->generate_pdf($html, $name);
        }
    }

    public function combine_pdf($purchases_id)
    {
        $this->sma->checkPermissions('pdf');

        foreach ($purchases_id as $purchase_id) {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $inv = $this->purchases_model->getPurchaseByID($purchase_id);
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $this->data['rows'] = $this->purchases_model->getAllPurchaseItems($purchase_id);
            $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);
            $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
            $this->data['created_by'] = $this->site->getUser($inv->created_by);
            $this->data['inv'] = $inv;
            $this->data['return_purchase'] = $inv->return_id ? $this->purchases_model->getPurchaseByID($inv->return_id) : NULL;
            $this->data['return_rows'] = $inv->return_id ? $this->purchases_model->getAllPurchaseItems($inv->return_id) : NULL;
            $inv_html = $this->load_view($this->theme . 'purchases/pdf', $this->data, true);
            if (!$this->Settings->barcode_img) {
                $inv_html = preg_replace("'\<\?xml(.*)\?\>'", '', $inv_html);
            }
            $html[] = array(
                'content' => $inv_html,
                'footer' => '',
            );
        }

        $name = lang("purchases") . ".pdf";
        $this->sma->generate_pdf($html, $name);
    }

    public function email($purchase_id = null)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $purchase_id = $this->input->get('id');
        }
        $inv = $this->purchases_model->getPurchaseByID($purchase_id);
        $this->form_validation->set_rules('to', $this->lang->line("to") . " " . $this->lang->line("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', $this->lang->line("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', $this->lang->line("cc"), 'trim|valid_emails');
        $this->form_validation->set_rules('bcc', $this->lang->line("bcc"), 'trim|valid_emails');
        $this->form_validation->set_rules('note', $this->lang->line("message"), 'trim');

        if ($this->form_validation->run() == true) {
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $to = $this->input->post('to');
            $subject = $this->input->post('subject');
            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = null;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = null;
            }
            $supplier = $this->site->getCompanyByID($inv->supplier_id);
            $this->load->library('parser');
            $parse_data = array(
                'reference_number' => $inv->reference_no,
                'contact_person' => $supplier->name,
                'company' => $supplier->company,
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '" alt="' . $this->Settings->site_name . '"/>',
            );
            $msg = $this->input->post('note');
            $message = $this->parser->parse_string($msg, $parse_data);
            $attachment = $this->pdf($purchase_id, null, 'S');

            try {
                if ($this->sma->send_email($to, $subject, $message, null, null, $attachment, $cc, $bcc)) {
                    delete_files($attachment);
                    $this->db->update('purchases', array('status' => 'ordered'), array('id' => $purchase_id));
                    $this->session->set_flashdata('message', $this->lang->line("email_sent"));
                    admin_redirect("purchases");
                }
            } catch (Exception $e) {
                $this->session->set_flashdata('error', $e->getMessage());
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } elseif ($this->input->post('send_email')) {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            if (file_exists('./themes/' . $this->Settings->theme . '/admin/views/email_templates/purchase.html')) {
                $purchase_temp = file_get_contents('themes/' . $this->Settings->theme . '/admin/views/email_templates/purchase.html');
            } else {
                $purchase_temp = file_get_contents('./themes/default/admin/views/email_templates/purchase.html');
            }
            $this->data['subject'] = array(
                'name' => 'subject',
                'id' => 'subject',
                'type' => 'text',
                'value' => $this->form_validation->set_value('subject', lang('purchase_order') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name),
            );
            $this->data['note'] = array(
                'name' => 'note',
                'id' => 'note',
                'type' => 'text',
                'value' => $this->form_validation->set_value('note', $purchase_temp),
            );
            $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);

            $this->data['id'] = $purchase_id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'purchases/email', $this->data);
        }
    }

    public function add($quote_id = null)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_message('is_natural_no_zero', $this->lang->line("no_zero_required"));
        if ($this->input->post('purchase_type') == 1) {
            $this->form_validation->set_rules('warehouse', $this->lang->line("warehouse"), 'required|is_natural_no_zero');
        }
        $this->form_validation->set_rules('supplier', $this->lang->line("supplier"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        if ($this->Settings->cost_center_selection == 1 && $this->Settings->modulary == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }
        if ($this->Settings->management_consecutive_suppliers == 1) {
            $this->form_validation->set_rules('consecutive_supplier', lang("consecutive_supplier"), 'required');
        }
        if ($this->input->post('payment_status') == 'paid') {
            $this->form_validation->set_rules('payment_reference_no', lang("payment_reference_no"), 'required');
            $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        }
        $this->session->unset_userdata('csrf_token');
        if ($quote = $this->quotes_model->getQuoteByID($quote_id)) {
            if (!$quote->supplier_id > 0) {
                $this->session->set_flashdata('error', lang("invalid_quote"));
                admin_redirect('purchases');
            }
        }
        if ($this->form_validation->run() == true) {
            $support_document = $this->input->post('support_document') ? true : false;
            if ($this->Settings->purchase_datetime_management == 1) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = $this->input->post('date') . " " . date('H:i:s');
            }
            $this->site->validate_movement_date($date);
            $date = $this->site->movement_setted_datetime($date);
            $unit_update_price = $this->input->post('unit_update_price');
            $prorate_shipping_cost = $this->input->post('prorate_shipping_cost');
            $purchase_from_import = $this->input->post('purchase_from_import') ? 1 : 0;
            $tag_id = $this->input->post('tag');
            $unit_update_margin = $this->input->post('unit_update_margin');
            $pg_update_price = $this->input->post('pg_update_price');
            $warehouse_id = $this->input->post('warehouse') ? $this->input->post('warehouse') : $this->Settings->default_warehouse;
            $supplier_id = $this->input->post('supplier');
            $biller_id = $this->input->post('biller');
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false && $this->Settings->modulary == 1) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect('purchases/add');
            }
            $status = $this->input->post('status') ? $this->input->post('status') : 'pending';
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $supplier_details = $this->site->getCompanyByID($supplier_id);
            $supplier = $supplier_details->name != '-'  ? $supplier_details->name : $supplier_details->company;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $paid_by = $this->input->post('paid_by');
            $descuento_orden = $this->input->post('order_discount_method');
            $quote_id = $this->input->post('quote_id') ? $this->input->post('quote_id') : null;
            $ptype = $this->input->post('purchase_type') ? $this->input->post('purchase_type') : 1;
            $consecutive_supplier = $this->input->post('consecutive_supplier');
            if ($ptype == 1) {
                $p_prefix =  $this->site->getReference('po', true);
                $p_reference = 'po';
            } else {
                $p_status = 'service_received';
                $p_prefix =  $this->site->getReference('ex', true);
                $p_reference = 'ex';
            }
            $document_type_id = $this->input->post('document_type_id');
            $reference = null;
            if ($this->input->post('reference_no')) {
                $reference = $this->input->post('reference_no');
            }
            $total = 0;
            $product_tax = 0;
            $consumption_purchase = 0;
            $item_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $assumed_retentions_text = "";
            $new_pv = [];
            //Retenciones
            if ($this->input->post('rete_applied')) {
                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');
                $rete_fuente_id = $this->input->post('rete_fuente_id');
                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');
                $rete_iva_id = $this->input->post('rete_iva_id');
                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');
                $rete_ica_id = $this->input->post('rete_ica_id');
                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');
                $rete_other_id = $this->input->post('rete_otros_id');
                $rete_fuente_assumed = $this->input->post('rete_fuente_assumed') ? 1 : 0;
                $rete_iva_assumed = $this->input->post('rete_iva_assumed') ? 1 : 0;
                $rete_ica_assumed = $this->input->post('rete_ica_assumed') ? 1 : 0;
                $rete_other_assumed = $this->input->post('rete_otros_assumed') ? 1 : 0;
                $rete_fuente_assumed_account = $this->input->post('rete_fuente_assumed_account');
                $rete_iva_assumed_account = $this->input->post('rete_iva_assumed_account');
                $rete_ica_assumed_account = $this->input->post('rete_ica_assumed_account');
                $rete_bomberil_assumed_account = $this->input->post('rete_bomberil_assumed_account');
                $rete_autoaviso_assumed_account = $this->input->post('rete_autoaviso_assumed_account');
                $rete_other_assumed_account = $this->input->post('rete_otros_assumed_account');
                $rete_bomberil_percentage = $this->input->post('rete_bomberil_tax');
                $rete_bomberil_total = $this->sma->formatDecimal($this->input->post('rete_bomberil_valor'));
                $rete_bomberil_account = $this->input->post('rete_bomberil_account');
                $rete_bomberil_base = $this->input->post('rete_bomberil_base');
                $rete_bomberil_id = $this->input->post('rete_bomberil_id');
                $rete_autoaviso_percentage = $this->input->post('rete_autoaviso_tax');
                $rete_autoaviso_total = $this->sma->formatDecimal($this->input->post('rete_autoaviso_valor'));
                $rete_autoaviso_account = $this->input->post('rete_autoaviso_account');
                $rete_autoaviso_base = $this->input->post('rete_autoaviso_base');
                $rete_autoaviso_id = $this->input->post('rete_autoaviso_id');
                $total_retenciones =
                    ($rete_fuente_assumed ? 0 : $rete_fuente_total) +
                    ($rete_iva_assumed ? 0 : $rete_iva_total) +
                    ($rete_ica_assumed ? 0 : $rete_ica_total) +
                    ($rete_ica_assumed ? 0 : $rete_bomberil_total) +
                    ($rete_ica_assumed ? 0 : $rete_autoaviso_total) +
                    ($rete_other_assumed ? 0 : $rete_other_total);
                if ($rete_fuente_assumed) {
                    $assumed_retentions_text .= "Rete Fuente asumida (" . $this->sma->formatMoney($rete_fuente_total) . "), ";
                }
                if ($rete_iva_assumed) {
                    $assumed_retentions_text .= "Rete IVA asumida(" . $this->sma->formatMoney($rete_iva_total) . "), ";
                }
                if ($rete_ica_assumed) {
                    $assumed_retentions_text .= "Rete ICA asumida(" . $this->sma->formatMoney($rete_ica_total) . "), ";
                    if ($rete_bomberil_total > 0) {
                        $assumed_retentions_text .= "Tasa Bomberil asumida(" . $this->sma->formatMoney($rete_bomberil_total) . "), ";
                    }
                    if ($rete_autoaviso_total > 0) {
                        $assumed_retentions_text .= "Tasa Avisos y Tableros asumida(" . $this->sma->formatMoney($rete_autoaviso_total) . "), ";
                    }
                }
                if ($rete_other_assumed) {
                    $assumed_retentions_text .= "Rete OTRAS asumida(" . $this->sma->formatMoney($rete_other_total) . "), ";
                }
                $assumed_retentions_text = trim($assumed_retentions_text, ", ");
                $rete_applied = TRUE;
            } else {
                $rete_applied = FALSE;
                $total_retenciones = 0;
            }
            //Retenciones

            $archivo = false;
            if (isset($_FILES["xls_file"])) {
                $archivo = $_FILES['xls_file']['tmp_name'];
            }

            if ($archivo) {
                $this->load->library('excel');
                $invalids_products = false;
                $objPHPExcel = PHPExcel_IOFactory::load($archivo);
                $hojas = $objPHPExcel->getWorksheetIterator();
                foreach ($hojas as $hoja) {
                    $data = $hoja->toArray();
                    unset($data[0]);
                    foreach ($data as $data_row) {
                        if (empty($data_row[0])) {
                            continue;
                        }
                        $product_excel['code'] = $data_row[0];
                        $product_excel['reference'] = $data_row[1];
                        $product_excel['name'] = $data_row[2];
                        $product_excel['quantity'] = $data_row[3];
                        $product_excel['variant_code'] = $data_row[4];
                        $product_excel['unit_cost'] = $data_row[5];
                        if ($product_excel_data = $this->products_model->get_product_by_code_reference($product_excel)) {
                            $ped_unit = $this->site->getUnitByID($product_excel_data['unit']);
                            $product_excel_data['product_id'] = $product_excel_data['id'];
                            $product_excel_data['tax_rate_id'] = $product_excel_data['tax_rate'];
                            $product_excel_data['real_unit_cost'] = $product_excel['unit_cost'];
                            $product_excel_data['quantity'] = $product_excel['quantity'];
                            if ($item_fixed = $this->site->fix_item_tax($document_type_id, $product_excel_data, true, true)) {
                            } else {
                                $item_fixed = false;
                            }
                            $option = false;
                            if ($product_excel['variant_code']) {
                                $option = $this->purchases_model->getProductVariantByCode($product_excel['variant_code'], $product_excel_data['id']);
                                if (!$option) {
                                    // $this->sma->print_arrays($product_excel, $product_excel_data);
                                    $this->session->set_flashdata('error', lang("variant_not_found") . " ( " . $product_excel_data['name'] . " - " . $product_excel['variant_code'] . " ).");
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                            }
                            $product = array(
                                'product_id' => $product_excel_data['id'],
                                'product_code' => $product_excel_data['code'],
                                'product_name' => $product_excel_data['name'],
                                'net_unit_cost' => $item_fixed ? $item_fixed['net_unit_cost'] : NULL,
                                'unit_cost' => $item_fixed ? $item_fixed['unit_cost'] : NULL,
                                'quantity' => $product_excel['quantity'],
                                'quantity_balance' => $product_excel['quantity'],
                                'product_unit_id' => $ped_unit ? $ped_unit->id : NULL,
                                'product_unit_code' => $ped_unit ? $ped_unit->code : NULL,
                                'unit_quantity' => $product_excel['quantity'],
                                'warehouse_id' => $warehouse_id,
                                'item_tax' => ($item_fixed ? $item_fixed['item_tax'] : NULL),
                                'tax_rate_id' => $item_fixed ? $item_fixed['tax_rate_id'] : NULL,
                                'tax' => $item_fixed ? $item_fixed['tax'] : NULL,
                                'subtotal' => $item_fixed ? $item_fixed['subtotal'] : NULL,
                                'real_unit_cost' => $item_fixed ? $item_fixed['unit_cost'] : NULL,
                                'option_id' => ($option ? $option->id : NULL),
                                'supplier_part_no' => NULL,
                                'item_tax_2' => NULL,
                                'original_tax_rate_id' => NULL,
                            );
                            $products[] = $product;
                            $product_tax += $product['item_tax'];
                            $total += $product['net_unit_cost'] * $product['quantity'];
                        } else {
                            // exit(var_dump($product_excel['reference']));
                            $invalids_products = $invalids_products.($invalids_products ? "</br>" : "")." <b>Código</b> ".$product_excel['code']." <b>Referencia</b> ".$product_excel['reference']." <b>Nombre</b> ".$product_excel['name'];
                        }
                    }
                }
                if ($invalids_products) {
                    // exit(var_dump($invalids_products));
                    $this->session->set_userdata('invalid_csv_sale_products', '<h2>Los siguientes productos no existen</h2> : </br></br></br>'.$invalids_products);
                    admin_redirect("purchases/add");
                }
                $total_prorrated_shipping = 0;
                // $this->sma->print_arrays($products);
            } else {
                $item_id = 0;
                $i = sizeof($_POST['product_code']);
                $total_prorrated_shipping = 0;
                for ($r = 0; $r < $i; $r++) {
                    $item_id = $_POST['product_id'][$r];
                    $pr_item_data = $this->purchases_model->getProductByID($item_id);
                    $item_type = $_POST['product_type'][$r];
                    $item_code = $_POST['product_code'][$r];
                    $item_name = $_POST['product_name'][$r];
                    $item_net_cost = $this->sma->formatDecimal($_POST['net_cost'][$r]);
                    $unit_cost = $this->sma->formatDecimal($_POST['unit_cost'][$r]);
                    $real_unit_cost = $this->sma->formatDecimal($_POST['real_unit_cost'][$r]);
                    $item_unit_quantity = str_replace(",", "", $_POST['quantity'][$r]);
                    $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' ? $_POST['product_option'][$r] : null;
                    $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                    $item_unit_tax_val = isset($_POST['unit_product_tax'][$r]) ? $_POST['unit_product_tax'][$r] : null;
                    $item_tax_2_rate = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : null;
                    $item_unit_tax_2_val = isset($_POST['unit_product_tax_2'][$r]) ? $_POST['unit_product_tax_2'][$r] : null;
                    $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                    $item_unit_discount = isset($_POST['product_unit_discount_val'][$r]) ? $_POST['product_unit_discount_val'][$r] : null;
                    $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : null;
                    $serialModal_serial = isset($_POST['serialModal_serial'][$r]) ? $_POST['serialModal_serial'][$r] : null;
                    $product_unit_id_selected = isset($_POST['product_unit_id_selected'][$r]) ? $_POST['product_unit_id_selected'][$r] : null;
                    $profitability_margin = isset($_POST['profitability_margin'][$r]) ? $_POST['profitability_margin'][$r] : null;
                    $shipping_unit_cost = isset($_POST['shipping_unit_cost'][$r]) ? $_POST['shipping_unit_cost'][$r] : null;
                    $unit_product_tax_2_percentage = isset($_POST['unit_product_tax_2_percentage'][$r]) ? $_POST['unit_product_tax_2_percentage'][$r] : null;

                    $new_option_assigned = isset($_POST['new_option_assigned'][$r]) ? $_POST['new_option_assigned'][$r] : null;
                    $new_option_assigned_code = isset($_POST['new_option_assigned_code'][$r]) ? $_POST['new_option_assigned_code'][$r] : null;
                    $new_option_assigned_option_id = isset($_POST['new_option_assigned_option_id'][$r]) ? $_POST['new_option_assigned_option_id'][$r] : null;
                    $new_option_assigned_name = isset($_POST['new_option_assigned_name'][$r]) ? $_POST['new_option_assigned_name'][$r] : null;
                    $new_option_assigned_consecutive = isset($_POST['new_option_assigned_consecutive'][$r]) ? $_POST['new_option_assigned_consecutive'][$r] : null;
                    $new_option_assigned_weight = isset($_POST['new_option_assigned_weight'][$r]) ? $_POST['new_option_assigned_weight'][$r] : null;

                    $item_expiry = (isset($_POST['expiry'][$r]) && !empty($_POST['expiry'][$r])) ? $this->sma->fsd($_POST['expiry'][$r]) : null;
                    $supplier_part_no = (isset($_POST['part_no'][$r]) && !empty($_POST['part_no'][$r])) ? $_POST['part_no'][$r] : null;
                    if (!$supplier_part_no && $this->Settings->purchases_products_supplier_code == 1) {
                        $this->session->set_flashdata('error', lang("invalid_supplier_part_no"));
                        admin_redirect($_SERVER['HTTP_REFERER']);
                    }
                    $item_unit = $_POST['product_unit'][$r];
                    $item_base_unit = $_POST['product_purchase_unit'][$r];
                    $item_quantity = $_POST['product_base_quantity'][$r];
                    if (isset($item_code) && isset($real_unit_cost) && isset($unit_cost) && isset($item_quantity)) {
                        if (!($item_quantity > 0)) {
                            $this->session->set_flashdata('error', sprintf(lang("invalid_supplier_part_no"), ($item_code." - ".$item_name)));
                            admin_redirect($_SERVER['HTTP_REFERER']);
                        }
                        $product_details = $this->purchases_model->getProductByCode($item_code);
                        if ($item_expiry) {
                            $today = date('Y-m-d');
                            if ($item_expiry <= $today) {
                                $this->session->set_flashdata('error', lang('product_expiry_date_issue') . ' (' . $product_details->name . ')');
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        }
                        $pr_discount = $item_unit_discount;
                        $unit_cost = $this->sma->formatDecimal($unit_cost - $pr_discount);
                        $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                        $product_discount += $pr_item_discount;
                        $pr_item_tax = $this->sma->formatDecimal($item_unit_tax_val * $item_unit_quantity);
                        $pr_item_tax_2 = $this->sma->formatDecimal($item_unit_tax_2_val * $item_unit_quantity);
                        $item_tax = $item_unit_tax_val;
                        $item_tax_2 = $item_unit_tax_2_val;
                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $tax_details_2 = $this->site->getTaxRateByID($item_tax_2_rate);
                        $tax_details_2_secondary = $this->site->getTaxRate2ByID($item_tax_2_rate);
                        $tax = $tax_2 = 0;
                        if ($item_tax_rate) {
                            $tax = $this->sma->formatDecimal($tax_details->rate, 0) . "%";
                            if ($tax_details->type == 2 && $tax_details->rate < 1 && $item_tax > 0) {
                                $tax = $item_tax;
                            }
                        }
                        if ($item_tax_2_rate && $tax_details_2) {
                            $tax_2 = $this->sma->formatDecimal($tax_details_2->rate, 0) . "%";
                            if ($tax_details_2->type == 2 && $tax_details_2->rate < 1 && $item_tax_2 > 0) {
                                $tax_2 = $item_tax_2;
                            }
                        }
                        $item_tax_2_percentage = $item_tax_2_rate;
                        if ($item_tax_2_rate && $tax_details_2_secondary) {
                            $item_tax_2_percentage =  $unit_product_tax_2_percentage;
                        }

                        $product_tax += $pr_item_tax + $pr_item_tax_2;
                        $consumption_purchase += $ptype == 1 ? $pr_item_tax_2 : 0;
                        $subtotal = (($item_net_cost * $item_unit_quantity) + $pr_item_tax + $pr_item_tax_2 + $shipping_unit_cost);
                        $unit = $this->site->getUnitByID($item_unit > 0 ? $item_unit : $item_base_unit);
                        $total_prorrated_shipping += ($shipping_unit_cost * $item_quantity);
                        $product = array(
                            'product_id' => $item_id,
                            'product_code' => $item_code,
                            'product_name' => $item_name,
                            'option_id' => $new_option_assigned ? NULL : $item_option,
                            'net_unit_cost' => $item_net_cost,
                            'unit_cost' => $this->sma->formatDecimal($item_net_cost + $item_tax + $item_tax_2 + $shipping_unit_cost),
                            'quantity' => $item_quantity,
                            'product_unit_id' => ($item_unit > 0 ? $item_unit : $item_base_unit),
                            'product_unit_code' => isset($unit->code) ? $unit->code : NULL,
                            'unit_quantity' => $item_unit_quantity,
                            'quantity_balance' => $item_quantity,
                            'quantity_received' => $item_quantity,
                            'warehouse_id' => $warehouse_id,
                            'item_tax' => $pr_item_tax,
                            'tax_rate_id' => $item_tax_rate,
                            'original_tax_rate_id' => $item_tax_rate != $pr_item_data->tax_rate ? $pr_item_data->tax_rate : NULL,
                            'tax' => $tax,
                            'item_tax_2' => $pr_item_tax_2,
                            'tax_rate_2_id' => $item_tax_2_rate,
                            'tax_2' => ($ptype == 2 || $ptype == 3) ? $tax_2 : $item_tax_2_percentage,
                            'discount' => $item_discount,
                            'item_discount' => $pr_item_discount,
                            'subtotal' => $this->sma->formatDecimal($subtotal),
                            'expiry' => $item_expiry,
                            'real_unit_cost' => $real_unit_cost,
                            'date' => date('Y-m-d', strtotime($date)),
                            'status' => (isset($p_status) ? $p_status : $status),
                            'supplier_part_no' => $supplier_part_no,
                            'serial_no' => $serialModal_serial ? $serialModal_serial : $item_serial,
                            'product_unit_id_selected' => $product_unit_id_selected,
                            'profitability_margin' => $profitability_margin,
                            'shipping_unit_cost' => $shipping_unit_cost,
                            'consumption_purchase' => $ptype == 1 ? $pr_item_tax_2 : 0,
                        );
                        if ($new_option_assigned) {
                            $product['new_option_assigned'] = $item_id."_".$new_option_assigned_option_id;
                            $new_pv[$item_id."_".$new_option_assigned_option_id] = [
                                'product_id' => $item_id,
                                'code' => $new_option_assigned_code,
                                'name' => $new_option_assigned_name,
                                'pv_code_consecutive' => $new_option_assigned_consecutive,
                                'weight' => $new_option_assigned_weight,
                            ];
                        }
                        $products[] = ($product + $gst_data);
                        $total += $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity));
                    }
                }
            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }
            if ($descuento_orden == 2) { //DESCUENTO GLOBAL
                $order_discount = $this->site->calculateDiscount($this->input->post('discount'), ($total));
                $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount));
            } else { // DESCUENTO A CADA PRODUCTO
                $order_discount = 0;
                $total_discount = 0;
            }
            $actual_currency = $this->input->post('currency');
            $actual_currency_trm = $this->input->post('trm');
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), (($descuento_orden == 1 ? $total - $order_discount : $total + $product_discount)));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax));
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($prorate_shipping_cost == 1 ? 0 : $this->sma->formatDecimal($shipping)) - $order_discount + $total_prorrated_shipping));
            $current_currency = $this->currency_model->get_currency_by_code($actual_currency);
            $default_currency = $this->currency_model->get_currency_by_code($this->Settings->default_currency);
            if ($actual_currency != $this->Settings->default_currency && $quote_id == null) {
                $note .= sprintf(
                    lang('diferent_currency_trm'),
                    $actual_currency,
                    $current_currency->name,
                    $this->sma->formatMoney($actual_currency_trm),
                    $this->Settings->default_currency,
                    $default_currency->name,
                    $this->sma->formatMoney($grand_total)
                );
            }
            $nota_shipping = "";
            if ($shipping > 0 && $prorate_shipping_cost == 1) {
                $nota_shipping = " El valor del flete por " . $this->sma->formatMoney($total_prorrated_shipping) . " ha sido distribuido en los costos de los productos, el valor de la factura descontando los fletes es de " . $this->sma->formatMoney($grand_total - $total_prorrated_shipping);
            }
            $data = array(
                'reference_no' => str_replace(' ', '', $reference),
                'date' => $date,
                'supplier_id' => $supplier_id,
                'supplier' => $supplier,
                'warehouse_id' => $warehouse_id,
                'note' => $note . $nota_shipping . $assumed_retentions_text,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $descuento_orden == 2 ? $this->input->post('discount') : '',
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $prorate_shipping_cost == 1 ? 0 : $this->sma->formatDecimal($shipping),
                'prorated_shipping_cost' => $prorate_shipping_cost == 1 ? $this->sma->formatDecimal($total_prorrated_shipping) : 0,
                'grand_total' => $this->sma->formatDecimal($grand_total),
                'status' => $status,
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'order_discount_method' => $descuento_orden,
                'purchase_currency' => $actual_currency,
                'purchase_currency_trm' => $actual_currency_trm,
                'purchase_type' => $ptype,
                'biller_id' => $biller_id,
                'consecutive_supplier' => $consecutive_supplier,
                'document_type_id' => $document_type_id,
                'purchase_origin' => $this->input->post('purchase_origin'),
                'purchase_origin_reference_no' => $this->input->post('purchase_origin_reference_no'),
                'consumption_purchase' => $consumption_purchase,
                'purchase_from_import' => $purchase_from_import,
                'tag_id' => $tag_id,
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }
            if ($support_document) {
                $document_type = $this->site->getDocumentTypeById($document_type_id);
                $resolucion = "";
                if ($document_type->save_resolution_in_sale == 1) {
                    $resolucion = $this->site->textoResolucion($document_type);
                    $data['resolucion'] = $resolucion;
                }

                $data['generationTransmissionMode'] = $this->input->post('generationTransmissionMode');
            }
            $payments = [];
            $payment_reference_no = $this->input->post('payment_reference_no');
            $j = count($_POST['amount-paid']);
            $cash_amount_payment = 0;
            $total_paid = 0;
            if ($j > 0 && (($status == 'received') ||  $ptype == 3)) {
                for ($i = 0; $i < $j; $i++) {
                    if (($ptype == 2 || $ptype == 3) && $_POST['paid_by'][$i] == 'expense_causation') {
                        $data['expense_causation'] = 1;
                        continue;
                    }
                    if ($_POST['due_payment'][$i] == 1) {
                        $pm = $this->site->getPaymentMethodByCode($_POST['paid_by'][$i]);
                        $data['due_payment_method_id'] = $pm->id;
                    } else {
                        if ($_POST['paid_by'][$i] == 'cash') {
                            $cash_amount_payment += $_POST['amount-paid'][$i];
                        }
                        $total_paid +=$_POST['amount-paid'][$i];
                        if (empty($_POST['paid_by'][$i])) {
                            $this->session->set_flashdata('error', lang("invalid_payment_method"));
                            admin_redirect($_SERVER['HTTP_REFERER']);
                        }
                        $paid_by = $_POST['paid_by'][$i];
                        $payment = array(
                            'date' => $date,
                            'amount' => $this->sma->formatDecimal($_POST['amount-paid'][$i]),
                            'paid_by' => $_POST['paid_by'][$i],
                            'cheque_no' => $_POST['cheque_no'][$i],
                            'cc_no' => $_POST['pcc_no'][$i],
                            'cc_holder' => $_POST['pcc_holder'][$i],
                            'cc_month' => $_POST['pcc_month'][$i],
                            'cc_year' => $_POST['pcc_year'][$i],
                            'cc_type' => $_POST['pcc_type'][$i],
                            'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                            'note' => $_POST['payment_note'][$i],
                            'type' => 'sent',
                            'mean_payment_code_fe' => $_POST['mean_payment_code_fe'][$i],
                            'document_type_id' => $payment_reference_no,
                        );
                        $payments[] = $payment;
                        //IVA PROPORCIONAL
                        if ($this->Settings->tax_rate_traslate && $data['total_tax'] > 0) {
                            $purchase_taxes = [];
                            foreach ($products as $row) {
                                $proporcion_pago = ($_POST['amount-paid'][$i] * 100) / $data['grand_total'];
                                $total_iva = ($row['item_tax'] + $row['item_tax_2']) * ($proporcion_pago / 100);
                                if (!isset($purchase_taxes[$row['tax_rate_id']])) {
                                    $purchase_taxes[$row['tax_rate_id']] = $total_iva;
                                } else {
                                    $purchase_taxes[$row['tax_rate_id']] += $total_iva;
                                }
                            }
                            $tax_rate_traslate_ledger_id = $this->site->getPaymentMethodParameter('tax_rate_traslate');
                            $data_tax_rate_traslate = $data_taxrate_traslate = array(
                                'tax_rate_traslate_ledger_id' => $tax_rate_traslate_ledger_id->payment_ledger_id,
                                'purchase_taxes' => $purchase_taxes,
                            );
                        }
                    }
                }
            } else if ($j > 0 && ($status == 'pending')) {
                $data['suggested_paid_by'] = $_POST['paid_by'][0];
                $data['suggested_paid_amount'] = $_POST['paid_by'][0] != 'Credito' ? $_POST['amount-paid'][0] : 0;
            }
            if ($rete_applied) {
                $data['rete_fuente_percentage'] = $rete_fuente_percentage;
                $data['rete_fuente_total'] = $rete_fuente_total;
                $data['rete_fuente_account'] = $rete_fuente_account;
                $data['rete_fuente_base'] = $rete_fuente_base;
                $data['rete_fuente_id'] = $rete_fuente_id;
                $data['rete_iva_percentage'] = $rete_iva_percentage;
                $data['rete_iva_total'] = $rete_iva_total;
                $data['rete_iva_account'] = $rete_iva_account;
                $data['rete_iva_base'] = $rete_iva_base;
                $data['rete_iva_id'] = $rete_iva_id;
                $data['rete_ica_percentage'] = $rete_ica_percentage;
                $data['rete_ica_total'] = $rete_ica_total;
                $data['rete_ica_account'] = $rete_ica_account;
                $data['rete_ica_base'] = $rete_ica_base;
                $data['rete_ica_id'] = $rete_ica_id;
                $data['rete_other_percentage'] = $rete_other_percentage;
                $data['rete_other_total'] = $rete_other_total;
                $data['rete_other_account'] = $rete_other_account;
                $data['rete_other_base'] = $rete_other_base;
                $data['rete_other_id'] = $rete_other_id;

                $data['rete_bomberil_percentage'] = $rete_bomberil_percentage;
                $data['rete_bomberil_total'] = $rete_bomberil_total;
                $data['rete_bomberil_account'] = $rete_bomberil_account;
                $data['rete_bomberil_base'] = $rete_bomberil_base;
                $data['rete_bomberil_id'] = $rete_bomberil_id;

                $data['rete_autoaviso_percentage'] = $rete_autoaviso_percentage;
                $data['rete_autoaviso_total'] = $rete_autoaviso_total;
                $data['rete_autoaviso_account'] = $rete_autoaviso_account;
                $data['rete_autoaviso_base'] = $rete_autoaviso_base;
                $data['rete_autoaviso_id'] = $rete_autoaviso_id;

                $data['rete_fuente_assumed'] = $rete_fuente_assumed;
                $data['rete_iva_assumed'] = $rete_iva_assumed;
                $data['rete_ica_assumed'] = $rete_ica_assumed;
                $data['rete_other_assumed'] = $rete_other_assumed;
                $data['rete_fuente_assumed_account'] = $rete_fuente_assumed ? $rete_fuente_assumed_account : NULL;
                $data['rete_iva_assumed_account'] = $rete_iva_assumed ? $rete_iva_assumed_account : NULL;
                $data['rete_ica_assumed_account'] = $rete_ica_assumed ? $rete_ica_assumed_account : NULL;
                $data['rete_bomberil_assumed_account'] = $rete_ica_assumed ? $rete_bomberil_assumed_account : NULL;
                $data['rete_autoaviso_assumed_account'] = $rete_ica_assumed ? $rete_autoaviso_assumed_account : NULL;
                $data['rete_other_assumed_account'] = $rete_other_assumed ? $rete_other_assumed_account : NULL;
                if ($total_retenciones > 0) {
                    $payment = array(
                        'date'         => $date,
                        'amount'       => $total_retenciones,
                        'reference_no' => 'retencion',
                        'paid_by'      => 'retencion',
                        'cheque_no'    => '',
                        'cc_no'        => '',
                        'cc_holder'    => '',
                        'cc_month'     => '',
                        'cc_year'      => '',
                        'cc_type'      => '',
                        'created_by'   => $this->session->userdata('user_id'),
                        'type'         => 'sent',
                        'note'         => 'Retenciones',
                    );
                    $payments[] = $payment;
                    $total_paid +=$total_retenciones;
                }
            }
            $total_payment = 0;
            if (count($payments) > 0) {
                foreach ($payments as $payment) {
                    $total_payment += $payment['amount'];
                }
            }
            $total_balance = $this->sma->formatDecimal(($data['total'] + $data['total_tax']) - $total_paid);
            if ($total_balance > 0 && $total_balance < 0.01) {
                $payments[0]['amount'] += $total_balance;
            }
            $data['payment_status'] = 'pending';
            if ($data['status'] == 'received' && $total_payment == $data['grand_total']) {
                $data['payment_status'] = 'paid';
                $data['payment_term'] = 0;
            } else if (($data['status'] == 'received' || $data['status'] == 'pending') && $total_payment < $data['grand_total']) {
                if ($total_payment == 0) {
                    $data['payment_status'] = 'pending';
                } else if ($total_payment > 0) {
                    $data['payment_status'] = 'partial';
                }

                $data['payment_term'] = $this->input->post('purchase_payment_term');
                $due_date = $this->input->post('purchase_payment_term') ? date('Y-m-d', strtotime('+' . $this->input->post('purchase_payment_term') . ' days', strtotime($date))) : null;
                $data['due_date'] = $due_date;
                if ($data['status'] == 'received' && $this->input->post('purchase_payment_term') <= 0 && !isset($data['expense_causation'])) {
                    $this->session->set_flashdata('error', lang("payment_term_almost_be_greather_than_zero"));
                    admin_redirect($_SERVER['HTTP_REFERER']);
                }
            }
            $payment_affects_register = 0;
            if (($data['payment_status'] == 'partial' || $data['payment_status'] == 'paid') && $cash_amount_payment > 0) {
                if ($this->Settings->purchase_payment_affects_cash_register == 2) {
                    if ($this->input->post('payment_affects_register') != null) {
                        $payment_affects_register = $this->input->post('payment_affects_register');
                    } else {
                        $this->session->set_flashdata('error', lang('affectation_not_indicated'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } else {
                    $payment_affects_register = $this->Settings->purchase_payment_affects_cash_register;
                }
            } else {
                $payment_affects_register = 0;
            }
            if (count($payments) > 0) {
                foreach ($payments as $p_id => $payment) {
                    if ($payment_affects_register == 0) {
                        $payments[$p_id]['type'] = 'sent_2';
                    }
                }
            }
            $data['payment_affects_register'] = $payment_affects_register;
            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            $document_type = $this->site->getDocumentTypeById($document_type_id);
            if ($support_document) {
                if ($this->site->invoice_date_existing_in_resolution_date_range($date, $document_type) == FALSE) {
                    $this->session->set_flashdata("error", lang("invoice_date_out_of_range"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            if (!$this->session->userdata('purchase_processing')) {
                $this->session->set_userdata('purchase_processing', 1);
            } else {
                $this->session->set_flashdata('error', 'Se interrumpió el proceso de envío por que se detectó que ya hay uno en proceso, prevención de duplicación.');
                admin_redirect("purchases");
            }
        }

        if ($this->form_validation->run() == true && $purchaseId = $this->purchases_model->addPurchase($data, $products, $payments, $ptype, false, (isset($data_taxrate_traslate) ? $data_taxrate_traslate : null), $quote_id, $unit_update_price, $pg_update_price, $unit_update_margin, $new_pv)) {
            $this->session->set_userdata('remove_pols', 1);

            if ($data['status'] == 'pending') {
                $msg_warn = lang('purchase_pending');
                if ($this->Settings->modulary) {
                    $msg_warn .= lang('purchase_pending_not_accounted');
                }
                $this->session->set_flashdata('warning', $msg_warn);
            } else {
                if ($this->Settings->supportingDocument == YES && $support_document) {
                    $resolutionData = $this->SupportDocument_model->getResolutionData($this->input->post('document_type_id'));

                    if ($resolutionData->factura_electronica == YES) {
                        $this->SupportDocument_model->sendElectronicDocument($purchaseId);
                    }
                }

                $this->session->set_flashdata('message', $this->lang->line("purchase_added"));
            }

            if ($this->Settings->supportingDocument == YES && $support_document) {
                admin_redirect('purchases/supporting_document_index');
            }
            if ($this->session->userdata('purchase_processing')) {
                $this->session->unset_userdata('purchase_processing');
            }
            admin_redirect('purchases');
        } else {
            if ($quote_id) {
                $this->data['quote'] = $this->purchases_model->getQuoteByID($quote_id);
                $supplier_id = $this->data['quote']->supplier_id;
                $items = $this->purchases_model->getAllQuoteItems($quote_id);
                krsort($items);
                $c = rand(100000, 9999999);
                foreach ($items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    $tipo_gasto = false;
                    if ($this->data['quote']->quote_type == 2) {
                        $tipo_gasto = true;
                        $row = $item;
                    }
                    if ($tipo_gasto == false && $row->type == 'combo') {
                        $combo_items = $this->site->getProductComboItems($row->id, $item->warehouse_id);
                        foreach ($combo_items as $citem) {
                            $crow = $this->site->getProductByID($citem->id);
                            if (!$crow) {
                                $crow = json_decode('{}');
                                $crow->qty = $item->quantity;
                            } else {
                                unset($crow->details, $crow->product_details, $crow->price);
                                $crow->qty = $citem->qty * $item->quantity;
                            }
                            $crow->base_quantity = $item->quantity;
                            $crow->base_unit = $crow->unit ? $crow->unit : $item->product_unit_id;
                            $crow->base_unit_cost = $item->unit_price ? $item->unit_price + $item->item_discount : $row->unit_cost;
                            $crow->unit = $item->product_unit_id;
                            $crow->discount = $item->discount ? $item->discount : '0';
                            $supplier_cost = $supplier_id ? $this->getSupplierCost($supplier_id, $crow) : $crow->cost;
                            $crow->cost = $supplier_cost ? $supplier_cost : 0;
                            $crow->tax_rate = $item->tax_rate_id;
                            $crow->real_unit_cost = $item->unit_price ? $item->unit_price + $item->item_discount : 0;
                            $crow->expiry = '';
                            $options = $this->purchases_model->getProductOptions($crow->id);
                            $units = $this->site->get_product_units($row->id);
                            $tax_rate = $this->site->getTaxRateByID($crow->tax_rate);
                            $ri = $this->Settings->item_addition ? $crow->id : $c;
                            $pr[$ri] = array('id' => $c, 'item_id' => $crow->id, 'label' => $crow->name . " (" . $crow->code . ")", 'row' => $crow, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                            $c++;
                        }
                    } elseif ($tipo_gasto == true || $row->type == 'standard') {
                        if (!$row) {
                            $row = json_decode('{}');
                            $row->quantity = 0;
                        } else {
                            unset($row->details, $row->product_details);
                        }
                        if ($tipo_gasto == true) {
                            $category_expense = $this->db->where('code', $item->product_code)->get('expense_categories')->row();
                            if ($item->tax_rate_id != $category_expense->tax_rate_id) {
                                $row->expense_diferent_tax_rate_id = true;
                                $row->expense_tax_rate_id = $category_expense->tax_rate_id;
                            } else {
                                $row->expense_tax_rate_id = $item->tax_rate_id;
                            }
                        }
                        $row->id = $item->product_id;
                        $row->code = $item->product_code;
                        $row->name = $item->product_name;
                        $row->base_quantity = $item->quantity;
                        $row->serialModal_serial = $item->serial_no;
                        $row->base_unit = isset($row->unit) ? $row->unit : $item->product_unit_id;
                        $row->base_unit_cost = $item->unit_price ? $item->unit_price + $item->item_discount : $row->unit_cost;
                        $row->unit = $item->product_unit_id;
                        $row->qty = $item->quantity;
                        $row->option = $item->option_id;
                        $row->discount = $item->discount ? $item->discount : '0';
                        if (!$tipo_gasto) {
                            $supplier_cost = $supplier_id ? $this->getSupplierCost($supplier_id, $row) : $row->cost;
                        }
                        $row->cost = $item->net_unit_price;
                        $row->tax_rate = $item->tax_rate_id;
                        $row->tax_rate_2 = $item->tax_rate_2_id;
                        $row->product_unit_id_selected = $item->product_unit_id;
                        $row->expiry = '';
                        $row->real_unit_cost = $item->real_unit_price ? $item->real_unit_price + $item->item_discount - ($this->data['quote']->quote_type == 2 ? $item->item_tax + $item->item_tax_2 : 0) : 0;
                        if ($tipo_gasto) {
                            $row->tax_method = "1";
                        }
                        $options = $this->purchases_model->getProductOptions($row->id);
                        $units = $this->site->get_product_units($row->id);
                        $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                        if ($tax_rate) {
                            if ($tax_rate->rate < 1 && $tax_rate->type == 2 && $item->item_tax > 0) {
                                $tax_rate->rate = $item->item_tax;
                            }
                        }
                        $tax_rate_2 = $this->site->getTaxRateByID($row->tax_rate_2);
                        if ($tax_rate_2) {
                            if ($tax_rate_2->rate < 1 && $tax_rate_2->type == 2 && $item->item_tax_2 > 0) {
                                $tax_rate_2->rate = $item->item_tax_2;
                            }
                        }
                        $ri = $this->Settings->item_addition ? $row->id : $c;
                        $pr[$ri] = array(
                            'id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                            'row' => $row, 'tax_rate' => $tax_rate, 'tax_rate_2' => $tax_rate_2, 'units' => $units, 'options' => $options
                        );
                        $c++;
                    }
                }
                $this->data['quote_items'] = json_encode($pr);
            }
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $trates = $this->site->getAllTaxRates();
            $tax_rates = [];
            foreach ($trates as $key => $tr) {
                $tax_rates[$tr->id] = $tr;
            }

            $this->data['new_supplier_id'] = $this->input->get('new_supplier_id') ? $this->input->get('new_supplier_id') : false;
            $this->data['quote_id'] = $quote_id;
            $this->data['suppliers'] = $this->site->getAllCompanies('supplier');
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['tax_rates'] = $tax_rates;
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['ponumber'] = ''; //$this->site->getReference('po');
            $this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['brands'] = $this->site->getAllBrands();
            $this->data['tags'] = $this->site->get_tags_by_module(4);
            if ($this->Settings->management_weight_in_variants == 1) {
                $this->data['variants'] = $this->products_model->getAllVariants();
            }
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $this->data['reference'] = (isset($this->data['quote']) && $this->data['quote']->quote_type == 2 ? $this->site->getReference('ex', true) : $this->site->getReference('po', true)) . "-";
            $this->load->helper('string');
            $value = random_string('alnum', 20);
            $this->session->set_userdata('user_csrf', $value);
            $this->data['csrf'] = $this->session->userdata('user_csrf');
            $this->data['price_groups'] = $this->site->getAllPriceGroups();
            $this->data['units'] = $this->site->get_all_units();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('purchases'), 'page' => lang('purchases')), array('link' => '#', 'page' => lang('add_purchase')));
            $meta = array('page_title' => lang('add_purchase'), 'bc' => $bc);
            if ($this->input->get('product_id')) {
                $c = rand(100000, 9999999);
                $row = $this->site->getProductByID($this->input->get('product_id'));
                $row->base_quantity = 1;
                $row->serialModal_serial = NULL;
                $row->base_unit = $row->unit;
                $row->base_unit_cost = $row->cost;
                $row->qty = 1;
                $row->option = NULL;
                $row->discount = 0;
                $row->tax_rate_2 = NULL;
                $row->product_unit_id_selected = $row->unit;
                $row->expiry = '';
                $row->real_unit_cost = $row->cost;
                $options = $this->purchases_model->getProductOptions($row->id);
                $units = $this->site->get_product_units($row->id);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                if ($tax_rate) {
                    if ($tax_rate->rate < 1 && $tax_rate->type == 2 && $item->item_tax > 0) {
                        $tax_rate->rate = $item->item_tax;
                    }
                }
                $tax_rate_2 = $this->site->getTaxRateByID($row->tax_rate_2);
                if ($tax_rate_2) {
                    if ($tax_rate_2->rate < 1 && $tax_rate_2->type == 2 && $item->item_tax_2 > 0) {
                        $tax_rate_2->rate = $item->item_tax_2;
                    }
                }
                $ri = $this->Settings->item_addition ? $row->id : $c;
                $pr = array(
                    'id' => $c,
                    'item_id' => $row->id,
                    'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row,
                    'tax_rate' => $tax_rate,
                    'tax_rate_2' => $tax_rate_2,
                    'units' => $units,
                    'options' => $options
                );
                $this->data['product_added'] = json_encode($pr);
            }
            if ($this->input->post()) { //SI VIENE DESDE UN SUBMIT, REDIRECCIONAMOS A LA PÁGINA EN QUE ESTABA PARA EL CASO DE ORDEN DE GASTO O COMPRA
                $this->session->set_flashdata('error', $this->data['error']);
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->page_construct('purchases/add', $meta, $this->data);
            }
        }
    }

    public function edit($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->purchases_model->getPurchaseByID($id);
        $purchase_has_payments = false;
        $purchase_has_multi_payments = false;
        $purchase_payments = NULL;
        $direct_paid_by = 'Credito';
        $direct_paid_amount = 0;
        if ($inv->status == 'received' && $inv->grand_total != ($inv->grand_total - $inv->paid)) {
            if ($purchase_payments = $this->purchases_model->getPurchasePayments($id, true)) {
                $purchase_has_payments = true;
                foreach ($purchase_payments as $pp) {
                    if ($pp->multi_payment == 1) {
                        $purchase_has_multi_payments = true;
                        break;
                    } else {
                        $direct_paid_by = $pp->paid_by;
                        $direct_paid_amount = $pp->amount;
                    }
                }
            }
        }
        if ($purchase_has_multi_payments) {
            $this->session->set_flashdata('error', lang('purchase_has_multi_payments'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }
        if (!$this->Owner && !$this->Admin && $inv->status != 'pending') {
            $this->session->set_flashdata('error', lang('cannot_edit_purchase_completed'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }

        else if ($inv->status == 'returned') {
            $this->session->set_flashdata('error', lang('purchase_x_action'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        } else if (!$this->session->userdata('edit_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->form_validation->set_message('is_natural_no_zero', $this->lang->line("no_zero_required"));
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        $this->form_validation->set_rules('warehouse', $this->lang->line("warehouse"), 'required|is_natural_no_zero');
        $this->form_validation->set_rules('supplier', $this->lang->line("supplier"), 'required');
        if ($this->Settings->management_consecutive_suppliers == 1) {
            $this->form_validation->set_rules('consecutive_supplier', lang("consecutive_supplier"), 'required');
        }
        $this->session->unset_userdata('csrf_token');
        $inv_items = $this->purchases_model->getAllPurchaseItems($id);
        $pitems = [];
        $product_has_movements_after_date = false;
        if ($this->form_validation->run() == true) {
            if ($this->Settings->purchase_datetime_management == 1) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = $this->input->post('date') . " " . date('H:i:s');
            }

            $purchase_date_changed = $this->input->post('purchase_date_changed');
            if ($purchase_date_changed != 1) {
                $date = date('Y-m-d H:i:s');
            }

            $warehouse_id = $this->input->post('warehouse');
            $supplier_id = $this->input->post('supplier');
            $biller_id = $this->input->post('biller');
            $prorate_shipping_cost = $this->input->post('prorate_shipping_cost');
            $pg_update_price = $this->input->post('pg_update_price');
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            $support_document = $this->input->post('support_document') ? true : false;
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false && $this->Settings->modulary == 1) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect('purchases/add');
            }
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $supplier_details = $this->site->getCompanyByID($supplier_id);
            $purchase_from_import = $this->input->post('purchase_from_import') ? 1 : 0;
            $tag_id = $this->input->post('tag');
            $supplier = ($supplier_details->company != '-' && $supplier_details->company != '')  ? $supplier_details->company : $supplier_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            if (strpos($note, '~') !== false) {
                $note = explode('~', $note);
                $note = $note[0];
            }
            $payment_status = $this->input->post('payment_status');
            $paid_by = $this->input->post('paid_by');
            if (($payment_status == 'partial' || $payment_status == 'paid') && $paid_by == "cash") {
                if ($this->Settings->purchase_payment_affects_cash_register == 2) {
                    if ($this->input->post('payment_affects_register') != null) {
                        $payment_affects_register = $this->input->post('payment_affects_register');
                    } else {
                        $this->session->set_flashdata('error', lang('affectation_not_indicated'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } else {
                    $payment_affects_register = $this->Settings->purchase_payment_affects_cash_register;
                }
            } else {
                $payment_affects_register = 0;
            }
            $payment_term = $this->input->post('purchase_payment_term');
            $due_date = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;
            $descuento_orden = $this->input->post('order_discount_method');
            $quote_id = $this->input->post('quote_id') ? $this->input->post('quote_id') : null;
            $ptype = $this->input->post('purchase_type') ? $this->input->post('purchase_type') : 1;
            $prev_reference = $this->input->post('prev_reference');
            $status = $this->input->post('status');
            $p_status = $status;
            if ($status == 'received' && $ptype == 2) {
                $p_status = 'service_received';
            }

            $document_type_id = $this->input->post('document_type_id');
            $reference = null;
            if ($this->input->post('reference_no')) {
                $reference = $this->input->post('reference_no');
            }
            $total = 0;
            $product_tax = 0;
            $item_tax = 0;
            $product_discount = 0;
            $i = sizeof($_POST['product_code']);
            $gst_data = [];
            $new_pv = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $assumed_retentions_text = "";
            //Retenciones
            if ($this->input->post('rete_applied')) {
                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');
                $rete_fuente_id = $this->input->post('rete_fuente_id');
                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');
                $rete_iva_id = $this->input->post('rete_iva_id');
                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');
                $rete_ica_id = $this->input->post('rete_ica_id');
                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');
                $rete_other_id = $this->input->post('rete_otros_id');
                $rete_fuente_assumed = $this->input->post('rete_fuente_assumed') ? 1 : 0;
                $rete_iva_assumed = $this->input->post('rete_iva_assumed') ? 1 : 0;
                $rete_ica_assumed = $this->input->post('rete_ica_assumed') ? 1 : 0;
                $rete_other_assumed = $this->input->post('rete_otros_assumed') ? 1 : 0;
                $rete_fuente_assumed_account = $this->input->post('rete_fuente_assumed_account');
                $rete_iva_assumed_account = $this->input->post('rete_iva_assumed_account');
                $rete_ica_assumed_account = $this->input->post('rete_ica_assumed_account');
                $rete_other_assumed_account = $this->input->post('rete_otros_assumed_account');

                $rete_bomberil_percentage = $this->input->post('rete_bomberil_tax');
                $rete_bomberil_total = $this->sma->formatDecimal($this->input->post('rete_bomberil_valor'));
                $rete_bomberil_account = $this->input->post('rete_bomberil_account');
                $rete_bomberil_assumed_account = $this->input->post('rete_bomberil_assumed_account');
                $rete_bomberil_base = $this->input->post('rete_bomberil_base');
                $rete_bomberil_id = $this->input->post('rete_bomberil_id');

                $rete_autoaviso_percentage = $this->input->post('rete_autoaviso_tax');
                $rete_autoaviso_total = $this->sma->formatDecimal($this->input->post('rete_autoaviso_valor'));
                $rete_autoaviso_account = $this->input->post('rete_autoaviso_account');
                $rete_autoaviso_assumed_account = $this->input->post('rete_autoaviso_assumed_account');
                $rete_autoaviso_base = $this->input->post('rete_autoaviso_base');
                $rete_autoaviso_id = $this->input->post('rete_autoaviso_id');


                $total_retenciones =
                    ($rete_fuente_assumed ? 0 : $rete_fuente_total) +
                    ($rete_iva_assumed ? 0 : $rete_iva_total) +
                    ($rete_ica_assumed ? 0 : $rete_ica_total) +
                    ($rete_ica_assumed ? 0 : $rete_bomberil_total) +
                    ($rete_ica_assumed ? 0 : $rete_autoaviso_total) +
                    ($rete_other_assumed ? 0 : $rete_other_total);

                if ($rete_fuente_assumed) {
                    $assumed_retentions_text .= "Rete Fuente asumida (" . $this->sma->formatMoney($rete_fuente_total) . "), ";
                }
                if ($rete_iva_assumed) {
                    $assumed_retentions_text .= "Rete IVA asumida(" . $this->sma->formatMoney($rete_iva_total) . "), ";
                }
                if ($rete_ica_assumed) {
                    $assumed_retentions_text .= "Rete ICA asumida(" . $this->sma->formatMoney($rete_ica_total) . "), ";
                    if ($rete_bomberil_total > 0) {
                        $assumed_retentions_text .= "Tasa Bomberil asumida(" . $this->sma->formatMoney($rete_bomberil_total) . "), ";
                    }
                    if ($rete_autoaviso_total > 0) {
                        $assumed_retentions_text .= "Tasa Avisos y Tableros asumida(" . $this->sma->formatMoney($rete_autoaviso_total) . "), ";
                    }
                }
                if ($rete_other_assumed) {
                    $assumed_retentions_text .= "Rete OTRAS asumida(" . $this->sma->formatMoney($rete_other_total) . "), ";
                }
                $assumed_retentions_text = trim($assumed_retentions_text, ", ");
                $rete_applied = TRUE;
            } else {
                $rete_applied = FALSE;
                $total_retenciones = 0;
            }
            //Retenciones
            $changes = "";
            $total_prorrated_shipping = 0;
            for ($r = 0; $r < $i; $r++) {
                $purchase_item_id = $_POST['purchase_item_id'][$r];
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_net_cost = $this->sma->formatDecimal($_POST['net_cost'][$r]);
                $unit_cost = $this->sma->formatDecimal($_POST['unit_cost'][$r]);
                $real_unit_cost = $this->sma->formatDecimal($_POST['real_unit_cost'][$r]);
                $item_unit_quantity = str_replace(",", "", $_POST['quantity'][$r]);
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' ? $_POST['product_option'][$r] : null;
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_unit_tax_val = isset($_POST['unit_product_tax'][$r]) ? $_POST['unit_product_tax'][$r] : null;
                $item_tax_2_rate = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : null;
                $item_unit_tax_2_val = isset($_POST['unit_product_tax_2'][$r]) ? $_POST['unit_product_tax_2'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit_discount = isset($_POST['product_unit_discount_val'][$r]) ? $_POST['product_unit_discount_val'][$r] : null;
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : null;
                $product_unit_id_selected = isset($_POST['product_unit_id_selected'][$r]) ? $_POST['product_unit_id_selected'][$r] : null;
                $profitability_margin = isset($_POST['profitability_margin'][$r]) ? $_POST['profitability_margin'][$r] : null;
                $item_expiry = (isset($_POST['expiry'][$r]) && !empty($_POST['expiry'][$r])) ? $this->sma->fsd($_POST['expiry'][$r]) : null;
                $supplier_part_no = (isset($_POST['part_no'][$r]) && !empty($_POST['part_no'][$r])) ? $_POST['part_no'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];
                $serialModal_serial = isset($_POST['serialModal_serial'][$r]) ? $_POST['serialModal_serial'][$r] : null;
                $shipping_unit_cost = isset($_POST['shipping_unit_cost'][$r]) ? $_POST['shipping_unit_cost'][$r] : null;

                $new_option_assigned = isset($_POST['new_option_assigned'][$r]) ? $_POST['new_option_assigned'][$r] : null;
                $new_option_assigned_code = isset($_POST['new_option_assigned_code'][$r]) ? $_POST['new_option_assigned_code'][$r] : null;
                $new_option_assigned_option_id = isset($_POST['new_option_assigned_option_id'][$r]) ? $_POST['new_option_assigned_option_id'][$r] : null;
                $new_option_assigned_name = isset($_POST['new_option_assigned_name'][$r]) ? $_POST['new_option_assigned_name'][$r] : null;
                $new_option_assigned_consecutive = isset($_POST['new_option_assigned_consecutive'][$r]) ? $_POST['new_option_assigned_consecutive'][$r] : null;
                $new_option_assigned_weight = isset($_POST['new_option_assigned_weight'][$r]) ? $_POST['new_option_assigned_weight'][$r] : null;

                if (isset($item_code) && isset($real_unit_cost) && isset($unit_cost) && isset($item_quantity)) {

                    $product_details = $this->purchases_model->getProductByCode($item_code);
                    if ($item_expiry) {
                        $today = date('Y-m-d');
                        if ($item_expiry <= $today) {
                            $this->session->set_flashdata('error', lang('product_expiry_date_issue') . ' (' . $product_details->name . ')');
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                    $pr_discount = $item_unit_discount;
                    $unit_cost = $this->sma->formatDecimal($unit_cost - $pr_discount);
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $this->sma->formatDecimal($item_unit_tax_val * $item_unit_quantity);
                    $pr_item_tax_2 = $this->sma->formatDecimal($item_unit_tax_2_val * $item_unit_quantity);
                    $item_tax = $item_unit_tax_val;
                    $item_tax_2 = $item_unit_tax_2_val;
                    $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                    $tax_details_2 = $this->site->getTaxRateByID($item_tax_2_rate);
                    $tax = $tax_2 = 0;
                    if ($item_tax_rate) {
                        $tax = $this->sma->formatDecimal($tax_details->rate, 0) . "%";
                        if ($tax_details->type == 2 && $tax_details->rate < 1 && $item_tax > 0) {
                            $tax = $item_tax;
                        }
                    }
                    if ($item_tax_2_rate && $tax_details_2) {
                        $tax_2 = $this->sma->formatDecimal($tax_details_2->rate, 0) . "%";
                        if ($tax_details_2->type == 2 && $tax_details_2->rate < 1 && $item_tax_2 > 0) {
                            $tax_2 = $item_tax_2;
                        }
                    }
                    $product_tax += $pr_item_tax + $pr_item_tax_2;
                    $subtotal = (($item_net_cost * $item_unit_quantity) + $pr_item_tax + $pr_item_tax_2);
                    $unit = $this->site->getUnitByID($item_unit);
                    if ($this->Settings->ignore_purchases_edit_validations == 0 && $this->purchases_model->product_has_movements_after_date($item_id, $date, $id)) {
                        // $this->session->set_flashdata('error', lang('purchase_new_date_before_movements'));
                        // admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
                    }
                    $product = array(
                        'id' => $purchase_item_id,
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'option_id' => $new_option_assigned ? NULL : $item_option,
                        'net_unit_cost' => $item_net_cost,
                        'unit_cost' => $this->sma->formatDecimal($item_net_cost + $item_tax + $item_tax_2 + $shipping_unit_cost),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $item_unit,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity' => $item_unit_quantity,
                        'quantity_balance' => $item_quantity,
                        'quantity_received' => $item_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'item_tax_2' => $pr_item_tax_2,
                        'tax_rate_2_id' => $item_tax_2_rate,
                        'tax_2' => $tax_2,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'expiry' => $item_expiry,
                        'real_unit_cost' => $real_unit_cost,
                        'date' => date('Y-m-d', strtotime($date)),
                        'status' => (isset($p_status) ? $p_status : $status),
                        'supplier_part_no' => $supplier_part_no,
                        'serial_no' => $serialModal_serial ? $serialModal_serial : $item_serial,
                        'product_unit_id_selected' => $product_unit_id_selected,
                        'shipping_unit_cost' => $shipping_unit_cost,
                        'profitability_margin' => $profitability_margin,
                    );
                    if ($new_option_assigned) {
                        $product['new_option_assigned'] = $item_id."_".$new_option_assigned_option_id;
                        $new_pv[$item_id."_".$new_option_assigned_option_id] = [
                            'product_id' => $item_id,
                            'code' => $new_option_assigned_code,
                            'name' => $new_option_assigned_name,
                            'pv_code_consecutive' => $new_option_assigned_consecutive,
                            'weight' => $new_option_assigned_weight,
                        ];
                    }
                    $total_prorrated_shipping +=($shipping_unit_cost * $item_quantity);
                    $items[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity));
                    if ($purchase_item_id != "undefined") {
                        $changes .= " (Edición) " . $item_name . " : ";
                        foreach ($product as $field => $field_value) {
                            if (!empty($pitems[$purchase_item_id]->{$field}) && !empty($field_value) && $pitems[$purchase_item_id]->{$field} != $field_value) {
                                if (is_numeric($field_value)) {
                                    $changes .= $field . " de " . $this->sma->formatQuantity(($pitems[$purchase_item_id]->{$field} > 0 ? $pitems[$purchase_item_id]->{$field} : 0)) . " a " . $this->sma->formatQuantity($field_value) . ", ";
                                } else {
                                    $changes .= $field . " de " . $pitems[$purchase_item_id]->{$field} . " a " . $field_value . ", ";
                                }
                            }
                        }
                    } else {
                        $changes .= " (Adición) " . $item_name . " cantidad " . $this->sma->formatQuantity($item_quantity);
                    }
                    $changes = trim($changes, ", ") . " || ";
                } else {
                    $this->session->set_flashdata('error', lang('some_items_doesnt_have_correct_data'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            if (empty($items)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                foreach ($items as $item) {
                    $item["status"] = ($status == 'partial') ? 'received' : $p_status;
                    $products[] = $item;
                }
                krsort($products);
            }
            $order_discount = $this->site->calculateDiscount($this->input->post('discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount));
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax));
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal(($prorate_shipping_cost == 1 ? 0 : $shipping)) - $order_discount + $total_prorrated_shipping));

            $actual_currency = $this->input->post('currency');
            $actual_currency_trm = $this->input->post('trm');
            $current_currency = $this->currency_model->get_currency_by_code($actual_currency);
            $default_currency = $this->currency_model->get_currency_by_code($this->Settings->default_currency);
            if ($actual_currency != $this->Settings->default_currency && $quote_id == null) {
                $note .= sprintf(
                    lang('diferent_currency_trm'),
                    $actual_currency,
                    $current_currency->name,
                    $this->sma->formatMoney($actual_currency_trm),
                    $this->Settings->default_currency,
                    $default_currency->name,
                    $this->sma->formatMoney($grand_total)
                );
            }
            $note .= " ~ " . sprintf(lang('purchase_edited_at_by'), date('d-m-Y H:i:s'), $this->session->userdata('username'));
            $data = array(
                'reference_no' => str_replace(" ", "", $reference),
                'date' => $date,
                'supplier_id' => $supplier_id,
                'supplier' => $supplier,
                'warehouse_id' => $warehouse_id,
                'note' => $note . "  " . $assumed_retentions_text,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $descuento_orden == 2 ? $this->input->post('discount') : '',
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $prorate_shipping_cost == 1 ? 0 : $this->sma->formatDecimal($shipping),
                'prorated_shipping_cost' => $prorate_shipping_cost == 1 ? $this->sma->formatDecimal($total_prorrated_shipping) : 0,
                'grand_total' => $grand_total,
                'status' => $status,
                'payment_status' => $payment_status,
                'payment_term' => $payment_term,
                'due_date' => $due_date,
                'order_discount_method' => $descuento_orden,
                'purchase_currency' => $actual_currency,
                'purchase_currency_trm' => $actual_currency_trm,
                'purchase_type' => $ptype,
                'payment_affects_register' => $payment_affects_register,
                'biller_id' => $biller_id,
                'document_type_id' => $document_type_id,
                'purchase_type' => $inv->purchase_type,
                'purchase_from_import' => $purchase_from_import,
                'tag_id' => $tag_id,
                'consecutive_supplier' => $this->Settings->management_consecutive_suppliers ? $this->input->post('consecutive_supplier') : $inv->consecutive_supplier
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }

            if ($support_document) {
                $document_type = $this->site->getDocumentTypeById($document_type_id);
                $resolucion = "";
                if ($document_type->save_resolution_in_sale == 1) {
                    $resolucion = $this->site->textoResolucion($document_type);
                    $data['resolucion'] = $resolucion;
                }

                $data['generationTransmissionMode'] = $this->input->post('generationTransmissionMode');
            }

            $payments = [];
            $payment_reference_no = $this->input->post('payment_reference_no');
            $j = count($_POST['amount-paid']);
            $cash_amount_payment = 0;
            if ($j > 0 && ($status == 'received')) {
                for ($i = 0; $i < $j; $i++) {
                    if (($ptype == 2 || $ptype == 3) && $_POST['paid_by'][$i] == 'expense_causation') {
                        $data['expense_causation'] = 1;
                        continue;
                    }
                    if ($_POST['due_payment'][$i] == 1) {
                        $pm = $this->site->getPaymentMethodByCode($_POST['paid_by'][$i]);
                        $data['due_payment_method_id'] = $pm->id;
                    } else {
                        if (isset($_POST['paid_by'])) {
                            if ($_POST['paid_by'][$i] == 'cash') {
                                $cash_amount_payment += $_POST['amount-paid'][$i];
                            }
                            $paid_by = $_POST['paid_by'][$i];
                            $payment = array(
                                'date' => $date,
                                'amount' => $this->sma->formatDecimal($_POST['amount-paid'][$i]),
                                'paid_by' => $_POST['paid_by'][$i],
                                'cheque_no' => $_POST['cheque_no'][$i],
                                'cc_no' => $_POST['pcc_no'][$i],
                                'cc_holder' => $_POST['pcc_holder'][$i],
                                'cc_month' => $_POST['pcc_month'][$i],
                                'cc_year' => $_POST['pcc_year'][$i],
                                'cc_type' => $_POST['pcc_type'][$i],
                                // 'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                'note' => $_POST['payment_note'][$i],
                                'type' => 'sent',
                                'document_type_id' => $payment_reference_no,
                            );
                            $payments[] = $payment;
                            //IVA PROPORCIONAL
                            if ($this->Settings->tax_rate_traslate && $data['total_tax'] > 0) {
                                $purchase_taxes = [];
                                foreach ($products as $row) {
                                    $proporcion_pago = ($_POST['amount-paid'][$i] * 100) / $data['grand_total'];
                                    $total_iva = ($row['item_tax'] + $row['item_tax_2']) * ($proporcion_pago / 100);
                                    if (!isset($purchase_taxes[$row['tax_rate_id']])) {
                                        $purchase_taxes[$row['tax_rate_id']] = $total_iva;
                                    } else {
                                        $purchase_taxes[$row['tax_rate_id']] += $total_iva;
                                    }
                                }
                                $tax_rate_traslate_ledger_id = $this->site->getPaymentMethodParameter('tax_rate_traslate');
                                $data_tax_rate_traslate = $data_taxrate_traslate = array(
                                    'tax_rate_traslate_ledger_id' => $tax_rate_traslate_ledger_id->payment_ledger_id,
                                    'purchase_taxes' => $purchase_taxes,
                                );
                            }
                        }
                            
                    }
                }
            } else if ($j > 0 && ($status == 'pending')) {
                $data['suggested_paid_by'] = $_POST['paid_by'][0];
                $data['suggested_paid_amount'] = $_POST['paid_by'][0] != 'Credito' ? $_POST['amount-paid'][0] : 0;
            }
            if ($rete_applied) {
                $data['rete_fuente_percentage'] = $rete_fuente_percentage;
                $data['rete_fuente_total'] = $rete_fuente_total;
                $data['rete_fuente_account'] = $rete_fuente_account;
                $data['rete_fuente_base'] = $rete_fuente_base;
                $data['rete_fuente_id'] = $rete_fuente_id;
                $data['rete_iva_percentage'] = $rete_iva_percentage;
                $data['rete_iva_total'] = $rete_iva_total;
                $data['rete_iva_account'] = $rete_iva_account;
                $data['rete_iva_base'] = $rete_iva_base;
                $data['rete_iva_id'] = $rete_iva_id;
                $data['rete_ica_percentage'] = $rete_ica_percentage;
                $data['rete_ica_total'] = $rete_ica_total;
                $data['rete_ica_account'] = $rete_ica_account;
                $data['rete_ica_base'] = $rete_ica_base;
                $data['rete_ica_id'] = $rete_ica_id;
                $data['rete_other_percentage'] = $rete_other_percentage;
                $data['rete_other_total'] = $rete_other_total;
                $data['rete_other_account'] = $rete_other_account;
                $data['rete_other_base'] = $rete_other_base;
                $data['rete_other_id'] = $rete_other_id;
                $data['rete_fuente_assumed'] = $rete_fuente_assumed;
                $data['rete_iva_assumed'] = $rete_iva_assumed;
                $data['rete_ica_assumed'] = $rete_ica_assumed;
                $data['rete_other_assumed'] = $rete_other_assumed;
                $data['rete_bomberil_percentage'] = $rete_bomberil_percentage;
                $data['rete_bomberil_total'] = $rete_bomberil_total;
                $data['rete_bomberil_account'] = $rete_bomberil_account;
                $data['rete_bomberil_base'] = $rete_bomberil_base;
                $data['rete_bomberil_id'] = $rete_bomberil_id;
                $data['rete_autoaviso_percentage'] = $rete_autoaviso_percentage;
                $data['rete_autoaviso_total'] = $rete_autoaviso_total;
                $data['rete_autoaviso_account'] = $rete_autoaviso_account;
                $data['rete_autoaviso_base'] = $rete_autoaviso_base;
                $data['rete_autoaviso_id'] = $rete_autoaviso_id;
                $data['rete_fuente_assumed_account'] = $rete_fuente_assumed ? $rete_fuente_assumed_account : NULL;
                $data['rete_iva_assumed_account'] = $rete_iva_assumed ? $rete_iva_assumed_account : NULL;
                $data['rete_ica_assumed_account'] = $rete_ica_assumed ? $rete_ica_assumed_account : NULL;
                $data['rete_bomberil_assumed_account'] = $rete_ica_assumed ? $rete_bomberil_assumed_account : NULL;
                $data['rete_autoaviso_assumed_account'] = $rete_ica_assumed ? $rete_autoaviso_assumed_account : NULL;
                $data['rete_other_assumed_account'] = $rete_other_assumed ? $rete_other_assumed_account : NULL;
                $payment = array(
                    'date'         => $date,
                    // 'reference_no' => $this->site->getReference('ppay'),
                    'amount'       => $total_retenciones,
                    'reference_no' => 'retencion',
                    'purchase_id' => $inv->id,
                    'paid_by'      => 'retencion',
                    'cheque_no'    => '',
                    'cc_no'        => '',
                    'cc_holder'    => '',
                    'cc_month'     => '',
                    'cc_year'      => '',
                    'cc_type'      => '',
                    'created_by'   => $this->session->userdata('user_id'),
                    'type'         => 'sent',
                    'note'         => 'Retenciones',
                );
                $payments[] = $payment;
            }
            $total_payment = 0;
            if (count($payments) > 0) {
                foreach ($payments as $payment) {
                    $total_payment += $payment['amount'];
                }
            }
            if ($total_payment == $data['grand_total']) {
                $data['payment_status'] = 'paid';
                $data['payment_term'] = 0;
            } else if ($total_payment < $data['grand_total']) {
                if ($total_payment == 0) {
                    $data['payment_status'] = 'pending';
                } else if ($total_payment > 0) {
                    $data['payment_status'] = 'partial';
                }
                $data['payment_term'] = $this->input->post('purchase_payment_term');
                $due_date = $this->input->post('purchase_payment_term') ? date('Y-m-d', strtotime('+' . $this->input->post('purchase_payment_term') . ' days', strtotime($date))) : null;
                $data['due_date'] = $due_date;
                if ($this->input->post('purchase_payment_term') <= 0 && $status == 'received' && !isset($data['expense_causation'])) {
                    $this->session->set_flashdata('error', lang("payment_term_almost_be_greather_than_zero"));
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
            $payment_affects_register = 0;
            if (($data['payment_status'] == 'partial' || $data['payment_status'] == 'paid') && $cash_amount_payment > 0) {
                if ($this->Settings->purchase_payment_affects_cash_register == 2) {
                    if ($this->input->post('payment_affects_register') != null) {
                        $payment_affects_register = $this->input->post('payment_affects_register');
                    } else {
                        $this->session->set_flashdata('error', lang('affectation_not_indicated'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } else {
                    $payment_affects_register = $this->Settings->purchase_payment_affects_cash_register;
                }
            } else {
                $payment_affects_register = 0;
            }
            $data['payment_affects_register'] = $payment_affects_register;
            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            $txt_fields_changed = sprintf(lang('user_fields_changed'), $this->session->first_name . " " . $this->session->last_name, lang($this->m), $id) . $changes;
            // $this->sma->print_arrays($data, $products, $payments, $new_pv);
        }
        if ($this->form_validation->run() == true && $this->purchases_model->updatePurchase($id, $data, $products, $payments, $prev_reference, $new_pv, $pg_update_price)) {
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 1,
                'table_name' => 'purchases',
                'record_id' => $id,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $txt_fields_changed,
            ]);
            $this->session->set_userdata('remove_pols', 1);

            if ($data['status'] == 'pending') {
                $msg_warn = lang('purchase_pending');
                if ($this->Settings->modulary) {
                    $msg_warn .= lang('purchase_pending_not_accounted');
                }
                $this->session->set_flashdata('warning', $msg_warn);
            } else {
                if ($this->Settings->supportingDocument == YES && $support_document) {
                    $resolutionData = $this->SupportDocument_model->getResolutionData($this->input->post('document_type_id'));

                    if ($resolutionData->factura_electronica == YES) {
                        $this->SupportDocument_model->sendElectronicDocument($id);
                    }
                }
            }

            $this->session->set_flashdata('message', $this->lang->line("purchase_updated"));

            if ($this->Settings->supportingDocument == YES && $support_document) {
                admin_redirect('purchases/supporting_document_index');
            } else {
                admin_redirect('purchases');
            }
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['inv'] = $inv;
            if ($this->Settings->disable_editing) {
                if ($inv->date <= date('Y-m-d', strtotime('-' . $this->Settings->disable_editing . ' days'))) {
                    $this->session->set_flashdata('error', sprintf(lang("purchase_x_edited_older_than_x_days"), $this->Settings->disable_editing));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            $c = rand(100000, 9999999);
            $edit_status = 1;
            foreach ($inv_items as $item) {
                if ($inv->purchase_type == 1) {
                    $row = $this->site->getProductByID($item->product_id);
                } else {
                    $row = $this->site->getExpenseCategory($item->product_id);
                }
                // $row = $this->site->getProductByID($item->product_id);
                $row->expiry = (($item->expiry && $item->expiry != '0000-00-00') ? $this->sma->hrsd($item->expiry) : '');
                $row->id = $item->product_id;
                $row->purchase_item_id = $item->id;
                $row->code = $item->product_code;
                $row->name = $item->product_name;
                $row->base_quantity = $item->quantity;
                $row->base_unit = isset($row->unit) ? $row->unit : $item->product_unit_id;
                $row->base_unit_cost = ($item->unit_cost ? $item->unit_cost + $item->item_discount : $row->unit_cost) - $item->shipping_unit_cost;
                $row->unit = $item->product_unit_id;
                $row->qty = $item->quantity;
                $row->option = $item->option_id;
                $row->discount = $item->item_discount > 0 ? (string) ($item->item_discount / $item->quantity) : '0';
                $row->cost = $item->net_unit_cost;
                $row->tax_rate = $item->tax_rate_id;
                $row->tax_rate_2 = $item->tax_rate_2_id;
                $row->shipping_unit_cost = $item->shipping_unit_cost;
                $row->expiry = '';
                $row->real_unit_cost = $item->real_unit_cost;
                $row->product_unit_id_selected = $item->product_unit_id_selected ? $item->product_unit_id_selected : $item->product_unit_id;
                $row->product_unit_id = $item->product_unit_id;
                if ($this->Settings->product_variant_per_serial == 1) {
                    $options = false;
                } else {
                    $options = $this->purchases_model->getProductOptions($row->id);
                }
                $units = $this->site->get_product_units($row->id);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                if ($tax_rate) {
                    if ($tax_rate->rate < 1 && $tax_rate->type == 2 && $item->item_tax > 0) {
                        $tax_rate->rate = $item->item_tax;
                    }
                }
                if ($inv->purchase_type == 1) {
                    $tax_rate_2 = false;
                } else {
                    $tax_rate_2 = $this->site->getTaxRateByID($row->tax_rate_2);
                    if ($tax_rate_2) {
                        if ($tax_rate_2->rate < 1 && $tax_rate_2->type == 2 && $item->item_tax_2 > 0) {
                            $tax_rate_2->rate = $item->item_tax_2;
                        }
                    }
                }
                $row->oqty = $item->quantity;
                $row->supplier_part_no = $item->supplier_part_no;
                $row->received = $item->quantity_received ? $item->quantity_received : $item->quantity;
                $row->quantity_balance = $item->quantity_balance + ($item->quantity - $row->received);
                if ($this->Settings->product_variant_per_serial == 1) {
                    $row->serialModal_serial = $item->serial_no;
                }
                if ($item->quantity_balance < $item->quantity) {
                    $edit_status = 0;
                }
                $row->expense_tax_rate_id = $row->tax_rate;
                $row->edit_item = true;
                if ($this->Settings->ignore_purchases_edit_validations == 0) {
                    if ($this->purchases_model->product_has_movements_after_date($item->product_id, $inv->date, $id) || ($row->quantity_balance < $row->qty)) {
                        $product_has_movements_after_date = true;
                        $row->edit_item = false;
                    }
                }
                $ri = $this->Settings->item_addition ? $item->id : $c;
                $product_price_groups = $this->products_model->getProductPriceGroups($row->id);
                $pr[$ri] = array(
                    'id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'tax_rate' => $tax_rate, 'tax_rate_2' => $tax_rate_2, 'units' => $units, 'options' => $options, 'order' => round(microtime(true) * 1000), 'product_price_groups' => $product_price_groups
                );
                $c++;
            }
            // $this->sma->print_arrays($pr);
            if ($inv->status == 'received') {
                $edit_status = 0;
            }
            $this->data['inv_items'] = json_encode($pr);
            // exit(var_dump($product_has_movements_after_date));
            $this->data['product_has_movements_after_date'] = $product_has_movements_after_date;
            $this->data['purchase_has_payments'] = $purchase_has_payments;
            $this->data['purchase_has_multi_payments'] = $purchase_has_multi_payments;
            $this->data['id'] = $id;
            $this->data['suppliers'] = $this->site->getAllCompanies('supplier');
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['tags'] = $this->site->get_tags_by_module(4);
            if ($this->Settings->management_weight_in_variants == 1) {
                $this->data['variants'] = $this->products_model->getAllVariants();
            }

            $trates = $this->site->getAllTaxRates();
            $tax_rates = [];
            foreach ($trates as $key => $tr) {
                $tax_rates[$tr->id] = $tr;
            }
            $this->data['tax_rates'] = $tax_rates;
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['ponumber'] = ''; //$this->site->getReference('po');
            $this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['edit_status'] = $edit_status;
            $this->data['purchase_payments'] = $purchase_payments;
            $this->data['direct_paid_by'] = $direct_paid_by;
            $this->data['direct_paid_amount'] = $direct_paid_amount;
            $this->data['price_groups'] = $this->site->getAllPriceGroups();
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $this->load->helper('string');
            $value = random_string('alnum', 20);
            $this->session->set_userdata('user_csrf', $value);
            $this->session->set_userdata('remove_pols', 1);
            $this->data['csrf'] = $this->session->userdata('user_csrf');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('purchases'), 'page' => lang('purchases')), array('link' => '#', 'page' => lang('edit_purchase')));
            $meta = array('page_title' => lang('edit_purchase'), 'bc' => $bc);
            $this->page_construct('purchases/edit', $meta, $this->data);
        }
    }

    public function purchase_by_csv()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_message('is_natural_no_zero', $this->lang->line("no_zero_required"));
        $this->form_validation->set_rules('warehouse', $this->lang->line("warehouse"), 'required|is_natural_no_zero');
        $this->form_validation->set_rules('supplier', $this->lang->line("supplier"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        if ($this->Settings->cost_center_selection == 1 && $this->Settings->modulary == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }
        if ($this->Settings->management_consecutive_suppliers == 1) {
            $this->form_validation->set_rules('consecutive_supplier', lang("consecutive_supplier"), 'required');
        }
        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $warehouse_id = $this->input->post('warehouse');
            $supplier_id = $this->input->post('supplier');
            $biller_id = $this->input->post('biller');
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false && $this->Settings->modulary == 1) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect('purchases/add');
            }
            $status = $this->input->post('status');
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $supplier_details = $this->site->getCompanyByID($supplier_id);
            $supplier = $supplier_details->name != '-'  ? $supplier_details->name : $supplier_details->company;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $paid_by = $this->input->post('paid_by');
            $descuento_orden = $this->input->post('order_discount_method');
            $quote_id = $this->input->post('quote_id') ? $this->input->post('quote_id') : null;
            $ptype = $this->input->post('purchase_type') ? $this->input->post('purchase_type') : 1;
            $consecutive_supplier = $this->input->post('consecutive_supplier');
            $prorate_shipping_cost = $this->input->post('prorate_shipping_cost');
            if ($ptype == 1) {
                $p_prefix =  $this->site->getReference('po', true);
                $p_reference = 'po';
            } else {
                $p_status = 'service_received';
                $p_prefix =  $this->site->getReference('ex', true);
                $p_reference = 'ex';
            }
            $document_type_id = $this->input->post('document_type_id');
            $reference = null;
            if ($this->input->post('reference_no')) {
                $reference = $this->input->post('reference_no');
            }
            $total = 0;
            $product_tax = 0;
            $consumption_purchase = 0;
            $item_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            //Retenciones
            $rete_data = [];
            if ($this->input->post('rete_applied')) {
                $rete_data['rete_fuente_percentage'] = $this->input->post('rete_fuente_tax');
                $rete_data['rete_fuente_account'] = $this->input->post('rete_fuente_account');
                $rete_data['rete_fuente_id'] = $this->input->post('rete_fuente_id');
                $rete_data['rete_fuente_apply'] = $this->input->post('rete_fuente_apply');
                $rete_data['rete_fuente_base'] = NULL;
                $rete_data['rete_fuente_total'] = NULL;
                $rete_data['rete_iva_percentage'] = $this->input->post('rete_iva_tax');
                $rete_data['rete_iva_account'] = $this->input->post('rete_iva_account');
                $rete_data['rete_iva_id'] = $this->input->post('rete_iva_id');
                $rete_data['rete_iva_apply'] = $this->input->post('rete_iva_apply');
                $rete_data['rete_iva_base'] = NULL;
                $rete_data['rete_iva_total'] = NULL;
                $rete_data['rete_ica_percentage'] = $this->input->post('rete_ica_tax');
                $rete_data['rete_ica_account'] = $this->input->post('rete_ica_account');
                $rete_data['rete_ica_id'] = $this->input->post('rete_ica_id');
                $rete_data['rete_ica_apply'] = $this->input->post('rete_ica_apply');
                $rete_data['rete_ica_base'] = NULL;
                $rete_data['rete_ica_total'] = NULL;
                $rete_data['rete_other_percentage'] = $this->input->post('rete_otros_tax');
                $rete_data['rete_other_account'] = $this->input->post('rete_otros_account');
                $rete_data['rete_other_id'] = $this->input->post('rete_otros_id');
                $rete_data['rete_other_apply'] = $this->input->post('rete_otros_apply');
                $rete_data['rete_other_base'] = NULL;
                $rete_data['rete_other_total'] = NULL;
                $rete_applied = TRUE;
            } else {
                $rete_applied = FALSE;
            }
            if (isset($_FILES["userfile"])) {
                $archivo = $_FILES['userfile']['tmp_name'];
                $this->load->library('excel');
                $invalids_products = false;
                $objPHPExcel = PHPExcel_IOFactory::load($archivo);
                $hojas = $objPHPExcel->getWorksheetIterator();
                $rw = 0;
                foreach ($hojas as $hoja) {
                    $rw ++;
                    $data = $hoja->toArray();
                    unset($data[0]);
                    foreach ($data as $data_row) {
                        if (empty($data_row[0])) {
                            continue;
                        }
                        $csv_pr['product_code'] = $data_row[0];
                        $csv_pr['unit_cost'] = $data_row[1];
                        $csv_pr['quantity'] = $data_row[2];
                        $csv_pr['product_variant'] = $data_row[3];
                        $csv_pr['item_tax_rate'] = $data_row[4];
                        $csv_pr['discount'] = $data_row[5];
                        if (isset($csv_pr['product_code']) && isset($csv_pr['unit_cost']) && isset($csv_pr['quantity'])) {
                            if ($product_details = $this->purchases_model->getProductByCode($csv_pr['product_code'])) {
                                if ($product_details->attributes == 1 && empty($csv_pr['product_variant'])) {
                                    $this->session->set_flashdata('error', lang("variant_required") . " ( " . $product_details->name . " ). " . lang("line_no") . " " . $rw);
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                                if ($csv_pr['product_variant'] && $this->Settings->product_variant_per_serial == 0) {
                                    $item_option = $this->purchases_model->getProductVariantByName($csv_pr['product_variant'], $product_details->id);
                                    if (!$item_option) {
                                        $this->session->set_flashdata('error', lang("variant_not_found") . " ( " . $product_details->name . " - " . $csv_pr['product_variant'] . " ). " . lang("line_no") . " " . $rw);
                                        redirect($_SERVER["HTTP_REFERER"]);
                                    }
                                } else {
                                    $item_option = json_decode('{}');
                                    $item_option->id = null;
                                }
                                $item_code = $csv_pr['product_code'];
                                $item_unit_cost = $this->sma->formatDecimal($csv_pr['unit_cost']);
                                $item_quantity = $csv_pr['quantity'];
                                $quantity_balance = $csv_pr['quantity'];
                                $item_tax_rate = $csv_pr['item_tax_rate'];
                                $item_discount = $csv_pr['discount'];
                                $item_serial = ($this->Settings->product_variant_per_serial == 1 ? $csv_pr['product_variant'] : NULL);
                                $tax = "";
                                $pr_item_tax = 0;
                                $gst_data = [];
                                $tax_details = ((isset($item_tax_rate) && !empty($item_tax_rate)) ? $this->purchases_model->getTaxRateByCode($item_tax_rate) : $this->site->getTaxRateByID($product_details->tax_rate));
                                if ($tax_details) {
                                    if ($product_details->tax_method == 0) {
                                        $item_net_cost = $this->sma->remove_tax_from_amount($tax_details->id, $item_unit_cost);
                                        $item_tax = $item_unit_cost - $item_net_cost;
                                    } else {
                                        $item_net_cost = $item_unit_cost;
                                        $ctax = $this->site->calculateTax($product_details, $tax_details, $item_unit_cost);
                                        $item_tax = $ctax['amount'];
                                        $item_unit_cost += $item_tax;
                                    }
                                    $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);
                                    $tax = $this->sma->formatDecimal($tax_details->rate) . "%";
                                } else {
                                    $this->session->set_flashdata('error', 'Código de tarifa de IVA inválido, por favor revise la tabla que se muestra en el formulario');
                                    admin_redirect("purchases/purchase_by_csv");
                                }
                                $pr_item_discount = 0;
                                $pr_discount = 0;
                                if ($item_discount) {
                                    $pr_discount = $this->site->calculateDiscount($item_discount, $item_net_cost);
                                    $pr_item_discount = $this->sma->formatDecimal(($pr_discount * $item_quantity));
                                    $product_discount += $pr_item_discount;
                                    $item_net_cost = $item_net_cost - $pr_discount;
                                    $ctax = $this->site->calculateTax($product_details, $tax_details, $item_net_cost, NULL, 1);
                                    $item_tax = $ctax['amount'];
                                    $tax = $ctax['tax'];
                                    $item_unit_cost = $item_net_cost + $item_tax;
                                    $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);
                                } else {
                                    $item_unit_cost = $item_net_cost + $item_tax;
                                }

                                $item_tax_2 = NULL;
                                $tax_2 = NULL;
                                $pr_item_tax_2 = NULL;
                                if ($this->Settings->ipoconsumo == 1 && $product_details->consumption_purchase_tax > 0) {
                                    $item_tax_2 = $product_details->consumption_purchase_tax;
                                    $tax_2 = $product_details->consumption_purchase_tax;
                                    $pr_item_tax_2 = $this->sma->formatDecimal($item_tax_2 * $item_quantity);
                                    $consumption_purchase += $pr_item_tax_2;
                                }
                                $product_tax += $pr_item_tax + $pr_item_tax_2;
                                $subtotal = $this->sma->formatDecimal(((($item_net_cost * $item_quantity) + $pr_item_tax + $pr_item_tax_2)));
                                $unit = $this->site->getUnitByID($product_details->unit);
                                $product = array(
                                    'product_id' => $product_details->id,
                                    'product_code' => $item_code,
                                    'product_name' => $product_details->name,
                                    'option_id' => $item_option->id,
                                    'net_unit_cost' => $item_net_cost,
                                    'unit_cost' => $this->sma->formatDecimal(($item_net_cost + $item_tax + $item_tax_2)),
                                    'quantity' => $item_quantity,
                                    'product_unit_id' => $product_details->unit,
                                    'product_unit_code' => isset($unit->code) ? $unit->code : NULL,
                                    'unit_quantity' => $item_quantity,
                                    'quantity_balance' => $quantity_balance,
                                    'warehouse_id' => $warehouse_id,
                                    'item_tax' => $pr_item_tax,
                                    'tax_rate_id' => $tax_details ? $tax_details->id : null,
                                    'tax' => $tax,
                                    'item_tax_2' => $pr_item_tax_2,
                                    'tax_rate_2_id' => $item_tax_2,
                                    'tax_2' => $tax_2,
                                    'discount' => $item_discount,
                                    'item_discount' => $pr_item_discount,
                                    'expiry' => null,
                                    'subtotal' => $subtotal,
                                    'date' => date('Y-m-d', strtotime($date)),
                                    'status' => $status,
                                    'real_unit_cost' => $this->sma->formatDecimal(($item_net_cost + $item_tax + $pr_discount)),
                                    'consumption_purchase' => $pr_item_tax_2,
                                    'shipping_unit_cost' => null,
                                    'serial_no' => $item_serial,
                                    'original_tax_rate_id' => null,
                                    'supplier_part_no' => null
                                );
                                $products[] = $product;
                                $total += $this->sma->formatDecimal(($item_net_cost * $item_quantity));
                            } else {
                                $this->session->set_flashdata('error', $this->lang->line("pr_not_found") . " ( " . $csv_pr['code'] . " ). " . $this->lang->line("line_no") . " " . $rw);
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                            $rw++;
                        }
                    }
                }
            }
            if ($descuento_orden == 2) { //DESCUENTO GLOBAL
                $order_discount = $this->site->calculateDiscount($this->input->post('discount'), ($total));
                $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount));
            } else { // DESCUENTO A CADA PRODUCTO
                $order_discount = 0;
                $total_discount = 0;
            }
            $actual_currency = $this->input->post('currency');
            $actual_currency_trm = $this->input->post('trm');
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), (($descuento_orden == 1 ? $total - $order_discount : $total + $product_discount)));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax));
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount));
            $current_currency = $this->currency_model->get_currency_by_code($actual_currency);
            $default_currency = $this->currency_model->get_currency_by_code($this->Settings->default_currency);
            if ($actual_currency != $this->Settings->default_currency && $quote_id == null) {
                $note .= sprintf(
                    lang('diferent_currency_trm'),
                    $actual_currency,
                    $current_currency->name,
                    $this->sma->formatMoney($actual_currency_trm),
                    $this->Settings->default_currency,
                    $default_currency->name,
                    $this->sma->formatMoney($grand_total)
                );
            }
            //aumenta costo unitario, pero no subtotal del producto, aumenta el grand_total de la compra
            if ($shipping > 0 && $prorate_shipping_cost == 1) {
                foreach ($products as $pr_key => $pr) {
                    $pshipping_gtotal = $grand_total + $order_discount - $shipping;
                    $pr['quantity'] = $this->sma->formatDecimal(($pr['quantity']));
                    if ($pshipping_gtotal > 0) {
                        $pshipping_peso = ((((($pr['unit_cost']) * $pr['quantity'])) * 100) / $pshipping_gtotal);
                        $pshipping_peso = (($pshipping_peso / 100));
                        $shipping = $shipping * $pshipping_peso;
                        $shipping = $shipping / $pr['quantity'];
                    }
                    $pshipping_item = $this->sma->formatDecimal($shipping, ($this->Settings->rounding == 1 ? $this->Settings->decimals : 4));
                    $products[$pr_key]['shipping_unit_cost'] = ($pshipping_item);
                    $products[$pr_key]['unit_cost'] = $products[$pr_key]['unit_cost'] + $pshipping_item;
                }
            }
            $nota_shipping = "";
            if ($shipping > 0 && $prorate_shipping_cost == 1) {
                $nota_shipping = " El valor del flete por " . $this->sma->formatMoney($shipping) . " ha sido distribuido en los costos de los productos, el valor de la factura descontando los fletes es de " . $this->sma->formatMoney($grand_total - $shipping);
            }
            $data = array(
                'reference_no' => $reference,
                'date' => $date,
                'supplier_id' => $supplier_id,
                'supplier' => $supplier,
                'warehouse_id' => $warehouse_id,
                'note' => $note . $nota_shipping,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $descuento_orden == 2 ? $this->input->post('discount') : '',
                'order_discount' => $order_discount,
                'total_discount' => $total_discount + $product_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->Settings->prorate_shipping_cost == 1 ? 0 : $this->sma->formatDecimal($shipping),
                'prorated_shipping_cost' => $this->Settings->prorate_shipping_cost == 1 ? $this->sma->formatDecimal($shipping) : 0,
                'grand_total' => $this->sma->formatDecimal($grand_total, 2),
                'status' => $status,
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'order_discount_method' => $descuento_orden,
                'purchase_currency' => $actual_currency,
                'purchase_currency_trm' => $actual_currency_trm,
                'purchase_type' => $ptype,
                'biller_id' => $biller_id,
                'consecutive_supplier' => $consecutive_supplier,
                'document_type_id' => $document_type_id,
                'purchase_origin' => $this->input->post('purchase_origin'),
                'purchase_origin_reference_no' => $this->input->post('purchase_origin_reference_no'),
                'consumption_purchase' => $consumption_purchase,
                'update_by_csv' => 1,
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }


            $payments = [];
            $total_retenciones = 0;

            if ($rete_applied) {
                if ($rete_data['rete_fuente_percentage'] > 0) {
                    $amount_rete = 0;
                    if ($rete_data['rete_fuente_apply'] == 'TX') {
                        $amount_rete = $data['total_tax'] - $data['consumption_purchase'];
                    } else if ($rete_data['rete_fuente_apply'] == 'TO') {
                        $amount_rete = $data['grand_total'];
                    } else if ($rete_data['rete_fuente_apply'] == 'ST') {
                        $amount_rete = $data['total'];
                    }
                    $rete_data['rete_fuente_base'] = $amount_rete;
                    $rete_total = $amount_rete * ($rete_data['rete_fuente_percentage'] / 100);
                    $rete_data['rete_fuente_total'] = $rete_total;
                    $total_retenciones += $rete_total;
                }

                if ($rete_data['rete_iva_percentage'] > 0) {
                    $amount_rete = 0;
                    if ($rete_data['rete_iva_apply'] == 'TX') {
                        $amount_rete = $data['total_tax'] - $data['consumption_purchase'];
                    } else if ($rete_data['rete_iva_apply'] == 'TO') {
                        $amount_rete = $data['grand_total'];
                    } else if ($rete_data['rete_iva_apply'] == 'ST') {
                        $amount_rete = $data['total'];
                    }
                    $rete_data['rete_iva_base'] = $amount_rete;
                    $rete_total = $amount_rete * ($rete_data['rete_iva_percentage'] / 100);
                    $rete_data['rete_iva_total'] = $rete_total;
                    $total_retenciones += $rete_total;
                }

                if ($rete_data['rete_ica_percentage'] > 0) {
                    $amount_rete = 0;
                    if ($rete_data['rete_ica_apply'] == 'TX') {
                        $amount_rete = $data['total_tax'] - $data['consumption_purchase'];
                    } else if ($rete_data['rete_ica_apply'] == 'TO') {
                        $amount_rete = $data['grand_total'];
                    } else if ($rete_data['rete_ica_apply'] == 'ST') {
                        $amount_rete = $data['total'];
                    }
                    $rete_data['rete_ica_base'] = $amount_rete;
                    $rete_total = $amount_rete * ($rete_data['rete_ica_percentage'] / 100);
                    $rete_data['rete_ica_total'] = $rete_total;
                    $total_retenciones += $rete_total;
                }

                if ($rete_data['rete_other_percentage'] > 0) {
                    $amount_rete = 0;
                    if ($rete_data['rete_other_apply'] == 'TX') {
                        $amount_rete = $data['total_tax'] - $data['consumption_purchase'];
                    } else if ($rete_data['rete_other_apply'] == 'TO') {
                        $amount_rete = $data['grand_total'];
                    } else if ($rete_data['rete_other_apply'] == 'ST') {
                        $amount_rete = $data['total'];
                    }
                    $rete_data['rete_other_base'] = $amount_rete;
                    $rete_total = $amount_rete * ($rete_data['rete_other_percentage'] / 100);
                    $rete_data['rete_other_total'] = $rete_total;
                    $total_retenciones += $rete_total;
                }

                // if (isset($rete_data['rete_fuente_total'])) {
                $data['rete_fuente_percentage'] = $rete_data['rete_fuente_percentage'];
                $data['rete_fuente_total'] = $rete_data['rete_fuente_total'];
                $data['rete_fuente_account'] = $rete_data['rete_fuente_account'];
                $data['rete_fuente_base'] = $rete_data['rete_fuente_base'];
                $data['rete_fuente_id'] = $rete_data['rete_fuente_id'];
                // }
                // if (isset($rete_data['rete_iva_total'])) {
                $data['rete_iva_percentage'] = $rete_data['rete_iva_percentage'];
                $data['rete_iva_total'] = $rete_data['rete_iva_total'];
                $data['rete_iva_account'] = $rete_data['rete_iva_account'];
                $data['rete_iva_base'] = $rete_data['rete_iva_base'];
                $data['rete_iva_id'] = $rete_data['rete_iva_id'];
                // }
                // if (isset($rete_data['rete_ica_total'])) {
                $data['rete_ica_percentage'] = $rete_data['rete_ica_percentage'];
                $data['rete_ica_total'] = $rete_data['rete_ica_total'];
                $data['rete_ica_account'] = $rete_data['rete_ica_account'];
                $data['rete_ica_base'] = $rete_data['rete_ica_base'];
                $data['rete_ica_id'] = $rete_data['rete_ica_id'];
                // }
                // if (isset($rete_data['rete_other_total'])) {
                $data['rete_other_percentage'] = $rete_data['rete_other_percentage'];
                $data['rete_other_total'] = $rete_data['rete_other_total'];
                $data['rete_other_account'] = $rete_data['rete_other_account'];
                $data['rete_other_base'] = $rete_data['rete_other_base'];
                $data['rete_other_id'] = $rete_data['rete_other_id'];
                // }
                $payment = array(
                    'date'         => $date,
                    'amount'       => $total_retenciones,
                    'reference_no' => 'retencion',
                    'paid_by'      => 'retencion',
                    'cheque_no'    => '',
                    'cc_no'        => '',
                    'cc_holder'    => '',
                    'cc_month'     => '',
                    'cc_year'      => '',
                    'cc_type'      => '',
                    'created_by'   => $this->session->userdata('user_id'),
                    'type'         => 'sent',
                    'note'         => 'Retenciones',
                );
                $payments[] = $payment;
            }


            $payment_reference_no = $this->input->post('payment_reference_no');
            $j = count($_POST['paid_by']);
            $cash_amount_payment = 0;
            $amount_to_pay = $data['grand_total'] - $total_retenciones;
            if ($j > 0 && $status == 'received') {
                for ($i = 0; $i < $j; $i++) {
                    if (($ptype == 2 || $ptype == 3) && $_POST['paid_by'][$i] == 'expense_causation') {
                        $data['expense_causation'] = 1;
                        continue;
                    }
                    if ($_POST['due_payment'][$i] == 1) {
                        $pm = $this->site->getPaymentMethodByCode($_POST['paid_by'][$i]);
                        $data['due_payment_method_id'] = $pm->id;
                    } else {
                        if ($_POST['paid_by'][$i] == 'cash') {
                            $cash_amount_payment += $amount_to_pay;
                        }
                        if (empty($_POST['paid_by'][$i])) {
                            $this->session->set_flashdata('error', lang("invalid_payment_method"));
                            admin_redirect($_SERVER['HTTP_REFERER']);
                        }
                        $paid_by = $_POST['paid_by'][$i];
                        $payment = array(
                            'date' => $date,
                            'amount' => $this->sma->formatDecimal($amount_to_pay),
                            'paid_by' => $_POST['paid_by'][$i],
                            'cheque_no' => $_POST['cheque_no'][$i],
                            'cc_no' => $_POST['pcc_no'][$i],
                            'cc_holder' => $_POST['pcc_holder'][$i],
                            'cc_month' => $_POST['pcc_month'][$i],
                            'cc_year' => $_POST['pcc_year'][$i],
                            'cc_type' => $_POST['pcc_type'][$i],
                            'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                            'note' => $_POST['payment_note'][$i],
                            'type' => 'sent',
                            'document_type_id' => $payment_reference_no,
                        );
                        $payments[] = $payment;
                        //IVA PROPORCIONAL
                        if ($this->Settings->tax_rate_traslate && $data['total_tax'] > 0) {
                            $purchase_taxes = [];
                            foreach ($products as $row) {
                                $proporcion_pago = ($amount_to_pay * 100) / $data['grand_total'];
                                $total_iva = ($row['item_tax'] + $row['item_tax_2']) * ($proporcion_pago / 100);
                                if (!isset($purchase_taxes[$row['tax_rate_id']])) {
                                    $purchase_taxes[$row['tax_rate_id']] = $total_iva;
                                } else {
                                    $purchase_taxes[$row['tax_rate_id']] += $total_iva;
                                }
                            }
                            $tax_rate_traslate_ledger_id = $this->site->getPaymentMethodParameter('tax_rate_traslate');
                            $data_tax_rate_traslate = $data_taxrate_traslate = array(
                                'tax_rate_traslate_ledger_id' => $tax_rate_traslate_ledger_id->payment_ledger_id,
                                'purchase_taxes' => $purchase_taxes,
                            );
                        }
                    }
                }
            }

            $total_payment = 0;
            if (count($payments) > 0) {
                foreach ($payments as $payment) {
                    $total_payment += $payment['amount'];
                }
            }
            $data['payment_status'] = 'pending';
            if ($data['status'] == 'received' && $total_payment == $data['grand_total']) {
                $data['payment_status'] = 'paid';
                $data['payment_term'] = 0;
            } else if ($data['status'] == 'received' && $total_payment < $data['grand_total']) {
                if ($total_payment == 0) {
                    $data['payment_status'] = 'pending';
                } else if ($total_payment > 0) {
                    $data['payment_status'] = 'partial';
                }
                $data['payment_term'] = $_POST['payment_term'][0];
                $due_date = $_POST['payment_term'][0] ? date('Y-m-d', strtotime('+' . $_POST['payment_term'][0] . ' days', strtotime($date))) : null;
                $data['due_date'] = $due_date;
                if ($_POST['payment_term'][0] <= 0 && !isset($data['expense_causation'])) {
                    $this->session->set_flashdata('error', lang("payment_term_almost_be_greather_than_zero"));
                    admin_redirect($_SERVER['HTTP_REFERER']);
                }
            }
            $payment_affects_register = 0;
            if (($data['payment_status'] == 'partial' || $data['payment_status'] == 'paid') && $cash_amount_payment > 0) {
                if ($this->Settings->purchase_payment_affects_cash_register == 2) {
                    if ($this->input->post('payment_affects_register') != null) {
                        $payment_affects_register = $this->input->post('payment_affects_register');
                    } else {
                        $this->session->set_flashdata('error', lang('affectation_not_indicated'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } else {
                    $payment_affects_register = $this->Settings->purchase_payment_affects_cash_register;
                }
            } else {
                $payment_affects_register = 0;
            }
            $data['payment_affects_register'] = $payment_affects_register;
            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            // $this->sma->print_arrays($data, $products, $payments);
        }

        if ($this->form_validation->run() == true && $this->purchases_model->addPurchase($data, $products, $payments)) {
            $this->session->set_flashdata('message', $this->lang->line("purchase_added"));
            admin_redirect("purchases");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['suppliers'] = $this->site->getAllCompanies('supplier');
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['ponumber'] = ''; //$this->site->getReference('po');
            $this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['brands'] = $this->site->getAllBrands();
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $this->data['reference'] = (isset($this->data['quote']) && $this->data['quote']->quote_type == 2 ? $this->site->getReference('ex', true) : $this->site->getReference('po', true)) . "-";

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('purchases'), 'page' => lang('purchases')), array('link' => '#', 'page' => lang('add_purchase_by_csv')));
            $meta = array('page_title' => lang('add_purchase_by_csv'), 'bc' => $bc);
            $this->page_construct('purchases/purchase_by_csv', $meta, $this->data);
        }
    }

    public function delete($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->purchases_model->deletePurchase($id)) {
            if ($this->input->is_ajax_request()) {
                $this->sma->send_json(array('error' => 0, 'msg' => lang("purchase_deleted")));
            }
            $this->session->set_flashdata('message', lang('purchase_deleted'));
            admin_redirect('welcome');
        }
    }

    public function suggestions()
    {
        $term = $this->input->get('term', true);
        $supplier_id = $this->input->get('supplier_id', true);
        $biller_id = $this->input->get('biller_id', true);
        $support_document = $this->input->get('support_document', true);
        $purchase_type = $this->input->get('purchase_type', true);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }
        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];

        if ($purchase_type == 1) {
            $rows = $this->purchases_model->getProductNames($sr, $biller_id, $supplier_id);
            if ($rows) {
                $r = 0;
                foreach ($rows as $row) {
                    $c = uniqid(mt_rand(), true);
                    $option = false;
                    $row->item_tax_method = $row->tax_method;
                    if ($this->Settings->product_variant_per_serial == 1) {
                        $options = false;
                    } else {
                        $options = $this->purchases_model->getProductOptions($row->id);
                    }
                    if ($options) {
                        $opt = $option_id && $r == 0 ? $this->purchases_model->getProductOptionByID($option_id) : current($options);
                        if ($row->based_on_gram_value == 0 && (!$option_id || $r > 0)) {
                            $option_id = $opt->id;
                        }
                    } else {
                        $opt = json_decode('{}');
                        $opt->cost = 0;
                        $option_id = FALSE;
                    }

                    if (isset($row->variant_selected)) {
                        $row->variant_selected = $row->variant_selected;
                        $row->option = $row->variant_selected;
                    } else {
                        $row->option = $option_id;
                    }
                    $row->supplier_part_no = '';
                    if ($row->supplier1 == $supplier_id) {
                        $row->supplier_part_no = $row->supplier1_part_no;
                    } elseif ($row->supplier2 == $supplier_id) {
                        $row->supplier_part_no = $row->supplier2_part_no;
                    } elseif ($row->supplier3 == $supplier_id) {
                        $row->supplier_part_no = $row->supplier3_part_no;
                    } elseif ($row->supplier4 == $supplier_id) {
                        $row->supplier_part_no = $row->supplier4_part_no;
                    } elseif ($row->supplier5 == $supplier_id) {
                        $row->supplier_part_no = $row->supplier5_part_no;
                    }
                    // if ($opt->cost != 0) {
                    //     $row->cost = $opt->cost;
                    // }
                    $row->serial = '';
                    $row->cost = $supplier_id ? $this->getSupplierCost($supplier_id, $row) : $row->cost;
                    $row->real_unit_cost = $row->cost;
                    $row->prev_unit_cost = $row->cost + $row->consumption_purchase_tax;
                    $row->prev_price = $row->price;
                    $row->base_quantity = 1;
                    $row->base_unit = $row->unit;
                    $row->base_unit_cost = $row->cost;
                    $row->unit = $row->purchase_unit ? $row->purchase_unit : $row->unit;
                    $row->product_unit_id_selected = $row->purchase_unit ? $row->purchase_unit : $row->unit;
                    $row->new_entry = 1;
                    $row->expiry = '';
                    $row->qty = 1;
                    $row->quantity_balance = '';
                    $row->discount = '0';
                    $row->edit_item = true;
                    unset($row->details, $row->product_details, $row->price, $row->file, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    $cnt_units_prices =  $this->site->get_all_product_units_prices($row->id) ? count($this->site->get_all_product_units_prices($row->id)) + 1 : 1;
                    $row->cnt_units_prices = $cnt_units_prices;
                    $units = $this->site->get_product_units($row->id);
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    // $tax_rate = NULL;
                    if ($this->Settings->prioridad_precios_producto == 5 && $units[$row->unit]->operation_value > 0) {
                        $row->base_quantity = $units[$row->unit]->operation_value;
                        $row->qty = $units[$row->unit]->operation_value;
                    }
                    if (!$this->Settings->ipoconsumo) {
                        $row->consumption_purchase_tax = 0;
                    }
                    $product_price_groups = $this->products_model->getProductPriceGroups($row->id);
                    $pr[] = array('id' => sha1($c . $r), 'item_id' => ($row->id.($row->option)), 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options, 'product_price_groups' => $product_price_groups);
                    $r++;
                }
                $this->sma->send_json($pr);
            } else {
                $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
            }
        } else {

            $rows = $this->purchases_model->getExpensesCategoriesNames($term, $support_document, $purchase_type);

            if ($rows) {
                foreach ($rows as $row) {
                    $c = uniqid(mt_rand(), true);
                    $option = false;
                    $options = FALSE;
                    $row->tax_method = "1";
                    $opt = json_decode('{}');
                    $opt->cost = 0;
                    $option_id = FALSE;
                    $row->option = $option_id;
                    $row->supplier_part_no = '';
                    $row->real_unit_cost = $row->cost;
                    $row->base_quantity = 1;
                    $row->base_unit = 1;
                    $row->base_unit_cost = $row->cost;
                    $row->unit = 1;
                    $row->new_entry = 1;
                    $row->expiry = '';
                    $row->qty = 1;
                    $row->type = 'manual';
                    $row->quantity_balance = '';
                    $row->discount = '0';
                    unset($row->details, $row->product_details, $row->price, $row->file, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);

                    $units = $this->site->get_product_units($row->id);
                    $row->expense_tax_rate_id = $row->tax_rate;
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $tax_rate_2 = $this->site->getTaxRateByID($row->tax_rate_2);
                    $category_expense = $this->db->where('code', $row->code)->get('expense_categories')->row();
                    $pr[] = array(
                        'id' => $row->id, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                        'row' => $row, 'tax_rate' => $tax_rate, 'tax_rate_2' => $tax_rate_2, 'units' => $units, 'options' => $options
                    );
                }
            } else {
                $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
            }
            $this->sma->send_json($pr);
        }
    }

    public function purchase_actions()
    {

        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete');
                    foreach ($_POST['val'] as $id) {
                        $this->purchases_model->deletePurchase($id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("purchases_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('form_action') == 'combine') {

                    $html = $this->combine_pdf($_POST['val']);
                } elseif ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('purchases'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('Hora'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('supplier'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('status'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('grand_total'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('paid'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('balance'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('payment_status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $purchase = $this->purchases_model->getPurchaseByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrsd($purchase->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, date("H:i:s", strtotime($purchase->date)));
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $purchase->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $purchase->supplier);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $purchase->status);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatMoney($purchase->grand_total));
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $this->sma->formatMoney($purchase->paid));
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $this->sma->formatMoney($purchase->grand_total - $purchase->grand_total));
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $purchase->payment_status);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);

                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'purchases_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                } elseif ($this->input->post('form_action') == 'sync_payments') {
                    foreach ($_POST['val'] as $id) {
                        $this->site->syncPurchasePayments($id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("purchases_payments_synchronized"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } else if ($this->input->post('form_action') == 'post_purchase') {

                    $msg = '';
                    foreach ($_POST['val'] as $id) {
                        $msg .= $this->purchases_model->recontabilizarCompra($id);
                    }
                    if ($this->session->userdata('reaccount_error')) {
                        $this->session->set_flashdata('error', $msg);
                        $this->session->unset_userdata('reaccount_error');
                    } else {
                        $this->session->set_flashdata('message', $msg);
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_purchase_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function payments($id = null)
    {
        $this->sma->checkPermissions(false, true);

        $this->data['payments'] = $this->purchases_model->getPurchasePayments($id);
        $this->data['inv'] = $this->purchases_model->getPurchaseByID($id);
        $this->load_view($this->theme . 'purchases/payments', $this->data);
    }

    public function payment_note($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $payment = $this->purchases_model->getPaymentByID($id);
        $inv = $this->purchases_model->getPurchaseByID($payment->purchase_id);
        $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['payment'] = $payment;
        $this->data['page_title'] = $this->lang->line("payment_note");

        $this->load_view($this->theme . 'purchases/payment_note', $this->data);
    }

    public function email_payment($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $payment = $this->purchases_model->getPaymentByID($id);
        $inv = $this->purchases_model->getPurchaseByID($payment->purchase_id);
        $supplier = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['inv'] = $inv;
        $this->data['payment'] = $payment;
        if (!$supplier->email) {
            $this->sma->send_json(array('msg' => lang("update_supplier_email")));
        }
        $this->data['supplier'] = $supplier;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['payment'] = $payment;
        $this->data['page_title'] = lang("payment_note");
        $html = $this->load_view($this->theme . 'purchases/payment_note', $this->data, TRUE);

        $html = str_replace(array('<i class="fa fa-2x">&times;</i>', 'modal-', '<p>&nbsp;</p>', '<p style="border-bottom: 1px solid #666;">&nbsp;</p>', '<p>' . lang("stamp_sign") . '</p>'), '', $html);
        $html = preg_replace("/<img[^>]+\>/i", '', $html);
        // $html = '<div style="border:1px solid #DDD; padding:10px; margin:10px 0;">'.$html.'</div>';

        $this->load->library('parser');
        $parse_data = array(
            'stylesheet' => '<link href="' . $this->data['assets'] . 'styles/helpers/bootstrap.min.css" rel="stylesheet"/>',
            'name' => $supplier->company && $supplier->company != '-' ? $supplier->company :  $supplier->name,
            'email' => $supplier->email,
            'heading' => lang('payment_note') . '<hr>',
            'msg' => $html,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
        );
        $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/email_con.html');
        $message = $this->parser->parse_string($msg, $parse_data);
        $subject = lang('payment_note') . ' - ' . $this->Settings->site_name;

        if ($this->sma->send_email($supplier->email, $subject, $message)) {
            $this->sma->send_json(array('msg' => lang("email_sent")));
        } else {
            $this->sma->send_json(array('msg' => lang("email_failed")));
        }
    }

    public function add_payment($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $this->load->helper('security');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $purchase = $this->purchases_model->getPurchaseByID($id);
        if ($purchase->payment_status == 'paid' && $purchase->grand_total == $purchase->paid) {
            $this->session->set_flashdata('error', lang("purchase_already_paid"));
            $this->sma->md();
        } else if ($purchase->status == 'pending') {
            $this->session->set_flashdata('error', lang("cannot_register_payment_purchase_pending"));
            $this->sma->md();
        }

        $this->form_validation->set_rules('document_type_id', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->input->post('trm_difference')) {
            $this->form_validation->set_rules('trm_difference_operator', lang('trm_difference_operator'), 'required');
        }

        if ($this->form_validation->run() == true) {

            $paid_by = $this->input->post('paid_by');

            if ($paid_by == "cash") {
                if ($this->Settings->purchase_payment_affects_cash_register == 2) {
                    if ($this->input->post('payment_affects_register') != null) {
                        $payment_affects_register = $this->input->post('payment_affects_register');
                    } else {
                        $this->session->set_flashdata('error', lang('affectation_not_indicated'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } else {
                    $payment_affects_register = $this->Settings->purchase_payment_affects_cash_register;
                }
            }

            if ($this->Owner || $this->Admin) {
                if ($this->input->post('date')) {
                    $date = $this->sma->fld(trim($this->input->post('date')));
                } else {
                    $date = date('Y-m-d H:i:s');
                }
            } else {
                $date = date('Y-m-d H:i:s');
            }

            $pagadoBB = $this->input->post('amount-paid') + (isset($total_retenciones) ? $total_retenciones : 0);
            $saldoBB = ceil($purchase->grand_total - $purchase->paid);

            if ($pagadoBB == $saldoBB) {
                $pagadoBB = $purchase->grand_total - $purchase->paid;
            } else {
                $pagadoBB = $this->input->post('amount-paid');
            }

            // exit("Pagado : ".$pagadoBB.", Balance : ".$saldoBB);

            if ($pagadoBB > ($purchase->grand_total - $purchase->paid)) {
                $this->session->set_flashdata('error', lang('mount_paid_greather_than_balance'));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            $biller_id = $purchase->biller_id;
            $document_type_id = $this->input->post('document_type_id');
            $referenceBiller = $this->site->getReferenceBiller($biller_id, $document_type_id);

            if ($referenceBiller) {
                $reference = $referenceBiller;
            } else {
                $reference = $this->site->getReference('ppay');
            }

            $payment = array(
                'date' => $date,
                'purchase_id' => $this->input->post('purchase_id'),
                'reference_no' => $reference,
                'amount' => $pagadoBB,
                'paid_by' => $paid_by,
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->sma->clear_tags($this->input->post('note')),
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'type' => 'sent',
                'document_type_id' => $document_type_id,
            );

            if ($this->Settings->cost_center_selection != 2 && $this->Settings->modulary == 1) {

                if ($purchase->cost_center_id > 0) { //SI LA COMPRA YA TIENE CENTRO DE COSTO DEFINIDO

                    $payment['cost_center_id'] = $purchase->cost_center_id;
                } else { //SI NO TIENE CENTRO DE COSTO

                    if ($this->Settings->cost_center_selection == 0) { //SI EL CENTRO SE DEFINE POR SUCURSAL

                        if ($purchase->biller_id == NULL && $this->input->post('biller')) {
                            $biller_id = $this->input->post('biller');
                        } else if ($purchase->biller_id > 0) {
                            $biller_id = $purchase->biller_id;
                        }

                        $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
                        if ($biller_cost_center) {
                            $payment['cost_center_id'] = $biller_cost_center->id;
                        } else {
                            $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                            admin_redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else if ($this->Settings->cost_center_selection == 1) { //SI EL CENTRO SE DEFINE ESCOGIÉNDOLO

                        if ($this->input->post('cost_center_id')) {
                            $payment['cost_center_id'] = $this->input->post('cost_center_id');
                        } else {
                            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
                        }
                    }
                }
            }

            if ($trm_difference = $this->input->post('trm_difference')) {

                $trm_difference_operator = $this->input->post('trm_difference_operator');

                $payment['trm_difference'] =  $trm_difference_operator == '+' ? $trm_difference : $trm_difference * -1;
                $trm_difference_ledger_id = $this->site->getPaymentMethodParameter('trm_difference');
                $payment['trm_difference_ledger_id'] = $trm_difference_ledger_id ? $trm_difference_ledger_id->payment_ledger_id : 0;
            }

            //IVA PROPORCIONAL

            if ($this->Settings->tax_rate_traslate && $purchase->total_tax > 0) {

                $inv_items = $this->purchases_model->getAllPurchaseItems($id);

                $purchase_taxes = [];

                foreach ($inv_items as $row) {

                    $proporcion_pago = ($pagadoBB * 100) / $purchase->grand_total;
                    $total_iva = ($row->item_tax + $row->item_tax_2) * ($proporcion_pago / 100);

                    if (!isset($purchase_taxes[$row->tax_rate_id])) {
                        $purchase_taxes[$row->tax_rate_id] = $total_iva;
                    } else {
                        $purchase_taxes[$row->tax_rate_id] += $total_iva;
                    }
                }

                $tax_rate_traslate_ledger_id = $this->site->getPaymentMethodParameter('tax_rate_traslate');
                $data_tax_rate_traslate = $data_taxrate_traslate = array(
                    'tax_rate_traslate_ledger_id' => $tax_rate_traslate_ledger_id->payment_ledger_id,
                    'purchase_taxes' => $purchase_taxes,
                );

                // exit(var_dump($data_tax_rate_traslate));
            }

            //IVA PROPORCIONAL

            if ($this->Settings->cashier_close != 2 && $paid_by == "cash") {
                if ($payment_affects_register) {
                    if (!$this->pos_model->getRegisterState($pagadoBB)) {
                        $this->session->set_flashdata('error', lang('not_enough_cash_register'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } else {
                    $payment['type'] = 'sent_2';
                }
            }


            // if ($payment_affects_register && !$this->pos_model->getRegisterState($pagadoBB)) {
            //     $this->session->set_flashdata('error', lang('not_enough_cash_register'));
            //     redirect($_SERVER["HTTP_REFERER"]);
            // } else {
            //     $payment['type'] = 'sent_2';
            // }
            // else {
            //     redirect($_SERVER["HTTP_REFERER"]);
            // }

            //Retenciones
            if ($this->input->post('rete_applied') == 1) {

                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');

                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');

                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');

                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');

                $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total;

                // $payment['amount'] -= $total_retenciones;

                $retencion = [];
                $retencion['rete_fuente_percentage'] = $rete_fuente_percentage;
                $retencion['rete_fuente_total'] = $rete_fuente_total;
                $retencion['rete_fuente_account'] = $rete_fuente_account;
                $retencion['rete_fuente_base'] = $rete_fuente_base;
                $retencion['rete_iva_percentage'] = $rete_iva_percentage;
                $retencion['rete_iva_total'] = $rete_iva_total;
                $retencion['rete_iva_account'] = $rete_iva_account;
                $retencion['rete_iva_base'] = $rete_iva_base;
                $retencion['rete_ica_percentage'] = $rete_ica_percentage;
                $retencion['rete_ica_total'] = $rete_ica_total;
                $retencion['rete_ica_account'] = $rete_ica_account;
                $retencion['rete_ica_base'] = $rete_ica_base;
                $retencion['rete_other_percentage'] = $rete_other_percentage;
                $retencion['rete_other_total'] = $rete_other_total;
                $retencion['rete_other_account'] = $rete_other_account;
                $retencion['rete_other_base'] = $rete_other_base;
                $retencion['total_retenciones'] = $total_retenciones;

                // exit('Total Fuente : '.$rete_fuente_total."\n Total IVA : ".$rete_iva_total."\n Total ICA : ".$rete_ica_total."\n Total Otros : ".$rete_other_total."\n Total Retención : ".$total_retenciones);
            }
            //Retenciones

            $data = array();

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('add_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->purchases_model->addPayment($payment, (isset($retencion) ? $retencion : null), (isset($data_taxrate_traslate) ? $data_taxrate_traslate : null))) {
            $this->session->set_flashdata('message', lang("payment_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {


            if ($purchase->rete_fuente_total != 0 || $purchase->rete_iva_total != 0 || $purchase->rete_ica_total != 0 || $purchase->rete_other_total != 0) {
                $rete_applied = true;
            } else {
                $rete_applied = false;
            }

            $this->data['rete_applied'] = $rete_applied;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['inv'] = $purchase;
            $this->data['payment_ref'] = ''; //$this->site->getReference('ppay');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');

            if ($this->Settings->cost_center_selection != 2 && $purchase->cost_center_id == NULL) {
                if ($this->Settings->cost_center_selection == 1) {
                    $this->data['cost_centers'] = $this->site->getAllCostCenters();
                }
            }

            $this->load_view($this->theme . 'purchases/add_payment', $this->data);
        }
    }

    public function edit_payment($id = null)
    {
        $this->sma->checkPermissions('edit', true);
        $this->load->helper('security');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $payment = array(
                'date' => $date,
                'purchase_id' => $this->input->post('purchase_id'),
                'reference_no' => $this->input->post('reference_no'),
                'amount' => $this->input->post('amount-paid'),
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->sma->clear_tags($this->input->post('note')),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('edit_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->purchases_model->updatePayment($id, $payment)) {
            $this->session->set_flashdata('message', lang("payment_updated"));
            admin_redirect("purchases");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['payment'] = $this->purchases_model->getPaymentByID($id);
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load_view($this->theme . 'purchases/edit_payment', $this->data);
        }
    }

    public function delete_payment($id = null)
    {
        $this->sma->checkPermissions('delete', true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->purchases_model->deletePayment($id)) {
            //echo lang("payment_deleted");
            $this->session->set_flashdata('message', lang("payment_deleted"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function expenses($id = null)
    {
        // $this->sma->print_arrays($this->session->userdata());
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('expenses')));
        $meta = array('page_title' => lang('expenses'), 'bc' => $bc);

        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['documents_types'] = $this->site->get_multi_module_document_types([36]);
        $this->data['users'] = $this->site->get_all_users();
        $this->data['categories'] = $this->purchases_model->getExpenseCategories();
        $this->page_construct('purchases/expenses', $meta, $this->data);
    }

    public function getExpenses()
    {
        $this->sma->checkPermissions('expenses');

        if ($this->input->post('start_date')) {
            $start_date = $this->input->post('start_date');
            $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }
        if ($this->input->post('end_date')) {
            $end_date = $this->input->post('end_date');
            $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date = date('Y-m-d H:i');
            }
        }
        $document_type_id = NULL;
        if ($this->input->post('pexppayment_reference_no')) {
            $document_type_id = $this->input->post('pexppayment_reference_no');
        }
        $payment_method = NULL;
        if ($this->input->post('pexppayment_method')) {
            $payment_method = $this->input->post('pexppayment_method');
        }
        $biller_id = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $supplier_id = $this->input->post('supplier') ? $this->input->post('supplier') : NULL;
        $category = $this->input->post('category') ? $this->input->post('category') : NULL;


        $detail_link = anchor('admin/purchases/expense_note/$1', '<i class="fa fa-file-text-o"></i> ' . lang('expense_note'), 'data-toggle="modal" data-target="#myModal2"');
        $edit_link = anchor('admin/purchases/edit_expense/$1', '<i class="fa fa-edit"></i> ' . lang('edit_expense'), 'data-toggle="modal" data-target="#myModal"');
        //$attachment_link = '<a href="'.base_url('assets/uploads/$1').'" target="_blank"><i class="fa fa-chain"></i></a>';
        $delete_link = "<a href='#' class='po' title='<b>" . $this->lang->line("delete_expense") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('purchases/delete_expense/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
            . lang('delete_expense') . "</a>";
        $contabilizar_gasto = '<a class="reaccount_expense_link" data-invoiceid="$1"><i class="fas fa-sync-alt"></i> ' . lang('post_expense') . ' </a>';
        $action = '<div class="text-center">
                    <div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions')
            . '<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>' . $detail_link . '</li>
                            <li>' . $edit_link . '</li>' .
            ($this->Owner || $this->Admin ? '<li>' . $delete_link . '</li>' : '') .
            ($this->Owner || $this->Admin ? '<li>' . $contabilizar_gasto . '</li>' : '')
            . '</ul>
                    </div>
                </div>';

        $this->load->library('datatables');

        $this->datatables
            ->select($this->db->dbprefix('expenses') . ".id as id, date, reference, {$this->db->dbprefix('expense_categories')}.name as category, amount, note, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user, attachment", false)
            ->from('expenses')
            ->join('users', 'users.id=expenses.created_by', 'left')
            ->join('expense_categories', 'expense_categories.id=expenses.category_id', 'left')
            ->group_by('expenses.id');

        /* FILTROS */

        if ($start_date) {
            // $this->db->where('expenses.date >= ', $start_date);
            $this->datatables->where("CAST({$this->db->dbprefix('expenses')}.date AS DATE) >=", $start_date);
        }
        if ($end_date) {
            // $this->db->where('expenses.date <= ', $end_date);
            $this->datatables->where("CAST({$this->db->dbprefix('expenses')}.date AS DATE) <=", $end_date);
        }
        if ($document_type_id) {
            $this->db->where('expenses.document_type_id', $document_type_id);
        }
        if ($user_id) {
            $this->db->where('expenses.created_by', $user_id);
        }
        if ($payment_method) {
            $this->db->where('expenses.paid_by', $payment_method);
        }
        if ($biller_id) {
            $this->db->where('expenses.biller_id', $biller_id);
        }
        if ($supplier_id) {
            $this->db->where('expenses.supplier_id', $supplier_id);
        }
        if ($category) {
            $this->db->where('expenses.category_id', $category);
        }

        /* FILTROS */

        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }
        //$this->datatables->edit_column("attachment", $attachment_link, "attachment");
        $this->datatables->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }

    public function expense_note($id = null)
    {
        $expense = $this->purchases_model->getExpenseByID($id);
        $this->data['user'] = $this->site->getUser($expense->created_by);
        $this->data['category'] = $expense->category_id ? $this->purchases_model->getExpenseCategoryByID($expense->category_id) : NULL;
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller',  $expense->biller_id, false);
        $this->data['supplier'] = $expense->supplier_id ? $this->site->getCompanyByID($expense->supplier_id) : NULL;
        $this->data['expense'] = $expense;
        $this->data['page_title'] = $this->lang->line("expense_note");
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($expense->cost_center_id);
        }
        $this->load_view($this->theme . 'purchases/expense_note', $this->data);
    }

    public function add_expense()
    {

        $this->sma->checkPermissions('expenses', true);
        $this->load->helper('security');

        //$this->form_validation->set_rules('reference', lang("reference"), 'required');
        $this->form_validation->set_rules('amount', lang("amount"), 'required');
        $this->form_validation->set_rules('category', lang("category"), 'required');
        $this->form_validation->set_rules('supplier', lang("supplier"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->Settings->cost_center_selection == 1 && $this->Settings->modulary == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }

        if ($this->form_validation->run() == true) {

            if ($this->session->userdata('post_sending')) {
                if ($this->session->userdata('post_sending_saved')) {
                    $this->session->set_flashdata('error', lang('cannot_resend_post_saved'));
                    redirect($_SERVER["HTTP_REFERER"]);
                } else {
                    $this->session->set_flashdata('warning', lang('cannot_resend_post'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_userdata('post_sending', 1);
            }

            if ($this->Settings->cashier_close != 2) {
                if (!$this->pos_model->getRegisterState($this->input->post('amount'))) {
                    $this->session->set_flashdata('error', lang('not_enough_cash_register'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }

            $date = date('Y-m-d H:i:s');

            $ptax_value = $this->input->post('exp_ptax_value') ? $this->input->post('exp_ptax_value') : 0;
            $ptax_2_value = $this->input->post('exp_ptax_2_value') ? $this->input->post('exp_ptax_2_value') : 0;
            $amount = $this->input->post('amount');

            $amount = $amount + $ptax_value + $ptax_2_value;

            $biller_id = $this->input->post('biller', true);
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);

            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false && $this->Settings->modulary == 1) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect($_SERVER["HTTP_REFERER"]);
            }

            $data = array(
                'date' => $date,
                'amount' => $amount,
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'note' => $this->input->post('note', true),
                'category_id' => $this->input->post('category', true),
                'biller_id' => $this->input->post('biller', true),
                'tax_rate_id' => $this->input->post('exp_ptax'),
                'tax_val' =>  $ptax_value,
                'tax_rate_2_id' =>  $this->input->post('exp_ptax_2'),
                'tax_val_2' =>  $ptax_2_value,
                'supplier_id' =>  $this->input->post('supplier'),
                'document_type_id' => $this->input->post('document_type_id'),
            );

            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }

            $item = array(
                'tax_rate_id' => $this->input->post('exp_ptax'),
                'item_tax' =>  $ptax_value,
                'tax_rate_2_id' =>  $this->input->post('exp_ptax_2'),
                'item_tax_2' =>  $ptax_2_value,
                'net_unit_cost' =>  $this->input->post('amount'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            //$this->sma->print_arrays($data);

            if ($this->input->post('exp_rete_applied')) {
                $rete_fuente_percentage = $this->input->post('exp_rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('exp_rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('exp_rete_fuente_account');
                $rete_fuente_base = $this->input->post('exp_rete_fuente_base');
                $rete_fuente_id = $this->input->post('exp_rete_fuente_id');

                $rete_iva_percentage = $this->input->post('exp_rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('exp_rete_iva_valor'));
                $rete_iva_account = $this->input->post('exp_rete_iva_account');
                $rete_iva_base = $this->input->post('exp_rete_iva_base');
                $rete_iva_id = $this->input->post('exp_rete_iva_id');

                $rete_ica_percentage = $this->input->post('exp_rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('exp_rete_ica_valor'));
                $rete_ica_account = $this->input->post('exp_rete_ica_account');
                $rete_ica_base = $this->input->post('exp_rete_ica_base');
                $rete_ica_id = $this->input->post('exp_rete_ica_id');

                $rete_other_percentage = $this->input->post('exp_rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('exp_rete_otros_valor'));
                $rete_other_account = $this->input->post('exp_rete_otros_account');
                $rete_other_base = $this->input->post('exp_rete_otros_base');
                $rete_other_id = $this->input->post('exp_rete_otros_id');
                $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total;
                $rete_applied = TRUE;

                $data['rete_fuente_percentage'] = $rete_fuente_percentage;
                $data['rete_fuente_total'] = $rete_fuente_total;
                $data['rete_fuente_account'] = $rete_fuente_account;
                $data['rete_fuente_base'] = $rete_fuente_base;
                $data['rete_fuente_id'] = $rete_fuente_id;
                $data['rete_iva_percentage'] = $rete_iva_percentage;
                $data['rete_iva_total'] = $rete_iva_total;
                $data['rete_iva_account'] = $rete_iva_account;
                $data['rete_iva_base'] = $rete_iva_base;
                $data['rete_iva_id'] = $rete_iva_id;
                $data['rete_ica_percentage'] = $rete_ica_percentage;
                $data['rete_ica_total'] = $rete_ica_total;
                $data['rete_ica_account'] = $rete_ica_account;
                $data['rete_ica_base'] = $rete_ica_base;
                $data['rete_ica_id'] = $rete_ica_id;
                $data['rete_other_percentage'] = $rete_other_percentage;
                $data['rete_other_total'] = $rete_other_total;
                $data['rete_other_account'] = $rete_other_account;
                $data['rete_other_base'] = $rete_other_base;
                $data['rete_other_id'] = $rete_other_id;
            }
            // $this->sma->print_arrays($data);
        } elseif ($this->input->post('add_expense')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->purchases_model->addExpense($data, $item)) {
            $this->session->set_flashdata('message', lang("expense_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['exnumber'] = ''; //$this->site->getReference('ex');
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['categories'] = $this->purchases_model->getExpenseCategories();
            $this->data['reference'] = $this->Settings->expense_prefix . "-";
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $this->load_view($this->theme . 'purchases/add_expense', $this->data);
        }
    }

    public function edit_expense($id = null)
    {
        if (!$this->session->userdata('cash_in_hand')) {
            $this->session->set_flashdata('error', lang("no_data_pos_register"));
            admin_redirect('pos/index');
        }
        $this->sma->checkPermissions('edit', true);
        $this->load->helper('security');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->purchases_model->getExpenseByID($id);
        if ($inv->date < $this->session->userdata('register_open_time')) {
            $this->session->set_flashdata('error', lang("expense_cant_be_edited_belongs_other_register"));
            admin_redirect('pos/index');
        }
        if ((!$this->Owner && !$this->Admin) && $inv->created_by != $this->session->userdata('user_id')) {
            $this->session->set_flashdata('error', lang("expense_cant_be_edited_belongs_other_user"));
            admin_redirect('pos/index');
        }
        $this->form_validation->set_rules('reference', lang("reference"), 'required');
        $this->form_validation->set_rules('amount', lang("amount"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == true) {

            $ptax_value = $this->input->post('exp_ptax_value') ? $this->input->post('exp_ptax_value') : 0;
            $ptax_2_value = $this->input->post('exp_ptax_2_value') ? $this->input->post('exp_ptax_2_value') : 0;
            $amount = $this->input->post('amount');

            $amount = $amount + $ptax_value + $ptax_2_value;

            $biller_id = $this->input->post('biller', true);
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);

            $data = array(
                'date' => $this->input->post('date'),
                'reference' => $inv->reference,
                'amount' => $amount,
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'note' => $this->input->post('note', true),
                'category_id' => $this->input->post('category', true),
                'biller_id' => $this->input->post('biller', true),
                'tax_rate_id' => $this->input->post('exp_ptax'),
                'tax_val' =>  $ptax_value,
                'tax_rate_2_id' =>  $this->input->post('exp_ptax_2'),
                'tax_val_2' =>  $ptax_2_value,
                'supplier_id' =>  $inv->supplier_id,
                'document_type_id' =>  $inv->document_type_id,
            );

            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }

            if ($this->input->post('rete_applied')) {
                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');
                $rete_fuente_id = $this->input->post('rete_fuente_id');

                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');
                $rete_iva_id = $this->input->post('rete_iva_id');

                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');
                $rete_ica_id = $this->input->post('rete_ica_id');

                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');
                $rete_other_id = $this->input->post('rete_otros_id');
                $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total;
                $rete_applied = TRUE;

                $data['rete_fuente_percentage'] = $rete_fuente_percentage;
                $data['rete_fuente_total'] = $rete_fuente_total;
                $data['rete_fuente_account'] = $rete_fuente_account;
                $data['rete_fuente_base'] = $rete_fuente_base;
                $data['rete_fuente_id'] = $rete_fuente_id;
                $data['rete_iva_percentage'] = $rete_iva_percentage;
                $data['rete_iva_total'] = $rete_iva_total;
                $data['rete_iva_account'] = $rete_iva_account;
                $data['rete_iva_base'] = $rete_iva_base;
                $data['rete_iva_id'] = $rete_iva_id;
                $data['rete_ica_percentage'] = $rete_ica_percentage;
                $data['rete_ica_total'] = $rete_ica_total;
                $data['rete_ica_account'] = $rete_ica_account;
                $data['rete_ica_base'] = $rete_ica_base;
                $data['rete_ica_id'] = $rete_ica_id;
                $data['rete_other_percentage'] = $rete_other_percentage;
                $data['rete_other_total'] = $rete_other_total;
                $data['rete_other_account'] = $rete_other_account;
                $data['rete_other_base'] = $rete_other_base;
                $data['rete_other_id'] = $rete_other_id;
            }

            $item = array(
                'tax_rate_id' => $this->input->post('exp_ptax'),
                'item_tax' =>  $ptax_value,
                'tax_rate_2_id' =>  $this->input->post('exp_ptax_2'),
                'item_tax_2' =>  $ptax_2_value,
                'net_unit_cost' =>  $this->input->post('amount'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            //$this->sma->print_arrays($data);

        } elseif ($this->input->post('edit_expense')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->purchases_model->updateExpense($id, $data, $item)) {
            $this->session->set_flashdata('message', lang("expense_updated"));
            admin_redirect("purchases/expenses");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['expense'] = $inv;
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['categories'] = $this->purchases_model->getExpenseCategories();
            $this->load_view($this->theme . 'purchases/edit_expense', $this->data);
        }
    }

    public function delete_expense($id = null)
    {
        $this->sma->checkPermissions('delete', true);
        if (!$this->Owner && !$this->Admin) {
            admin_redirect('sales');
        }

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $expense = $this->purchases_model->getExpenseByID($id);
        if ($this->purchases_model->deleteExpense($id)) {
            if ($expense->attachment) {
                unlink($this->upload_path . $expense->attachment);
            }
            $this->sma->send_json(array('error' => 0, 'msg' => lang("expense_deleted")));
        }
    }

    public function expense_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    foreach ($_POST['val'] as $id) {
                        $this->purchases_model->deleteExpense($id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("expenses_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } else if ($this->input->post('form_action') == 'reaccount') {
                    foreach ($_POST['val'] as $id) {
                        $this->purchases_model->recontabilizar_gasto($id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("expense_posted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } else if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('expenses'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('amount'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('note'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('created_by'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $expense = $this->purchases_model->getExpenseByID($id);
                        $user = $this->site->getUser($expense->created_by);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($expense->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $expense->reference);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $this->sma->formatMoney($expense->amount));
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $expense->note);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $user->first_name . ' ' . $user->last_name);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'expenses_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_expense_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function view_return($id = null)
    {
        $this->sma->checkPermissions('return_purchases');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->purchases_model->getReturnByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['payments'] = $this->purchases_model->getPaymentsForPurchase($id);
        $this->data['user'] = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $this->purchases_model->getAllReturnItems($id);
        $this->data['purchase'] = $this->purchases_model->getPurchaseByID($inv->purchase_id);
        $this->load_view($this->theme . 'purchases/view_return', $this->data);
    }

    public function return_purchase($id = null)
    {
        $this->sma->checkPermissions('return_purchases');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $purchase = $this->purchases_model->getPurchaseByID($id);
        $supportDocument = $this->input->post('supportDocument') ? true : false;
        $this->form_validation->set_rules('return_surcharge', lang("return_surcharge"), 'greater_than_equal_to[0]');
        $this->form_validation->set_rules('document_type_id', lang("reference_no"), 'required');
        if ($this->Settings->cost_center_selection == 1 && $this->Settings->modulary == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }

        if ($purchase->return_other_concepts == YES) {
            $this->session->set_flashdata('error', lang("return_other_concepts_existing"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true) {
            $si_return = [];
            $document_type_id = $this->input->post('document_type_id');
            $reference = null;
            if ($this->input->post('reference_no')) {
                $reference = $this->input->post('reference_no');
            }

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
                $invoiceDate = $this->input->post('invoiceDate');
                if (strtotime($date) < strtotime($invoiceDate)) {
                    $this->session->set_flashdata('error', lang("return_date_purchase_invalid"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $this->site->validate_movement_date($date);

            $return_surcharge = $this->input->post('return_surcharge') ? $this->input->post('return_surcharge') : 0;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $supplier_details = $this->site->getCompanyByID($purchase->supplier_id);
            $biller_id = $this->input->post('biller');
            $consecutive_supplier = $this->input->post('supplier_cosecutive');
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            $descuento_orden = $this->input->post('order_discount_method');

            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false && $this->Settings->modulary == 1) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect('purchases/add');
            }

            $total = 0;
            $product_tax = 0;
            $ipoconsumo = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = sizeof($_POST['product_code']);

            $assumed_retentions_text = "";
            //Retenciones
            if ($this->input->post('rete_applied')) {
                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');
                $rete_fuente_id = $this->input->post('rete_fuente_id');

                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');
                $rete_iva_id = $this->input->post('rete_iva_id');

                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');
                $rete_ica_id = $this->input->post('rete_ica_id');

                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');
                $rete_other_id = $this->input->post('rete_otros_id');

                $rete_bomberil_percentage = $this->input->post('rete_bomberil_tax');
                $rete_bomberil_total = $this->sma->formatDecimal($this->input->post('rete_bomberil_valor'));
                $rete_bomberil_account = $this->input->post('rete_bomberil_account');
                $rete_bomberil_base = $this->input->post('rete_bomberil_base');
                $rete_bomberil_id = $this->input->post('rete_bomberil_id');

                $rete_autoaviso_percentage = $this->input->post('rete_autoaviso_tax');
                $rete_autoaviso_total = $this->sma->formatDecimal($this->input->post('rete_autoaviso_valor'));
                $rete_autoaviso_account = $this->input->post('rete_autoaviso_account');
                $rete_autoaviso_base = $this->input->post('rete_autoaviso_base');
                $rete_autoaviso_id = $this->input->post('rete_autoaviso_id');

                $rete_fuente_assumed = $this->input->post('rete_fuente_assumed') ? 1 : 0;
                $rete_iva_assumed = $this->input->post('rete_iva_assumed') ? 1 : 0;
                $rete_ica_assumed = $this->input->post('rete_ica_assumed') ? 1 : 0;
                $rete_other_assumed = $this->input->post('rete_otros_assumed') ? 1 : 0;

                $rete_fuente_assumed_account = $this->input->post('rete_fuente_assumed_account');
                $rete_iva_assumed_account = $this->input->post('rete_iva_assumed_account');
                $rete_ica_assumed_account = $this->input->post('rete_ica_assumed_account');
                $rete_bomberil_assumed_account = $this->input->post('rete_bomberil_assumed_account');
                $rete_autoaviso_assumed_account = $this->input->post('rete_autoaviso_assumed_account');
                $rete_other_assumed_account = $this->input->post('rete_otros_assumed_account');

                $total_retenciones =
                    ($rete_fuente_assumed ? 0 : $rete_fuente_total) +
                    ($rete_iva_assumed ? 0 : $rete_iva_total) +
                    ($rete_ica_assumed ? 0 : $rete_ica_total) +
                    ($rete_ica_assumed ? 0 : $rete_bomberil_total) +
                    ($rete_ica_assumed ? 0 : $rete_autoaviso_total) +
                    ($rete_other_assumed ? 0 : $rete_other_total);

                if ($rete_fuente_assumed) {
                    $assumed_retentions_text .= "Rete Fuente asumida (" . $this->sma->formatMoney($rete_fuente_total) . "), ";
                }
                if ($rete_iva_assumed) {
                    $assumed_retentions_text .= "Rete IVA asumida(" . $this->sma->formatMoney($rete_iva_total) . "), ";
                }
                if ($rete_ica_assumed) {
                    $assumed_retentions_text .= "Rete ICA asumida(" . $this->sma->formatMoney($rete_ica_total) . "), ";
                    if ($rete_bomberil_total > 0) {
                        $assumed_retentions_text .= "Tasa Bomberil asumida(" . $this->sma->formatMoney($rete_bomberil_total) . "), ";
                    }
                    if ($rete_autoaviso_total > 0) {
                        $assumed_retentions_text .= "Tasa Auto Avisos y Tableros asumida(" . $this->sma->formatMoney($rete_autoaviso_total) . "), ";
                    }
                }
                if ($rete_other_assumed) {
                    $assumed_retentions_text .= "Rete OTRAS asumida(" . $this->sma->formatMoney($rete_other_total) . "), ";
                }
                $assumed_retentions_text = trim($assumed_retentions_text, ", ");
                $rete_applied = TRUE;

                // exit('Total Fuente : '.$rete_fuente_total."\n Total IVA : ".$rete_iva_total."\n Total ICA : ".$rete_ica_total."\n Total Otros : ".$rete_other_total."\n Total Retención : ".$total_retenciones);
            } else {
                $rete_applied = FALSE;
                $total_retenciones = 0;
            }
            //Retenciones
            $total_shipping_cost = 0;
            for ($r = 0; $r < $i; $r++) {
                if ($_POST['quantity'][$r] == 0 || $_POST['product_base_quantity'][$r] == 0) {
                    continue;
                }
                $purchase_item_id = $_POST['purchase_item_id'][$r];
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_net_cost = $this->sma->formatDecimal($_POST['net_cost'][$r]);
                $shipping_unit_cost = $this->sma->formatDecimal($_POST['shipping_unit_cost'][$r]);
                $unit_cost = $this->sma->formatDecimal($_POST['unit_cost'][$r]);
                $real_unit_cost = $this->sma->formatDecimal($_POST['real_unit_cost'][$r]);
                $item_unit_quantity = (0 - $_POST['quantity'][$r]);
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' ? $_POST['product_option'][$r] : null;
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_unit_tax_val = isset($_POST['unit_product_tax'][$r]) ? $_POST['unit_product_tax'][$r] : null;
                $item_tax_2_rate_id = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : null;
                $item_tax_2_rate = isset($_POST['unit_product_tax_2_percentage'][$r]) ? $_POST['unit_product_tax_2_percentage'][$r] : null;
                $item_unit_tax_2_val = isset($_POST['unit_product_tax_2'][$r]) ? $_POST['unit_product_tax_2'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit_discount = isset($_POST['product_unit_discount_val'][$r]) ? $_POST['product_unit_discount_val'][$r] : null;
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : null;
                $product_unit_id_selected = isset($_POST['product_unit_id_selected'][$r]) ? $_POST['product_unit_id_selected'][$r] : null;
                $item_expiry = (isset($_POST['expiry'][$r]) && !empty($_POST['expiry'][$r])) ? $this->sma->fsd($_POST['expiry'][$r]) : null;
                $supplier_part_no = (isset($_POST['part_no'][$r]) && !empty($_POST['part_no'][$r])) ? $_POST['part_no'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $original_tax_rate_id = $_POST['original_tax_rate_id'][$r];
                $item_quantity = (0 - $_POST['product_base_quantity'][$r]);
                $total_shipping_cost += ($shipping_unit_cost * $item_quantity);
                if (isset($item_code) && isset($real_unit_cost) && isset($unit_cost) && isset($item_quantity)) {
                    $product_details = $this->purchases_model->getProductByCode($item_code);
                    if ($item_expiry) {
                        $today = date('Y-m-d');
                        if ($item_expiry <= $today) {
                            $this->session->set_flashdata('error', lang('product_expiry_date_issue') . ' (' . $product_details->name . ')');
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                    $pr_discount = $item_unit_discount;
                    $unit_cost = $this->sma->formatDecimal($unit_cost - $pr_discount);
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $this->sma->formatDecimal($item_unit_tax_val * $item_unit_quantity);
                    $pr_item_tax_2 = $this->sma->formatDecimal($item_unit_tax_2_val * $item_unit_quantity);
                    $item_tax = $item_unit_tax_val;
                    $item_tax_2 = $item_unit_tax_2_val;
                    $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                    $tax_details_2 = $this->site->getTaxRateByID($item_tax_2_rate);
                    $tax = $tax_2 = 0;
                    if ($tax_details) {
                        $tax = $this->sma->formatDecimal($tax_details->rate, 0) . "%";
                        if ($tax_details->type == 2 && $tax_details->rate < 1 && $item_tax > 0) {
                            $tax = $item_tax;
                        }
                    }
                    if ($tax_details_2) {
                        $tax_2 = $this->sma->formatDecimal($tax_details_2->rate, 0) . "%";
                        if ($tax_details_2->type == 2 && $tax_details_2->rate < 1 && $item_tax_2 > 0) {
                            $tax_2 = $item_tax_2;
                        }
                    }
                    $product_tax += $pr_item_tax + $pr_item_tax_2;
                    $ipoconsumo += $pr_item_tax_2;
                    $subtotal = (($item_net_cost * $item_unit_quantity) + $pr_item_tax + $pr_item_tax_2);
                    $unit = $this->site->getUnitByID($item_unit);

                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'option_id' => $item_option,
                        'net_unit_cost' => $item_net_cost,
                        'unit_cost' => $this->sma->formatDecimal($item_net_cost + $item_tax + $item_tax_2 + $shipping_unit_cost),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $item_unit,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity' => $item_unit_quantity,
                        'quantity_balance' => $item_quantity,
                        'warehouse_id' => $purchase->warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'item_tax_2' => $pr_item_tax_2,
                        'tax_rate_2_id' => $item_tax_2_rate_id,
                        'tax_2' => $item_tax_2_rate,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'original_tax_rate_id' => $original_tax_rate_id,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'real_unit_cost' => $real_unit_cost,
                        'purchase_item_id' => $purchase_item_id,
                        'status' => $purchase->purchase_type == 1 ? 'received' : 'service_received',
                        'shipping_unit_cost' => 0 - $shipping_unit_cost,
                        'consumption_purchase' => $purchase->purchase_type == 1 ? $pr_item_tax_2 : 0,
                        'supplier_part_no' => NULL,
                    );
                    $products[] = ($product);
                    $total += $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity));
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }
            if ($descuento_orden == 2) { //DESCUENTO GLOBAL
                $order_discount = (float) $this->site->calculateDiscount($this->input->post('discount'), ($total));
                if ($order_discount > 0) {
                    $order_discount = $order_discount * -1;
                }
                $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount));
            } else { // DESCUENTO A CADA PRODUCTO
                $order_discount = 0;
                $total_discount = 0;
            }

            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax));
            $shipping = (float) $this->input->post('shipping');
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($return_surcharge) - $order_discount) - $shipping + $total_shipping_cost);
            $nota_shipping = "";
            if ($total_shipping_cost < 0 && $this->Settings->prorate_shipping_cost == 1) {
                $nota_shipping = " El valor del flete por " . $this->sma->formatMoney($total_shipping_cost) . " ha sido distribuido en los costos de los productos, el valor de la factura descontando los fletes es de " . $this->sma->formatMoney($grand_total - $total_shipping_cost);
            }

            $data = array(
                'date' => $date,
                'purchase_id' => $id,
                'reference_no' => $reference,
                'purchase_type' => $purchase->purchase_type,
                'supplier_id' => $purchase->supplier_id,
                'supplier' => $purchase->supplier,
                'warehouse_id' => $purchase->warehouse_id,
                'note' => $note . $nota_shipping . " " . $assumed_retentions_text,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => ($this->input->post('discount') ? $this->input->post('order_discount') : null),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'surcharge' => $this->sma->formatDecimal($return_surcharge),
                'grand_total' => $grand_total,
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'return_purchase_ref' => $purchase->reference_no,
                'status' => 'returned',
                'payment_status' => $purchase->payment_status == 'paid' ? 'due' : 'pending',
                'biller_id' => $purchase->biller_id,
                'document_type_id' => $document_type_id,
                'shipping' => $this->input->post('shipping') ? $this->input->post('shipping') * -1 : 0,
                'prorated_shipping_cost' => $total_shipping_cost,
                'consumption_purchase' => $purchase->purchase_type == 1 ? $ipoconsumo : 0,
                'due_payment_method_id' => $purchase->due_payment_method_id,
                'expense_causation' => $purchase->expense_causation,
                'consecutive_supplier' => $consecutive_supplier,
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            $payments = [];
            if ($rete_applied) {
                $data['rete_fuente_percentage'] = $rete_fuente_percentage;
                $data['rete_fuente_total'] = $rete_fuente_total;
                $data['rete_fuente_account'] = $rete_fuente_account;
                $data['rete_fuente_base'] = $rete_fuente_base;
                $data['rete_fuente_id'] = $rete_fuente_id;
                $data['rete_iva_percentage'] = $rete_iva_percentage;
                $data['rete_iva_total'] = $rete_iva_total;
                $data['rete_iva_account'] = $rete_iva_account;
                $data['rete_iva_base'] = $rete_iva_base;
                $data['rete_iva_id'] = $rete_iva_id;
                $data['rete_ica_percentage'] = $rete_ica_percentage;
                $data['rete_ica_total'] = $rete_ica_total;
                $data['rete_ica_account'] = $rete_ica_account;
                $data['rete_ica_base'] = $rete_ica_base;
                $data['rete_ica_id'] = $rete_ica_id;
                $data['rete_other_percentage'] = $rete_other_percentage;
                $data['rete_other_total'] = $rete_other_total;
                $data['rete_other_account'] = $rete_other_account;
                $data['rete_other_base'] = $rete_other_base;
                $data['rete_other_id'] = $rete_other_id;

                $data['rete_bomberil_percentage'] = $rete_bomberil_percentage;
                $data['rete_bomberil_total'] = $rete_bomberil_total;
                $data['rete_bomberil_account'] = $rete_bomberil_account;
                $data['rete_bomberil_base'] = $rete_bomberil_base;
                $data['rete_bomberil_id'] = $rete_bomberil_id;

                $data['rete_autoaviso_percentage'] = $rete_autoaviso_percentage;
                $data['rete_autoaviso_total'] = $rete_autoaviso_total;
                $data['rete_autoaviso_account'] = $rete_autoaviso_account;
                $data['rete_autoaviso_base'] = $rete_autoaviso_base;
                $data['rete_autoaviso_id'] = $rete_autoaviso_id;

                $data['rete_fuente_assumed'] = $rete_fuente_assumed;
                $data['rete_iva_assumed'] = $rete_iva_assumed;
                $data['rete_ica_assumed'] = $rete_ica_assumed;
                $data['rete_other_assumed'] = $rete_other_assumed;
                $data['rete_fuente_assumed_account'] = $rete_fuente_assumed ? $rete_fuente_assumed_account : NULL;
                $data['rete_iva_assumed_account'] = $rete_iva_assumed ? $rete_iva_assumed_account : NULL;
                $data['rete_ica_assumed_account'] = $rete_ica_assumed ? $rete_ica_assumed_account : NULL;
                $data['rete_bomberil_assumed_account'] = $rete_ica_assumed ? $rete_bomberil_assumed_account : NULL;
                $data['rete_autoaviso_assumed_account'] = $rete_ica_assumed ? $rete_autoaviso_assumed_account : NULL;
                $data['rete_other_assumed_account'] = $rete_other_assumed ? $rete_other_assumed_account : NULL;

                $retpayment = array(
                    'date'         => $date,
                    'amount'       => ($total_retenciones * -1),
                    'reference_no' => 'retencion',
                    'paid_by'      => 'retencion',
                    'cheque_no'    => '',
                    'cc_no'        => '',
                    'cc_holder'    => '',
                    'cc_month'     => '',
                    'cc_year'      => '',
                    'cc_type'      => '',
                    'created_by'   => $this->session->userdata('user_id'),
                    'type'         => 'sent',
                    'note'         => 'Retenciones',
                );
                $payment[] = $retpayment;
            }

            $purchase_return_balance = ($data['grand_total'] * -1) - $this->input->post('amount-paid') - $total_retenciones;
            if ($this->input->post('amount-paid') && $this->input->post('amount-paid') > 0) {
                $data_payment = array(
                    'date' => $date,
                    'amount' => (0 - $this->input->post('amount-paid')),
                    'paid_by' => $this->input->post('paid_by'),
                    'cheque_no' => $this->input->post('cheque_no'),
                    'cc_no' => $this->input->post('pcc_no'),
                    'cc_holder' => $this->input->post('pcc_holder'),
                    'cc_month' => $this->input->post('pcc_month'),
                    'cc_year' => $this->input->post('pcc_year'),
                    'cc_type' => $this->input->post('pcc_type'),
                    'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                    'type' => 'returned',
                    'mean_payment_code_fe' => $_POST['mean_payment_code_fe'],
                    'document_type_id' => $this->input->post('payment_reference_no'),
                );
                $data['payment_status'] = $grand_total == $this->input->post('amount-paid') ? 'paid' : 'partial';

                if ($this->input->post('paid_by') == 'deposit') {
                    $this->form_validation->set_rules('deposit_document_type_id', lang("deposit_document_type_id"), 'required');
                    $data_payment['deposit_document_type_id'] = $this->input->post('deposit_document_type_id');
                }

                $payment[] = $data_payment;
            }

            $discount_amount = $this->input->post('discount_amount');
            $discount_ledger_id = $this->input->post('discount_ledger_id');
            $discount_description = $this->input->post('discount_description');
            if ($this->input->post('amount-discounted') && $this->input->post('amount-discounted') > 0) {
                foreach ($discount_amount as $index => $ds_amount) {
                    $ds_ledger = $discount_ledger_id[$index];
                    $ds_description = $discount_description[$index];
                    $purchase_discounts[$index]['amount'] = $ds_amount;
                    $purchase_discounts[$index]['ledger_id'] = $ds_ledger;
                    $purchase_discounts[$index]['description'] = $ds_description;
                    $data_payment = array(
                        'date' => $date,
                        'amount' => $ds_amount * -1,
                        'paid_by' => 'discount',
                        'cheque_no' => NULL,
                        'cc_no' => NULL,
                        'cc_holder' => NULL,
                        'cc_month' => NULL,
                        'cc_year' => NULL,
                        'cc_type' => NULL,
                        'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                        'type' => 'returned',
                        'discount_ledger_id' => $ds_ledger,
                        'note' => $ds_description
                    );
                    $purchase_return_balance -= $ds_amount;
                    $payment[] = $data_payment;
                }
            }


            $ce_retentions_data = [];
            if ($this->input->post('ce_retention_type')) {
                $ce_retention_type = $this->input->post('ce_retention_type');
                $ce_retention_total = $this->input->post('ce_retention_total');
                $ce_retention_base = $this->input->post('ce_retention_base');
                $ce_retention_account = $this->input->post('ce_retention_account');
                $ce_retention_percentage = $this->input->post('ce_retention_percentage');
                $ce_retention_assumed = $this->input->post('ce_retention_assumed');
                $ce_retention_assumed_account = $this->input->post('ce_retention_assumed_account');
                $ce_rete_data_payment = [];
                foreach ($ce_retention_type as $ce_rete_reference => $ce_rete_type_arr) {
                    foreach ($ce_rete_type_arr as $rcretekey => $ce_rete_type) {

                        $ce_retentions_data[$ce_rete_reference]['rete_'.$ce_rete_type.'_total'] = $ce_retention_total[$ce_rete_reference][$rcretekey];
                        $ce_retentions_data[$ce_rete_reference]['rete_'.$ce_rete_type.'_base'] = $ce_retention_base[$ce_rete_reference][$rcretekey];
                        $ce_retentions_data[$ce_rete_reference]['rete_'.$ce_rete_type.'_account'] = $ce_retention_account[$ce_rete_reference][$rcretekey];
                        $ce_retentions_data[$ce_rete_reference]['rete_'.$ce_rete_type.'_percentage'] = $ce_retention_percentage[$ce_rete_reference][$rcretekey];
                        $ce_retentions_data[$ce_rete_reference]['rete_'.$ce_rete_type.'_assumed'] = $ce_retention_assumed[$ce_rete_reference][$rcretekey];
                        $ce_retentions_data[$ce_rete_reference]['rete_'.$ce_rete_type.'_assumed_account'] = $ce_retention_assumed_account[$ce_rete_reference][$rcretekey];

                        if (isset($ce_retentions_data[$ce_rete_reference]['total_rete'])) {
                            $ce_retentions_data[$ce_rete_reference]['total_rete'] += $ce_retention_total[$ce_rete_reference][$rcretekey];
                        } else {
                            $ce_retentions_data[$ce_rete_reference]['total_rete'] = $ce_retention_total[$ce_rete_reference][$rcretekey];
                        }

                        if (isset($ce_rete_data_payment[$ce_rete_reference])) {
                            $ce_rete_data_payment[$ce_rete_reference]['amount'] += ($ce_retention_total[$ce_rete_reference][$rcretekey] * -1);
                            $ce_rete_data_payment[$ce_rete_reference]['note'] .= 'rete '.$ce_rete_type.' por '.$this->sma->formatMoney($ce_retention_total[$ce_rete_reference][$rcretekey]).', ';

                            $ce_rete_data_payment[$ce_rete_reference]['rete_'.$ce_rete_type.'_total'] = $ce_retention_total[$ce_rete_reference][$rcretekey];
                            $ce_rete_data_payment[$ce_rete_reference]['rete_'.$ce_rete_type.'_account'] = $ce_retention_account[$ce_rete_reference][$rcretekey];
                            $ce_rete_data_payment[$ce_rete_reference]['rete_'.$ce_rete_type.'_base'] = $ce_retention_base[$ce_rete_reference][$rcretekey];
                            $ce_rete_data_payment[$ce_rete_reference]['rete_'.$ce_rete_type.'_percentage'] = $ce_retention_percentage[$ce_rete_reference][$rcretekey];
                            $ce_rete_data_payment[$ce_rete_reference]['rete_'.$ce_rete_type.'_assumed'] = $ce_retention_assumed[$ce_rete_reference][$rcretekey];
                            $ce_rete_data_payment[$ce_rete_reference]['rete_'.$ce_rete_type.'_assumed_account'] = $ce_retention_assumed_account[$ce_rete_reference][$rcretekey];
                        } else {
                            $ce_rete_data_payment[$ce_rete_reference] = array(
                                'date' => $date,
                                'amount' => $ce_retention_total[$ce_rete_reference][$rcretekey] * -1,
                                'paid_by' => 'cash',
                                'reference_no' => $ce_rete_reference,
                                'cheque_no' => NULL,
                                'cc_no' => NULL,
                                'cc_holder' => NULL,
                                'cc_month' => NULL,
                                'cc_year' => NULL,
                                'cc_type' => NULL,
                                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                'type' => 'returned',
                                'note' => 'Retención posterior a '.$ce_rete_type.' aplicada en CE '.$ce_rete_reference.' con total de '.$this->sma->formatMoney($ce_retention_total[$ce_rete_reference][$rcretekey]).', ',
                                'rete_'.$ce_rete_type.'_total' =>$ce_retention_total[$ce_rete_reference][$rcretekey],
                                'rete_'.$ce_rete_type.'_account' =>$ce_retention_account[$ce_rete_reference][$rcretekey],
                                'rete_'.$ce_rete_type.'_base' =>$ce_retention_base[$ce_rete_reference][$rcretekey],
                                'rete_'.$ce_rete_type.'_percentage' =>$ce_retention_percentage[$ce_rete_reference][$rcretekey],
                                'rete_'.$ce_rete_type.'_assumed' =>$ce_retention_assumed[$ce_rete_reference][$rcretekey],
                                'rete_'.$ce_rete_type.'_assumed_account' =>$ce_retention_assumed_account[$ce_rete_reference][$rcretekey],
                            );
                        }
                        $purchase_return_balance -= $ce_retention_total[$ce_rete_reference][$rcretekey];
                    }
                }
                if (count($ce_rete_data_payment) > 0) {
                    foreach ($ce_rete_data_payment as $rcdata) {
                        $payment[] = $rcdata;
                    }
                }
                if (count($ce_retentions_data) > 0) {
                    $si_return['ce_retentions_data'] = $ce_retentions_data;
                }
            }

            if ($purchase_return_balance > 0) {
                $data_payment = array(
                    'date' => $date,
                    'amount' => ($purchase_return_balance * -1),
                    'paid_by' => 'due',
                    'cheque_no' => NULL,
                    'cc_no' => NULL,
                    'cc_holder' => NULL,
                    'cc_month' => NULL,
                    'cc_year' => NULL,
                    'cc_type' => NULL,
                    'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                    'type' => 'returned',
                    'document_type_id' => $this->input->post('payment_reference_no'),
                    'mean_payment_code_fe' => "ZZZ"
                );
                $data['payment_status'] = 'paid';
                $payment[] = $data_payment;
            }
            if (count($si_return) == 0) {
                $si_return = true;
            }
            if (!$this->session->userdata('purchase_processing')) {
                $this->session->set_userdata('purchase_processing', 1);
            } else {
                $this->session->set_flashdata('error', 'Se interrumpió el proceso de envío por que se detectó que ya hay uno en proceso, prevención de duplicación.');
                admin_redirect("purchases");
            }

        }

        if ($this->form_validation->run() == true && $purchaseId = $this->purchases_model->addPurchase($data, $products, $payment, $purchase->purchase_type, $si_return)) {
            $this->site->syncPurchasePayments($data['purchase_id']);
            $this->site->updateReference('rep');

            /**
             * Crear aquí el método para envío de DS.
             */
            /**********************************************************************************************************/
            if ($this->Settings->supportingDocument == YES && $supportDocument) {
                $resolutionData = $this->SupportDocument_model->getResolutionData($this->input->post('document_type_id'));

                if ($resolutionData->factura_electronica == YES) {
                    $this->SupportDocument_model->sendElectronicDocument($purchaseId);
                }

                $this->session->set_flashdata('message', lang("return_purchase_added"));
                admin_redirect("purchases/supporting_document_index");
            }
            /**********************************************************************************************************/

            $this->session->set_flashdata('message', lang("return_purchase_added"));
            if ($this->session->userdata('purchase_processing')) {
                $this->session->unset_userdata('purchase_processing');
            }
            admin_redirect("purchases");
        } else {
            $total_ce_payments = $this->purchases_model->get_total_payments_for_purchase($id);
            if ($total_ce_payments) {
                $total_ce_rete_amount = $total_ce_payments->fuente_total + $total_ce_payments->iva_total + $total_ce_payments->ica_total + $total_ce_payments->other_total + $total_ce_payments->autoaviso_total + $total_ce_payments->bomberil_total;
            } else {
                $total_ce_rete_amount = 0;
            }
            // if ($return_type = 1 && ($total_ce_payments->fuente_total > 0 || $total_ce_payments->iva_total > 0 || $total_ce_payments->ica_total > 0 || $total_ce_payments->other_total > 0)) {
            //     $this->session->set_flashdata('error', lang('cannot_return_partialy_ce_wh'));
            //     admin_redirect($_SERVER['HTTP_REFERER']);
            // }

            $this->data['supportDocument'] = false;

            $documentType = $this->site->getDocumentTypeById($purchase->document_type_id);
            if ($documentType->module == 35) {
                $this->data['supportDocument'] = true;
            }

            if ($purchase->status != 'received' && $purchase->status != 'partial') {
                $this->session->set_flashdata('error', lang("purchase_status_x_received"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if ($this->Settings->disable_editing) {
                if ($purchase->date <= date('Y-m-d', strtotime('-' . $this->Settings->disable_editing . ' days'))) {
                    $this->session->set_flashdata('error', sprintf(lang("purchase_x_edited_older_than_x_days"), $this->Settings->disable_editing));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }

            $c = rand(100000, 9999999);
            $total_quantity_available = 0;
            $inv_items = $this->purchases_model->getAllPurchaseItems($id);

            foreach ($inv_items as $item) {
                if ($item->quantity == 0) {
                    $item->quantity = 1;
                }
                if ($purchase->purchase_type == 1) {
                    $row = $this->site->getProductByID($item->product_id);
                    if ($row->attributes) {
                        $wh_product=$this->site->getWarehouseProductsVariants($item->option_id, $purchase->warehouse_id);
                        $row->wh_quantity = ($wh_product ? $wh_product[0]->quantity : 0);
                    } else {
                        $wh_product=$this->site->getWarehouseProducts($item->product_id, $purchase->warehouse_id);
                        $row->wh_quantity = ($wh_product ? $wh_product[0]->quantity : 0);
                    }
                    if ($this->Settings->include_pending_order_quantity_stock == 1) {
                        $order_pending_quantity = $this->site->get_Product_pending_order_quantity($item->product_id, $item->warehouse_id, $item->option_id);
                        $row->wh_quantity = $row->wh_quantity - $order_pending_quantity;
                        $row->op_pending_quantity = $order_pending_quantity;
                    }
                } else {
                    $row = $this->site->getExpenseCategory($item->product_id);
                }
                // $this->sma->print_arrays($wh_product);
                $row->expiry = (($item->expiry && $item->expiry != '0000-00-00') ? $this->sma->hrsd($item->expiry) : '');
                $row->base_quantity = $item->quantity - $item->returned_quantity;
                $row->base_unit = (isset($row->unit) && $row->unit) ? $row->unit : $item->product_unit_id;
                $row->base_unit_cost = (isset($row->cost) && $row->cost) ? $row->cost : $item->unit_cost;
                $row->unit = $item->product_unit_id;
                $row->qty = $item->quantity - $item->returned_quantity;
                $row->oqty = $item->quantity - $item->returned_quantity;
                $total_quantity_available += $row->oqty;
                $row->purchase_item_id = $item->id;
                $row->original_tax_rate_id = $item->original_tax_rate_id;
                $row->supplier_part_no = $item->supplier_part_no;
                $row->shipping_unit_cost = $item->shipping_unit_cost;
                $row->consumption_purchase_tax = $item->item_tax_2 / $item->quantity;
                $row->received = $item->quantity_received ? $item->quantity_received : $item->quantity;
                $row->quantity_balance = $item->quantity_balance + ($item->quantity - $row->received);
                $row->discount = $item->discount ? ($item->item_discount / $item->quantity) . "" : '0';
                $options = $this->purchases_model->getProductOptions($row->id, $item->option_id);
                $row->option = !empty($item->option_id) ? $item->option_id : '';
                $row->real_unit_cost = $item->real_unit_cost;
                $row->cost = $this->sma->formatDecimal($item->net_unit_cost + ($item->item_discount / $item->quantity));
                $row->tax_rate = $item->tax_rate_id;
                unset($row->details, $row->product_details, $row->price, $row->file, $row->product_group_id);
                $units = $this->site->get_product_units($row->id);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $tax_rate_2 = false;
                $ri = $this->Settings->item_addition ? $row->id : $c;
                $row->item_tax = $item->item_tax / $item->quantity;
                $row->item_tax_2 = $item->item_tax_2 / $item->quantity;
                $row->tax_rate_2_id = $item->tax_rate_2_id;
                $row->net_unit_cost = $item->net_unit_cost;
                $row->unit_cost = $item->unit_cost;
                $row->item_discount = $item->item_discount / $item->quantity;
                $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'units' => $units, 'tax_rate' => $tax_rate, 'tax_rate_2' => $tax_rate_2, 'options' => $options, 'order' => $item->id);

                $c++;
            }

            if ($total_quantity_available <= 0) {
                $this->session->set_flashdata('error', lang("purchase_totally_returned"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            $this->data['id'] = $id;
            $this->data['reference'] = '';
            $this->data['inv'] = $purchase;
            $this->data['inv_items'] = json_encode($pr);
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');

            // $allow_payment = ($purchase->payment_status == 'paid' || $purchase->payment_status == 'partial') ? TRUE : FALSE;
            $allow_payment = TRUE;

            $purchase_payments = $this->purchases_model->getPaymentsForPurchase($id);
            $amount_discounted = 0;
            $discounts = false;
            $ce_retentions = false;
            if ($purchase_payments) {
                $cnt_ds = 0;
                foreach ($purchase_payments as $payment) {

                    if ($payment->ce_total_retention > 0) {
                        if ($payment->rete_fuente_total > 0) {
                            if (isset($ce_retentions[$payment->reference_no]['fuente'])) {
                                $ce_retentions[$payment->reference_no]['fuente']['total'] += $payment->rete_fuente_total;
                            } else {
                                $ce_retentions[$payment->reference_no]['fuente']['total'] = $payment->rete_fuente_total;
                                $ce_retentions[$payment->reference_no]['fuente']['base'] = $payment->rete_fuente_base;
                                $ce_retentions[$payment->reference_no]['fuente']['account'] = $payment->rete_fuente_account;
                                $ce_retentions[$payment->reference_no]['fuente']['percentage'] = $payment->rete_fuente_percentage;
                                $ce_retentions[$payment->reference_no]['fuente']['assumed'] = $payment->rete_fuente_assumed;
                                $ce_retentions[$payment->reference_no]['fuente']['assumed_account'] = $payment->rete_fuente_assumed_account;
                            }
                        }

                        if ($payment->rete_iva_total > 0) {
                            if (isset($ce_retentions[$payment->reference_no]['iva'])) {
                                $ce_retentions[$payment->reference_no]['iva']['total'] += $payment->rete_iva_total;
                            } else {
                                $ce_retentions[$payment->reference_no]['iva']['total'] = $payment->rete_iva_total;
                                $ce_retentions[$payment->reference_no]['iva']['base'] = $payment->rete_iva_base;
                                $ce_retentions[$payment->reference_no]['iva']['account'] = $payment->rete_iva_account;
                                $ce_retentions[$payment->reference_no]['iva']['percentage'] = $payment->rete_iva_percentage;
                                $ce_retentions[$payment->reference_no]['iva']['assumed'] = $payment->rete_iva_assumed;
                                $ce_retentions[$payment->reference_no]['iva']['assumed_account'] = $payment->rete_iva_assumed_account;
                            }
                        }

                        if ($payment->rete_ica_total > 0) {
                            if (isset($ce_retentions[$payment->reference_no]['ica'])) {
                                $ce_retentions[$payment->reference_no]['ica']['total'] += $payment->rete_ica_total;
                            } else {
                                $ce_retentions[$payment->reference_no]['ica']['total'] = $payment->rete_ica_total;
                                $ce_retentions[$payment->reference_no]['ica']['base'] = $payment->rete_ica_base;
                                $ce_retentions[$payment->reference_no]['ica']['account'] = $payment->rete_ica_account;
                                $ce_retentions[$payment->reference_no]['ica']['percentage'] = $payment->rete_ica_percentage;
                                $ce_retentions[$payment->reference_no]['ica']['assumed'] = $payment->rete_ica_assumed;
                                $ce_retentions[$payment->reference_no]['ica']['assumed_account'] = $payment->rete_ica_assumed_account;
                            }
                        }

                        if ($payment->rete_other_total > 0) {
                            if (isset($ce_retentions[$payment->reference_no][$payment->rete_other_id])) {
                                $ce_retentions[$payment->reference_no]['other']['total'] += $payment->rete_other_total;
                            } else {
                                $ce_retentions[$payment->reference_no]['other']['total'] = $payment->rete_other_total;
                                $ce_retentions[$payment->reference_no]['other']['base'] = $payment->rete_other_base;
                                $ce_retentions[$payment->reference_no]['other']['account'] = $payment->rete_other_account;
                                $ce_retentions[$payment->reference_no]['other']['assumed'] = $payment->rete_other_assumed;
                                $ce_retentions[$payment->reference_no]['other']['assumed_account'] = $payment->rete_other_assumed_account;
                            }
                        }

                        if ($payment->rete_bomberil_total > 0) {
                            if (isset($ce_retentions[$payment->reference_no][$payment->rete_bomberil_id])) {
                                $ce_retentions[$payment->reference_no]['bomberil']['total'] += $payment->rete_bomberil_total;
                            } else {
                                $ce_retentions[$payment->reference_no]['bomberil']['total'] = $payment->rete_bomberil_total;
                                $ce_retentions[$payment->reference_no]['bomberil']['base'] = $payment->rete_bomberil_base;
                                $ce_retentions[$payment->reference_no]['bomberil']['account'] = $payment->rete_bomberil_account;
                                $ce_retentions[$payment->reference_no]['bomberil']['percentage'] = $payment->rete_bomberil_percentage;
                                $ce_retentions[$payment->reference_no]['bomberil']['assumed'] = $payment->rete_bomberil_assumed;
                                $ce_retentions[$payment->reference_no]['bomberil']['assumed_account'] = $payment->rete_bomberil_assumed_account;
                            }
                        }

                        if ($payment->rete_autoaviso_total > 0) {
                            if (isset($ce_retentions[$payment->reference_no][$payment->rete_autoaviso_id])) {
                                $ce_retentions[$payment->reference_no]['autoaviso']['total'] += $payment->rete_autoaviso_total;
                            } else {
                                $ce_retentions[$payment->reference_no]['autoaviso']['total'] = $payment->rete_autoaviso_total;
                                $ce_retentions[$payment->reference_no]['autoaviso']['base'] = $payment->rete_autoaviso_base;
                                $ce_retentions[$payment->reference_no]['autoaviso']['account'] = $payment->rete_autoaviso_account;
                                $ce_retentions[$payment->reference_no]['autoaviso']['percentage'] = $payment->rete_autoaviso_percentage;
                                $ce_retentions[$payment->reference_no]['autoaviso']['assumed'] = $payment->rete_autoaviso_assumed;
                                $ce_retentions[$payment->reference_no]['autoaviso']['assumed_account'] = $payment->rete_autoaviso_assumed_account;
                            }
                        }
                    }
                    if ($payment->paid_by == 'discount') {
                        $amount_discounted += $payment->amount;
                        $discounts[$cnt_ds]['amount'] = $payment->amount;
                        $discounts[$cnt_ds]['ledger_id'] = $payment->discount_ledger_id;
                        $discounts[$cnt_ds]['description'] = $payment->note;
                        $cnt_ds++;
                    }
                }
            }

            $this->data['discounts'] = $discounts;
            $this->data['payment_ref'] = '';
            $this->data['allow_payment'] = $allow_payment;
            $this->data['amount_discounted'] = $amount_discounted;
            $this->data['ce_retentions'] = $ce_retentions;
            $this->data['total_ce_rete_amount'] = $total_ce_rete_amount;

            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->page_construct('purchases/return_purchase', ['page_title' => lang('return_purchase')], $this->data);
        }
    }

    public function getSupplierCost($supplier_id, $product)
    {
        switch ($supplier_id) {
            case $product->supplier1:
                $cost =  $product->supplier1price > 0 ? $product->supplier1price : $product->cost;
                break;
            case $product->supplier2:
                $cost =  $product->supplier2price > 0 ? $product->supplier2price : $product->cost;
                break;
            case $product->supplier3:
                $cost =  $product->supplier3price > 0 ? $product->supplier3price : $product->cost;
                break;
            case $product->supplier4:
                $cost =  $product->supplier4price > 0 ? $product->supplier4price : $product->cost;
                break;
            case $product->supplier5:
                $cost =  $product->supplier5price > 0 ? $product->supplier5price : $product->cost;
                break;
            default:
                $cost = $product->cost;
        }
        return $cost;
    }

    public function update_status($id)
    {

        $this->form_validation->set_rules('status', lang("status"), 'required');

        if ($this->form_validation->run() == true) {
            $status = $this->input->post('status');
            $note = $this->sma->clear_tags($this->input->post('note'));
        } elseif ($this->input->post('update')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'purchases');
        }

        if ($this->form_validation->run() == true && $this->purchases_model->updateStatus($id, $status, $note)) {

            if ($status == 'received') {
                $msg = $this->purchases_model->recontabilizarCompra($id);
            } else {
                $msg = "";
            }
            if ($this->session->userdata('reaccount_error')) {
                $this->session->set_flashdata('error', lang('status_updated') . " " . $msg);
                $this->session->unset_userdata('reaccount_error');
            } else {
                $this->session->set_flashdata('message', lang('status_updated') . " " . $msg);
            }
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'purchases');
        } else {

            $this->data['inv'] = $this->purchases_model->getPurchaseByID($id);
            $this->data['returned'] = FALSE;
            if ($this->data['inv']->status == 'returned' || $this->data['inv']->return_id) {
                $this->data['returned'] = TRUE;
            }
            $this->data['received'] = FALSE;
            if ($this->data['inv']->status == 'received') {
                $this->data['received'] = TRUE;
            }
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'purchases/update_status', $this->data);
        }
    }

    public function opcionesWithHolding($type)
    {
        $q = $this->purchases_model->getWithHolding($type, 'P');
        if ($q !== FALSE) {
            $options = "<option value=''>Seleccione...</option>";
            foreach ($q as $row => $data) {
                $options .= "<option value='" . $data['id'] . "' data-account='" . $data['account_id'] . "' data-assumedaccount='" . $data['assumed_account_id'] . "' data-minbase='" . $data['min_base'] . "' data-apply='" . $data['apply'] . "' data-percentage='" . $data['percentage'] . "'>" . $data['description'] . "</option>";
            }
            echo $options;
        } else {
            echo "<option value=''>Sin resultados</option>";
        }
    }

    public function getTaxInfoJson()
    {

        $tax_id_1 = $this->input->post('tax_1');
        $tax_id_2 = $this->input->post('tax_2');

        $tax_1 = $this->site->getTaxRateByID($tax_id_1);
        $tax_2 = $this->site->getTaxRateByID($tax_id_2);

        $arr = array('tax_1' => $tax_1, 'tax_2' => $tax_2);

        $this->sma->send_json($arr);
    }

    public function contabilizarCompra($purchase_id = null, $json = null)
    {

        $this->form_validation->set_rules('invoice_id', lang("invoice_id"), 'required');

        if ($this->form_validation->run() == true) {

            $purchase_id = $this->input->post('invoice_id');
            $msg = $this->purchases_model->recontabilizarCompra($purchase_id);
            if ($this->session->userdata('reaccount_error')) {
                $this->session->set_flashdata('error', $msg);
                $this->session->unset_userdata('reaccount_error');
            } else {
                $this->session->set_flashdata('message', $msg);
            }
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'purchases');
        } else {

            $purchase = $this->purchases_model->getPurchaseByID($purchase_id, true);
            $exists = $this->site->getEntryTypeNumberExisting($purchase, false, ($purchase['status'] == 'returned' ? true : false));
            $this->data['exists'] = $exists;
            $this->data['inv'] = $purchase;
            $payments = $this->purchases_model->getPurchasePayments($purchase_id);
            $alert = false;
            if ($payments) {
                foreach ($payments as $pmnt) {
                    if ($pmnt->multi_payment && (($pmnt->rete_fuente_total > 0 && !$pmnt->rete_fuente_id) || ($pmnt->rete_iva_total > 0 && !$pmnt->rete_iva_id) || ($pmnt->rete_ica_total > 0 && !$pmnt->rete_ica_id) || ($pmnt->rete_other_total > 0 && !$pmnt->rete_other_id))) {
                        $alert = true;
                    }
                }
            }

            if ($alert) {
                $this->data['alert'] = lang('purchase_with_incomplete_payment_data');
            }
            $this->load_view($this->theme . 'purchases/recontabilizar', $this->data);
        }
    }

    public function validate_supplier_ref()
    {
        $supplier_id = $this->input->get('supplier_id');
        $reference_no = $this->input->get('reference_no');
        $po_edit = $this->input->get('po_edit');
        $id_purchase = $this->input->get('id_purchase');
        $document_type_prefix = $this->input->get('document_type_prefix');
        if ($reference_no != '') {
            echo $this->purchases_model->validateSupplierReferenceNo($supplier_id, $reference_no, $po_edit, $id_purchase, $document_type_prefix);
        } else {
            echo "false";
        }
    }

    public function purchase_suggestions()
    {
        $supplier_id = $this->input->get('supplier_id');
        $biller_id = $this->input->get('biller_id');
        $purchase_id = $this->input->get('purchase_id');
        $pp_reference_no = $this->input->get('pp_reference_no');
        $rows_start = $this->input->get('rows_start');
        $num_rows = $this->input->get('num_rows');
        $rdata = [
            'supplier_id' => $supplier_id,
            'biller_id' => $biller_id,
            'purchase_id' => $purchase_id,
            'rows_start' => $rows_start,
            'num_rows' => $num_rows,
            'pp_reference_no' => $pp_reference_no,
        ];
        $purchases = $this->purchases_model->getPurchasesPaymentPending($rdata);
        if ($purchases) {
            foreach ($purchases as $purchase) {
                $purchase->purchase_selected = false;
                $purchase->amount_to_paid = 0;
                $purchase->rete_fuente_total = 0;
                $purchase->rete_iva_total = 0;
                $purchase->rete_ica_total = 0;
                $purchase->rete_other_total = 0;
                $purchase->rete_bomberil_total = 0;
                $purchase->rete_autoaviso_total = 0;
                $rete_fuente_base_paid = $rete_iva_base_paid = $rete_ica_base_paid = $rete_other_base_paid = 0;
                if ($bases = $this->purchases_model->get_total_payments_for_purchase($purchase->id)) {
                    $rete_fuente_base_paid = $bases->fuente_base;
                    $rete_iva_base_paid = $bases->iva_base;
                    $rete_ica_base_paid = $bases->ica_base;
                    $rete_other_base_paid = $bases->other_base;
                }
                $purchase->rete_fuente_base_paid = $purchase->rete_fuente_base + $rete_fuente_base_paid;
                $purchase->rete_ica_base_paid = $purchase->rete_ica_base + $rete_ica_base_paid;
                $purchase->rete_iva_base_paid = $purchase->rete_iva_base + $rete_iva_base_paid;
                $purchase->rete_other_base_paid = $purchase->rete_other_base + $rete_other_base_paid;
                $purchase->purchase_in_view = false;
                $purchase->max_date = date('Y-m-d', strtotime($purchase->date));
                $purchase->activated_manually = false;
                $purchase->deactivated_manually = false;
            }
            $this->sma->send_json($purchases);
        } else {
            $this->sma->send_json(array('response' => 0));
        }
    }

    public function itemSelectUnit($product_id, $warehouse_id)
    {

        if ($this->Settings->prioridad_precios_producto == 11) {
            $q = $this->db->query("
                SELECT U.name, 0 as valor_unitario, UP.cantidad, UP.id, U.id as product_unit_id FROM {$this->db->dbprefix('unit_prices')} AS UP
                    INNER JOIN {$this->db->dbprefix('units')} AS U ON U.id = UP.unit_id
                WHERE UP.id_product = {$product_id} AND UP.status = 1
                GROUP BY UP.cantidad ASC
                ");
        } else {
            $q = $this->db->query("
                SELECT 1 as num, U2.name, P.price as valor_unitario, 1 as cantidad, 0 as id, U2.id as product_unit_id, P.main_unit_margin_update_price AS margin_update_price FROM {$this->db->dbprefix('products')} AS P
                    INNER JOIN {$this->db->dbprefix('units')} AS U2 ON U2.id = P.unit
                WHERE P.id = {$product_id}
                    UNION
                SELECT 2 as num, U.name, UP.valor_unitario, UP.cantidad, UP.id, U.id as product_unit_id, UP.margin_update_price FROM {$this->db->dbprefix('unit_prices')} AS UP
                    INNER JOIN {$this->db->dbprefix('units')} AS U ON U.id = UP.unit_id
                WHERE UP.id_product = {$product_id} AND UP.status = 1

                ORDER BY num ASC, cantidad ASC
                ");
        }
        $data = [];
        $qty = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id));
        if ($qty->num_rows() > 0) {
            $iqty = $qty->row();
            $item_qty = $iqty->quantity;
        } else {
            $item_qty = 0;
        }
        if ($q->num_rows() > 0) {
            $cnt = 0;
            foreach (($q->result()) as $up) {
                $cnt++;
                $data[] = array(
                                $cnt,
                                $up->name.($up->margin_update_price > 0 ? " (Margen ".$this->sma->formatDecimals($up->margin_update_price)."%)" : ""),
                                $this->sma->formatDecimal($item_qty / ($up->cantidad > 0 ? $up->cantidad : 1)),
                                $up->id,
                                $product_id,
                                $up->product_unit_id
                            );
            }
        }

        $dataJSON = array(
            'aaData' => $data,
            'iTotalDisplayRecords' => count($data),
            'iTotalRecords' => count($data),
            'sEcho' => count($data),
            'sColumns' => 'cnt,units.name,unit_prices.valor_unitario,unit_prices.cantidad,unit_prices.id, product_id',
        );

        echo json_encode($dataJSON);
    }

    public function iusuggestions()
    {
        $product_id = $this->input->get_post('product_id');
        $unit_price_id = $this->input->get_post('unit_price_id');
        $supplier_id = $this->input->get_post('supplier_id');
        $product_unit_id = $this->input->get_post('product_unit_id');
        $unit_quantity = $this->input->get_post('unit_quantity');
        $unit_price = $this->db->get_where('unit_prices', array('id' => $unit_price_id, 'id_product' => $product_id));
        $unit_price = $unit_price->row();
        $rows = $this->purchases_model->getProductNamesIU($product_id);
        if ($rows) {
            $r = 0;
            foreach ($rows as $row) {
                $c = uniqid(mt_rand(), true);
                $option = false;
                $row->item_tax_method = $row->tax_method;
                $options = $this->purchases_model->getProductOptions($row->id);
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->purchases_model->getProductOptionByID($option_id) : current($options);
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->cost = 0;
                    $option_id = FALSE;
                }
                $row->option = $option_id;
                if ($opt->cost != 0) {
                    $row->cost = $opt->cost;
                }
                $row->serial = '';
                $row->cost = $supplier_id ? $this->getSupplierCost($supplier_id, $row) : $row->cost;
                $row->real_unit_cost = $row->cost;
                $row->base_quantity = 1;
                $row->base_unit = $row->unit;
                $row->base_unit_cost = $row->cost;
                $row->unit = $row->purchase_unit ? $row->purchase_unit : $row->unit;
                $row->new_entry = 1;
                $row->expiry = '';
                $row->qty = 1;
                $row->quantity_balance = '';
                $row->discount = '0';
                $row->edit_item = true;
                unset($row->details, $row->product_details, $row->price, $row->file, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                $cnt_units_prices =  $this->site->get_all_product_units_prices($row->id) ? count($this->site->get_all_product_units_prices($row->id)) + 1 : 1;
                $row->cnt_units_prices = $cnt_units_prices;
                // $units = $this->site->get_product_units($row->id);
                $units = $this->site->get_product_units($row->id);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $tax_rate_2 = false;

                $row->base_quantity = ((isset($unit_price->cantidad) ? $unit_price->cantidad : 1) * $unit_quantity);
                $row->qty = $row->base_quantity > 0 ? $row->base_quantity : 1 * $unit_quantity;
                $row->base_unit = $row->unit;
                $row->unit_price_id = $unit_price_id;
                $row->product_unit_id = (isset($unit_price->unit_id) ? (int) $unit_price->unit_id : $row->unit);
                $row->product_unit_id_selected = $product_unit_id ? $product_unit_id : $row->unit;
                $product_price_groups = $this->products_model->getProductPriceGroups($row->id);
                $pr = array('id' => sha1($c . $r), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'tax_rate' => $tax_rate, 'tax_rate_2' => $tax_rate_2, 'units' => $units, 'options' => $options, 'product_price_groups' => $product_price_groups);
                $r++;
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function purchaseView($purchase_id = null, $download_pdf = false)
    {
        $this->sma->checkPermissions('index', true);

        $this->load->library('qr_code');

        if ($this->input->get('id')) {
            $purchase_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->purchases_model->getPurchaseByID($purchase_id);

        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $detail_payment_text = "";
        $payment_text = lang('due')." ".$inv->payment_term." ".($inv->payment_term > 1 || $inv->payment_term == 0 ? "Días" : "Día");
        $paid_in_cash = false;
        $paid_mixed = false;
        $this->data['payments'] = $this->purchases_model->getPaymentsForPurchase($purchase_id);
        if ($this->data['payments']) {
            foreach ($this->data['payments'] as $payment) {
                if ($payment->paid_by == 'retention' || $payment->paid_by == 'retencion') {
                    continue;
                }
                if ($payment->date == $inv->date) {
                    $paid_in_cash = true;
                } else {
                    if ($paid_in_cash) {
                        $paid_in_cash = false;
                        $paid_mixed = true;
                    }
                }
                $detail_payment_text.= lang($payment->paid_by)." (".$this->sma->formatMoney($payment->amount)."), ";
            }
        }
        $inv->note .= ". Pagos : ".trim($detail_payment_text, ", ");
        // exit(var_dump($inv->note));
        if ($paid_in_cash) {
            $payment_text = lang('payment_type_cash');
        }
        if ($paid_mixed) {
            $payment_text = lang('mixed');
        }
        $this->data['document_type'] = $this->site->getDocumentTypeById(NULL, $inv->reference_no);
        $this->data['payment_text'] = $payment_text;
        $document_type = $this->site->getDocumentTypeById(NULL, $inv->reference_no);
        if ($this->data['document_type'] && ($this->data['document_type']->module == 35) && empty($inv->resolucion)) {
            $inv->resolucion = $this->site->textoResolucion($document_type);
        }
        $this->data['rows'] = $this->purchases_model->getAllPurchaseItems($purchase_id);
        $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $inv->biller_id, false);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        // $this->sma->print_arrays($this->data['created_by']);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['return_purchase'] = $this->purchases_model->getReturnsInvoicesByPurchaseID($purchase_id);
        $this->data['return_rows'] = $this->purchases_model->getAllReturnsItems($purchase_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter(NULL, $inv->reference_no);
        $this->data['sma'] = $this->sma;
        $tipo_regimen = $this->Settings->tipo_regimen ? $this->site->get_types_vat_regime($this->Settings->tipo_regimen) : false;
        $this->data['tipo_regimen'] = $tipo_regimen ? lang($tipo_regimen->description) : '';

        $this->data['signature_root'] = is_file("assets/uploads/signatures/" . $this->Settings->digital_signature) ? base_url() . 'assets/uploads/signatures/' . $this->Settings->digital_signature : false;
        $this->data['download'] = $download_pdf;
        $this->data['for_email'] = false;
        $currency = $this->site->getCurrencyByCode($inv->purchase_currency);
        $trmrate = 1;
        if (!empty($inv->purchase_currency) && $inv->purchase_currency != $this->Settings->default_currency) {
            $actual_currency_rate = $currency->rate;
            $trmrate = $actual_currency_rate / $inv->purchase_currency_trm;
        }
        $this->data['trmrate'] = $trmrate;
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->cost_center_id);
        }
        $this->data['biller_logo'] = 2;
        $this->data['show_code'] = 1;
        $this->data['tax_indicator'] = 1;
        $url = 'purchases/purchase_view';
        if ($inv->status == 'returned') {
            $url = "purchases/return_purchase_view";
        }
        if (!empty($inv->resolucion)) {
            $url = "purchases/support_document_view";
        }
        $this->data['product_detail_font_size'] = 0;
        if ($inv->module_invoice_format_id != NULL) {
            $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
            if ($document_type_invoice_format) {
                $this->data['qty_decimals'] = $document_type_invoice_format->qty_decimals;
                $this->data['tax_indicator'] = $document_type_invoice_format->tax_indicator;
                $this->data['value_decimals'] = $document_type_invoice_format->value_decimals;
                $this->data['biller_logo'] = $document_type_invoice_format->logo;
                $this->data['show_code'] = $document_type_invoice_format->product_show_code;
                $this->data['product_detail_font_size'] = $document_type_invoice_format->product_detail_font_size > 0 ? $document_type_invoice_format->product_detail_font_size : 0;
                $url = $document_type_invoice_format->format_url;
            }
        }
        $taxes = $this->site->getAllTaxRates();
        $taxes_details = [];
        foreach ($taxes as $tax) {
            $taxes_details[$tax->id] = $tax->name;
        }
        $this->data['taxes_details'] = $taxes_details;

        $taxes2 = $this->site->getSecondTaxes();
        $taxes_details_2 = [];
        foreach ($taxes2 as $tax) {
            $taxes_details_2[$tax->id] = $tax->code;
        }
        $this->data['taxes_details_2'] = $taxes_details_2;

        $dir = 'themes/default/admin/assets/images/qr_code/';
        $filename = $dir . 'QRCodeDS.png';
        if (!file_exists($dir)) {
            mkdir($dir, 0777);
        }
        $qrCode = $this->SupportDocument_model->createQRCode($inv);
        QRcode::png($qrCode, $filename, 'L', 3, 0);

        // $url = 'purchases/purchase_view';
        $this->load_view($this->theme . $url, $this->data);
    }

    public function validate_supplier_consecutive($consecutive, $supplier_id)
    {
        echo $this->purchases_model->validate_supplier_consecutive($consecutive, $supplier_id);
    }

    public function add_support_document($quote_id = null)
    {
        $this->sma->checkPermissions('add', null, 'supportingDocument');

        $this->data['PER_OPERATION'] = self::PER_OPERATION;
        $this->data['WEEKLY_ACCUMULATED'] = self::WEEKLY_ACCUMULATED;
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['quote_id'] = $quote_id;
        $this->data['suppliers'] = $this->site->getAllCompanies('supplier');
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['categories'] = $this->site->getAllCategories();
        $trates = $this->site->getAllTaxRates();
        $tax_rates = [];
        foreach ($trates as $tr) {
            $tax_rates[$tr->id] = $tr;
        }
        $this->data['tax_rates'] = $tax_rates;
        $this->data['warehouses'] = $this->site->getAllWarehouses(1);
        $this->data['ponumber'] = '';
        $this->data['currencies'] = $this->site->getAllCurrencies();
        if ($this->Settings->cost_center_selection == 1) {
            $this->data['cost_centers'] = $this->site->getAllCostCenters();
        }
        $this->data['reference'] = (isset($this->data['quote']) && $this->data['quote']->quote_type == 2 ? $this->site->getReference('ex', true) : $this->site->getReference('po', true)) . "-";
        $this->load->helper('string');
        $value = random_string('alnum', 20);
        $this->session->set_userdata('user_csrf', $value);
        $this->data['csrf'] = $this->session->userdata('user_csrf');

        if ($this->input->post()) {
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->page_construct('purchases/add_support_document', ['page_title' => lang('add_support_document')], $this->data);
        }
    }

    public function edit_support_document($id)
    {
        $this->sma->checkPermissions('edit', null, 'supportingDocument');

        $inv = $this->purchases_model->getPurchaseByID($id);
        $inv_items = $this->purchases_model->getAllPurchaseItems($id);
        $this->data['PER_OPERATION'] = self::PER_OPERATION;
        $this->data['WEEKLY_ACCUMULATED'] = self::WEEKLY_ACCUMULATED;
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['inv'] = $inv;
        if ($this->Settings->disable_editing) {
            if ($inv->date <= date('Y-m-d', strtotime('-' . $this->Settings->disable_editing . ' days'))) {
                $this->session->set_flashdata('error', sprintf(lang("purchase_x_edited_older_than_x_days"), $this->Settings->disable_editing));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        }
        $c = rand(100000, 9999999);
        $edit_status = 1;
        foreach ($inv_items as $item) {
            if ($inv->purchase_type == 1) {
                $row = $this->site->getProductByID($item->product_id);
            } else {
                $row = $this->site->getExpenseCategory($item->product_id);
            }
            // $row = $this->site->getProductByID($item->product_id);
            $row->expiry = (($item->expiry && $item->expiry != '0000-00-00') ? $this->sma->hrsd($item->expiry) : '');
            $row->id = $item->product_id;
            $row->purchase_item_id = $item->id;
            $row->code = $item->product_code;
            $row->name = $item->product_name;
            $row->base_quantity = $item->quantity;
            $row->base_unit = isset($row->unit) ? $row->unit : $item->product_unit_id;
            $row->base_unit_cost = $item->unit_cost ? $item->unit_cost + $item->item_discount : $row->unit_cost;
            $row->unit = $item->product_unit_id;
            $row->qty = $item->quantity;
            $row->option = $item->option_id;
            $row->discount = $item->item_discount > 0 ? (string) ($item->item_discount / $item->quantity) : '0';
            $row->cost = $item->net_unit_cost;
            $row->tax_rate = $item->tax_rate_id;
            $row->tax_rate_2 = $item->tax_rate_2_id;
            $row->shipping_unit_cost = $item->shipping_unit_cost;
            $row->expiry = '';
            $row->real_unit_cost = $item->real_unit_cost;
            $row->product_unit_id_selected = $item->product_unit_id_selected ? $item->product_unit_id_selected : $item->product_unit_id;
            $row->product_unit_id = $item->product_unit_id;
            if ($this->Settings->product_variant_per_serial == 1) {
                $options = false;
            } else {
                $options = $this->purchases_model->getProductOptions($row->id);
            }
            $units = $this->site->get_product_units($row->id);
            $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
            if ($tax_rate) {
                if ($tax_rate->rate < 1 && $tax_rate->type == 2 && $item->item_tax > 0) {
                    $tax_rate->rate = $item->item_tax;
                }
            }
            if ($inv->purchase_type == 1) {
                $tax_rate_2 = false;
            } else {
                $tax_rate_2 = $this->site->getTaxRateByID($row->tax_rate_2);
                if ($tax_rate_2) {
                    if ($tax_rate_2->rate < 1 && $tax_rate_2->type == 2 && $item->item_tax_2 > 0) {
                        $tax_rate_2->rate = $item->item_tax_2;
                    }
                }
            }

            $row->oqty = $item->quantity;
            $row->supplier_part_no = $item->supplier_part_no;
            $row->received = $item->quantity_received ? $item->quantity_received : $item->quantity;
            $row->quantity_balance = $item->quantity_balance + ($item->quantity - $row->received);
            if ($this->Settings->product_variant_per_serial == 1) {
                $row->serialModal_serial = $item->serial_no;
            }
            if ($item->quantity_balance < $item->quantity) {
                $edit_status = 0;
            }
            $row->expense_tax_rate_id = $row->tax_rate;
            $row->edit_item = true;
            if ($this->Settings->ignore_purchases_edit_validations == 0 && ($row->quantity_balance < $row->qty)) {
                $row->edit_item = false;
            }
            $ri = $this->Settings->item_addition ? $row->id : $c;

            $pr[$ri] = array(
                'id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                'row' => $row, 'tax_rate' => $tax_rate, 'tax_rate_2' => $tax_rate_2, 'units' => $units, 'options' => $options, 'order' => $item->id
            );
            $c++;
        }
        if ($inv->status == 'received') {
            $edit_status = 0;
        }

        $product_has_movements_after_date = false;
        if ($this->Settings->ignore_purchases_edit_validations == 0) {
            foreach ($inv_items as $iik => $inv_item) {
                $pitems[$inv_item->id] = $inv_item;
                if ($this->Settings->ignore_purchases_edit_validations == 0 && $this->purchases_model->product_has_movements_after_date($inv_item->product_id, $inv->date, $id)) {
                    $product_has_movements_after_date = true;
                }
            }
        }

        $purchase_has_payments = false;
        $purchase_has_multi_payments = false;
        if ($purchase_payments = $this->purchases_model->getPurchasePayments($id, true)) {
            $purchase_has_payments = true;
            foreach ($purchase_payments as $pp) {
                if ($pp->multi_payment == 1) {
                    $purchase_has_multi_payments = true;
                    break;
                }
            }
        }

        $this->data['inv_items'] = json_encode($pr);
        $this->data['product_has_movements_after_date'] = $product_has_movements_after_date;
        $this->data['purchase_has_payments'] = $purchase_has_payments;
        $this->data['purchase_has_multi_payments'] = $purchase_has_multi_payments;
        $this->data['id'] = $id;
        $this->data['suppliers'] = $this->site->getAllCompanies('supplier');
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['tags'] = $this->site->get_tags_by_module(4);

        $trates = $this->site->getAllTaxRates();
        $tax_rates = [];
        foreach ($trates as $key => $tr) {
            $tax_rates[$tr->id] = $tr;
        }
        $this->data['tax_rates'] = $tax_rates;
        $this->data['warehouses'] = $this->site->getAllWarehouses(1);
        $this->data['ponumber'] = '';
        $this->data['currencies'] = $this->site->getAllCurrencies();
        $this->data['edit_status'] = $edit_status;
        $this->data['purchase_payments'] = $purchase_payments;
        if ($this->Settings->cost_center_selection == 1) {
            $this->data['cost_centers'] = $this->site->getAllCostCenters();
        }
        $this->load->helper('string');
        $value = random_string('alnum', 20);
        $this->session->set_userdata('user_csrf', $value);
        $this->session->set_userdata('remove_pols', 1);
        $this->data['csrf'] = $this->session->userdata('user_csrf');

        $this->page_construct('purchases/edit_support_document', ['page_title' => lang('edit_support_document')], $this->data);
    }

    public function zebra_products_print($purchase_id = null, $download_pdf = false)
    {
        $this->sma->checkPermissions('index', true);

        if ($this->input->get('id')) {
            $purchase_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->purchases_model->getPurchaseByID($purchase_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['rows'] = $this->purchases_model->getAllPurchaseItems($purchase_id);
        $this->data['supplier'] = $this->site->getCompanyByID($inv->supplier_id);
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $inv->biller_id);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['payments'] = $this->purchases_model->getPaymentsForPurchase($purchase_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['return_purchase'] = $this->purchases_model->getReturnsInvoicesByPurchaseID($purchase_id);
        $this->data['return_rows'] = $this->purchases_model->getAllReturnsItems($purchase_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter(NULL, $inv->reference_no);
        $this->data['document_type'] = $this->site->getDocumentTypeById(NULL, $inv->reference_no);
        $this->data['sma'] = $this->sma;
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        $taxes = $this->site->getAllTaxRates();
        $taxes_details = [];
        foreach ($taxes as $tax) {
            $taxes_details[$tax->id] = $tax->rate;
        }
        $this->data['taxes_details'] = $taxes_details;
        $this->data['signature_root'] = is_file("assets/uploads/signatures/" . $this->Settings->digital_signature) ? base_url() . 'assets/uploads/signatures/' . $this->Settings->digital_signature : false;
        $this->data['download'] = $download_pdf;
        $this->data['for_email'] = false;
        $currency = $this->site->getCurrencyByCode($inv->purchase_currency);
        $trmrate = 1;
        if (!empty($inv->purchase_currency) && $inv->purchase_currency != $this->Settings->default_currency) {
            $actual_currency_rate = $currency->rate;
            $trmrate = $actual_currency_rate / $inv->purchase_currency_trm;
        }
        $this->data['trmrate'] = $trmrate;
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->cost_center_id);
        }
        $this->data['biller_logo'] = 2;
        $url = 'purchases/zebra_products_print';
        if ($inv->module_invoice_format_id != NULL) {
            $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
            if ($document_type_invoice_format) {
                $this->data['qty_decimals'] = $document_type_invoice_format->qty_decimals;
                $this->data['value_decimals'] = $document_type_invoice_format->value_decimals;
                $this->data['biller_logo'] = $document_type_invoice_format->logo;
                $url = $document_type_invoice_format->format_url;
            }
        }
        $this->load_view($this->theme . $url, $this->data);
    }

    public function adjustment_zebra_products_print($purchase_id = null, $download_pdf = false)
    {
        $this->sma->checkPermissions('index', true);

        if ($this->input->get('id')) {
            $purchase_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->purchases_model->getPurchaseByID($purchase_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['rows'] = $this->purchases_model->getAllPurchaseItems($purchase_id);
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $inv->biller_id);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['payments'] = $this->purchases_model->getPaymentsForPurchase($purchase_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['return_purchase'] = $this->purchases_model->getReturnsInvoicesByPurchaseID($purchase_id);
        $this->data['return_rows'] = $this->purchases_model->getAllReturnsItems($purchase_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter(NULL, $inv->reference_no);
        $this->data['document_type'] = $this->site->getDocumentTypeById(NULL, $inv->reference_no);
        $this->data['sma'] = $this->sma;
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        $taxes = $this->site->getAllTaxRates();
        $taxes_details = [];
        foreach ($taxes as $tax) {
            $taxes_details[$tax->id] = $tax->rate;
        }
        $this->data['taxes_details'] = $taxes_details;
        $this->data['signature_root'] = is_file("assets/uploads/signatures/" . $this->Settings->digital_signature) ? base_url() . 'assets/uploads/signatures/' . $this->Settings->digital_signature : false;
        $this->data['download'] = $download_pdf;
        $this->data['for_email'] = false;
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->cost_center_id);
        }
        $this->data['biller_logo'] = 2;
        $url = 'purchases/zebra_products_print';
        $this->load_view($this->theme . $url, $this->data);
    }

    public function contabilizarGasto($expense_id = null, $json = null)
    {
        $this->form_validation->set_rules('invoice_id', lang("invoice_id"), 'required');
        if ($this->form_validation->run() == true) {
            $expense_id = $this->input->post('invoice_id');
            $msg = $this->purchases_model->recontabilizar_gasto($expense_id);
            $this->session->set_flashdata('message', $msg);
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'purchases/expenses');
        } else {
            $expense = $this->purchases_model->getExpenseByID($expense_id, true);
            $exists = $this->site->getEntryTypeNumberExisting($expense, false);
            $this->data['exists'] = $exists;
            $this->data['inv'] = $expense;
            $this->load_view($this->theme . 'purchases/recontabilizar_gasto', $this->data);
        }
    }

    public function return_other_concepts(int $purchase_id = NULL)
    {
        $this->sma->checkPermissions();

        $discounts = FALSE;
        $amount_discounted = 0;
        $purchase_data = $this->purchases_model->getPurchaseByID($purchase_id);
        $documentType = $this->site->getDocumentTypeById($purchase_data->document_type_id);
        $purchase_returns = $this->purchases_model->getReturnsInvoicesByPurchaseID($purchase_id);

        if (abs($purchase_returns->grand_total) == $purchase_data->grand_total) {
            $this->session->set_flashdata("error", lang('Purchase_fully_returned'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($purchase_data->status != 'received') {
            $this->session->set_flashdata('error', lang("purchase_status_x_received"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->Settings->cost_center_selection == DEFINED_IN_EACH_RECORD) {
            $this->data['cost_centers'] = $this->site->getAllCostCenters();
        }

        $this->data['payment_ref'] = '';
        $this->data['isElectronic'] = $documentType->factura_electronica;
        $this->data["purchase_data"] = $purchase_data;
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data["credit_note_concepts"] = $this->purchases_model->get_credit_note_concepts($documentType->factura_electronica);
        $this->data['allow_payment'] = ($purchase_data->payment_status == 'paid' || $purchase_data->payment_status == 'partial') ? TRUE : FALSE;
        if ($this->Settings->cost_center_selection == 1) {
            $this->data['cost_centers'] = $this->site->getAllCostCenters();
        }

        $this->page_construct('purchases/add_return_other_concepts', ['page_title' => lang('return_other_concepts_purcahes')], $this->data);
    }

    public function save_return_other_concepts()
    {
        $return_data = $this->build_return_data();
        $return_item_data = $this->build_return_item_data();
        $payment_data = $this->build_payment_data();

        $return_created = $this->purchases_model->insert_return_other_concepts($return_data, $return_item_data, $payment_data);

        if ($return_created !== FALSE) {
            $this->session->set_flashdata("message", "La devolución fue creada exitosamente.");
        } else {
            $this->session->set_flashdata("error", "No fue posible crear la Devolución.");
        }

        admin_redirect('purchases');
    }

    private function build_return_data()
    {
        $total_retenciones = 0;
        if ($this->input->post('withholdings_applied') == 1) {
            $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
            $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
            $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
            $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
            $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total;
        }


        $credit_note_data = [
            "date" => ($this->Owner || $this->Admin) ? $this->sma->fld(trim($this->input->post('date'))) : date('Y-m-d H:i:s'),
            "supplier_id" => $this->input->post("supplier_id"),
            "supplier" => $this->input->post("supplier_name"),
            "warehouse_id" => $this->input->post("warehouse_id"),
            "note" => $this->input->post("note"),
            "total" => -abs($this->input->post("concept_value")),
            "product_tax" => -abs($this->input->post("concept_tax")),
            "total_tax" => -abs($this->input->post("concept_tax")),
            "grand_total" => -abs($this->input->post("total_value_concept") + $total_retenciones),
            "paid" => -abs($this->input->post("total_value_concept") + $total_retenciones),
            "status" => "returned",
            "payment_status" => "paid",
            "created_by" => $this->session->userdata("user_id"),
            "return_purchase_ref" => $this->input->post("purchase_reference"),
            "purchase_id" => $this->input->post("purchase_id"),
            'order_discount_method' => $this->input->post('order_discount_method'),
            "purchase_currency" => $this->input->post("purchase_currency"),
            'purchase_type' => $this->input->post('purchase_type'),
            'payment_affects_register' => YES,
            "biller_id" => $this->input->post("biller_id"),
            'document_type_id' => $this->input->post('document_type_id'),
            'registration_date' => date('Y-m-d H:i:s')
        ];

        $biller_cost_center = $this->site->getBillerCostCenter($this->input->post("biller_id"));
        if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
            $credit_note_data['cost_center_id'] = $biller_cost_center->id;
        } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
            $credit_note_data['cost_center_id'] = $this->input->post('cost_center_id');
        }

        if ($this->input->post('withholdings_applied') == 1) {
            $credit_note_data['rete_fuente_percentage'] = $this->input->post('rete_fuente_tax');
            $credit_note_data['rete_fuente_total'] = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
            $credit_note_data['rete_fuente_account'] = $this->input->post('rete_fuente_account');
            $credit_note_data['rete_fuente_base'] = $this->input->post('rete_fuente_base');
            $credit_note_data['rete_fuente_id'] = $this->input->post('rete_fuente_id');

            $credit_note_data['rete_iva_percentage'] = $this->input->post('rete_iva_tax');
            $credit_note_data['rete_iva_total'] = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
            $credit_note_data['rete_iva_account'] = $this->input->post('rete_iva_account');
            $credit_note_data['rete_iva_base'] = $this->input->post('rete_iva_base');
            $credit_note_data['rete_iva_id'] = $this->input->post('rete_iva_id');

            $credit_note_data['rete_ica_percentage'] = $this->input->post('rete_ica_tax');
            $credit_note_data['rete_ica_total'] = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
            $credit_note_data['rete_ica_account'] = $this->input->post('rete_ica_account');
            $credit_note_data['rete_ica_base'] = $this->input->post('rete_ica_base');
            $credit_note_data['rete_ica_id'] = $this->input->post('rete_ica_id');

            $credit_note_data['rete_other_percentage'] = $this->input->post('rete_otros_tax');
            $credit_note_data['rete_other_total'] = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
            $credit_note_data['rete_other_account'] = $this->input->post('rete_otros_account');
            $credit_note_data['rete_other_base'] = $this->input->post('rete_otros_base');
            $credit_note_data['rete_other_id'] = $this->input->post('rete_other_id');
        }

        return $credit_note_data;
    }

    private function build_return_item_data()
    {
        $credit_note_item_data = [
            "product_id" => 999999999,
            "product_code" => $this->input->post("concept_id"),
            "product_name" => $this->input->post("concept_name"),
            "quantity" => $this->sma->formatDecimal(-abs(1)),
            "warehouse_id" => $this->input->post("warehouse_id"),
            "item_tax" => $this->sma->formatDecimal(-abs($this->input->post("concept_tax"))),
            "tax_rate_id" => $this->input->post("concept_tax_rate_id"),
            "tax" => $this->sma->formatDecimal($this->input->post("concept_tax_rate"), 0) . "%",
            "subtotal" => $this->sma->formatDecimal(-abs(($this->input->post("concept_value") * 1) + $this->input->post("concept_tax"))),
            'status' => 'received',
            "unit_quantity" => $this->sma->formatDecimal(-abs(1)),
            'registration_date' => date('Y-m-d H:i:s'),
            "net_unit_cost" => $this->sma->formatDecimal($this->input->post("concept_value")),
            "unit_cost" => $this->sma->formatDecimal($this->input->post("concept_value") + $this->input->post("concept_tax")),
            "real_unit_cost" => $this->sma->formatDecimal($this->input->post("concept_value") + $this->input->post("concept_tax"))
        ];

        return $credit_note_item_data;
    }

    private function build_payment_data()
    {
        $payment = [];

        $purchase_data = $this->purchases_model->getPurchaseByID($this->input->post("purchase_id"));
        $date = date('Y-m-d H:i:s');
        $total = -abs($this->input->post("concept_value"));
        $grand_total = $this->input->post("total_value_concept");

        $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
        $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
        $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
        $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
        $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total;

        if ($this->input->post('withholdings_applied') == 1) {
            $retencion = array(
                'date'         => $date,
                'amount'       => -abs($total_retenciones),
                'reference_no' => 'retencion',
                'paid_by'      => 'retencion',
                'cheque_no'    => '',
                'cc_no'        => '',
                'cc_holder'    => '',
                'cc_month'     => '',
                'cc_year'      => '',
                'cc_type'      => '',
                'created_by'   => $this->session->userdata('user_id'),
                'type'         => 'sent',
                'note'         => 'Retenciones',
            );
            $payment[] = $retencion;
        }

        $purchase_return_balance = 0;
        // Cuando existe valor a devolver
        if ($this->input->post('amount-paid') && $this->input->post('amount-paid') > 0) {
            $data_payment = array(
                'date'          => $date,
                'document_type_id'  => $this->input->post('payment_reference_no'),
                'amount'        => -abs($this->input->post('amount-paid')),
                'paid_by'       => $this->input->post('paid_by'),
                'cheque_no'     => $this->input->post('cheque_no'),
                'cc_no'         => $this->input->post('pcc_no'),
                'cc_holder'     => $this->input->post('pcc_holder'),
                'cc_month'      => $this->input->post('pcc_month'),
                'cc_year'       => $this->input->post('pcc_year'),
                'cc_type'       => $this->input->post('pcc_type'),
                'created_by'    => $this->session->userdata('user_id'),
                'type'          => 'returned',
                'comm_base'     => 0,
                'comm_amount'   => 0,
                'comm_perc'     => 0,
                'mean_payment_code_fe' => $this->input->post('mean_payment_code_fe')
            );

            if ($this->input->post('paid_by') == 'deposit') {
                $this->form_validation->set_rules('deposit_document_type_id', lang("deposit_document_type_id"), 'required');
                $data_payment['deposit_document_type_id'] = $this->input->post('deposit_document_type_id');
            }

            $purchase_return_balance = -abs($grand_total) - $this->input->post('amount-paid');
            $payment[] = $data_payment;
        } else {
            $data_payment = array(
                'date'                  => $date,
                'document_type_id'      => $this->input->post('payment_reference_no'),
                'amount'                => -abs($grand_total),
                'paid_by'               => 'due',
                'cheque_no'             => NULL,
                'cc_no'                 => NULL,
                'cc_holder'             => NULL,
                'cc_month'              => NULL,
                'cc_year'               => NULL,
                'cc_type'               => NULL,
                'created_by'            => $this->session->userdata('user_id'),
                'type'                  => 'returned',
                'mean_payment_code_fe'  => "ZZZ"
            );
            $data['payment_status'] = 'paid';
            $payment[] = $data_payment;
        }

        if ($purchase_return_balance > 0) {
            $pay_ref = $this->input->post('payment_reference_no');
            $data_payment = array(
                'date'          => $date,
                'document_type_id'  => $pay_ref,
                'amount'        => -abs($purchase_return_balance),
                'paid_by'       => 'due',
                'cheque_no'     => NULL,
                'cc_no'         => NULL,
                'cc_holder'     => NULL,
                'cc_month'      => NULL,
                'cc_year'       => NULL,
                'cc_type'       => NULL,
                'created_by'    => $this->session->userdata('user_id'),
                'type'          => 'returned',
            );
            $data['payment_status'] = 'paid';
            $payment[] = $data_payment;
        }

        return $payment;
    }

    public function sendElectronicDocument($purchaseId)
    {
        if ($_SERVER['SERVER_NAME'] == 'localhost') {
            return true;
            return true;
            return true;
        }
        $purchase = $this->site->getPurchaseByID($purchaseId);

        if ($this->Settings->supportingDocument == YES) {
            $resolutionData = $this->SupportDocument_model->getResolutionData($purchase->document_type_id);

            if ($resolutionData->factura_electronica == YES) {
                $this->SupportDocument_model->sendElectronicDocument($purchaseId);
            }
        }

        $this->session->set_flashdata('error', 'El tipo de documento no está configurado como "Tipo electrónico"');
        admin_redirect('purchases/supporting_document_index');
    }

    public function showPreviousRequest($documentId)
    {
        $purchases = $this->site->getPurchaseByID($documentId);
        $resolution = $this->SupportDocument_model->getResolutionData($purchases->document_type_id);

        if ($resolution->factura_electronica == YES) {
            $this->SupportDocument_model->buildRequestFile($documentId, TRUE);
        } else {
            $this->session->set_flashdata('error', 'El tipo de documento no está configurado como "Tipo electrónico"');
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function checkStatus($documentId)
    {
        $response = $this->SupportDocument_model->cadenaWsCheckStatus($documentId);
        $this->SupportDocument_model->manageResponseCheckStatus($response, $documentId);

        $this->session->set_flashdata('error', 'No fue posible continuar con el proceso.');
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function sendEmail($documentId)
    {
        $this->SupportDocument_model->sendEmail($documentId);
    }

    public function getMessage()
    {
        $documentId = $this->input->post('documentId');
        $purchase = $this->site->getPurchaseByID($documentId);

        if ($purchase->statusCode == 200) {
            $response = [
                'title' => $purchase->statusMessage,
                'text' => $purchase->statusDescription
            ];
        } else {
            $response = [
                'title' => $purchase->errorMessage,
                'text' => $purchase->errorReason
            ];
        }

        echo json_encode($response);
    }

    public function syncDocumentReception()
    {
        $documentsReception = $this->documentReception_model->getAllDocumentsReceivedClient();
        if (!empty($documentsReception)) {
            foreach ($documentsReception as $document) {
                $purchase = $this->purchases_model->getPurchaseByReferenceNo($document->documentId);
                if (!empty($purchase)) {
                    $this->purchases_model->update_reference_invoice(['existsInDocumentsReception' => YES], $purchase->id);
                }
            }
        }

        echo json_encode([
            'status'  => TRUE,
            'message' => $this->lang->line('processCompletedSuccessfully')
        ]);
    }

    public function invoice_date_existing_in_resolution_date_range()
    {
        $date = ($this->input->get('format_the_date') == 1) ? $this->sma->fld($this->input->get("date")) : $this->input->get("date");
        $document_type_id = $this->input->get("document_type_id");
        $document_type = $this->site->getDocumentTypeById($document_type_id);

        if ($this->site->invoice_date_existing_in_resolution_date_range($date, $document_type) == FALSE) {
            echo json_encode(FALSE);
        } else {
            echo json_encode(TRUE);
        }
    }

    public function add_import()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        $this->form_validation->set_rules('date', lang("date"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('tag', lang("tag"), 'required');
        $this->form_validation->set_rules('supplier', lang("supplier"), 'required');
        $this->session->unset_userdata('csrf_token');
        if ($this->form_validation->run() == true) {
            $document_type_id = $this->input->post('document_type_id');
            $supplier_id = $this->input->post('supplier');
            $date = $this->sma->fld($this->input->post('date'));
            $biller = $this->input->post('biller');
            $tag = $this->input->post('tag');
            $status = $this->input->post('status');
            $product_id = $this->input->post('product_id');
            $purchase_item_id = $this->input->post('purchase_item_id');
            $purchase_id = $this->input->post('purchase_id');
            $quantity = $this->input->post('quantity');
            $net_unit_cost = $this->input->post('net_unit_cost');
            $adjustment_cost = $this->input->post('adjustment_cost');
            $rows = [];
            foreach ($product_id as $pkey => $p_id) {
                $rows[] = [
                            'product_id' => $adjustment_cost[$pkey] > 0 ? $p_id : NULL ,
                            'expense_category_id' => $adjustment_cost[$pkey] > 0 ? NULL : $p_id,
                            'purchase_item_id' => $purchase_item_id[$pkey],
                            'purchase_id' => $purchase_id[$pkey],
                            'quantity' => $quantity[$pkey],
                            'net_unit_cost' => $net_unit_cost[$pkey],
                            'adjustment_cost' => $adjustment_cost[$pkey],
                            'total_adjustment_cost' => ($adjustment_cost[$pkey] * $quantity[$pkey]),
                            ];
            }
            $data = [
                        'document_type_id' => $document_type_id,
                        'supplier_id' => $supplier_id,
                        'date' => $date,
                        'biller_id' => $biller,
                        'tag_id' => $tag,
                        'status' => $status,
                        'created_by' => $this->session->userdata('user_id'),
                        'registration_date' => date('Y-m-d H:i:s'),
                    ];
            $this->purchases_model->import = $data;
            $this->purchases_model->import_detail = $rows;
            // $this->sma->print_arrays($data, $rows);
        }

        if ($this->form_validation->run() == true && $purchaseId = $this->purchases_model->addImport()) {
            $this->session->set_flashdata('message', $this->lang->line("import_added"));
            $this->session->set_userdata('remove_imls', 1);
            admin_redirect('purchases/imports');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $trates = $this->site->getAllTaxRates();
            $tax_rates = [];
            foreach ($trates as $key => $tr) {
                $tax_rates[$tr->id] = $tr;
            }
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['tax_rates'] = $tax_rates;
            $this->load->helper('string');
            $value = random_string('alnum', 20);
            $this->session->set_userdata('user_csrf', $value);
            $this->data['csrf'] = $this->session->userdata('user_csrf');
            $this->data['price_groups'] = $this->site->getAllPriceGroups();
            $this->data['units'] = $this->site->get_all_units();
            $this->data['tags'] = $this->site->get_tags_by_module(4);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('purchases'), 'page' => lang('purchases')), array('link' => '#', 'page' => lang('add_import')));
            $meta = array('page_title' => lang('add_import'), 'bc' => $bc);
            $this->page_construct('purchases/add_import', $meta, $this->data);
        }
    }

    public function get_tagged_purchases_pending(){
        $this->purchases_model->biller_id = $this->input->post('biller_id');
        $this->purchases_model->tag_id = $this->input->post('tag_id');
        $this->purchases_model->purchase_id = $this->input->post('purchase_id');
        $purchases = $this->purchases_model->get_tagged_purchases_pending();
        echo json_encode($purchases);
    }

    public function purchase_import_suggestions()
    {
        $this->purchases_model->term = $this->input->get('term', true);
        $this->purchases_model->ptype = $this->input->get('ptype', true);
        $this->purchases_model->biller_id = $this->input->get('biller_id', true);
        $purchases = $this->purchases_model->get_purchases_pending_by_reference();
        if ($purchases) {
            $data = [];
            foreach ($purchases as $purchase) {
                $data[] = array('id' => $purchase->id, 'text' => $purchase->reference_no, 'value' => $purchase->id);
            }
            $this->sma->send_json(['results'=>$data]);
        } else {
            $this->sma->send_json(['results'=>NULL]);
        }
    }

    public function imports($warehouse_id = null)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['documents_types'] = $this->site->get_multi_module_document_types([5, 6, 21, 22]);
        $this->data['users'] = $this->site->get_all_users();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('imports')));
        $meta = array('page_title' => lang('imports'), 'bc' => $bc);
        $this->page_construct('purchases/imports', $meta, $this->data);
    }

     public function getImports()
    {
        if ($this->input->post('start_date')) {
            $start_date = $this->input->post('start_date');
            $start_date = $this->Settings->purchase_datetime_management == 1 ? $this->sma->fld($start_date) : ($start_date . " 00:00:00");
        } else {
            $start_date = NULL;
        }
        if ($this->input->post('end_date')) {
            $end_date = $this->input->post('end_date');
            $end_date = $this->Settings->purchase_datetime_management == 1 ? $this->sma->fld($end_date) : ($end_date . " 23:59:59");
        } else {
            $end_date = NULL;
        }
        $purchase_document_type_id = $this->input->post('purchase_reference_no') ? $this->input->post('purchase_reference_no') : NULL;
        $biller_id = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $status = $this->input->post('status') ? $this->input->post('status') : NULL;

        $edit_link = anchor('admin/purchases/edit_import/$1', '<i class="fa-solid fa-pen-to-square"></i> ' . lang('edit_import'));
        $detail_link = anchor('admin/purchases/importView/$1', '<i class="fa fa-file-text-o"></i> ' . lang('import_detail'));


        $action = '<div class="text-center"><div class="btn-group text-left">
            <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
            <i class="fas fa-ellipsis-v fa-lg"></i></button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li>' . $edit_link . '</li>
            </ul>
        </div></div>';

                // <li>' . $detail_link . '</li>
        $this->load->library('datatables');

        $this->datatables->select("
                            imports.id as id,
                            imports.date,
                            imports.reference_no,
                            companies.company,
                            tags.description,
                            imports.status
                            ")
                ->from('imports');

        $this->datatables->join('companies', 'companies.id = imports.biller_id', 'left');
        $this->datatables->join('tags', 'imports.tag_id = tags.id', 'left');

        if ($biller_id) {
            $this->datatables->where('imports.biller_id', $biller_id);
        }
        if ($user_id) {
            $this->datatables->where('imports.created_by', $user_id);
        }
        if ($purchase_document_type_id) {
            $this->datatables->where('imports.document_type_id', $purchase_document_type_id);
        }
        if ($start_date) {
            $this->datatables->where('imports.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('imports.date <=', $end_date);
        }
        if ($status) {
            $this->datatables->where('imports.status', $status);
        }

        if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) {
            $this->datatables->where('biller_id', $this->session->userdata('biller_id'));
        }

        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Supplier) {
            $this->datatables->where('supplier_id', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }

    public function edit_import($id)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        $this->form_validation->set_rules('date', lang("date"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('supplier', lang("supplier"), 'required');
        $this->session->unset_userdata('csrf_token');
        $this->data['import'] = $this->purchases_model->get_import_by_id($id);
        $this->data['import_detail'] = $this->purchases_model->get_import_detail($id);
        if ($this->data['import']->status == 1) {
            $this->session->set_flashdata('error', lang("cannot_edit_import_completed"));
            admin_redirect('purchases/imports');
        }
        if ($this->form_validation->run() == true) {
            $document_type_id = $this->input->post('document_type_id');
            $supplier_id = $this->input->post('supplier');
            $deleted_detail = $this->input->post('deleted_detail') ? json_decode($this->input->post('deleted_detail')) : NULL;
            $date = $this->sma->fld($this->input->post('date'));
            $biller = $this->input->post('biller');
            $tag = $this->input->post('tag');
            $status = $this->input->post('status');
            $import_detail_id = $this->input->post('import_detail_id');
            $product_id = $this->input->post('product_id');
            $purchase_item_id = $this->input->post('purchase_item_id');
            $purchase_id = $this->input->post('purchase_id');
            $quantity = $this->input->post('quantity');
            $net_unit_cost = $this->input->post('net_unit_cost');
            $adjustment_cost = $this->input->post('adjustment_cost');
            $rows = [];
            foreach ($product_id as $pkey => $p_id) {
                $rows[] = [
                            'product_id' => $adjustment_cost[$pkey] > 0 ? $p_id : NULL ,
                            'expense_category_id' => $adjustment_cost[$pkey] > 0 ? NULL : $p_id,
                            'purchase_item_id' => $purchase_item_id[$pkey],
                            'purchase_id' => $purchase_id[$pkey],
                            'quantity' => $quantity[$pkey],
                            'net_unit_cost' => $net_unit_cost[$pkey],
                            'adjustment_cost' => $adjustment_cost[$pkey],
                            'total_adjustment_cost' => ($adjustment_cost[$pkey] * $quantity[$pkey]),
                            'import_detail_id' => $import_detail_id[$pkey],
                            ];
            }
            $data = [
                        'document_type_id' => $document_type_id,
                        'supplier_id' => $supplier_id,
                        'date' => $date,
                        'biller_id' => $biller,
                        'tag_id' => $tag,
                        'status' => $status,
                        'updated_by' => $this->session->userdata('user_id'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
            $this->purchases_model->deleted_detail = $deleted_detail;
            $this->purchases_model->import_id = $id;
            $this->purchases_model->import = $data;
            $this->purchases_model->import_detail = $rows;
        }
        if ($this->form_validation->run() == true && $purchaseId = $this->purchases_model->updateImport()) {
            $this->session->set_flashdata('message', $this->lang->line("import_updated"));
            $this->session->set_userdata('remove_imls', 1);
            admin_redirect('purchases/imports');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $trates = $this->site->getAllTaxRates();
            $tax_rates = [];
            foreach ($trates as $key => $tr) {
                $tax_rates[$tr->id] = $tr;
            }
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['tax_rates'] = $tax_rates;
            $this->data['id'] = $id;
            $this->load->helper('string');
            $value = random_string('alnum', 20);
            $this->session->set_userdata('user_csrf', $value);
            $this->data['csrf'] = $this->session->userdata('user_csrf');
            $this->data['price_groups'] = $this->site->getAllPriceGroups();
            $this->data['units'] = $this->site->get_all_units();
            $this->data['tags'] = $this->site->get_tags_by_module(4);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('purchases'), 'page' => lang('purchases')), array('link' => '#', 'page' => lang('edit_import')));
            $meta = array('page_title' => lang('edit_import'), 'bc' => $bc);
            $this->page_construct('purchases/edit_import', $meta, $this->data);
        }
    }

    public function change_tag($id = null)
    {
        $this->sma->checkPermissions(false, true);
        $this->data['inv'] = $this->purchases_model->getPurchaseByID($id);
        $this->form_validation->set_rules('change_tag', lang("change_tag"), 'required');
        if ($this->form_validation->run() == true) {
            $tag_id = $this->input->post('tag');
        }
        if ($this->form_validation->run() == true && $this->purchases_model->change_purchase_tag($id, $tag_id)) {
            $this->session->set_flashdata('message', lang("tag_changed"));
            admin_redirect("purchases");
        } else {
            $this->data['tags'] = $this->site->get_tags_by_module(4);
            $this->load_view($this->theme . 'purchases/change_tag', $this->data);
        }
    }

    public function delete_purchase($id = NULL)
    {
        $this->sma->checkPermissions();
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->purchases_model->getPurchaseByID($id);
        if ($inv->status != 'pending') {
            $this->sma->send_json(array('error' => 1, 'msg' => sprintf(lang("deleting_purchase_not_pending"), $inv->reference_no)));
        }
        if ($this->purchases_model->delete_purchase($id)) {
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 2,
                'table_name' => 'purchases',
                'record_id' => $id,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => sprintf(lang('user_deleted_purchase'), $inv->reference_no),
            ]);
            $this->sma->send_json(array('error' => 0, 'msg' => lang("purchase_deleted")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("cant_delete_purchase")));
        }
    }

    public function purchase_evaluation($id)
    {
        $this->form_validation->set_rules('returns', lang("returns"), 'required');
        if ($this->form_validation->run() == true) {
            $data_arr['returns'] = $this->input->post('returns');
            $data_arr['delivery_supply'] = $this->input->post('delivery_supply');
            $data_arr['discount_variations'] = $this->input->post('discount_variations');
            $data_arr['pqrs'] = $this->input->post('pqrs');
            $data_arr['terms_of_payments'] = $this->input->post('terms_of_payments');
            $data_arr['warranty'] = $this->input->post('warranty');
            $data_arr['created_by'] = $this->session->userdata('user_id');
        } elseif ($this->input->post('update')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'purchases');
        }
        if ($this->form_validation->run() == true && $this->purchases_model->updatePurchaseEvaluation($id, $data_arr)) {
            $this->session->set_flashdata('message', lang('status_updated'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'purchases');
        } else {
            $this->data['inv'] = $this->purchases_model->getPurchaseByID($id);
            $this->data['evaluation'] = $this->purchases_model->getPurchaseEvaluationById($id);
            if ($this->data['evaluation']) {
                $this->data['user_evaluator'] = $this->site->getUser($this->data['evaluation']->created_by);
            }
            $warehouse = $this->site->getWarehouseByID($this->data['inv']->warehouse_id);
            $this->data['warehouse'] = $warehouse;
            $this->data['returned'] = FALSE;
            if ($this->data['inv']->status == 'returned' || $this->data['inv']->return_id) {
                $this->data['returned'] = TRUE;
            }
            $this->data['received'] = FALSE;
            if ($this->data['inv']->status == 'received') {
                $this->data['received'] = TRUE;
            }
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'purchases/purchase_evaluation', $this->data);
        }
    }
}