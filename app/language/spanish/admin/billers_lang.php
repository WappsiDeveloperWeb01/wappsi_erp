<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 *
 */

$lang['add_biller']                     = "Agregar sucursales";
$lang['edit_biller']                    = "Editar sucursal";
$lang['delete_biller']                  = "Eliminar sucursal";
$lang['deactivate_biller']              = "Inactivar sucursal";
$lang['activate_biller']              	= "Activar sucursal";
$lang['delete_billers']                 = "Eliminar sucursales";
$lang['biller_added']                   = "Sucursal agreagada correctamente";
$lang['biller_updated']                 = "sucursal actualizada correctamente";
$lang['biller_deleted']                 = "sucursal eliminada correctamente";
$lang['billers_deleted']                = "Sucursales eliminadas correctamente";
$lang['no_biller_selected']             = "No hay sucursales seleccionada. Por favor seleccione al menos una.";
$lang['invoice_footer']                 = "Pie de página de factura";
$lang['biller_x_deleted_have_sales']    = "Accion fallida, la sucursal tiene ventas";
$lang['billers_x_deleted_have_sales']   = "algunos sucursales no pueden ser eliminadas ya que tiene ventas";
$lang['default_customer'] 				= "Cliente predeterminado";
$lang['default_warehouse'] 				= "Bodega predeterminada";
$lang['default_group_price'] 			= "Lista de precios predeterminada";
$lang['default_seller'] 				= "Vendedor predeterminado";

$lang['biller_deactivated']				= "La sucursal se ha desactivado";
$lang['biller_activated']				= "La sucursal se ha activado";
$lang['biller_x_deactivate_is_biller_default']    = "La sucursal a desactivar está parametrizada cómo por defecto, cambie dicho parámetro e inténtelo de nuevo";
$lang["product_order"]	= "Orden de productos";
$lang["default"]	= "por defecto";

$lang['label_location'] = "Localidad";
$lang['label_tradename'] = "Nombre comercial";

$lang['autoica_percentage'] = 'Porcentaje de autorretención al ICA';
$lang['autoica_account'] = 'Cuenta auxiliar de autorretención al ICA';
$lang['autoica_account_counterpart'] = 'Cuenta de contrapartida de autorretención al ICA';

$lang['bomberil_percentage'] = 'Porcentaje de Auto tasa Bomberil';
$lang['bomberil_account'] = 'Cuenta auxiliar de Auto tasa Bomberil';
$lang['bomberil_account_counterpart'] = 'Cuenta de contrapartida de Auto tasa Bomberil';

$lang['autoaviso_percentage'] = 'Porcentaje de Auto Aviso y Tableros';
$lang['autoaviso_account'] = 'Cuenta auxiliar de Auto Aviso y Tableros';
$lang['autoaviso_account_counterpart'] = 'Cuenta de contrapartida de Auto Aviso y Tableros';


$lang['user_fields_changed_settings']                   = '%s hizo los siguientes cambios en %s ';
$lang['from']                                           = 'DE';
$lang['to']                                             = 'A';
$lang['logo_square']                                    = 'Logo cuadrado';
$lang['coverage_state']								    = 'Departamento cobertura';
$lang['coverage_city']                                  = 'Ciudad cobertura';
$lang['coverage_country'] 								= 'Pais cobertura';
$lang['pos_document_type_default']						= 'Factura Pos por Defecto';
$lang['detal_document_type_default'] 					= 'Factura por Defecto';
$lang['purchases_document_type_default']                = 'Factura Compra por Defecto';
$lang['preparation_adjustment_document_type_id']        = 'Tipo de documento de ajuste para las preparaciones';
$lang['rete_autoica_percentage']                        = 'Porcentaje de autorretención al ICA';
$lang['rete_autoica_account']                           = 'Cuenta auxiliar de autorretención al ICA';
$lang['rete_autoica_account_counterpart']               = 'Cuenta de contrapartida de autorretención al ICA';
$lang['rete_bomberil_percentage']                       = 'Porcentaje de Auto tasa Bomberil';
$lang['rete_bomberil_account']                          = 'Cuenta auxiliar de Auto tasa Bomberil';
$lang['rete_bomberil_account_counterpart']              = 'Cuenta de contrapartida de Auto tasa Bomberil';
$lang['rete_autoaviso_percentage']                      = 'Porcentaje de Auto Aviso y Tableros';
$lang['rete_autoaviso_account']                         = 'Cuenta auxiliar de Auto Aviso y Tableros';
$lang['rete_autoaviso_account_counterpart']				= 'Cuenta de contrapartida de Auto Aviso y Tableros';
$lang["affiliate_consecutive"]                          = "Consecutivo del Afiliado";
$lang["old_branch_code"]                                = "Código Sucursal Viejo";