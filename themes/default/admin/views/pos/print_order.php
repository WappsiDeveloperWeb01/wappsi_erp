<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?= lang('sale_preinvoice') ?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
        </style>
    </head>

    <body>
    <div id="wrapper">
        <div id="receiptData">
            <div class="no-print">
                <?php
                    if ($message) {
                        ?>
                        <div class="alert alert-success">
                            <button data-dismiss="alert" class="close" type="button">×</button>
                            <?=is_array($message) ? print_r($message, true) : $message;?>
                        </div>
                        <?php
                    }
                ?>
            </div>
            <div>
                <div class="text-center">
                    <?= !empty($biller->logo) ? '<img src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>
                    <h3 style="text-transform:uppercase;"><?=$biller->company != '-' ? $biller->company : $biller->name;?></h3>
                    <?php
                        echo "<p>" . $biller->address . " " . $biller->city . " " . $biller->postal_code . " " . $biller->state . " " . $biller->country .
                        "<br>" . lang("tel") . ": " . $biller->phone;

                        echo "<br>";

                        echo "NIT : ".$this->Settings->numero_documento.($this->Settings->digito_verificacion > 0 ? '-'.$this->Settings->digito_verificacion : '');

                        echo "<br>";
                        if ($pos_settings->cf_title1 != "" && $pos_settings->cf_value1 != "") {
                            echo $pos_settings->cf_title1 . ": " . $pos_settings->cf_value1 . "<br>";
                        }
                        if ($pos_settings->cf_title2 != "" && $pos_settings->cf_value2 != "") {
                            echo $pos_settings->cf_title2 . ": " . $pos_settings->cf_value2 . "<br>";
                        }
                        echo '</p>';
                    ?>
                </div>
                <?php
                if ($Settings->invoice_view == 1 || $Settings->indian_gst) {
                    ?>
                    <div class="col-sm-12 text-center">
                        <h4 style="font-weight:bold;"><?=lang('sale_preinvoice');?></h4>
                    </div>
                    <?php
                }
                echo "<p>" .lang("date") . ": " . date('Y-m-d H:i:s')."<br>";
                if ($suspend_sale_id) {
                    echo "<p>" .lang("sale_preinvoice_number") . " : " . $suspend_sale_id."<br>";
                }
                if ($seller) {
                	echo "" .lang("seller") . ": " . ($seller->company != '-' ? $seller->company : $seller->name) . "<br>";
                }

                echo "<p>";
                echo lang("customer") . ": " . ($customer->company && $customer->company != '-' ? $customer->company : $customer->name).($customer->employer_company ? ' ('.$customer->employer_company.' - '.$customer->employer_branch.')' : '') . "<br>";
                if ($pos_settings->customer_details) {
                    if ($customer->vat_no != "-" && $customer->vat_no != "") {

                        if ($customer->tipo_documento == 6) {
                            echo  lang("vat_no") . ": " . $customer->vat_no."-".$customer->digito_verificacion." <br>";
                        } else {
                            echo  lang("vat_no") . ": " . $customer->vat_no." <br>";
                        }
                    }

                    if ($address) {
                        echo lang("tel") . ": " . $address->phone . "<br>";
                        echo lang("address") . ": " . $address->direccion . "<br>";
                        echo $address->city ." ".$address->state." ".$address->country ."<br>";
                    } else {
                        echo lang("tel") . ": " . $customer->phone . "<br>";
                        echo lang("address") . ": " . $customer->address . "<br>";
                        echo $customer->city ." ".$customer->state." ".$customer->country ."<br>";
                    }
                }
                echo "</p>";
                ?>

                <div style="clear:both;"></div>
                <table class="table table-condensed" id="table_items">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Nombre</th>
                            <th style="text-align: center;">Precio</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr class="invoice_footer">
                            <th><?=lang("sub_total");?></th>
                            <th class="text-right" id="subtotal"></th>
                        </tr>
                        <tr class="invoice_footer">
                            <th><?=lang("tax");?></th>
                            <th class="text-right" id="tax"></th>
                        </tr>
                        <tr class="invoice_footer">
                            <th><?=lang("retentions");?></th>
                            <th class="text-right" id="retentions"></th>
                        </tr>
                        <tr class="invoice_footer">
                            <th><?=lang("discount");?></th>
                            <th class="text-right" id="discount"></th>
                        </tr>
                        <tr class="invoice_footer">
                            <th><?=lang("order_tax");?></th>
                            <th class="text-right" id="order_tax"></th>
                        </tr>
                        <tr class="invoice_footer">
                            <th><?=lang("tip_amount");?></th>
                            <th class="text-right" id="tip_amount"></th>
                        </tr>
                        <tr class="invoice_footer">
                            <th><?=lang("shipping");?></th>
                            <th class="text-right" id="shipping"></th>
                        </tr>
                        <tr class="invoice_footer">
                            <th><?=lang("grand_total");?></th>
                            <th class="text-right" id="total"></th>
                        </tr>
                    </tfoot>
                </table>

                <p>Firma recibido :</p>
                <p class="sign_line" style="width: 100%;border-style: solid;border-width: 0px 0px 1px 0px;padding-top: 30px;margin-bottom: 30px;"></p>

                <p class="text-center"><b><?= lang($tipo_regimen->description) ?></b></p>
                <p class="text-center"><b><?= lang('sale_preinvoice_caption') ?></b></p>
                <p class="text-center"><?= $this->sma->decode_html($invoice_footer ? $invoice_footer : '') ?></p>
                <p class="text-center suggested_shipping">

                </p>
                <p class="text-center">
	                <?php
	                echo lang("sales_person") . ": " . $created_by->first_name." ".$created_by->last_name;
	                ?>
                </p>
            </div>

        </div>

        <div id="buttons" style="padding-top:10px; text-transform:uppercase;" class="no-print">

            <div class="alert alert-info loading_print_status" <?= $pos_settings->remote_printing != 4 ? 'style="display:none;"' : '' ?> >
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status') ?>
            </div>

            <div class="alert alert-success loading_print_status_success" style="display: none;">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status_success') ?>
            </div>

            <div class="alert alert-warning loading_print_status_warning" style="display: none;">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status_warning') ?>
            </div>

            <hr>
            <?php
            if ($message) {
                ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <?=is_array($message) ? print_r($message, true) : $message;?>
                </div>
                <?php
            } ?>
        </div>
    </div>
<script type="text/javascript">
	site = <?=json_encode(array('url' => base_url(), 'base_url' => admin_url(), 'assets' => $assets, 'settings' => $Settings, 'dateFormats' => $dateFormats))?>;
</script>
<script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>


<script type="text/javascript">

	$(document).ready(function(){
        if (localStorage.getItem('positems')) {
            var items = JSON.parse(localStorage.getItem('positems'));
            var html = '';
            var total = 0;
            $.each(items, function(index, item){
                if (item.state_readiness != '<?= CANCELLED; ?>') {
                    html += "<tr>"+
                                "<td>"+item.label+" x "+item.row.qty+"</td>"+
                                "<td class='text-right'>"+formatMoney(item.end_price * item.row.qty)+"</td>"+
                            "</tr>";
                }
            });
            $('#table_items > tbody').html(html);
            if (localStorage.getItem('total')) {
                  $('#subtotal').text(formatMoney(localStorage.getItem('total')));
            }
            if (localStorage.getItem('retenciones')) {
                retenciones = JSON.parse(localStorage.getItem('retenciones'));
                total_retenciones = formatDecimal(retenciones.total_rete_amount);
                $('#retentions').text(formatMoney(total_retenciones));
            }
            if (localStorage.getItem('tip_amount')) {
                tip_amount = JSON.parse(localStorage.getItem('tip_amount'));
                $('#tip_amount').text(formatMoney(tip_amount));
            }
            if (localStorage.getItem('discount')) {
                discount = JSON.parse(localStorage.getItem('discount'));
                $('#discount').text(formatMoney(discount));
            }
            if (localStorage.getItem('shipping')) {
                if ("<?= $pos_settings->home_delivery_in_invoice ?>" == 1) {
                    $('#shipping').text(formatMoney(localStorage.getItem('shipping')));
                } else if ("<?= $pos_settings->home_delivery_in_invoice ?>" == 0) {
                    var text = "<?= lang('shipping_not_include_in_grand_total') ?>";
                    $('.suggested_shipping').html(text.replace(/%s/g, formatMoney(localStorage.getItem('shipping'))));
                }
            }
            if (localStorage.getItem('gtotal')) {
                $('#total').text(formatMoney(localStorage.getItem('gtotal')));
            }
        } else {
            alert('Sin productos registrados para la venta');
            window.close();
        }
		$('tr.invoice_footer > th.text-right').each(function(index, th){
			if (parseFloat($(th).text()) < 1 || $(th).text() == "") {
				$('tr.invoice_footer').eq(index).css('display', 'none');
			}
            window.print();
		});
	});

function formatDecimal(x, d) {
    if (!d) { d = parseFloat(site.settings.decimals); } else { d = parseFloat(d)}
    if (site.settings.rounding == 1) { d += 2; }
    return parseFloat(accounting.formatNumber(x, d, '', '.'));
}

function formatMoney(x, symbol) {
    if(!symbol) { symbol = ''; }
    if(site.settings.sac == 1) {
        return (site.settings.display_symbol == 1 ? site.settings.symbol : '') +
            ''+formatSA(parseFloat(x).toFixed(site.settings.decimals)) +
            (site.settings.display_symbol == 2 ? site.settings.symbol : '');
    }
    var fmoney = accounting.formatMoney(x, symbol, site.settings.decimals, site.settings.thousands_sep == 0 ? ' ' : site.settings.thousands_sep, site.settings.decimals_sep, "%s%v");
    return (site.settings.display_symbol == 1 ? site.settings.symbol : '') +
        fmoney +
        (site.settings.display_symbol == 2 ? site.settings.symbol : '');
}

</script>