<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    @media only screen and (max-width: 1025px) {
        #pageTitle {
            display: none !important;
        }
    }
</style>
<script type="text/javascript">

    var count = 1, an = 1;
    var type_opt = {'addition': '<?= lang('addition'); ?>', 'subtraction': '<?= lang('subtraction'); ?>'};
    var max_qpr = "<?= isset($max_qpr) ? $max_qpr : 0 ?>"
    var close_year_adjustment = JSON.parse('<?= isset($close_year_adjustment) ? "true" : "false"; ?>');
    if (close_year_adjustment) {
        localStorage.setItem('adjustmet_type', '1');
    }
    var colors = JSON.parse('<?= $colors ? json_encode($colors) : 'false' ?>');
    if (colors) {
        colors.push({
            id: null,
            name: "<?= lang('select') ?>",
            status: "1",
            synchronized_store: "0"
        });
    }
    $(document).ready(function () {
        if (localStorage.getItem('remove_otols')) {
            if (localStorage.getItem('otoitems')) {
                localStorage.removeItem('otoitems');
            }
            if (localStorage.getItem('otoref')) {
                localStorage.removeItem('otoref');
            }
            if (localStorage.getItem('otowarehouse')) {
                localStorage.removeItem('otowarehouse');
            }
            if (localStorage.getItem('otobiller')) {
                localStorage.removeItem('otobiller');
            }
            if (localStorage.getItem('otonote')) {
                localStorage.removeItem('otonote');
            }
            if (localStorage.getItem('otodate')) {
                localStorage.removeItem('otodate');
            }
            if (localStorage.getItem('otoseller')) {
                localStorage.removeItem('otoseller');
            }
            localStorage.removeItem('remove_otols');
        }

        <?php if ($adjustment_items) { ?>
        localStorage.setItem('otoitems', JSON.stringify(<?= $adjustment_items; ?>));
        <?php } ?>

        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('otodate')) {
            $("#otodate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#otodate', function (e) {
            localStorage.setItem('otodate', $(this).val());
        });
        if (otodate = localStorage.getItem('otodate')) {
            $('#otodate').val(otodate);
        }
        <?php } ?>

        $("#add_item").autocomplete({
            source: function (request, response) {
                if (!$('#document_type_id').val() ) {
                    var msg = "";

                    if (!$('#document_type_id').val()) {
                        msg += "</br><?= lang('reference_no') ?>";
                    }

                    $('#add_item').val('').removeClass('ui-autocomplete-loading');

                    command: toastr.warning('<?=lang('select_above');?> : '+msg, '¡Atención!', {
                        "showDuration": "1200",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                    $('#add_item').focus();
                    return false;
                }


                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('transfers/oto_suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        biller_id: $('#otobiller').val(),
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });

            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {

                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    $("#add_item").val('');
                } else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $("#add_item").val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    $.ajax({
                        url:"<?= admin_url('products/getProductAVGCostByWarehouse'); ?>/"+ui.item.item_id+"/"+$('#otowarehouse').val(),
                    }).done(function(data){

                        if (data != false) {
                            ui.item.row.price = data;
                        }
                        var row = add_adjustment_item(ui.item);
                        if (row){
                            $("#add_item").val('');
                        }

                    }).fail(function(data){
                        console.log(data);
                    });

                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
        $('.skin-1').addClass('mini-navbar');
    });
    $(document).on('change', '#otobiller', function (e) {
        var sellerdefault = $('#otobiller option:selected').data('sellerdefault');
        var biller = $(this).val();
        $.ajax({
            url:'<?= admin_url("billers/getBillersDocumentTypes/63/".$origin_biller->id) ?>',
            type:'get',
            dataType:'JSON'
        }).done(function(data){
            response = data;
            $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "6000",
                "extendedTimeOut": "1000",
                });
            }
            if (response.status == 0) {
                header_alert('error', '<?= lang('biller_without_documents_types') ?>');
            } else {
                if ($('#document_type_id option').length > 1) {
                    header_alert('warning', '¡Atención! más de un tipo de documento asignado a la sucursal');
                }
            }
            $('#document_type_id').trigger('change');
        });
        if (site.settings.companies_for_adjustments_transfers == 'seller') {
            $.ajax({
                url: site.base_url + "sales/getSellers",
                type: "get",
                data: {
                    "biller_id": $('#otobiller').val()
                }
            }).done(function(data) {
                if (data != false) {
                    $('#otoseller').html(data);
                    if (sellerdefault) {
                        $('#otoseller').select2('val', sellerdefault);
                    } else if (otoseller = localStorage.getItem('otoseller')) {
                        $('#otoseller').select2('val', otoseller);
                    } 
                } else {
                    $('#otoseller').select2('val', '');
                }
            }).fail(function(data) {
                console.log(data);
            });
        } else if (otoseller = localStorage.getItem('otoseller')) {
            $('#otoseller').select2('val', otoseller);
        }
            
        localStorage.setItem('otobiller', $('#otobiller').val());
        $('#otowarehouse').select2('val', $('#otobiller option:selected').data('warehousedefault')).trigger('change');
        setTimeout(function() {
            if (biller && $('#otoseller').val() == 0) {
                $('#otoseller').select2('open');
            }
        }, 850);
    });
</script>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php
                            $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'add_adjustment');
                            echo admin_form_open_multipart("transfers/add_order", $attrib);
                            ?>
                            <div class="row">
                                <div class="col-md-4 col-sm-6">
                                    <?= lang('biller_origin', 'origin_biller') ?>
                                    <input class="form-control" value="<?= $origin_biller->name ?>" readonly>
                                </div>
                                <?php if ($Owner || $Admin) { ?>
                                    <div class="col-md-4 col-sm-6">
                                        <div class="form-group">
                                            <?= lang("date", "otodate"); ?>
                                            <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="otodate" required="required"'); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-md-4 col-sm-6" style="display:none;">
                                    <div class="form-group">
                                        <?= lang("reference_no", "document_type_id"); ?>
                                        <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                    </div>
                                </div>
                                <?php
                                    $bl[""] = "";
                                    $bldata = [];
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        $bldata[$biller->id] = $biller;
                                    }
                                ?>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <?= lang("destination_biller", "otobiller"); ?>
                                        <select name="biller" class="form-control" id="otobiller" required="required">
                                            <option value=""><?= lang('select') ?></option>
                                            <?php foreach ($billers as $biller): ?>
                                                <?php if ($origin_biller->id != $biller->id && $origin_biller->city == $biller->city): ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <?= lang("destination_warehouse", "otowarehouse"); ?>
                                        <?php
                                        $wh[''] = '';
                                        foreach ($warehouses as $warehouse) {
                                            $wh[$warehouse->id] = $warehouse->name;
                                        }
                                        echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ($warehouse_id ? $warehouse_id :$Settings->default_warehouse)), 'id="otowarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" '.($warehouse_id ? 'readonly' : '').' style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <?= lang($this->Settings->companies_for_adjustments_transfers, "otoseller"); ?>
                                        <?php 
                                        $third_Arr['0'] = lang("select") . ' ' . lang($this->Settings->companies_for_adjustments_transfers);
                                        if ($this->Settings->companies_for_adjustments_transfers != 'seller') {
                                            foreach ($sellers as $seller) {
                                                $third_Arr[$seller->id] = $seller->name;
                                            }
                                        }

                                         ?>
                                        <?php
                                        echo form_dropdown('seller_id', $third_Arr, (isset($_POST['seller']) ? $_POST['seller'] : ''), 'id="otoseller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">


                                <div class="clearfix"></div>


                                <div class="col-md-12 col-sm-12" id="sticker">
                                    <div class="form-group">
                                            <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("search_product_by_name_code") . '"'); ?>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12" style="overflow-x: auto;">
                                    <div class="form-group">
                                    <!-- <div class="col-md-12 col-sm-12" style="overflow-x: auto; padding-right: 0px !important; padding-left: 0px !important; margin-right: 15px; margin-left: 15px;"> -->
                                        <label class="table-label"><?= lang("products"); ?> *</label>
                                        <table id="otoTable" class="table items  table-bordered table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                    <th class="text-center"><?= lang("pieces"); ?></th>
                                                    <th class="text-center"><?= lang("color"); ?></th>
                                                    <th style="max-width: 30px !important; text-align: center;">
                                                        <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot></tfoot>
                                        </table>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <?= lang("note", "otonote"); ?>
                                            <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="otonote" style="margin-top: 10px; height: 100px;"'); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                <div class="col-md-12">
                                    <div id="" class="well well-sm" style="margin-bottom: 0;">
                                        <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                            <tr class="warning">
                                            <td><span class="totals_val text-left" id="titems">0</span></td>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12">
                                    <div
                                        class="fprom-group"><?php echo form_submit('add_adjustment', lang("submit"), 'id="add_adjustment_submit" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                        <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="serialModal" tabindex="-1" role="dialog" aria-labelledby="serialModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_serialModal"></h2>
                <h3><?= lang('ticket_data') ?></h3>
            </div>
            <div class="modal-body">
                <form id="serialModal_form">
                    <input type="hidden" id="serialModal_product_id">
                    <input type="hidden" id="serialModal_option_id">
                    <div class="col-sm-12 form-group">
                        <?= lang('serial_no', 'serial') ?>
                        <div class="input-group">
                            <input type="text" name="serialModal_serial" id="serialModal_serial" class="form-control" required>
                            <span class="input-group-addon pointer" id="random_num" style="padding: 1px 10px;">
                                <i class="fa fa-random"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= lang('meters', 'meters') ?>
                        <input type="text" name="serialModal_meters" id="serialModal_meters" class="form-control" required>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div class="col-sm-12 form-group">
                    <button class="btn btn-success continue_serial_modal" type="button"><?= lang('continue') ?></button>
                    <button class="btn btn-success send_serial_modal" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).on('submit', '#add_adjustment', function(e){
        if (!$('#otoseller').val()) {
            $('#otoseller').select2('open');
            $('#add_adjustment_submit').prop('disabled', false);
            e.preventDefault();
        }

        if (!$('#document_type_id').val()) {
            $('#document_type_id').select2('open');
            $('#add_adjustment_submit').prop('disabled', false);
            e.preventDefault();
        }


    });

</script>