var FancyWebSocket = function(url)
{
	var callbacks = {};
	var ws_url = url;
	var conn;

	this.bind = function(event_name, callback)
	{
		callbacks[event_name] = callbacks[event_name] || [];
		callbacks[event_name].push(callback);
		return this;
	};

	this.send = function(event_name, event_data)
	{
		console.log(event_data);
		this.conn.send( event_data );
		return this;
	};

	this.connect = function()
	{
		if ( typeof(MozWebSocket) == 'function' )
		this.conn = new MozWebSocket(url);
		else
		this.conn = new WebSocket(url);

		this.conn.onmessage = function(evt)
		{
			dispatch('message', evt.data);
		};

		this.conn.onclose = function(){dispatch('close',null)}
		this.conn.onopen = function(){dispatch('open',null)}
	};

	this.disconnect = function()
	{
		this.conn.close();
	};

	var dispatch = function(event_name, data)
	{
		if(data == null || data == "") {
		} else {
			var JSONdata = JSON.parse(data);

			load_preparation_order(JSONdata);
		}
	}
};

var Server;

$(document).ready(function()
{
	ws_connect();
});

function ws_connect(){
	Server = new FancyWebSocket('ws://'+site.local_ip+':9394');
    Server.bind('open', function()
	{
    });
    Server.bind('close', function( data )
	{
    });
    Server.bind('message', function( payload )
	{
    });
    Server.connect();

    console.log(Server);
	if (Server.conn === undefined || (Server.conn !== undefined && Server.conn.readyState > 1)) {
		Command: toastr.error('Revise el estado del Web Socket', 'Web Socket no activado');
	}
}

function send( text )
{
	ws_connect();
	setTimeout(function() {
		Server.send( 'message', text );
	}, 800);

}

function load_preparation_order(preparation_orders)
{
	var order_panels = '';
	var order_panels_modal = '';

	for (let i in preparation_orders) {
		if (sessionStorage.getItem("user_id") == preparation_orders[i].id_preparador) {

		var order_panel_body = ''
		var products_preparation_orders = preparation_orders[i].productos
		var initial_hour = preparation_orders[i].hora_orden.split(":");

		for (let j in products_preparation_orders) {
			if (products_preparation_orders[j].estado_producto == '3') {
				order_panel_body += '<p class="bg-danger panel_body_item">'+ products_preparation_orders[j].cantidad_producto +'  '+ products_preparation_orders[j].nombre_producto +'</p>'
			} else {
				order_panel_body += '<p class="panel_body_item">'+ products_preparation_orders[j].cantidad_producto +'  '+ products_preparation_orders[j].nombre_producto +'</p>'
			}
		}

		t1 = new Date();
    	t2 = new Date();

    	seconds_difference = t2.getSeconds() - initial_hour[2];
	    if (seconds_difference < 0) {
	    	seconds_difference = seconds_difference * -1;
	    }

	    seconds_difference = (seconds_difference < 10) ? '0'+ seconds_difference : seconds_difference;

  		order_panels += '<div class="col-md-2">'+
  							'<div class="panel panel-warning order_panel" id="'+ i +'">'+
  								'<div class="panel-heading panel_heading">'+
	      							'<div class="row">'+
	      								'<div class="col-xs-6">'+
	      									'<span class="label label-default">Mesa '+ preparation_orders[i].numero_mesa +'</span>'+
      									'</div>'+
										'<div class="col-xs-6 text-right">'+
											'<label>'+ preparation_orders[i].id_venta_suspendida +'</label>'+
										'</div>'+
									'</div>'+
	      							'<div class="row">'+
	      								'<div class="col-xs-6 text-left">'+
	      									'<label>'+ preparation_orders[i].nombre_vendedor +'</label>'+
      									'</div>'+
										'<div class="col-xs-6 text-right">'+
											'<label>'+ preparation_orders[i].diferencia_minutos +':'+ seconds_difference +'</label>'+
										'</div>'+
									'</div>'+
								'</div>'+
  								'<div class="panel-body panel_body">'+ order_panel_body + '</div>'+
							'</div>'+
						'</div>';

		order_panels_modal += '<div class="modal fade" tabindex="-1" role="dialog" id="orden_modal_'+ i +'">'+
								'<div class="modal-dialog modal-sm" role="document">'+
									'<div class="panel panel-warning">'+
		  								'<div class="panel-heading panel_heading">'+
			      							'<div class="row">'+
			      								'<div class="col-xs-6">'+
			      									'<span class="label label-default">Mesa '+ preparation_orders[i].numero_mesa +'</span>'+
		      									'</div>'+
												'<div class="col-xs-6 text-right">'+
													'<label>'+ preparation_orders[i].id_venta_suspendida +'</label>'+
												'</div>'+
											'</div>'+
			      							'<div class="row">'+
			      								'<div class="col-xs-6 text-left">'+
			      									'<label>'+ preparation_orders[i].nombre_vendedor +'</label>'+
		      									'</div>'+
												'<div class="col-xs-6 text-right">'+
													'<label>'+ preparation_orders[i].diferencia_minutos +':'+ seconds_difference +'</label>'+
												'</div>'+
											'</div>'+
										'</div>'+
		  								'<div class="panel-body panel_body">'+ order_panel_body + '</div>'+
									'</div>'+
								'</div>'+
							'</div>';
		}
	}

	$('#preparation_orders_container').html(order_panels);
	$('#modal_container').html(order_panels_modal);
	console.log(order_panels_modal);
}