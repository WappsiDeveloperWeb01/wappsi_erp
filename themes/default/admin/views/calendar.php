<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link href='<?= $assets ?>fullcalendar/css/fullcalendar.min.css' rel='stylesheet' />
<link href='<?= $assets ?>fullcalendar/css/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href="<?= $assets ?>fullcalendar/css/bootstrap-colorpicker.min.css" rel="stylesheet" />

<style>
    .fc th {
        padding: 10px 0px;
        vertical-align: middle;
        background:#F2F2F2;
        width: 14.285%;
    }
    .fc-content {
        cursor: pointer;
    }
    .fc-day-grid-event>.fc-content {
        padding: 4px;
    }

    .fc .fc-center {
        margin-top: 5px;
    }
    .error {
        color: #ac2925;
        margin-bottom: 15px;
    }
    .event-tooltip {
        width:150px;
        background: rgba(0, 0, 0, 0.85);
        color:#FFF;
        padding:10px;
        position:absolute;
        z-index:10001;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        cursor: pointer;
        font-size: 11px;
    }
</style>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id='calendar'></div>
                            <div class="modal fade cal_modal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <i class="fa fa-2x">&times;</i>
                                            </button>
                                            <h4 class="modal-title"></h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="error"></div>
                                            <form>
                                                <input type="hidden" value="" name="eid" id="eid">
                                                <div class="form-group">
                                                    <?= lang('title', 'title'); ?>
                                                    <?= form_input('title', set_value('title'), 'class="form-control tip" id="title" required="required"'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?= form_label(lang("type"), "type"."->".$updated); ?>
                                                    <?php $topts = [
                                                                1=>lang('calendar_event_type_holiday'),
                                                                2=>lang('calendar_event_type_special_day'),
                                                                3=>lang('calendar_event_type_birthday'),
                                                                4=>lang('calendar_event_type_reminder'),
                                                                5=>lang('calendar_event_type_tax_day'),
                                                                6=>lang('calendar_event_type_other'),
                                                            ] ?>
                                                    <?= form_dropdown(["name"=>"type", "id"=>"type", "class"=>"form-control select2"], $topts); ?>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <?= lang('start', 'start'); ?>
                                                            <?= form_input('start', set_value('start'), 'class="form-control datetime" id="start" required="required"'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <?= lang('end', 'end'); ?>
                                                            <?= form_input('end', set_value('end'), 'class="form-control datetime" id="end"'); ?>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="col-md-4">
                                                        <div class="form-group">
                                                            <?= lang('event_color', 'color'); ?>
                                                            <div class="input-group">
                                                                <span class="input-group-addon" id="event-color-addon" style="width:2em;"></span>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                    <div class="col-md-4">
                                                        <label class="color-option" style="background-color: #d50000" data-color="#d50000"></label>
                                                        <label class="color-option" style="background-color: #ad1457" data-color="#ad1457"></label>
                                                        <label class="color-option" style="background-color: #f4511e" data-color="#f4511e"></label>
                                                        <label class="color-option" style="background-color: #e4c441" data-color="#e4c441"></label>
                                                        <label class="color-option" style="background-color: #0b8043" data-color="#0b8043"></label>
                                                        <label class="color-option" style="background-color: #1484c6" data-color="#1484c6"></label>
                                                        <label class="color-option" style="background-color: #8e24aa" data-color="#8e24aa"></label>
                                                        <label class="color-option" style="background-color: #616161" data-color="#616161"></label>
                                                        <label class="color-option" style="background-color: #795548" data-color="#795548"></label>
                                                        <label class="color-option" style="background-color: #f6bf26" data-color="#f6bf26"></label>
                                                        <input type="hidden" name="color" id="color" class="form-control input-md">

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <?= lang('description', 'description'); ?>
                                                    <textarea class="form-control skip" id="description" name="description" rows="6" style="resize: none;"></textarea>
                                                </div>

                                            </form>
                                        </div>
                                        <div class="modal-footer"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body -->

<script type="text/javascript">
    var currentLangCode = '<?= $cal_lang; ?>', moment_df = '<?= strtoupper($dateFormats['js_sdate']); ?> HH:mm', cal_lang = {},
    tkname = "<?=$this->security->get_csrf_token_name()?>", tkvalue = "<?=$this->security->get_csrf_hash()?>";
    cal_lang['add_event'] = '<?= lang('add_event'); ?>';
    cal_lang['edit_event'] = '<?= lang('edit_event'); ?>';
    cal_lang['delete'] = '<?= lang('delete'); ?>';
    cal_lang['event_error'] = '<?= lang('event_error'); ?>';
</script>
<script src='<?= $assets ?>fullcalendar/js/moment.min.js'></script>
<script src="<?= $assets ?>fullcalendar/js/fullcalendar.min.js"></script>
<script src="<?= $assets ?>fullcalendar/js/lang-all.js"></script>
<script src='<?= $assets ?>fullcalendar/js/bootstrap-colorpicker.min.js'></script>
<script src='<?= $assets ?>fullcalendar/js/main.js'></script>

<?php if ($updated != 0): ?>
    <?php if ($updated == 1): ?>
        <script type="text/javascript">
            swal({
                title: "¡Actualización completa!",
                text: "El sistema se ha actualizado",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            }, function () {

            });
        </script>
    <?php else: ?>
        <script type="text/javascript">
            swal({
                title: "¡Error en la actualización!",
                text: "Hubo un error con la actualización, por favor contáctese con soporte técnico.",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            }, function () {

            });
        </script>
    <?php endif ?>
<?php endif ?>